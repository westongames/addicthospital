// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "Gimka/UnrealEngine/GKGameModeBase.h"

#include "AddictHospitalGameMode.generated.h"

UCLASS(minimalapi)
class AAddictHospitalGameMode : public AGKGameModeBase
{
	GENERATED_BODY()

public:
	AAddictHospitalGameMode();
};



