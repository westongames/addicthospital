// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Database/GKDatabaseItem.h"

class GKGameManager;

/**
 * 
 */
class GKGoapAction2 : public GKDatabaseItem16
{
	GKGoapSetStates Conditions;			// Must be satisfied before this action can be taken. Only Conditions that "matter" are here.
	GKGoapSetStates Effects;			// Things that are added to worldStates when this action takes place.
	int32 Cost;							// The numeric cost of this action

public:
	GKGoapAction2(const FName& ID, int32 Cost,
		const GKGoapSetStates& Conditions, const GKGoapSetStates& Effects);
	~GKGoapAction2();

	void Log(const GKGameManager* GameManager) const;

public:		// Accessors
	GK_GETCR(GKGoapSetStates, Conditions);
	GK_GETCR(GKGoapSetStates, Effects);
	GK_GET(int32, Cost);
};
