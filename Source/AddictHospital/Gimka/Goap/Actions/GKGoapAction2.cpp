// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Goap/Actions/GKGoapAction2.h"

GKGoapAction2::GKGoapAction2(const FName& iID, int32 iCost,
	const GKGoapSetStates& iConditions, const GKGoapSetStates& iEffects)
	: GKDatabaseItem16(iID), Conditions(iConditions), Effects(iEffects), Cost(iCost)
{
}

GKGoapAction2::~GKGoapAction2()
{
}

void GKGoapAction2::Log(const GKGameManager* GameManager) const
{
	GKLog1("GoapAction", GetID().ToString());
	Conditions.Log(GameManager);
	Effects.Log(GameManager);
}