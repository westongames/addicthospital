// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Database/GKDatabase.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Goap/Actions/GKGoapAction2.h"
/**
 * 
 */
class GKGoapActionDB : public GKDatabase<GKGoapAction2>
{
	// Keep references
	const GKGoapStateBoolDB* StateBoolDB;
	const GKGoapStateIntDB* StateIntDB;

public:
	const static FName StaticID;

	GKGoapActionDB(const GKGoapStateBoolDB* StateBoolDB, const GKGoapStateIntDB* StateIntDB);
	~GKGoapActionDB();

protected:
	// Inherited via GKDatabase
	virtual void OnParseData(GKGameManager* GameManager) override;
	virtual bool CanInitialize() override;
};
