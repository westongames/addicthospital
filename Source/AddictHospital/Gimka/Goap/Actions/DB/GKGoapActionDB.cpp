// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Goap/Actions/DB/GKGoapActionDB.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Tools/Macros/GKLogger.h"

const FName GKGoapActionDB::StaticID("GoapAction");

GKGoapActionDB::GKGoapActionDB(const GKGoapStateBoolDB* iStateBoolDB, const GKGoapStateIntDB* iStateIntDB)
	: GKDatabase(StaticID)
{
	StateBoolDB = iStateBoolDB;
	StateIntDB = iStateIntDB;
}

GKGoapActionDB::~GKGoapActionDB()
{
}

void GKGoapActionDB::OnParseData(GKGameManager* GameManager)
{
	//GKGoapGoal goalHasWeapon;
	//goalHasWeapon.GetStates().Add(GKGoapState::HasWeapon);
	//Add(goalHasWeapon);
	//GKLog("OnParseData");
	Add(GKGoapAction2("buyWeapon", 1,
		// Conditions
		GKGoapSetStates( { "hasMoney" }, {}, *StateBoolDB, *StateIntDB),
		// Effects
		GKGoapSetStates( { "hasWeapon" }, {}, *StateBoolDB, *StateIntDB)));

	Add(GKGoapAction2("stealWeapon", 2,
		// Conditions
		GKGoapSetStates( {}, {}, *StateBoolDB, *StateIntDB),
		// Effects
		GKGoapSetStates({ "hasWeapon" }, {}, *StateBoolDB, *StateIntDB)));

	Add(GKGoapAction2("killEnemy", 4,
		// Conditions
		GKGoapSetStates( { "hasWeapon", "closeToEnemy"}, {}, *StateBoolDB, *StateIntDB),
		// Effects
		GKGoapSetStates({ "enemyDeath" }, {}, *StateBoolDB, *StateIntDB)));

	Add(GKGoapAction2("approachEnemy", 8,
		// Conditions
		GKGoapSetStates({ }, { }, *StateBoolDB, *StateIntDB),
		// Effects
		GKGoapSetStates({ "closeToEnemy" }, {}, *StateBoolDB, *StateIntDB)));

	Add(GKGoapAction2("getMoney", 16,
		// Conditions
		GKGoapSetStates({ }, { }, *StateBoolDB, *StateIntDB),
		// Effects
		GKGoapSetStates({ "hasMoney" }, {}, *StateBoolDB, *StateIntDB)));

	//=====  Fighting against 1


	Log(GameManager);
}

bool GKGoapActionDB::CanInitialize()
{
	// Init the required DB first
	return StateBoolDB->GetIsInitialized() && StateIntDB->GetIsInitialized();
}