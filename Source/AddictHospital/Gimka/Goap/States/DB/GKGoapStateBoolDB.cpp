// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapStateBoolDB.h"
#include "Gimka/Tools/Macros/GKLogger.h"

const FName GKGoapStateBoolDB::StaticID("GoapStateBool");

void GKGoapStateBool::Log(GKGameManager* GameManager) const
{
	//UE_LOG(LogTemp, Warning, TEXT("  StateBool %s"), ID);
	GKLog1("  StateBool", ID.ToString());
}

GKGoapStateBoolDB::GKGoapStateBoolDB()
	: GKDatabase(StaticID)
{
}

GKGoapStateBoolDB::~GKGoapStateBoolDB()
{
}

void GKGoapStateBoolDB::OnParseData(GKGameManager* GameManager)
{
	Add(GKGoapStateBool("hasMoney"));
	Add(GKGoapStateBool("hasWeapon"));
	Add(GKGoapStateBool("killEnemy"));
	Add(GKGoapStateBool("closeToEnemy"));
	Add(GKGoapStateBool("enemyDeath"));
	

	//GKLog("StatBool::OnParseData");
	Log(GameManager);
}

//void GKGoapStateBoolDB::Log() const
//{
//	GKLog1("StateBoolDB", Num());
//	for (int i = 0; i < Num(); ++i)
//	{
//		const GKGoapStateBool& state = operator[](i);
//		GKLog1("StateBool ", state.GetID().ToString());
//	}
//}