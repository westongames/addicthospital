// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Database/GKDatabase.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Database/GKDatabaseItem.h"

/**
 * Store information of a bool goap state
 */
struct GKGoapStateBool : public GKDatabaseItem16
{
public:
	GKGoapStateBool(const FName& iID)
		: GKDatabaseItem16(iID){}

	void Log(GKGameManager* GameManager) const;
};

class GKGoapStateBoolDB : public GKDatabase<GKGoapStateBool>
{
public:
	const static FName StaticID;

	GKGoapStateBoolDB();
	virtual ~GKGoapStateBoolDB();

	//void Log(const GKGoapStateBoolDB& StateBoolDB, const GKGoapStateIntDB& StateIntDB);


	// Inherited via GKDatabase
	virtual void OnParseData(GKGameManager* GameManager) override;

	//void Log() const;

};
