// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Database/GKDatabase.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Database/GKDatabaseItem.h"

/**
 * Store information of a bool goap state 
 */
struct GKGoapStateInt : public GKDatabaseItem16
{
public:
	GKGoapStateInt(const FName& iID)
		: GKDatabaseItem16(iID) {}

	void Log(GKGameManager* GameManager) const;
};

class GKGoapStateIntDB : public GKDatabase<GKGoapStateInt>
{
public:
	const static FName StaticID;

	GKGoapStateIntDB();
	~GKGoapStateIntDB();

	// Inherited via GKDatabase
	virtual void OnParseData(GKGameManager* GameManager) override;
};
