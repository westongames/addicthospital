// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapStateIntDB.h"
#include "GKGoapStateIntDB.h"

const FName GKGoapStateIntDB::StaticID("GoapStateInt");

void GKGoapStateInt::Log(GKGameManager* GameManager) const
{
	//UE_LOG(LogTemp, Warning, TEXT("  StateInt %s"), ID.ToString());
	GKLog1("  StateInt", ID.ToString());
}

GKGoapStateIntDB::GKGoapStateIntDB()
	: GKDatabase(StaticID)
{
}

GKGoapStateIntDB::~GKGoapStateIntDB()
{
}

void GKGoapStateIntDB::OnParseData(GKGameManager* GameManager)
{
}
