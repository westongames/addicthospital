// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapHistoryStates.h"

GKGoapHistoryStates::GKGoapHistoryStates()
{
}

GKGoapHistoryStates::~GKGoapHistoryStates()
{
}

void GKGoapHistoryStates::Init(GKIGoapStates* iOriginalStates)
{

}

int32 GKGoapHistoryStates::FindBool(uint16 ID)
{
	return false;
}

int32 GKGoapHistoryStates::NumBools() const
{
	return int32();
}

int32 GKGoapHistoryStates::NumInts() const
{
	return int32();
}

void GKGoapHistoryStates::Reset()
{
}

void GKGoapHistoryStates::Log(const GKGameManager* GameManager) const
{
}

void GKGoapHistoryStates::Add(uint16 ID)
{
	// Simply Add (duplicates within same level is find. The latest one override the previous value)
	ArrayBools.Add({ Level, ID, true });
}

bool GKGoapHistoryStates::Contains(uint16 ID) const
{
	// Search throughout history
	for (int i = ArrayBools.Num() - 1; i >= 0; --i)
	{
		if (ArrayBools[i].ID == ID) return ArrayBools[i].Value;
	}

	return OriginalStates->Contains(ID);;
}

void GKGoapHistoryStates::Remove(uint16 ID)
{
	// Simply Add with Value = false
	ArrayBools.Add({ Level, ID, false });

}

int32 GKGoapHistoryStates::GetInt(uint16 ID) const
{
	// Search throughout history
	for (int i = ArrayInts.Num() - 1; i >= 0; --i)
	{
		if (ArrayInts[i].ID == ID) return ArrayInts[i].Value;
	}

	return OriginalStates->GetInt(ID);;
}

void GKGoapHistoryStates::SetInt(uint16 ID, int32 Value)
{
	// Simply Add with Value
	ArrayInts.Add({ Level, ID, Value });
}

int32 GKGoapHistoryStates::ModifyInt(uint16 ID, int32 Delta)
{
	int32 Value = GetInt(ID) + Delta;
	SetInt(ID, Value);
	return Value;
}

void GKGoapHistoryStates::Undo()
{
	check(Level > 0);
	--Level;

	int index;
	for (index = ArrayBools.Num() - 1; index >= 0; --index)
	{
		if (ArrayBools[index].Level <= Level) break;
	}
	ArrayBools.SetNum(index + 1);

	for (int i = ArrayInts.Num() - 1; i >= 0; --i)
	{
		if (ArrayInts[index].Level <= Level) break;
	}
	ArrayInts.SetNum(index + 1);

	// Level 0: 0, 1, 2
	// Level 1: 3, 4
	// Level 2: 5, 6, 7, 8
}

void GKGoapHistoryStates::NextLevel()
{
	++Level;
}
