// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapSetStates.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Goap/States/GKGoapState.h"
#include "Gimka/Goap/States/DB/GKGoapStateBoolDB.h"
#include "Gimka/Goap/States/DB/GKGoapStateIntDB.h"

GKGoapSetStates::GKGoapSetStates()
{
}

GKGoapSetStates::GKGoapSetStates(std::initializer_list<uint16> InitListBools, 
	std::initializer_list<TPairInitializer<const uint16&, const int32&>> InitListInts)
	: SetBools(InitListBools), MapInts(InitListInts)
{
	// Examples InitListInts: { { 1, 2 }, {2, 2} }
}

GKGoapSetStates::GKGoapSetStates(std::initializer_list<FName> InitListBools, 
	std::initializer_list<TPairInitializer<const FName&, const int32&>> InitListInts, 
	const GKGoapStateBoolDB& StateBoolDB, const GKGoapStateIntDB& StateIntDB)
{
	/*for (auto var = InitListBools.begin(); var != InitListBools.end(); ++var)
	{

	}*/
	//StateBoolDB.Log();


	for (auto var : InitListBools)
	{
		//GKLog1("Var ", var.ToString());
		int32 stateID = StateBoolDB.Find(var);
		SetBools.Add(stateID);
	}

	for (const TPairInitializer<const FName&, const int32&>& Element : InitListInts)
	{
		//GKLog1("Pair ", Element.Key.ToString()); 
		//GKLog1("Pairv ", Element.Value);
		int32 stateID = StateIntDB.Find(Element.Key);
		MapInts.Add(stateID, Element.Value);
	}

	/*for (auto var : InitListBools.begin())
	{

	}*/
}

GKGoapSetStates::~GKGoapSetStates()
{
}

// If already exists, it doesn't change anything.
void GKGoapSetStates::Add(uint16 ID)
{
	SetBools.Add(ID);
}

bool GKGoapSetStates::Contains(uint16 ID) const
{
	return SetBools.Contains(ID);
}

void GKGoapSetStates::Remove(uint16 ID)
{
	SetBools.Remove(ID);
}

int32 GKGoapSetStates::GetInt(uint16 ID) const
{
	const int32* Value = MapInts.Find(ID);
	return Value ? *Value : 0;
}

void GKGoapSetStates::SetInt(uint16 ID, int32 Value)
{
	MapInts.Add(ID, Value);
}

int32 GKGoapSetStates::ModifyInt(uint16 ID, int32 Delta)
{
	int32* Value = MapInts.Find(ID);
	if (Value)
	{
		*Value += Delta;
		return *Value;
	}
	else
	{
		MapInts.Add(ID, Delta);
		return Delta;
	}
}

void GKGoapSetStates::Copy(const GKGoapSetStates& other)
{
	Reset();
	SetBools.Append(other.SetBools);
	MapInts.Append(other.MapInts);
}

void GKGoapSetStates::Append(const GKGoapSetStates& other)
{
	SetBools.Append(other.SetBools);

	for (const auto& keyValueInt : other.GetMapInts())
	{
		// If the currentStates has less than the targetStates, then consider it not fulfilled
		// TODO: for now must be all but later might want to do partial
		if (keyValueInt.Value > 0) ModifyInt(keyValueInt.Key, +keyValueInt.Value);
	}
}


//bool GKGoapSetStates::SatisfiesAny(const GKGoapSetStates& states) const
//{
//	for (const auto& keyBool : states.GetSetBools())
//	{
//		if (Contains(keyBool)) return true;
//	}
//	for (const auto& keyValueInt : states.GetMapInts())
//	{
//		// If the worldStates has less than the SetStates, then consider it not fulfilled
//		if (ArrayInts[keyValueInt.Key] < keyValueInt.Value) return false;;
//	}
//	return true;
//}


void GKGoapSetStates::Log(const GKGameManager* GameManager) const
{
	GKGoapStateBoolDB* StateBoolDB = (GKGoapStateBoolDB*)GameManager->GetDatabase(GKGoapStateBoolDB::StaticID);
	GKGoapStateIntDB* StateIntDB = (GKGoapStateIntDB*)GameManager->GetDatabase(GKGoapStateIntDB::StaticID);

	GKLog2("GoapSetStates Bools, Ints", SetBools.Num(), MapInts.Num());
	for (uint16 value : SetBools)
	{
		GKLog1("  Bool", (*StateBoolDB)[value].GetID().ToString());
//		UE_LOG(LogTemp, Warning, TEXT("    %d", (GKGoapState::KeyBool)));
	}
	for (const auto& keyValue : MapInts)
	{
		UE_LOG(LogTemp, Warning, TEXT("  Int %s: %d"),
			*(*StateIntDB)[keyValue.Key].GetID().ToString(),
			keyValue.Value);
	}
}



bool GKGoapSetStates::IsEquals(const GKGoapSetStates& other) const
{
	// Note: use HashCode for pre-compare if possible to quickly determine if both states are different
	// Then use this to compare deeply
	return ContainsAll(other.GetSetBools()) && other.ContainsAll(GetSetBools()) &&
		HasAllIntsTheSame(other.GetMapInts()) && other.HasAllIntsTheSame(GetMapInts());
}

int32 GKGoapSetStates::NumBools() const
{
	return SetBools.Num();
}

int32 GKGoapSetStates::NumInts() const
{
	return MapInts.Num();
}

GKBits64 GKGoapSetStates::CalculateHashCode() const
{
	GKBits64 result;
	for (uint16 value : SetBools)
	{
		result.SetTrue(value & 63);
	}
	for (const auto& keyValue : MapInts)
	{
		result.SetTrue(keyValue.Key & 63);
	}
	return result;
}

void GKGoapSetStates::Reset()
{
	SetBools.Reset();
	MapInts.Reset();
}


