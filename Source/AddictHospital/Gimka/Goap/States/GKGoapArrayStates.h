// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKGoapState.h"
#include "GKIGoapStates.h"
#include "GKGoapSetStates.h"

//#include "GKGoapStates.h"
//#include "Gimka/Containers/Array/GKSortedArray.h"
/**
 * GKGoapAction.Conditions will be separated into 2 GKGoapMapStates, 1 for storing bool comparison, other for storing int comparison
 */
class GKGoapArrayStates : public GKIGoapStates
{
public:	
	GKGoapArrayStates(int32 CapacityBoolKeys, int32 CapacityIntKeys);
	virtual ~GKGoapArrayStates();

	FORCEINLINE virtual void Add(uint16 ID) override;
	FORCEINLINE virtual bool Contains(uint16 ID) const override ;
	FORCEINLINE virtual void Remove(uint16 ID) override;

	// int operation
	FORCEINLINE virtual int32 GetInt(uint16 ID) const override;
	FORCEINLINE virtual void SetInt(uint16, int32 Value) override;
	FORCEINLINE virtual int32 ModifyInt(uint16 ID, int32 Delta) override;

	//int32 DistanceTo(const GKGoapSetStates& states) const;
	//bool Satisfies(const GKGoapSetStates& states) const;

	// Inherited via GKIGoapStates
	virtual int32 NumBools() const override;
	virtual int32 NumInts() const override;

	int32 SlowCountBools() const;
	int32 SlowCountInts() const;

	virtual void Reset() override;
	virtual void Log(const GKGameManager* GameManager) const override;

private:
	TBitArray<FDefaultBitArrayAllocator> BitArrayBools;
	int32* ArrayInts;
	uint16 CapacityIntKeys;

};
