// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Goap/States/DB/GKGoapStateBoolDB.h"
#include "Gimka/Goap/States/DB/GKGoapStateIntDB.h"

class GKGameManager;
class GKGoapSetStates;

/**
 * 
 */
class GKIGoapStates
{
public:
	typedef TSet<uint16> TSetBools;
	typedef TMap<uint16, int32> TMapInts;

	GKIGoapStates();
	virtual ~GKIGoapStates();

	// bool operations, keys with same integer value as the ints are stored in different arrays.
	virtual int32 NumBools() const = 0;
	virtual void Add(uint16 ID) = 0;
	virtual bool Contains(uint16 ID) const = 0;
	virtual void Remove(uint16 ID) = 0;

	// int operation
	virtual int32 NumInts() const = 0;
	virtual int32 GetInt(uint16 ID) const = 0;
	virtual void SetInt(uint16 ID, int32 Value) = 0;
	virtual int32 ModifyInt(uint16 ID, int32 Delta) = 0;

	virtual void Reset() = 0;
	virtual void Log(const GKGameManager* GameManager) const = 0;

	bool ContainsAll(const TSetBools& other) const;
	bool HasAllIntsTheSame(const TMapInts& other) const;

	int32 DistanceTo(const GKGoapSetStates* goal) const;
	bool Satisfies(const GKGoapSetStates* goal) const;
	bool SatisfiesAny(const GKGoapSetStates* goal) const;

	void ApplyFrom(const GKGoapSetStates* effects);

};
