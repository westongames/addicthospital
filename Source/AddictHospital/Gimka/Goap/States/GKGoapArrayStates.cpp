// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapArrayStates.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKGoapArrayStates::GKGoapArrayStates(int32 iCapacityBoolKeys, int32 iCapacityIntKeys)
	: BitArrayBools(false, iCapacityBoolKeys)
{
	check(iCapacityBoolKeys <= 65535);
	check(iCapacityIntKeys <= 65535);
	CapacityIntKeys = iCapacityIntKeys;
	ArrayInts = new int32[CapacityIntKeys];
	FMemory::Memzero(ArrayInts, CapacityIntKeys * sizeof(int32));
}

GKGoapArrayStates::~GKGoapArrayStates()
{
	if (ArrayInts)
	{
		delete[] ArrayInts;
		ArrayInts = nullptr;
	}
}

void GKGoapArrayStates::Add(uint16 ID)
{
	BitArrayBools[ID] = true;
}

bool GKGoapArrayStates::Contains(uint16 ID) const
{
	return BitArrayBools[ID];
}

void GKGoapArrayStates::Remove(uint16 ID)
{
	BitArrayBools[ID] = false;
}

int32 GKGoapArrayStates::GetInt(uint16 ID) const
{
	return ArrayInts[ID];
}

void GKGoapArrayStates::SetInt(uint16 ID, int32 Value)
{
	ArrayInts[ID] = Value;
}

int32 GKGoapArrayStates::ModifyInt(uint16 ID, int32 Delta)
{
	return (ArrayInts[ID] += Delta);
}

//int32 GKGoapArrayStates::DistanceTo(const GKGoapSetStates& states) const
//{
//	int32 distance = 0;
//	for (const auto& keyBool : states.GetSetBools())
//	{
//		//GKLog1("SetStates ", keyBool);
//		//GKLog1("  Contains ", Contains(keyBool));
//		if (!Contains(keyBool)) ++distance;
//	}
//	for (const auto& keyValueInt : states.GetMapInts())
//	{
//		// If the currentStates has less than the target States, then consider it not fulfilled
//		if (ArrayInts[keyValueInt.Key] < keyValueInt.Value) ++distance;
//	}
//	return distance;
//}
//
//bool GKGoapArrayStates::Satisfies(const GKGoapSetStates& states) const
//{
//	for (const auto& keyBool : states.GetSetBools())
//	{
//		if (!Contains(keyBool)) return false;
//	}
//	for (const auto& keyValueInt : states.GetMapInts())
//	{
//		// If the currentStates has less than the target States, then consider it not fulfilled
//		if (ArrayInts[keyValueInt.Key] < keyValueInt.Value) return false;;
//	}
//	return true;
//
//}


int32 GKGoapArrayStates::NumBools() const
{
	check(false);		// Not implemented
	return int32();
}

int32 GKGoapArrayStates::NumInts() const
{
	check(false);			// Not implemented
	return int32();
}

int32 GKGoapArrayStates::SlowCountBools() const
{
	int32 Count = 0;
	for (int32 i = 0, count = BitArrayBools.Num(); i < count; ++i)
	{
		if (BitArrayBools[i]) ++Count;
	}
	return Count;
}

int32 GKGoapArrayStates::SlowCountInts() const
{
	int32 Count = 0;
	for (int32 i = 0, count = CapacityIntKeys; i < count; ++i)
	{
		if (ArrayInts[i] != 0) ++Count;
	}
	return Count;
}


void GKGoapArrayStates::Reset()
{
	FMemory::Memzero(ArrayInts, CapacityIntKeys * sizeof(int32));
	BitArrayBools.SetRange(0, BitArrayBools.Num(), false);
}

void GKGoapArrayStates::Log(const GKGameManager* GameManager) const
{
	const GKGoapStateBoolDB* StateBoolDB = (const GKGoapStateBoolDB*)GameManager->GetDatabase(GKGoapStateBoolDB::StaticID);
	const GKGoapStateIntDB* StateIntDB = (const GKGoapStateIntDB*)GameManager->GetDatabase(GKGoapStateIntDB::StaticID);
	GKLog2("GKGoapArrayStates Bools, Ints", SlowCountBools(), SlowCountInts());

	for (int32 i = 0, count = BitArrayBools.Num(); i < count; ++i)
	{
		if (BitArrayBools[i])
		{
			GKLog1("  Bool", (*StateBoolDB)[i].GetID().ToString());
		}
	}

	for (int32 i = 0, count = CapacityIntKeys; i < count; ++i)
	{
		if (ArrayInts[i] != 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("  Int %s: %d"),
				*(*StateIntDB)[i].GetID().ToString(),
				ArrayInts[i]);
			//GKLog1("  Int", (*StateIntDB)[i].GetID().ToString());
		}
	}
}
