// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Database/GKDatabase.h"

/**
 * List of keys, read from XML including the name/stringID
 */
class GKGoapState //: public GKDatabase<GKGoapState>
{
public:
	enum KeyBool : uint16
	{
		HasMoney,
		HasWeapon,
		CloseToTarget,
		CountBool
	};

	enum KeyInt : uint16
	{
		Material1,
		Material2,
		CountInt
	};

	const int MaxCount = CountBool > KeyInt::CountInt ? KeyBool::CountBool : KeyInt::CountInt;

	GKGoapState();
	~GKGoapState();

	GKGoapState(uint16 iID, int32 iValueInt, bool iValueBool)
	{
		ID = iID;
		ValueInt = iValueInt;
		ValueBool = iValueBool;
	}

	// Each states has Int and Bool. They are separated data.
	// We use them together here to keep it simple and space efficient?
	uint16 ID;
	int32 ValueInt;
	bool ValueBool;
	//FName Name;

};
