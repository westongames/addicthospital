// Fill out your copyright notice in the Description page of Project Settings.


#include "GKIGoapStates.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"

GKIGoapStates::GKIGoapStates()
{
}

GKIGoapStates::~GKIGoapStates()
{
}

bool GKIGoapStates::HasAllIntsTheSame(const TMapInts& other) const
{
	for (const auto& keyValue : other)
	{
		if (keyValue.Value > 0 && GetInt(keyValue.Key) != keyValue.Value) return false;
	}
	return true;
}


bool GKIGoapStates::ContainsAll(const TSetBools& other) const
{
	for (uint16 value : other)
	{
		if (!Contains(value)) return false;
	}
	return true;
}


int32 GKIGoapStates::DistanceTo(const GKGoapSetStates* goal) const
{
	int32 distance = 0;
	for (const auto& keyBool : goal->GetSetBools())
	{
		//GKLog1("SetStates ", keyBool);
		//GKLog1("  Contains ", Contains(keyBool));
		if (!Contains(keyBool)) ++distance;
	}
	for (const auto& keyValueInt : goal->GetMapInts())
	{
		// If the currentStates has less than the targetStates, then consider it not fulfilled
		// TODO: for now must be all but later might want to do partial
		if (keyValueInt.Value > 0 && GetInt(keyValueInt.Key) < keyValueInt.Value) ++distance;
	}
	return distance;
}

bool GKIGoapStates::Satisfies(const GKGoapSetStates* goal) const
{
	for (const auto& keyBool : goal->GetSetBools())
	{
		if (!Contains(keyBool)) return false;
	}
	for (const auto& keyValueInt : goal->GetMapInts())
	{
		// If the currentStates has less than the targetStates, then consider it not fulfilled
		// TODO: for now must be all but later might want to do partial
		if (keyValueInt.Value > 0 && GetInt(keyValueInt.Key) < keyValueInt.Value) return false;
	}
	return true;

}

bool GKIGoapStates::SatisfiesAny(const GKGoapSetStates* goal) const
{
	for (const auto& keyBool : goal->GetSetBools())
	{
		if (Contains(keyBool)) return true;
	}
	for (const auto& keyValueInt : goal->GetMapInts())
	{
		// If the currentStates has less than the targetStates, then consider it not fulfilled
		// TODO: for now must be all but later might want to do partial
		if (keyValueInt.Value > 0 && GetInt(keyValueInt.Key) >= keyValueInt.Value) return true;
	}
	return false;
}

void GKIGoapStates::ApplyFrom(const GKGoapSetStates* effects)
{
	for (const auto& keyBool : effects->GetSetBools())
	{
		Remove(keyBool);
	}
	for (const auto& keyValueInt : effects->GetMapInts())
	{
		// If the currentStates has less than the targetStates, then consider it not fulfilled
		// TODO: for now must be all but later might want to do partial
		if (keyValueInt.Value > 0) ModifyInt(keyValueInt.Key, -keyValueInt.Value);
	}
}
//
//void GKIGoapStates::Append(const GKGoapSetStates* effects)
//{
//	for (const auto& keyBool : effects->GetSetBools())
//	{
//		Add(keyBool);
//	}
//	for (const auto& keyValueInt : effects->GetMapInts())
//	{
//		// If the currentStates has less than the targetStates, then consider it not fulfilled
//		// TODO: for now must be all but later might want to do partial
//		if (keyValueInt.Value > 0) ModifyInt(keyValueInt.Key, -keyValueInt.Value);
//	}
//}


//
//bool GKIGoapStates::SatisfiesAny(const GKGoapSetStates* states, int32 newDistance) const
//{
//	int32 distance = 0;
//	bool result = false;
//	for (const auto& keyBool : states->GetSetBools())
//	{
//		if (Contains(keyBool))
//		{
//			result = true;
//		}
//		else
//		{
//			++distance;
//		}
//	}
//	for (const auto& keyValueInt : states->GetMapInts())
//	{
//		// If the currentStates has less than the targetStates, then consider it not fulfilled
//		// TODO: for now must be all but later might want to do partial
//		if (keyValueInt.Value > 0 && GetInt(keyValueInt.Key) >= keyValueInt.Value) return true;
//	}
//	return false;
//}