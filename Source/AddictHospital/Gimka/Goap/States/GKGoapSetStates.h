// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKIGoapStates.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Containers/Bits/GKBits.h"

/**
 * Interface, parent class for GKGoapArrayStates, GKGoapMapStates, GKGoapHistoryStates
 * These are bools. We'll have different class for ints.
 */
class GKGoapSetStates : public GKIGoapStates
{
public:
	GKGoapSetStates();
	GKGoapSetStates(std::initializer_list<uint16> InitListBools, 
		std::initializer_list<TPairInitializer<const uint16&, const int32&>> InitListInts);
	GKGoapSetStates(std::initializer_list<FName> InitListBools,
		std::initializer_list<TPairInitializer<const FName&, const int32&>> InitListInts,
		const GKGoapStateBoolDB& StateBoolDB, const GKGoapStateIntDB& StateIntDB);
	virtual ~GKGoapSetStates();

	void Copy(const GKGoapSetStates& other);
	void Append(const GKGoapSetStates& other);

	// bool operations, keys with same integer value as the ints are stored in different arrays.
	virtual int32 NumBools() const override;
	virtual void Add(uint16 ID) override;
	virtual bool Contains(uint16 ID) const override;
	virtual void Remove(uint16 ID) override;

	// int operation
	virtual int32 NumInts() const override;
	virtual int32 GetInt(uint16 ID) const override;
	virtual void SetInt(uint16 ID, int32 Value) override;
	virtual int32 ModifyInt(uint16 ID, int32 Delta) override;

	//bool SatisfiesAny(const GKGoapSetStates& states) const;

	// Used in GKGoapNode2 to quickly pre-compare if a Condition is equals to other node's Condition or not.
	// If the hashCode is the same, then deeper comparison will be performed
	GKBits64 CalculateHashCode() const;
	bool IsEquals(const GKGoapSetStates& other) const;

	virtual void Reset() override;

	virtual void Log(const GKGameManager* GameManager) const override;
	//const GKGoapStateBoolDB& StateBoolDB, const GKGoapStateIntDB& StateIntDB) const override;
private:
	TSetBools SetBools;
	TMapInts MapInts;

	// Inherited via GKIGoapStates
public:
	GK_GETCR(TSetBools, SetBools);
	GK_GETCR(TMapInts, MapInts);
};
