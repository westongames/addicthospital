// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKIGoapStates.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
class GKGoapHistoryStates: public GKIGoapStates
{
public:
	GKGoapHistoryStates();
	~GKGoapHistoryStates();

	void Init(GKIGoapStates* OriginalStates);

	// bool operations, keys with same integer value as the ints are stored in different arrays.
	virtual void Add(uint16 ID) override;
	virtual bool Contains(uint16 ID) const override;
	virtual void Remove(uint16 ID) override;

	// int operation
	virtual int32 GetInt(uint16 ID) const override;
	virtual void SetInt(uint16 ID, int32 Value) override;
	virtual int32 ModifyInt(uint16 ID, int32 Delta) override;

	// Reset to previous level
	void Undo();

	// Go to next level
	void NextLevel();

private:
	GKIGoapStates* OriginalStates;
	uint16 Level = 0;					// Current Level
	
	struct EntryBool
	{
		uint16 Level;			// The level of history
		uint16 ID;				// The ID
		bool Value;				// Might be false to indicates it has been removed
	};
	TArray<EntryBool> ArrayBools;

	struct EntryInt
	{
		uint16 Level;
		uint16 ID;
		int32 Value;
	};
	TArray<EntryInt> ArrayInts;

	int32 FindBool(uint16 ID);

public:
	GK_GET(uint16, Level);

	// Inherited via GKIGoapStates
	virtual int32 NumBools() const override;
	virtual int32 NumInts() const override;
	virtual void Reset() override;
	virtual void Log(const GKGameManager* GameManager) const override;
};
