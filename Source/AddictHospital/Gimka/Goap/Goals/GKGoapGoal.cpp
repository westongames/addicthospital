// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Goap/Goals/GKGoapGoal.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKGoapGoal::GKGoapGoal(const FName& ID)
	: GKDatabaseItem16(ID)
{
}

GKGoapGoal::GKGoapGoal(const FName& ID, const GKGoapSetStates& iStates)
	: GKDatabaseItem16(ID), States(iStates)
{

}

GKGoapGoal::~GKGoapGoal()
{
}

void GKGoapGoal::Log(GKGameManager* GameManager) const
{
	//UE_LOG(LogTemp, Warning, TEXT("Goal StateBool %d: %d StateInt %d"), 
	GKLog1("GoapGoal", GetID().ToString());
	States.Log(GameManager);// StateBoolDB, StateIntDB);
}