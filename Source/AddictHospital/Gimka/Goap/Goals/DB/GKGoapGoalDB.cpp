// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapGoalDB.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Tools/Macros/GKLogger.h"

const FName GKGoapGoalDB::StaticID("GoapGoal");

GKGoapGoalDB::GKGoapGoalDB(const GKGoapStateBoolDB* iStateBoolDB, const GKGoapStateIntDB* iStateIntDB)
	: GKDatabase(StaticID)
{
	StateBoolDB = iStateBoolDB;
	StateIntDB = iStateIntDB;
}

GKGoapGoalDB::~GKGoapGoalDB()
{
	
}

void GKGoapGoalDB::OnParseData(GKGameManager* GameManager)
{
	//GKGoapGoal goalHasWeapon;
	//goalHasWeapon.GetStates().Add(GKGoapState::HasWeapon);
	//Add(goalHasWeapon);
	//GKLog("OnParseData");

	Add(GKGoapGoal("getWeapon", GKGoapSetStates(
		{ "hasWeapon" },
		{}, 
		*StateBoolDB, *StateIntDB)));

	Add(GKGoapGoal("killEnemy", GKGoapSetStates(
		{ "enemyDeath" },
		{},
		*StateBoolDB, *StateIntDB)));

	Log(GameManager);
}

bool GKGoapGoalDB::CanInitialize()
{
	// Init the required DB first
	return StateBoolDB->GetIsInitialized() && StateIntDB->GetIsInitialized();
}

//void GKGoapGoalDB::Log()//const GKGoapStateBoolDB& StateBoolDB, const GKGoapStateIntDB& StateIntDB)
//{
//	GKLog1("GoapGoalDB", Num());
//	for (int32 i = 0; i < Num(); ++i)
//	{
//		operator[](i).Log(*StateBoolDB, *StateIntDB);
//	}
//}
