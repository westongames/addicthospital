// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Database/GKDatabase.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Containers/Struct/GKObject.h"
#include "Gimka/Goap/Goals/GKGoapGoal.h"
#include "Gimka/Goap/States/DB/GKGoapStateBoolDB.h"
#include "Gimka/Goap/States/DB/GKGoapStateIntDB.h"

///**
// * Store information of a bool goap state
// */
//struct GKGoapGoal : public GKObject
//{
//public:
//	GKGoapGoal(const FName& ID) : GKObject(ID) {}
//};

class GKGoapGoalDB : public GKDatabase<GKGoapGoal>
{
	// Keep references
	const GKGoapStateBoolDB* StateBoolDB;
	const GKGoapStateIntDB* StateIntDB;

public:
	const static FName StaticID;

	GKGoapGoalDB(const GKGoapStateBoolDB* StateBoolDB, const GKGoapStateIntDB* StateIntDB);
	~GKGoapGoalDB();

	//void Log();// const GKGoapStateBoolDB& StateBoolDB, const GKGoapStateIntDB& StateIntDB);

protected:
	// Inherited via GKDatabase
	virtual void OnParseData(GKGameManager* GameManager) override;
	virtual bool CanInitialize() override;
};
