// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Database/GKDatabaseItem.h"

class GKGameManager;

/**
 * 
 */
class GKGoapGoal : public GKDatabaseItem16
{
	GKGoapSetStates States;

public:
	GKGoapGoal(const FName& ID);
	GKGoapGoal(const FName& ID, const GKGoapSetStates& States);
	~GKGoapGoal();

	void Log(GKGameManager* GameManager) const;

public:
	GK_GETCR(GKGoapSetStates, States);
};
