// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Goap/States/GKIGoapStates.h"
#include "Gimka/Containers/Queue/GKPriorityQueue.h"
#include "GKGoapNode2.h"
#include "Gimka/Goap/States/GKGoapArrayStates.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Goap/Actions/GKGoapAction2.h"
//#include "Util/IndexPriorityQueue.h"

class GKGoapAction;
class GKGameManager;

/**
 * 
 */
class GKGoapPlanner2
{
public:
	GKGoapPlanner2(const GKGameManager* GameManager);
	~GKGoapPlanner2();

	bool Plan(const GKGoapSetStates& goal, const GKGoapArrayStates& worldStates,
		const TArray<const GKGoapAction2*>& arrayActions, TArray<const GKGoapAction2*>& queueActions);

private:
	GKGoapNode2::Queue QueueOpen;
	GKGoapNode2::Queue QueueClosed;

	const GKGameManager* GameManager;

	TArray<GKGoapNode2> ArrayNodes;
	
	// Track how many nodes have been created from ArrayNodes. 
	// We're using a separate variable because a node can be tentatively created just to check if it's already existed or not.
	int32 NodeCount;
	//int32 CalculateHeuristic(const GKGoapArrayStates& current, const GKGoapSetStates&) const;

	GKGoapNode2* FindInCreatedNode(const GKGoapNode2* node);
};
