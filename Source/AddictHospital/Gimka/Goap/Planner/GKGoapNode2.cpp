// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Goap/Planner/GKGoapNode2.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Goap/Actions/DB/GKGoapActionDB.h"

GKGoapNode2::GKGoapNode2()
{
}

GKGoapNode2::~GKGoapNode2()
{
}

void GKGoapNode2::SetAsStartingNode(const GKGoapSetStates& iConditions, const GKIGoapStates& worldStates, int32 arrayActionNums)
{
	// This is the goal node. So ActualCost is zero.
	ActualCost = 0;
	Parent = nullptr;

	// The initial conditions for the goal
	Conditions.Copy(iConditions);

	// Calculate HashCode
	ConditionHashCode = Conditions.CalculateHashCode();

	// No action yet
	ActionIndex = -1;
	ActionsBeenUsed.Reset();
	//ActionsBeenUsed.Add(false, arrayActionNums);

	// Calculate heuristic from worldStates to current conditions
	HeuristicCost = worldStates.DistanceTo(&Conditions);
	Cost = ActualCost + HeuristicCost;

	// It's open!
	IsClosed = false;
}

void GKGoapNode2::SetAsChildNodeOf(const GKGoapNode2* iParent, const GKIGoapStates& worldStates,
	int32 iActionIndex, const GKGoapAction2* action)	// int32 actionCost, const GKGoapSetStates& ActionEffects)
{
	Parent = iParent; ActionIndex = iActionIndex;

	// ActualCost is the parent's ActualCost + Action's Cost
	ActualCost = Parent->ActualCost + action->GetCost();

	// Copy conditions from parent, apply the action effects, and add the action conditions
	Conditions.Copy(Parent->Conditions);
	Conditions.ApplyFrom(&action->GetEffects());
	Conditions.Append(action->GetConditions());

	// Calculate HashCode
	ConditionHashCode = Conditions.CalculateHashCode();

	// Mark the new actionIndex as beenUsed so it won't be used again.
	ActionsBeenUsed = Parent->ActionsBeenUsed;
	check(!ActionsBeenUsed[ActionIndex]);
	ActionsBeenUsed.SetTrue(ActionIndex);
	
	// Recalculate heuristic from worldStates to current conditions
	HeuristicCost = worldStates.DistanceTo(&Conditions);
	Cost = ActualCost + HeuristicCost;

	// It's open!
	IsClosed = false;
}

void GKGoapNode2::SetAsChildNodeOfStep1(const GKGoapNode2* iParent, const GKGoapAction2* action)
{
	// ActualCost is the parent's ActualCost + Action's Cost
	ActualCost = iParent->ActualCost + action->GetCost();

	// Copy conditions from parent, apply the action effects, and add the action conditions
	Conditions.Copy(iParent->Conditions);
	Conditions.ApplyFrom(&action->GetEffects());
	Conditions.Append(action->GetConditions());

	// Calculate HashCode
	ConditionHashCode = Conditions.CalculateHashCode();
}

void GKGoapNode2::SetAsChildNodeOfStep2(GKGoapNode2* iParent, const GKIGoapStates& worldStates, int32 iActionIndex)
{
	Parent = iParent; ActionIndex = iActionIndex;

	// Mark the new actionIndex as beenUsed so it won't be used again.
	ActionsBeenUsed = Parent->ActionsBeenUsed;
	check(!ActionsBeenUsed[ActionIndex]);
	ActionsBeenUsed.SetTrue(ActionIndex);

	// Recalculate heuristic from worldStates to current conditions
	HeuristicCost = worldStates.DistanceTo(&Conditions);
	Cost = ActualCost + HeuristicCost;

	// It's open!
	IsClosed = false;
}

void GKGoapNode2::ReplaceWithTheSameButBetterActualCost(const GKGoapNode2* other, const GKGoapNode2* iParent, int32 actionIndex)
{
	// The other has the same Conditions but different sequence of Actions. 
	// So update Parent and actions.
	Parent = iParent; ActionIndex = other->ActionIndex;

	// Update the actual cost with the better one
	ActualCost = other->ActualCost;

	// Conditions are the same, so no need to copy. Similarly HashCode should be the same.

	// Use the parent ActionsBeenUsed and mark actionIndex
	ActionsBeenUsed = Parent->ActionsBeenUsed;
	check(!ActionsBeenUsed[ActionIndex]);
	ActionsBeenUsed.SetTrue(ActionIndex);

	// Heuristic from worldStates to current condition hasn't changed! Update Cost because ActualCost has changed!
	Cost = ActualCost + HeuristicCost;

	// When this happen, the node shouldn't have been closed
	check(!IsClosed);
}

//void GKGoapNode2::Reset()
//{
//
//}

void GKGoapNode2::Log(const GKGameManager* GameManager, const TArray <const GKGoapAction2*>& arrayActions)
{
	GKGoapActionDB* ActionDB = (GKGoapActionDB*)GameManager->GetDatabase(GKGoapActionDB::StaticID);
	const GKGoapNode2* ptr = this;
	FString str;
	while (ptr && ptr->Parent != nullptr)
	{
		if (str.Len() > 0) str += TEXT(" then ");
		const GKGoapAction2* action = arrayActions[ptr->ActionIndex];
		str += FString::Printf(TEXT("%s (%d+%d=%d,%d)"),
			*action->GetID().ToString(), ptr->ActualCost, ptr->HeuristicCost, ptr->Cost, ptr->ConditionHashCode.GetValue());
		ptr = ptr->Parent;
	}
	UE_LOG(LogTemp, Warning, TEXT(" === %s"), *str);

	//		GoapNode g = %d, h = %d, f = %d, actionIdx = %d/%d"),
	//str.Append()
	//UE_LOG(LogTemp, Warning, TEXT("GoapNode g = %d, h = %d, f = %d, actionIdx = %d/%d"), 
	//	ActualCost, HeuristicCost, Cost, ActionIndex, ActionsBeenUsed.Num());
	////GKLog1("Action"if (action) action->Log(GameManager); else GKLog("Action: null");
	Conditions.Log(GameManager);
}