// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Containers/Queue/GKPriorityQueue.h"
#include "Gimka/Goap/Actions/GKGoapAction2.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Containers/Bits/GKBits.h"

class GKGameManager;
/**
* 
 * 
 */
struct GKGoapNode2
{
	int32 ActualCost;			// g = The A* cost from 'start' to 'here'
	int32 HeuristicCost;		// h = The estimated remaining cost to 'goal' from 'here'
	int32 Cost;					// Cost = ActualCost + HeuristicCost

	GKGoapSetStates Conditions;				// The accumulative conditions of the goal at this node
	GKBits64 ConditionHashCode;				// Quick hashCode to speedup comparing Conditions (with other node's Conditions)

	int32 ActionIndex;						// The action that got us here (for replay purposes)
	const GKGoapNode2* Parent;

	// Keep tracks which action in the planner's arrayActions are already used.
	// Each action can only be used once.
	//TBitArray<FDefaultBitArrayAllocator> ActionsBeenUsed;
	GKBits64 ActionsBeenUsed;
	
	// IsClosed means the node has been expanded, children nodes have been created if exist, and it has been "moved" to ClosedList.
	// There are no separated ClosedList.
	// The node will be kept in the same sortedArray so it can be found easily.
	bool IsClosed;				

public:
	GKGoapNode2();
	~GKGoapNode2();

	void SetAsStartingNode(const GKGoapSetStates& goalConditions, const GKIGoapStates& worldStates,
		int32 arrayActionsNum);

	void SetAsChildNodeOf(const GKGoapNode2* parent, const GKIGoapStates& worldStates,
		int32 actionIndex, const GKGoapAction2* action);//int32 actionCost, const GKGoapSetStates& ActionEffects);

	void SetAsChildNodeOfStep1(const GKGoapNode2* parent, const GKGoapAction2* action);
	void SetAsChildNodeOfStep2(GKGoapNode2* parent, const GKIGoapStates& worldStates, int32 actionIndex);


		//const GKGoapSetStates& goalConditions, int g, 
		//const GKGoapNode2* parent, int32 actionIndex, const GKGoapSetStates& ActionEffects);

	void ReplaceWithTheSameButBetterActualCost(const GKGoapNode2* other, const GKGoapNode2* iParent, int32 actionIndex);

	FORCEINLINE bool HasActionBeenUsed(int32 iActionIndex) { return ActionsBeenUsed[iActionIndex]; }
	//FORCEINLINE bool 
	FORCEINLINE int32 AreAllConditionsSatisfied() const { return HeuristicCost == 0; }

	void Log(const GKGameManager* GameManager, const TArray <const GKGoapAction2*>& arrayActions);

	FORCEINLINE uint64 GetConditionHashCode() const { return ConditionHashCode.GetValue(); }

	struct ComparatorMin
	{
		static bool Compare(GKGoapNode2* node1, GKGoapNode2* node2)
		{
			return node1->Cost < node2->Cost;
		}
	};

	typedef GKPriorityQueue<GKGoapNode2*, ComparatorMin> Queue;

public:	// Accessors
	GK_GETCR(GKGoapSetStates, Conditions);
	GK_GET(int32, ActualCost);
	GK_GET(int32, HeuristicCost);
	GK_GET(int32, Cost);
	GK_GET(int32, ActionIndex);
	GKPTR_GET(const GKGoapNode2, Parent);
	GK_GETSET(bool, IsClosed);

	//FORCEINLINE int32 GetActualCost() const { return g; }
	//FORCEINLINE int32 GetHeuristicCost() const { return h; }
	//FORCEINLINE int32 GetCost() const { return f; }
	
};
