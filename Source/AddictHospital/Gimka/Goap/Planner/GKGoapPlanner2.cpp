// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGoapPlanner2.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKGoapPlanner2::GKGoapPlanner2(const GKGameManager* iGameManager)
{
	GameManager = iGameManager;
	ArrayNodes.AddDefaulted(1000);
}

GKGoapPlanner2::~GKGoapPlanner2()
{
}

bool GKGoapPlanner2::Plan(const GKGoapSetStates& goal, const GKGoapArrayStates& worldStates,
	const TArray <const GKGoapAction2*>& arrayActions, TArray<const GKGoapAction2*>& queueActions)
{
	// OptimizePotential: remove actions that are not relevant from arrayActions

	int32 actionNum = arrayActions.Num();
	check(actionNum < 64);		// Limit to 63 for now because we're using GKBits64 (might be able to go with 64)

	queueActions.Reset();
	if (worldStates.Satisfies(&goal))
	{
		// Already done!
		return true;
	}

	// Clear out prior results
	QueueOpen.Reset();
	QueueClosed.Reset();

	GKLog("WorldStates");
	worldStates.Log(GameManager);

	GKLog("Goal");
	goal.Log(GameManager);

	int32 distance = worldStates.DistanceTo(&goal);
	GKLog1("Distance to goal ", distance);

	GKLog1("Action Num", arrayActions.Num());
	for (int i = 0; i < arrayActions.Num(); ++i) GKLog1("  Action", arrayActions[i]->GetID().ToString());

	// Reset NodeCount
	NodeCount = 0;

	// TODO: need to memory pool the nodes, possibly create a GK class to maintain used and deleted list.
	// TODO: Should be able to release the node by itself.
	// Different than goapCPP. Instead of worldStates, we put the goal conditions for starting node.
	GKGoapNode2* StartingNode = &ArrayNodes[NodeCount++];// new GKGoapNode2();
	StartingNode->SetAsStartingNode(goal, worldStates, actionNum);// , 0, worldStates.DistanceTo(goal), nullptr, nullptr);
		//..worldStates, 0,
		//worldStates.DistanceTo(goal), nullptr, nullptr);

	QueueOpen.Enqueue(StartingNode);
	//GKLog1("QueueOpen.Enqueue Num", QueueOpen.Num());
	//QueueOpen.Log();
	//TArray<GKGoapNode2*> QueueNodes;
	//QueueNodes.Add(StartingNode);

	//for (int index = 0; index < QueueNodes.Num(); ++index)
	int32 iteration = 0;
	int32 NodeCreated = 1;
	int32 CostOfLastNodeClosed = 0;
	while (QueueOpen.HasItem())
	{
		//GKGoapNode2* currentNode = QueueNodes[index];
		GKGoapNode2* currentNode = QueueOpen.Dequeue();
		//GKLog1("QueueOpen.Dequeue Num", QueueOpen.Num());
		//QueueOpen.Log();
		GKLog3("Current Node Action, g, h ", currentNode->GetActionIndex(), currentNode->GetActualCost(), currentNode->GetHeuristicCost());
		//currentNode->Log(GameManager);

		if (currentNode->AreAllConditionsSatisfied())
		{
			GKLog2("All condition has been satisifed! Iteration, NodeCreated", iteration, NodeCreated);
			currentNode->Log(GameManager, arrayActions);
		}
		else
		{
			for (int32 actionIndex = 0; actionIndex < actionNum; ++actionIndex)
			{
				// If action has been used, skip
				if (currentNode->HasActionBeenUsed(actionIndex))
				{
					//GKLog1("Action has been used", actionIndex);
					continue;
				}

				// If action doesn't satisfy any of the current conditions, skip
				const GKGoapAction2* action = arrayActions[actionIndex];
				if (!action->GetEffects().SatisfiesAny(&currentNode->GetConditions()))
				{
					//GKLog1("Action doesn't satisfy any condition", actionIndex);
					continue;
				}

				// Tentatively, create the new node containing the new action
				// Initialize with only necessary setup to compare with existing nodes' conditions.
				GKGoapNode2* newNode = &ArrayNodes[NodeCount];
				//newNode->SetAsChildNodeOf(currentNode, worldStates, actionIndex, action);
				newNode->SetAsChildNodeOfStep1(currentNode, action);// action->GetCost(), action->GetEffects());
				
				GKLog1("Add action to current node", actionIndex);

				// Check if the new node is already existed.
				GKGoapNode2* nodeExisted = FindInCreatedNode(newNode);
				if (nodeExisted)
				{
					int32 deltaCost = newNode->GetActualCost() - nodeExisted->GetActualCost();

					if (nodeExisted->GetIsClosed())
					{
						// if it's already closed
						// Assume the closed one has better cost. If not we might want to re-add the new node because it's better?
						// The closed one might already has children so it can't be re-used but re-add the new node is possible if such case is required.
						// Need to find an example case where we should re-add. For now, just skip.
						check(deltaCost >= 0);// nodeExisted->GetActualCost() <= newNode->GetActualCost());
						continue;
					}

					if (deltaCost >= 0)
					{
						GKLog1("Node existed but the new one is not better, delta", deltaCost);
						//newNode->SetAsChildNodeOfStep2(currentNode, worldStates, actionIndex);
						//newNode->Log(GameManager, arrayActions);
						//GKLog1("Existed Node IsClosed", nodeExisted->GetIsClosed());
						//int32 NodeIndex = QueueOpen.FindExact(nodeExisted);
						//GKLog1("NodeIndex", NodeIndex);
						//nodeExisted->Log(GameManager, arrayActions);
						continue;			// Skip adding this
					}

					// The node is still open and the new one is better so replace it!
					GKLog1("Node existed but the new one is better by", deltaCost);
					//newNode->Log(GameManager, arrayActions);
					GKLog1("Existed Node IsClosed", nodeExisted->GetIsClosed());
					nodeExisted->Log(GameManager, arrayActions);

					nodeExisted->ReplaceWithTheSameButBetterActualCost(newNode, currentNode, actionIndex);

					// TODO: re-sort QueueOpen because of updated value
					int32 NodeIndex = QueueOpen.FindExact(nodeExisted);
					GKLog1("NodeIndex", NodeIndex);
					QueueOpen.ReSortAt(NodeIndex);
				}
				else
				{
					// It's a new node! Finish initialization!
					newNode->SetAsChildNodeOfStep2(currentNode, worldStates, actionIndex);

					// Add to QueueOpen
					//QueueNodes.Add(newNode);
					QueueOpen.Enqueue(newNode); ++NodeCount;
					NodeCreated++;
					QueueOpen.Log();
				}
			}
		}

		// We're done with the current node
		// TODO: put back to memory pool instead => currentNode can't be destroyed because it's still referred by child
		check(currentNode->GetCost() >= CostOfLastNodeClosed);		// A theory that the closedList nodes are increasing in cost
		CostOfLastNodeClosed = currentNode->GetCost();
		currentNode->SetIsClosed(true);
		 
		//delete currentNode;
		++iteration;
		//if (iteration == 5) break;
	}

	GKLog3("Planner done iteration, nodeCreated, nodeCount", iteration, NodeCreated, NodeCount);


	return false;
}


GKGoapNode2* GKGoapPlanner2::FindInCreatedNode(const GKGoapNode2* node)
{
	// Note: 2 nodes might have same conditions but different action sequence, and thus have different ActualCost.
	// For now just search sequentially, later need to make this sorted
	for (int i = 0; i < NodeCount; ++i)
	{
		GKGoapNode2& current = ArrayNodes[i];
		if (current.GetConditionHashCode() == node->GetConditionHashCode() &&
			current.GetConditions().IsEquals(node->GetConditions()))
		{
			return &current;
		}
	}
	return nullptr;
}


//int32 GKGoapPlanner2::CalculateHeuristic(const GKGoapArrayStates& current, const GKGoapSetStates& goal) const
//{
//	int32 h = current.DistanceTo(goal);
//	return int32();
//}
