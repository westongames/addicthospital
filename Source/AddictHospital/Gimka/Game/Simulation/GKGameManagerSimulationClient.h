// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
class GKGameManagerSimulationClient : public GKGameManagerSimulation
{
public:
	GKGameManagerSimulationClient(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	~GKGameManagerSimulationClient();

	//virtual void PostGameMessage(const GKMessageNetworkToClient& iMessage) override;
	//virtual void PostGameMessage(const GKMessageNetworkToClientCommand& iMessage) override;

protected:
	virtual void OnUpdateClient(float DeltaTime) override;

	void ProcessMessagesToClient();

	virtual void OnCommand(const GKCommand& Command) override;


private:
	//enum ClientState
	//{
	//	NeedToJoinGame,
	//	WaitForStartGame,
	//	Count
	//};


	TArray<GKMessageNetworkToClient> InboxMessageNetworkToClient;

	//ClientState State = ClientState::NeedToJoinGame;

	// Current server gametick. Sent from server via GKMessageNetworkToClient::ServerGameTick.
	// If client has done the prerequisite Command, it can advance up to this GameTick.
	uint32 ServerGameTick = 0;
	uint32 PrerequisiteCommandGameTick = 0;		// The required Command GameTick to execute before client can advance to ServerGameTick

	// If the difference of GameTick Server - Client is 60 or more (>= 1s), then advance ultraFast (5 gameTick each frame)
	const int32 AdvanceUltraFastDeltaGameTickThreshold = 60;
	const int32 AdvanceUltraFastCount = 5;			// 5 GameTicks per frame

	// If the different of GameTick Server - Client is 20 or more (>= 1/3s), then advance veryFast (2 gameTick each frame)
	const int32 AdvanceVeryFastDeltaGameTickThreshold = 20;
	const int32 AdvanceVeryFastCount = 2;			// 2 GameTicks per frame

	// If the different of GameTick Server - Client is 10 or more (>= 1/6s), then always do at least once per frame
	const int32 AdvanceFastDeltaGameTickThreshold = 10;

	// UI
	uint32 LastTimeLogScreenSyncingServer = 0;
};
