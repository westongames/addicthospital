// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
class GKGameManagerSimulationServer : public GKGameManagerSimulation
{
public:
	struct PlayerInfo
	{
		int32 PlayerID;
		int32 ClientGameTick;			// Current client gameTick limit (server knows because server sends UpdateGameTick)
	};
	GKGameManagerSimulationServer(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	~GKGameManagerSimulationServer();

	//virtual void PostGameMessage(const GKMessageNetworkToServer& iMessage) override;
	//virtual void PostGameMessage(const GKMessageNetworkToServerCommand& iMessage) override;

	virtual void PlayerJoin(GKNetworkPlayer* Player) override;

protected:
	virtual void OnUpdateServer(float DeltaTime) override;

	// Server will archive commands to be sent to late joining clients. 
	// The initial message might have no GameTick, the specified ServerGameTick is the exact GameTick the server executed the command.
	virtual void OnArchiveCommands(const GKMessageNetworkToClientCommand& Message, uint32 ServerGameTick) override;

	void ProcessMessagesToServer();



private:
	TArray<GKMessageNetworkToServer> InboxMessageNetworkToServer;
	//TArray<GKMessageNetworkToServerCommand> InboxMessageNetworkToServerPI;

	// Commands that have been executed and send to all clients will be placed here in order.
	TArray<GKMessageNetworkToClientCommand> ArchiveCommands;

	// Server needs to save all the outgoing messages because it needs to send them to client when client is joining.
	//TArray<GKMessageNetworkToServer> ArchiveMessageNetworkToServer;
	//TArray<GKMessageNetworkToClientCommand> ArchiveCommands;
};
