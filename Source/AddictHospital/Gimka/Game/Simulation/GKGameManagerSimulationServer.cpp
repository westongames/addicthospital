// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Game/Simulation/GKGameManagerSimulationServer.h"

GKGameManagerSimulationServer::GKGameManagerSimulationServer(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority)
	: GKGameManagerSimulation(ID, GameEngine, iHasAuthority)
{
}

GKGameManagerSimulationServer::~GKGameManagerSimulationServer()
{
}

void GKGameManagerSimulationServer::PlayerJoin(GKNetworkPlayer* Player)
{
	GKGameManager::PlayerJoin(Player);

	if (!Player->IsLocalPlayer())
	{
		// Server needs to track missing messages for clients
		Player->InitMissingCommandNum(ArchiveCommands.Num());
		GKLog2("Player missing command", (uint32)Player->GetPlayerID(), (uint32)ArchiveCommands.Num());
	}
}

void GKGameManagerSimulationServer::OnUpdateServer(float DeltaTime)
{
	ProcessMessagesToServer();

	//GKLog1("Server: ", GameTime.GetGameTick());
	//GetGameEngine()->PrintScreen(5.f, FColor::Green, FString("Hello server"));

	//GKLog1("DeltaTime = ", DeltaTime);
	int32 InitialArchiveCommandsNum = ArchiveCommands.Num();
	float fDeltaGameTick = GetGameSettings().CalculateGameTicks(DeltaTime);

	// Only Server can advance GameTick
	float FractionGameTick;
	DeltaGameTick = GameTime.PrecalcGameTicks(fDeltaGameTick, FractionGameTick);

	// If this is the first gameTick, only add by 1. Usually it will add by 18 (because of a lot of initial processing)
	uint32 ServerGameTick = GameTime.GetGameTick();
	if (ServerGameTick == 0 && DeltaGameTick > 1)
	{
		GKLog1("Limit ServerGameTick for the first one from ", DeltaGameTick);
		DeltaGameTick = 1;
		//NewGameTickTotal = 1.f;
	}
	if (DeltaGameTick > 1)
	{
		GKLog1("Server Update DeltaGameTick more than one", DeltaGameTick);
	}

	// Do World->OnGameTick, from 0 to multiple
	UpdateWorld(DeltaGameTick);

	// If we have precalc the next totalGameTick, even though the GameTick has been increased within UpdateWorld,	
	// set it to the precalc one because it contains the fraction too.
	GameTime.SetFractionGameTick(FractionGameTick);

	// If any client is missing previous command, send one
	// Also send this before sending the newer message (for current gameTick)
	GKNetworkManager* NetworkManager = GetNetworkManager();
	int32 NumPlayers = NetworkManager->NumPlayers();
	for (int32 i = 0; i < NumPlayers; ++i)
	{
		GKNetworkPlayer* Player = NetworkManager->GetPlayer(i);
		if (Player->IsServerPlayer()) continue;

		uint32 Index;
		if (Player->GetMissingCommandNextIndexToReceiveAndAdvance(Index))
		{
			const GKMessageNetworkToClientCommand& Message = ArchiveCommands[Index];
			PostNetworkMessage(Message, Player->GetPlayerID());
			//GKLog2("Will send missing command index XX to PlayerID", Index, (uint32)Player->GetPlayerID());
		}
	}


	// Post network message to update all clients.
	// If there're playerInputs, add those, otherwise just a simple message to update GameTick.
	// Note: playerInput for this gameTick, 
	// 	- will be marked with the current gameTick, which will be processed next turn.
	//	- will be sent in GKNetworkManager::Tick, which will happen after this GameEngine::Tick.
	//	- on server side, it will receive immediately, and be placed into ArrayInboxMessage*,
	//		which will be processed at beginning of next frame.
	if (DeltaGameTick > 0)
	{
		// If there were commands, send those commands
		if (ArchiveCommands.Num() > InitialArchiveCommandsNum)
		{
			for (int32 i = InitialArchiveCommandsNum, Count = ArchiveCommands.Num(); i < Count; ++i)
			{
				const GKMessageNetworkToClientCommand& Message = ArchiveCommands[i];
				check(Message.GetGameTick() <= GameTime.GetGameTick());
				PostNetworkMessage(Message);
				GKLog2("Posting ClientCommand at GameTick, with numPlayers", GameTime.GetGameTick(), (uint32)NumPlayers);
			}
		}
		else
		{
			// If there aren't commands, just send UpdateGameTick
			uint32 LastCommandGameTick = 0;
			if (ArchiveCommands.Num() > 0)
			{
				LastCommandGameTick = ArchiveCommands[ArchiveCommands.Num() - 1].GetGameTick();
			}

			GKMessageNetworkToClient Message = GKMessageNetworkToClient::CreateServerGameTick(
				GameTime.GetGameTick(), LastCommandGameTick);

			PostNetworkMessage(Message);
		}
	}
}

void GKGameManagerSimulationServer::ProcessMessagesToServer()
{
	// Get new messages if available and append them to inbox
	GetGameEngine()->GetNetworkManager()->RetrieveMessage(InboxMessageNetworkToServer);

	for (const GKMessageNetworkToServer& Message : InboxMessageNetworkToServer)
	{
		//GKLog("Server process message ", InboxMessageNetworkToServer.Num());
		switch (Message.GetMessageType())
		{
		case GKMessageNetworkToServer::MessageType::JoinGame:
		{
			GKMessageNetworkToClient NewMessage = GKMessageNetworkToClient::CreateStartGame(2);
			//PostNetworkMessage(NewMessage);
			//GKLog2("Server receiving the JoinGame message at ", GameTime.GetGameTick(), Message.GetGameTick());
			/*if (Message.GetGameTick() > ServerGameTick)
			{
				ServerGameTick = Message.GetGameTick();
			}*/
			break;
		}
		}
	}
	InboxMessageNetworkToServer.Empty();
}

void GKGameManagerSimulationServer::OnArchiveCommands(const GKMessageNetworkToClientCommand& Message, uint32 ServerGameTick)
{
	// If the ServerGameTick is the same, just append it.
	// This will ensure the client only has 1 GKMessageNetworkToClientCommand per gameTick
	int32 LastIndex = ArchiveCommands.Num() - 1;
	if (LastIndex >= 0)
	{
		if (ArchiveCommands[LastIndex].GetGameTick() == ServerGameTick)
		{
			ArchiveCommands[LastIndex].Append(Message);
			return;
		}
	}

	int32 index = ArchiveCommands.Add(Message);
	ArchiveCommands[index].SetGameTick(ServerGameTick);
	if (index > 0)
	{
		// This will enforce the clients to not miss the previous command
		// i.e. when clients have missing commands and receive the current one, it may do the current one without doing the missing commands first.
		ArchiveCommands[index].SetPrerequisiteCommandGameTick(ArchiveCommands[index - 1].GetGameTick());
	}
}

//void GKGameManagerSimulationServer::PostGameMessage(const GKMessageNetworkToServer& iMessage)
//{
//	InboxMessageNetworkToServer.Add(iMessage);
//}
//
//void GKGameManagerSimulationServer::PostGameMessage(const GKMessageNetworkToServerCommand& iMessage)
//{
//	InboxMessageNetworkToServerPI.Add(iMessage);
//}
