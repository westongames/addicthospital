// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Game/Simulation/GKGameManagerSimulationClient.h"
#include "Gimka/Network/GKMessageNetworkToServer.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKGameManagerSimulationClient::GKGameManagerSimulationClient(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority)
	: GKGameManagerSimulation(ID, GameEngine, iHasAuthority)
{
}

GKGameManagerSimulationClient::~GKGameManagerSimulationClient()
{
}

void GKGameManagerSimulationClient::OnUpdateClient(float DeltaTime)
{
	/*if (GetPlayerID() == 2)
	{
		GKLog1("Client2: ", GameTime.GetGameTick());
		if (GKLogger::GetPlayerID() != 2)
		{
			GKLog1("PlayerID is wrong at ", GKLogger::GetPlayerID());
			GKLogger::SetPlayerID(2);
		}
	}*/

	// Retrieve message commands, if available.
	GetNetworkManager()->RetrieveMessage(InboxCommands);

	uint32 ClientGameTick = GameTime.GetGameTick();

	// This is the highest GameTick this client is allowed to advance too.
	uint32 MaxGameTick = 0;			// Default to zero

	// If there's command then client can advance as long as the prerequisite of every command is fulfilled.
	uint32 NewLastCommandGameTickExecuted = GetLastCommandGameTickExecuted();
	if (InboxCommands.Num() > 0)
	{
		for (GKMessageNetworkToClientCommand& Message : InboxCommands)
		{
			// If prerequisiteCommandGameTick not executed yet, we can't execute this message
			if (Message.GetPrerequisiteCommandGameTick() > NewLastCommandGameTickExecuted) break;

			// Otherwise, yes, we can
			check(Message.GetGameTick() >= MaxGameTick);		// The next one is supposed to be higher or at least the same
			MaxGameTick = Message.GetGameTick();

			// We can stop when we can do the maximum DeltaGameTick per frame
			if (MaxGameTick - ClientGameTick >= (uint32)AdvanceUltraFastCount) break;
		}
		GKLog2("InboxCommand, MaxGameTick: ", (uint32)InboxCommands.Num(), MaxGameTick);
	}

	// TODO: next consider allowing to advance to servergametick if the prerequisite is fulfilled
	if (NewLastCommandGameTickExecuted >= PrerequisiteCommandGameTick)
	{
		MaxGameTick = ServerGameTick;
	}

	/*switch (State)
	{
		case ClientState::NeedToJoinGame:
		{
			check(ClientGameTick == 0);
			GKMessageNetworkToServer Message = GKMessageNetworkToServer::CreateJoinGame();
			GetGameEngine()->GetNetworkManager()->PostMessage(Message);
			State = ClientState::WaitForStartGame;
			return;
		}
		case ClientState::WaitForStartGame:
		{
			check(ClientGameTick == 0);
			return;
		}
	}*/

	ProcessMessagesToClient();

	// Default to zero
	float fDeltaGameTick = GetGameSettings().CalculateGameTicks(DeltaTime);
	DeltaGameTick = 0;

	// Client will only advance up to server's gameTick.
	// Initialliy, ServerGameTick == 0
	int32 DifGameTick = MaxGameTick - ClientGameTick;
	check(DifGameTick >= 0);

	float FractionGameTick = 0;
	if (DifGameTick > 0)
	{
		DeltaGameTick = GameTime.PrecalcGameTicks(fDeltaGameTick, FractionGameTick);

		if (DeltaGameTick > DifGameTick)
		{
			// If client is going to advance more then MaxGameTick, then limit it.
			DeltaGameTick = DifGameTick;
			//GameTime.AdvanceGameTicks(DeltaGameTick);
			GKLog1("Client is about to advance more than max", ClientGameTick);
		}
		else if (DifGameTick >= AdvanceUltraFastDeltaGameTickThreshold)
		{
			// Client needs to catch up fast. Do 5 GameTick every frame,
			// regardless if PossiblyDeltaGameTick is 0 or not.
			DeltaGameTick = AdvanceUltraFastCount;
			//GameTime.AdvanceGameTicks(DeltaGameTick);
			GKLog1("Client is advancing ultra fast", ClientGameTick);

			if ((ClientGameTick - LastTimeLogScreenSyncingServer) > (uint32)(AdvanceUltraFastCount * 60 * 5)
				&& (ServerGameTick - ClientGameTick > (uint32)(AdvanceUltraFastCount * 60 * 4)))
			{
				LastTimeLogScreenSyncingServer = ClientGameTick;
				FString Message = FString::Printf(TEXT("Syncing Server... %d%%"), ClientGameTick * 100 / ServerGameTick);
				GetGameEngine()->PrintScreen(5.f, FColor::Green, Message);
			}
		}
		else if (DifGameTick >= AdvanceVeryFastDeltaGameTickThreshold)
		{
			// Client needs to catch up fast. Do 5 GameTick every frame,
			// regardless if PossiblyDeltaGameTick is 0 or not.
			DeltaGameTick = AdvanceVeryFastCount;
			//GameTime.AdvanceGameTicks(DeltaGameTick);
			GKLog1("Client is advancing very fast", ClientGameTick);
		}
		else if (DifGameTick >= AdvanceFastDeltaGameTickThreshold)
		{
			// Client need to catch up a bit.
			// If this frame client is not doing GameTick (PossiblyDeltaGameTick == 0), force it to update once
			if (DeltaGameTick == 0)
			{
				DeltaGameTick = 1;
				//GameTime.AdvanceGameTicks(1);
				GKLog1("Client is advancing fast, forced update ", ClientGameTick);
			}
			else
			{
				// Otherwise, just advance as regular.
				//DeltaGameTick = GameTime.AdvanceGameTicks(fDeltaGameTick);
				//DeltaGameTick = PossiblyDeltaGameTick;
				//GKLog2("Client is advancing fast, regular update", (uint32)DeltaGameTick, ClientGameTick);
			}
		}
		else
		{
			// Just do as much as possibly based on client speed but no more than server.
			//DeltaGameTick = GameTime.AdvanceGameTicks(fDeltaGameTick);
			//DeltaGameTick = PossiblyDeltaGameTick;
			//GKLog2("Client just reg", (uint32)DeltaGameTick, ClientGameTick);
		}
	}

	// Do World->OnGameTick, from 0 to multiple
	UpdateWorld(DeltaGameTick);

	// If we have precalc the next totalGameTick, even though the GameTick has been increased within UpdateWorld,
	// add the fraction too because we might not be updating every frame and the fraction will adds up.
	if (DifGameTick > 0)
	{
		// We only do this if it's allowed to advance
		GameTime.SetFractionGameTick(FractionGameTick);
	}
}

void GKGameManagerSimulationClient::ProcessMessagesToClient()
{
	// Get new messages if available and append them to inbox
	GetGameEngine()->GetNetworkManager()->RetrieveMessage(InboxMessageNetworkToClient);

	for (const GKMessageNetworkToClient& Message : InboxMessageNetworkToClient)
	{
		switch (Message.GetMessageType())
		{
		case GKMessageNetworkToClient::MessageType::ServerGameTick:
		{
			/*GKLog3("Client now receiving the message at ", (uint32)GameTime.GetGameTick(),
				ServerGameTick, Message.GetGameTick());*/
			uint32 iServerGameTick, iPrerequisiteCommandGameTick;
			Message.GetServerGameTick(iServerGameTick, iPrerequisiteCommandGameTick);

			// The new message needs to have higher GameTick. If it's lower, we can asssume it's older message that has arrived late, so ignore it.
			if (iServerGameTick > ServerGameTick)
			{
				if (iPrerequisiteCommandGameTick > PrerequisiteCommandGameTick)
				{
					GKLog4("Server GameTick from & to, Prerequisite from & to: ", ServerGameTick, iServerGameTick,
						PrerequisiteCommandGameTick, iPrerequisiteCommandGameTick);
				}

				ServerGameTick = iServerGameTick;

				// Ideally, the new prerequisite also will be higher or at least equal. If not, something is wrong.
				check(iPrerequisiteCommandGameTick >= PrerequisiteCommandGameTick);
				PrerequisiteCommandGameTick = iPrerequisiteCommandGameTick;
			}
			break;
		}
		}
	}
	InboxMessageNetworkToClient.Empty();
}

void GKGameManagerSimulationClient::OnCommand(const GKCommand& Command)
{
	GKGameManagerSimulation::OnCommand(Command);
}


//void GKGameManagerSimulationClient::PostGameMessage(const GKMessageNetworkToClient& iMessage)
//{
//	InboxMessageNetworkToClient.Add(iMessage);
//}

//void GKGameManagerSimulationClient::PostGameMessage(const GKMessageNetworkToClientCommand& iMessage)
//{
//	InboxCommands.Add(iMessage);
//}
