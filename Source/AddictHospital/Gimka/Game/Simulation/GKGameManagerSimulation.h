// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * For simulation or turn based game, where reaction to playerInput doesn't need to be immediate.
 * The client gameTicks will always be behind the server. Clients will not advance GameTick further than Server.
 * i.e. in simulation like Rimworld or hospital, when player:
	- build a room, on the client show the room but non functional. Send to server. Server build the room and
		order the client to build it at the exact GameTick. The client that already has the room built, will have
		the room functional at the exact GameTick. There will be delay but the players won't expect the room
		to work immediately anyway.
	- order character to move, player knows the character doesn't represent him and it's only an order, so
		a slight delay before the character move makes sense.
 */
class GKGameManagerSimulation : public GKGameManager
{
public:
	GKGameManagerSimulation(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	~GKGameManagerSimulation();

protected:
	virtual void OnUpdateServer(float DeltaTime) override {}
	virtual void OnUpdateClient(float DeltaTime) override {}

private:


};
