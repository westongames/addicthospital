// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Entity/Entity/GKEntity.h"
//#include "Gimka/Math/GKIntVector.h"
//#include "Gimka/Containers/Array/GKArrayMapped.h"

class GKEntityManager;
class GKGameManager;
class GKGameEngine;

/**
 * Represent a layer. There may be multiple worlds, i.e. overland, underground, heaven, hell.
 * Owned by GKGameManager.
 * A world consist all the map data and all of entity managers.
 * The WorldVoxel is derived from World and contains structure for voxel world.
 * The game engine only deals with world. It doesn't know anything about WorldVoxel.
 * This way we can implement different world system for different game.
 */
class GKWorld
{
	//	GK_SINGLETON(GKWorld)
public:
	GKWorld(GKGameManager* GameManager);
	virtual ~GKWorld() {}
	//void Initialize(GKEntityManager** ArrayEntityManager, int EntityManagerCount);

	//	//int regionLandLevel, 
	//	const GKIntVector& EntityManagerCount);
	//GKEntityManager* GetEntityManager(const GKIntVector& EntityManagerIndex) const;
	//GKEntityManager* GetEntityManager(int32 indexX, int32 indexY, int32 indexZ) const;

	//void SetArrayEntityManagers(const TArray<GKEntityManager*> arrayEntityManagers);

	//int32 EntityManagerNum() { return ArrayEntityManagers.Num(); }
	//GKRegion* GetRegion(const FIntVector& RegionPos);

	// Called by GKGameManager::AdvanceGame, 0 or more times
	virtual void OnGameTick();

	// Called 1 time per frame
	virtual void OnPostFrame();

	virtual void GetEntityManagersForVisualUpdate(TArray<GKEntityManager*>& EntityManagers) {}

	GKEntityManager* GetEntityManager(int32 index) { return ArrayEntityManagers[index]; }

	virtual GKEntity* SpawnEntity(GKEntity::Type EntityType, uint32 EntityID,
		const FVector& Position, const FRotator& Rotation) { return nullptr; }

	GKEntity* FindEntity(uint32 EntityID);

	// The Update function, will use GKJobSystem
	//virtual void Update(float DeltaTime);

	GKGameEngine* GetGameEngine();

	void AddEntityManager(GKEntityManager* Manager);

protected:
	// Override to create subclass of GKRegion
	//virtual GKRegion* CreateRegion(const FIntVector& RegionPos);

	//virtual void UpdateRegion

	//virtual GKEntityManager* CreateEntityManager();

	//TArray<GKRegion*> ArrayRegions;
	TArray<GKEntityManager*> ArrayEntityManagers;		// Array of EntityManager
	//int RegionLandLevel;						// The Z value of the region that is at the overland level
	//int32 TickCountPerFrame;					// How many ticks per frame

	TArray<GKJob*> ArrayJobs;

	// For debug
	uint32 ID;

	GKGameManager* GameManager;			// Ref back to GameManager

public:
	//inline const FIntVector& GetRegionCount() const { return regionCount; }
	GK_GETCR(TArray<GKEntityManager*>, ArrayEntityManagers)

	const GKGameManager* GetGameManager() const { return GameManager; }
};
