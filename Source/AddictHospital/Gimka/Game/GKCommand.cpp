// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Game/GKCommand.h"

GKCommand::GKCommand()
{
}

GKCommand::~GKCommand()
{
}

//GKCommand GKCommand::CreateSpawnPawnEntity(uint32 EntityID, FVector Position, FRotator Rotation)
//{
//	Command = Type::SpawnPawnEntity;
//	Union1 = GKUnionStruct(EntityID);
//	Union2 = GKUnionStruct(Position);
//	Union3 = GKUnionStruct(Rotation);
//}

void GKCommand::Serialize(FArchive& Ar)
{
	Ar.SerializeIntPacked(Command);
	Union1.Serialize(Ar);
	if (Union1.ByteSize == 0) return;		// If the first union is empty, then no need to serialize the next two.
	Union2.Serialize(Ar);
	if (Union2.ByteSize == 0) return;		// If the second union is empty, then no need to serialize the third one.
	Union3.Serialize(Ar);
}