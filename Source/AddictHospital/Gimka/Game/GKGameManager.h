// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/System/GKManager.h"
#include "Gimka/System/Time/GKGameTimer.h"
#include "Gimka/System/GKGameSettings.h"
#include "Gimka/Network/GKMessageNetworkToServer.h"
#include "Gimka/Network/GKMessageNetworkToClient.h"
#include "Gimka/Network/GKMessageNetworkToServerCommand.h"
#include "Gimka/Network/GKMessageNetworkToClientCommand.h"
#include "Gimka/Game/GKWorld.h"
#include "Gimka/Containers/Array/GKSortedArray.h"
#include "Gimka/Containers/Struct/GKObject.h"

/**
 * GameManager handles logic only during gameplay.
 * GameEngine will handle everything else (sprite rendering through VisualManager and voxel rendering for the ground and network)
 * In every frame:
 * Game Tick: (repeat as needed)
 *		- DoGameMinutes:
 *			- Entities in GKRegion: 
 *				- Get & process messages store in each GKRegion MessageInbox.
 *				- DoGameMinutes: process messages store in each GKRegion MessageInbox. Store messages for ApplyChanges in each GKRegion MessageOutbox
 *			- Sort GKRegion MessageOutbox based on the destination GKRegion
 *		- Do ApplyChanges:
 *			- Each GKRegion check all other GKRegion MessageOutbox and get those messages and sort based on entities in the region
 *			- Entities in GKRegion:
 *				- Get & process messages store in each GKRegion MessageInbox: apply all changes.
 * UpdateVisual:
 *		- VisualManager will iterate all entities near camera and create or update them. The entities that are not updated after a while will be destroyed.
 *			
 */

class GKWorld;
class GKGameEngine;
class GKNetworkManager;

class GKGameManager : public GKManager
{
//	GK_SINGLETON(GKGameManager)

public:
	GKGameManager(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	virtual ~GKGameManager();

	void Initialize(GKWorld* World);

	//void Initialize();// int32 regionLandLevel, const FIntVector& regionCount);

	//void AdvanceGame(float DeltaTime);

	// Generate a unique ID for entities by combining this machine PlayerID and increasing number
	uint32 GenerateUniqueIDForLocalPlayer();

	bool HasAuthority() { return bHasAuthority; }

	inline void PostNetworkMessage(const GKMessageNetworkToClient& Message, uint32 TargetPlayerID = 0);
	inline void PostNetworkMessage(const GKMessageNetworkToClientCommand& Message, uint32 TargetPlayerID = 0);
	inline void PostNetworkMessage(const GKMessageNetworkToServer& Message);

	// To GameManager Inbox
	//virtual void PostGameMessage(const GKMessageNetworkToServer& iMessage);
	//virtual void PostGameMessage(const GKMessageNetworkToClient& iMessage);
	//virtual void PostGameMessage(const GKMessageNetworkToServerCommand& iMessage);
	//virtual void PostGameMessage(const GKMessageNetworkToClientCommand& iMessage);

	virtual void OnCreate() override;
	virtual void OnStartGame() override;
	virtual void OnUpdateGame(float DeltaTime) override;
	virtual void OnEndGame() override;
	virtual void OnDestroy() override;


	void AddCommand(const GKMessageNetworkToClientCommand& Command);
	inline GKNetworkManager* GetNetworkManager();

	virtual void PlayerJoin(GKNetworkPlayer* Player);
	virtual void PlayerDisconnected(GKNetworkPlayer* Player);

	uint32 GetGameTick() { return GameTime.GetGameTick(); }

	void AddDatabase(GKObject* database);
	FORCEINLINE const GKObject* GetDatabase(const FName& ID) const;
	void ParseDatabases();

protected:
	virtual void OnUpdateServer(float DeltaTime) {}
	virtual void OnUpdateClient(float DeltaTime) {}

	// Server will archive commands to be sent to late joining clients. 
	// The initial message might have no GameTick, the specified ServerGameTick is the exact GameTick the server executed the command.
	virtual void OnArchiveCommands(const GKMessageNetworkToClientCommand& Message, uint32 ServerGameTick) {}


	void UpdateWorld(int32 DeltagameTick);

	// Process InboxCommands up to the maximum GameTick
	void ProcessInboxCommands(uint32 MaxGameTick);

	virtual void OnCommand(const GKCommand& Command);

	GKEntity* FindEntity(uint32 EntityID);

	GKGameTimer GameTime;
	int32 DeltaGameTick;		// DeltaGameTick for current frame, 0 or more

	// On server side, put new command from player, initGame, or other external factors here to be executed on specified GameTick
	// or the next GameTick (if zero).
	// Server will set the GameTick (if zero, with the immediate next GameTick), execute the command, and send it to all clients.
	// Server will also remove the Command from InboxCommand and move it to ArchiveCommands, so when a new client joins,
	// server will send the Commands in ArchiveCommand one by one. (Client side doesn't have Archive).
	// On client side, it will receive the Commmand from server, put it into InboxCommands, and execute it at the exact specified
	// GameTick. 
	GKMessageNetworkToClientCommand::SortedArray InboxCommands;

private:
	bool bHasAuthority;				// True if it's the server

	// Most commands involve creating and then set up entity. This is a fast track so we don't need to search for Entity.
	GKEntity* LastEntityOnCommand = nullptr;

	uint32 LastCommandGameTickExecuted = 0;				// The last command gameTick executed

	GKObject::Map MapDatabases;		// The various databases

	GKWorld* World = nullptr;

	GKGameEngine* GameEngine;		// Ref back to GameEngine

	uint32 NextID = 0;				// To use for GenerateUniqueIDForLocalPlayer

	uint8 PlayerID = 0;				// PlayerID from 0 (not assigned yet), 1 (server), 2-255 (clients). See GKNetworkPlayer
	GKGameSettings GameSettings;


public:
	GK_GETCR(GKGameTimer, GameTime);
	GK_GET(int32, DeltaGameTick);
	GK_GET(GKWorld*, World);
	GK_GET(GKGameEngine*, GameEngine);
	GK_GETCR(GKGameSettings, GameSettings);
	GK_GET(uint8, PlayerID);					// Set in GKNetworkManager when assigning PlayerID
	GK_GET(uint32, LastCommandGameTickExecuted);
	
	//GK_GETSET(int32, ServerGameTick);
};
