// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Struct/GKUnion.h"

/**
 * The game will follow deterministic random. So server and client doesn't need to replicate but they will do the same thing.
 * The only thing that can make a difference is when there's a player input (build a room, move unit).
 * The playerInput will be executed on the server and will be sent to clients (that are running gameTick late
 * and will be executed exactly at the specified gameTick).
 * Each Command takes: 4 + 3 * 17 = 4 + 51 = 55 bytes.
 */

#define CMD1(CmdType, VarType1)																					\
	static GKCommand Create ## CmdType(VarType1 Value1)															\
		{ GKCommand Cmd; Cmd.Command = Type::CmdType; Cmd.Union1 = GKUnionStruct(Value1); return Cmd; }			\
	void Get ## CmdType(VarType1& Value1) const																	\
		{ check(Command == Type::CmdType); Union1.Get(Value1); }		

#define CMD2(CmdType, VarType1, VarType2)																		\
	static GKCommand Create ## CmdType(VarType1 Value1, VarType2 Value2)										\
		{ GKCommand Cmd; Cmd.Command = Type::CmdType; Cmd.Union1 = GKUnionStruct(Value1);						\
		  Cmd.Union2 = GKUnionStruct(Value2); return Cmd; }														\
	void Get ## CmdType(VarType1& Value1, VarType2& Value2) const												\
		{ check(Command == Type::CmdType); Union1.Get(Value1); Union2.Get(Value2); }		

#define CMD3(CmdType, VarType1, VarType2, VarType3)																\
	static GKCommand Create ## CmdType(VarType1 Value1, VarType2 Value2, VarType3 Value3)						\
		{ GKCommand Cmd; Cmd.Command = Type::CmdType; Cmd.Union1 = GKUnionStruct(Value1);						\
		  Cmd.Union2 = GKUnionStruct(Value2); Cmd.Union3 = GKUnionStruct(Value3); return Cmd; }					\
	void Get ## CmdType(VarType1& Value1, VarType2& Value2, VarType3& Value3) const 							\
		{ check(Command == Type::CmdType); Union1.Get(Value1); Union2.Get(Value2); Union3.Get(Value3); }		

struct GKCommand
{
public:
	enum Type
	{
		SpawnPawnEntity,
		SetVelocityAndAcceleration
	};
	GKCommand();
	~GKCommand();

	CMD3(SpawnPawnEntity, uint32, FVector, FRotator);						// EntityID, Position, Rotation
	CMD3(SetVelocityAndAcceleration, uint32, FVector, FVector);				// EntityID, Velocity, Acceleration

	void Serialize(FArchive& Ar);
private:
	uint32 Command;
	GKUnionStruct Union1, Union2, Union3;

public:
	GK_GET(uint32, Command);
};
