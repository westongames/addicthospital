// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Game/PlayerControlled/GKGameManagerPlayerControlled.h"
#include "Gimka/Tools/Macros/GKHeader.h"
/**
 * 
 */
class GKGameManagerPCClient : public GKGameManagerPlayerControlled
{
public:
	GKGameManagerPCClient(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	~GKGameManagerPCClient();

protected:
	virtual void OnUpdateClient(float DeltaTime) override;

private:
	uint32 PlayerID = 0;			// Server will give playerID that is not zero. If zero then need to wait server StartGame.
};
