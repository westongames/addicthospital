// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Game/PlayerControlled/GKGameManagerPCServer.h"

GKGameManagerPCServer::GKGameManagerPCServer(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority)
	: GKGameManagerPlayerControlled(ID, GameEngine, iHasAuthority)
{
}

GKGameManagerPCServer::~GKGameManagerPCServer()
{
}

void GKGameManagerPCServer::OnUpdateServer(float DeltaTime)
{
}
