// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Game/PlayerControlled/GKGameManagerPlayerControlled.h"

GKGameManagerPlayerControlled::GKGameManagerPlayerControlled(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority)
	: GKGameManager(ID, GameEngine, iHasAuthority)
{
}

GKGameManagerPlayerControlled::~GKGameManagerPlayerControlled()
{
}

//void GKGameManagerPlayerControlled::OnUpdateServer(float DeltaTime)
//{
//	//float fDeltaGameTick = GetGameSettings().CalculateGameTicks(DeltaTime);
//
//	//// Only Server can advance GameTick
//	//DeltaGameTick = GameTime.AdvanceGameTicks(fDeltaGameTick);
//	////GKLog1("DeltaGameTick = ", DeltaGameTick);
//
//	//// Do World->OnGameTick, from 0 to multiple
//	//UpdateWorld(DeltaGameTick);
//
//	//// Post network message to update all clients.
//	//// If there're playerInputs, add those, otherwise just a simple message to update GameTick.
//	//// Note: playerInput for this gameTick, 
//	//// 	- will be marked with the current gameTick, which will be processed next turn.
//	////	- will be sent in GKNetworkManager::Tick, which will happen after this GameEngine::Tick.
//	////	- on server side, it will receive immediately, and be placed into ArrayInboxMessage*,
//	////		which will be processed at beginning of next frame.
//	//if (DeltaGameTick > 0)
//	//{
//	//	GKMessageNetworkToAll Message(GKMessageNetwork::MessageType::ServerGameTick,
//	//		GameTime.GetGameTick(), true);
//	//	GetGameEngine()->GetNetworkManager()->PostMessage(Message);
//	//}
//}
//
//void GKGameManagerPlayerControlled::OnUpdateClient(float DeltaTime)
//{
//}