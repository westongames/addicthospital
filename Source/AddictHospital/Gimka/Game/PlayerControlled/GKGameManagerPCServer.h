// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Game/PlayerControlled/GKGameManagerPlayerControlled.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
class GKGameManagerPCServer : public GKGameManagerPlayerControlled
{
public:
	GKGameManagerPCServer(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	~GKGameManagerPCServer();

protected:
	virtual void OnUpdateServer(float DeltaTime) override;
};
