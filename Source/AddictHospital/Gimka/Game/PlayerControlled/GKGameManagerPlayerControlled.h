// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
class GKGameManagerPlayerControlled : public GKGameManager
{
public:
	GKGameManagerPlayerControlled(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority);
	~GKGameManagerPlayerControlled();

protected:
	virtual void OnUpdateServer(float DeltaTime) override {}
	virtual void OnUpdateClient(float DeltaTime) override {}
};
