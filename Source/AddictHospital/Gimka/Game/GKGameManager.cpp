// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Game/GKGameManager.h"
#include "Gimka/System/GKGameState.h"
#include "Gimka/System/Job/GKJobSystem3.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/Game/GKWorld.h"
#include "Gimka/Entity/Entity/GKPawnEntity.h"
#include "Gimka/System/GKGameEngine.h"
#include "Gimka/Network/GKNetworkManager.h"
#include "Gimka/Network/GKMessageNetworkToClientCommand.h"
#include "Gimka/Network/GKNetworkPlayer.h"


// Test
#include "Gimka/Goap/States/GKGoapState.h"
#include "Gimka/Goap/Actions/GKGoapAction2.h"
#include "Gimka/Goap/Planner/GKGoapPlanner2.h"
#include "Gimka/Goap/States/GKGoapSetStates.h"
#include "Gimka/Goap/Goals/GKGoapGoal.h"
#include "Gimka/Goap/States/DB/GKGoapStateBoolDB.h"
#include "Gimka/Goap/States/DB/GKGoapStateIntDB.h"
#include "Gimka/Goap/Goals/DB/GKGoapGoalDB.h"
#include "Gimka/Goap/Actions/DB/GKGoapActionDB.h"
#include "Gimka/Goap/Actions/GKGoapAction2.h"

//GKGameManager* GKGameManager::Instance;

GKGameManager::GKGameManager(uint32 ID, GKGameEngine* GameEngine, bool iHasAuthority)
	: GKManager(ID)
{
	NextID = 1;
	PlayerID = 0;
	//this->World = World;
	this->GameEngine = GameEngine;
	bHasAuthority = iHasAuthority;


}

GKGameManager::~GKGameManager()
{
	for (auto& db : MapDatabases)
	{
		delete db.Value;
	}
	MapDatabases.Empty();
}

void GKGameManager::Initialize(GKWorld* iWorld)
{
	check(this->World == nullptr);
	this->World = iWorld;

}


//void GKGameManager::Initialize()//int32 regionLandLevel, const FIntVector& regionCount)
//{
//	//GKJobSystem2::DestroyInstance();
//	//GKJobSystem2::GetInstance();
//
//	//GKWorld::DestroyInstance();
//}



void GKGameManager::OnUpdateGame(float DeltaTime)
{
	if (HasAuthority())
	{
		OnUpdateServer(DeltaTime);
	}
	else
	{
		OnUpdateClient(DeltaTime);
	}
}

uint32 GKGameManager::GenerateUniqueIDForLocalPlayer()
{
	return PlayerID * 10000000 + NextID++;
		//(((int32)PlayerID) << 24) + NextID++;
}

void GKGameManager::PostNetworkMessage(const GKMessageNetworkToClient& Message, uint32 TargetPlayerID)
{
	GameEngine->GetNetworkManager()->PostMessage(Message, TargetPlayerID);
}

void GKGameManager::PostNetworkMessage(const GKMessageNetworkToClientCommand& Message, uint32 TargetPlayerID)
{
	GameEngine->GetNetworkManager()->PostMessage(Message, TargetPlayerID);
}


void GKGameManager::PostNetworkMessage(const GKMessageNetworkToServer& Message)
{
	GameEngine->GetNetworkManager()->PostMessage(Message);
}

GKNetworkManager* GKGameManager::GetNetworkManager()
{ 
	return GameEngine->GetNetworkManager(); 
}

void GKGameManager::PlayerJoin(GKNetworkPlayer* Player)
{
	// Store the playerID if local
	if (Player->IsLocalPlayer())
	{
		if (HasAuthority())
		{
			// Server playerID is already set in OnCreate to 1
			check(PlayerID == 1);
			check(Player->GetPlayerID() == 1);
		}
		else
		{
			// Store the local playerID so we can use it to generate unique ID for entities that will be created by this player
			check(PlayerID == 0);
			check(Player->GetPlayerID() != 0);
			PlayerID = Player->GetPlayerID();
		}
	}

	GameEngine->PrintSystemLog(FString::Printf(TEXT("Player #%d has joined"), Player->GetPlayerID()));
}

void GKGameManager::PlayerDisconnected(GKNetworkPlayer* Player)
{
	if (Player->IsLocalPlayer())
	{
		PlayerID = 0;
	}
	GameEngine->PrintSystemLog(FString::Printf(TEXT("Player #%d has left"), Player->GetPlayerID()));
}




//void GKGameManager::PostGameMessage(const GKMessageNetworkToServer& iMessage)
//{
//	GKError("GKGameManager::PostMessage for Server not implemented");
//}
//
//void GKGameManager::PostGameMessage(const GKMessageNetworkToClient& iMessage)
//{
//	GKError("GKGameManager::PostMessage for Client not implemented");
//}
//
//void GKGameManager::PostGameMessage(const GKMessageNetworkToServerCommand& iMessage)
//{
//	GKError("GKGameManager::PostMessage for Server not implemented");
//}


//void GKGameManager::PostGameMessage(const GKMessageNetworkToClientCommand& iMessage)
//{
//	GKError("GKGameManager::PostMessage for Client not implemented");
//}

void GKGameManager::AddCommand(const GKMessageNetworkToClientCommand& Command)
{
	InboxCommands.Add(Command);
}

void GKGameManager::ProcessInboxCommands(uint32 MaxGameTick)
{
	int32 index = 0;
	uint32 GameTick = GameTime.GetGameTick();
	while (index < InboxCommands.Num())
	{
		const GKMessageNetworkToClientCommand& Message = InboxCommands[index];
		if (Message.GetGameTick() > MaxGameTick) break;

		// If this is a client, to make sure it doesn't miss any command, every command comes with a prerequisite.
		// Client needs to already done with prerequisite command. 
		// We won't even go here if this is not true because in GKGameManagerSimulationClient::OnUpdateClient it will
		// only calculate MaxGameTick up to Commands that have prerequisite fulfilled.
		check(HasAuthority() || LastCommandGameTickExecuted >= Message.GetPrerequisiteCommandGameTick());

		// If everything is good then we can execute this command and mark it done.
		++index;

		// The new command must have or at least equal gameTick to the last one executed
		check(LastCommandGameTickExecuted <= Message.GetGameTick());
		LastCommandGameTickExecuted = Message.GetGameTick();

		// Server will archive commands, to be sent to late joining clients.
		// Client will do nothing.
		OnArchiveCommands(Message, GameTick);			

		const TArray<GKCommand>& Commands = Message.GetArray();
		for (int32 i = 0, count = Commands.Num(); i < count; ++i)
		{
			OnCommand(Commands[i]);
			/*const GKCommand& Command = Commands[i];

			switch (Command.GetCommand())
			{
			case GKCommand::Type::SpawnPawnEntity:
				break;
			case GKCommand::Type::SetVelocityAndAcceleration:
				break;
			}*/
		}
	}

	// Remove from 0 up to index-1
	if (index > 0)
	{
		GKLog1("Remove Message ", index);
		InboxCommands.RemoveAt(0, index);
	}
}

GKEntity* GKGameManager::FindEntity(uint32 EntityID)
{
	if (LastEntityOnCommand != nullptr && LastEntityOnCommand->GetID() == EntityID)
	{
		return LastEntityOnCommand;
	}
	return World->FindEntity(EntityID);
}

void GKGameManager::AddDatabase(GKObject* database)
{
	check(!MapDatabases.Contains(database->GetID()));
	MapDatabases.Add(database->GetID(), database);
}

void GKGameManager::ParseDatabases()
{
	GKLog("ParseDatabases");

	// Parse all databases.
	// Some databases might need dependent databases to init first, so this will take a few rounds.
	int32 initLeft = MapDatabases.Num();;
	while (initLeft > 0)
	{
		for (auto db : MapDatabases)
		{
			if (db.Value->Init(this)) --initLeft;
		}
	}

	// Test
	GKGoapGoalDB* GoapGoalDB = (GKGoapGoalDB*)GetDatabase(GKGoapGoalDB::StaticID);
	GKGoapStateBoolDB* GoapStateBoolDB = (GKGoapStateBoolDB*)GetDatabase(GKGoapStateBoolDB::StaticID);
	GKGoapStateIntDB* GoapStateIntDB = (GKGoapStateIntDB*)GetDatabase(GKGoapStateIntDB::StaticID);
	GKGoapActionDB* GoapActionDB = (GKGoapActionDB*)GetDatabase(GKGoapActionDB::StaticID);

	//GKLog("GoapGoalDB");
	//GoapGoalDB->Log(GameManager);
	const GKGoapGoal& goal = (*GoapGoalDB)["killEnemy"];

	/*GKLog("Our Goal");
	GoapGoal.Log(this);

	GKLog("Our WorldStates");*/
	GKGoapArrayStates worldStates(GoapStateBoolDB->Num(), GoapStateIntDB->Num());
	//worldStates.Add(GoapStateBoolDB->Find("hasMoney"));
	//worldStates.Log(this);

	TArray<const GKGoapAction2*> arrayActions;
	//const GKGoapAction2& action = (*GoapActionDB)["buyWeapon"];
	arrayActions.Add(&(*GoapActionDB)["killEnemy"]);
	arrayActions.Add(&(*GoapActionDB)["buyWeapon"]);
	arrayActions.Add(&(*GoapActionDB)["getMoney"]);
	arrayActions.Add(&(*GoapActionDB)["stealWeapon"]);
	arrayActions.Add(&(*GoapActionDB)["approachEnemy"]);
	//arrayActions.Add(&(*GoapActionDB)["stealWeapon"]);
	//arrayActions.Add(&(*GoapActionDB)["stealWeapon"]);
	//arrayActions.Add(&(*GoapActionDB)["stealWeapon"]);
	//arrayActions.Add(&(*GoapActionDB)["stealWeapon"]);
	//GKLog("Our actions");
	//for (int i = 0; i < arrayActions.Num(); ++i) GKLog1("  Action", arrayActions[i]->GetID().ToString());

	//const GKGoapSetStates& goalStates = GoapGoal.GetStates();
	//int32 distance = worldStates.DistanceTo(goal.GetStates());
	//GKLog1("Distance to goal ", distance);

	GKGoapPlanner2 planner(this);
	TArray<const GKGoapAction2*> queueActions;
	planner.Plan(goal.GetStates(), worldStates, arrayActions, queueActions);
	//planner.Plan(GoapGoal, worldStates, )

}


const GKObject* GKGameManager::GetDatabase(const FName& iID) const
{
	return MapDatabases[iID];
}


void GKGameManager::OnCommand(const GKCommand& Command)
{
	switch (Command.GetCommand())
	{
	case GKCommand::Type::SpawnPawnEntity:
	{
		GKLog2("Command SpawnPawnEntity at GameTick, PlayerID", GameTime.GetGameTick(), (uint32)PlayerID);
		uint32 EntityID;
		FVector Position;
		FRotator Rotation;
		Command.GetSpawnPawnEntity(EntityID, Position, Rotation);
		LastEntityOnCommand = World->SpawnEntity(GKEntity::Type::PawnEntity, EntityID, Position, Rotation);
		break;
	}
	case GKCommand::Type::SetVelocityAndAcceleration:
	{
		uint32 EntityID;
		FVector Velocity;
		FVector Acceleration;
		Command.GetSetVelocityAndAcceleration(EntityID, Velocity, Acceleration);
		GKLog4("Command SetVelAndAccell at PlayerID, X, Y, Z", (float)PlayerID, Velocity.X, Velocity.Y, Velocity.Z);
		// A bit optimization, usually it will refers to the previous entity. 
		// If not, search Entity throughout the world. Might want to use jobs.
		GKMovingEntity* MovingEntity = (GKMovingEntity*)FindEntity(EntityID);
		if (MovingEntity == nullptr)
		{
			GKLog1("SetVelocityAndAcceleration, can't find entity", EntityID);
			return;
		}
		MovingEntity->SetVelocity(Velocity);
		MovingEntity->SetAcceleration(Acceleration);
		break;
	}
	}
}

void GKGameManager::OnCreate()
{
	if (HasAuthority())
	{
		PlayerID = 1;
	}
	ParseDatabases();
}

void GKGameManager::OnStartGame()
{
	// Create a pawn for test
	if (HasAuthority())
	{
		uint32 EntityID = GenerateUniqueIDForLocalPlayer();
		GKLog1("Entity ID = ", EntityID);
		GKMessageNetworkToClientCommand Message(GKMessageNetworkToClientCommand::MessageType::Command, 1);
		Message.Add(GKCommand::CreateSpawnPawnEntity(EntityID, FVector(0, 920.f, 260.f), FRotator::ZeroRotator));
		Message.Add(GKCommand::CreateSetVelocityAndAcceleration(EntityID, FVector(0.33f, 0.f, 0.f), FVector::ZeroVector));

		AddCommand(Message);
		
		//GKCommand Cmd = GKCommand::CreateSpawnPawnEntity(EntityID, FVector(0, 920.f, 260.f), FRotator::ZeroRotator);
		////uint32 EntityID;
		//FVector Position;
		//FRotator Rotation;
		//Cmd.GetSpawnPawnEntity(EntityID, Position, Rotation);
		//UE_LOG(LogTemp, Warning, TEXT("Position %s, Rotation %s"),
		//	*Position.ToString(), *Rotation.ToString());

		//GKUnionStruct Union(FVector(0, 920.f, 260.f));
		//GKLog1("X = ", Union.Union.Float[0]);
		//GKLog1("Y = ", Union.Union.Float[1]);
		//GKLog1("Z = ", Union.Union.Float[2]);

		//Union.Get(Position);
		//UE_LOG(LogTemp, Warning, TEXT("Position %s, Rotation %s"),
		//	*Position.ToString(), *Rotation.ToString());


		//GKEntityManager* EntityManager = World->GetEntityManager(0);// 1);

		//GKPawnEntity * Pawn = new GKPawnEntity(EntityManager, GenerateUniqueIDForLocalPlayer());

		//Pawn->SetVelocity(FVector::ZeroVector);
		//Pawn->SetPosition(FVector(0, 520.f, 260.f));	// FVector::ZeroVector);
		//Pawn->SetVelocity(FVector(0.33f, 0.f, 0.f));
	}
}

void GKGameManager::OnEndGame()
{
	GKLog("GKGameManager::OnEndGame");
	SAFE_DELETE(World);
}

void GKGameManager::OnDestroy()
{
	GKLog("GKGameManager::OnDestroy");
}

void GKGameManager::UpdateWorld(int32 DeltagameTick)
{
	// Do World->OnGameTick, from 0 to multiple
	for (int gameTick = 0; gameTick < DeltaGameTick; ++gameTick)
	{
		GameTime.Advance();
		ProcessInboxCommands(GameTime.GetGameTick());
		World->OnGameTick();
	}

	// Called OnPostFrame, once per frame
	World->OnPostFrame();
}

