// Fill out your copyright notice in the Description page of Project Settings.


#include "GKWorld.h"
//#include "Gimka/Game/GKRegion.h"
#include "Gimka/Tools/Macros/GKSource.h"
//#include "Gimka/Math/GKIntVector.h"
#include "Gimka/System/Job/GKJobSystem3.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Entity/Manager/GKEntityManager.h"

//GKWorld* GKWorld::Instance;

GKWorld::GKWorld(GKGameManager* iGameManager)
{
	this->GameManager = iGameManager;
}


//GKWorld::GKWorld()
//{
//	GKRegion* Region = new GKRegion(FIntVector(0, 0, 0));
//	Regions.Add(Region->GetHashKey(), Region);
//
	//Region = new GKRegion(FIntVector(16, 0, 0));
	//Regions.Add(Region->GetHashKey(), Region);

	/*Regions.Add(32, new GKRegion(FIntVector()));
	Regions.RemoveAt(1);
	Regions.Empty();*/

	//TickCountPerFrame = 1;

	// Note: this is required because it's GKWorldRegion that will be instanced.
	// And GKWorld::Instance needs to be updated too otherwise it will create another class.
//	Instance = this;

	// For debug
//PSS	ID = GKGameManager::GetInstance()->GenerateUniqueIDForLocalPlayer();
//}

//GKWorld::~GKWorld()
//{
//}

//void GKWorld::SetArrayEntityManagers(TArray<GKEntityManager*> arrayEntityManagers)
//{
//	// TODO: If not null we might need to cleanup the previous one?
//	checkSlow(ArrayEntityManagers.IsEmpty());
//	ArrayEntityManagers = arrayEntityManagers;
//
//	GKJobSystem2* jobSystem = GKJobSystem2::GetInstance();
//	for (GKEntityManager* manager : ArrayEntityManagers)
//	{
//		jobSystem->RegisterForEvent(manager, GKJobSystem2::Event::GameTick);
//		jobSystem->RegisterForEvent(manager, GKJobSystem2::Event::PostFrame);
//	}
//}


//void GKWorld::Initialize(/*int32 iRegionLandLevel, */
//	GKEntityManager** ArrayEntityManagers, const FIntVector& iRegionCount)
//{
//	//check(arrayRegions == nullptr);
//	GKIntVector regionSize(32, 32, 8);
//
//	GKRegion::InitializeStatic(regionSize);
//
//	RegionCount = iRegionCount;
//	this->RegionLandLevel = iRegionLandLevel;
//	ArrayRegions = new GKRegion * [RegionCount.X * RegionCount.Y * RegionCount.Z];
//
//	GKJobSystem2* jobSystem = GKJobSystem2::GetInstance();
//	GK_ITERATE_XYZ(int32, RegionCount.X, RegionCount.Y, RegionCount.Z)
//		GKRegion* region = ArrayRegions[index] = 
//			new GKRegion(
//				GKIntVector(x, y, z));
//		jobSystem->RegisterForEvent(region, GKJobSystem2::Event::GameTick);
//		jobSystem->RegisterForEvent(region, GKJobSystem2::Event::PostFrame);
//	GK_ITERATE_XYZ_END
//}

//void GKEntityManager* GKWorld::CreateEntityManager()
//{
//	return new GKEntityManager();
//}

GKGameEngine* GKWorld::GetGameEngine()
{
	check(GameManager);
	return GameManager->GetGameEngine();
}

void GKWorld::AddEntityManager(GKEntityManager* manager)
{
	ArrayEntityManagers.Add(manager);
	ArrayJobs.Add(manager);
}

// Note: this is logic only. Visual is in GKVisualManager.
// 0 or more gameTick per frame depending on gameSpeed.
void GKWorld::OnGameTick()
{
	GKLogger::SetGameTick(GameManager->GetGameTick());
	GKJobSystem3* JobSystem = GetGameEngine()->GetJobSystem();

	// GKRegion::JobGameTick
	//	- ProcessInbox: GKRegion.MessageInbox (because entity might not tick this frame)
	//	- GKEntity::JobGameTick (only once, including those for GameMinute, GameHour, etc)
	//		- CreateEntity: PostMessageRegion(CreateEntity, EntityInfo, To GKRegion)
	//		- UpdateEntity: PostMessageChanges to self
	//		- Send Message to other entities: PostMessageGlobal to other entities
	JobSystem->ExecuteEventGameTick(ArrayJobs);

	// GKRegion::JobApplyChanges
	//	- ProcessApplyChanges
	//		- Move to other region: PostMessageGlobal(MoveEntity, Self, TargetRegion, To GKRegion)
	//		- If necessary, mark visual dirty, then add to GKRegion::ArrayVisualDirty
	//		- If HP less than zero, destroy self, PostMessageRegion(DestroySelfIfHPLessZero, Self, To GKRegion)
	//		- CreateEntity: PostMessageRegion(CreateEntity, EntityInfo, To GKRegion)
	//		- Send Message to other entities: PostMessageGlobal or if same region, PostMessageInbox
	// - ProcessMessageRegion
	//		- MessageCreateEntity: create it. If it has actor and region is visible, MarkVisualDirty, add to GKRegion::ArrayVisualDirty
	//		- MessageDestroySelf: delete it. If it has actor, remove it from GKRegion:ArrayVisualDirty then PostMessageGlobal(DeleteActor, actorIndex, to GKVisualManager)
	//				Need to delete all messages for this entity
	JobSystem->ExecuteEventApplyChanges(ArrayJobs);

	// TODO: GKRegion::PostGameTick (non job)
	//	- ProcessMessageRegionExternal:
	//		- MessageMoveEntityToOtherRegion: delete from this region array & arrayVisualDirty and add to other region.
	//			- Create actor if needed if new region is visible: add to New Region ArrayVisualDirty
	//			- Delete actor if needed if new region is invisible, remove it from ArrayVisualDirty: add messageGlobal directly to GKVisualManager
	//	- Process MessageOutbox:
	//		- MessageForOtherEntity: put into other entity inbox or region's other entity inbox
	//		- Message DeleteActor for GKVisualManager
	//GKLog2("entityManager = ", (int32)ID, ArrayEntityManagers.Num());
	for (GKEntityManager* Manager : ArrayEntityManagers)
	{
		Manager->OnPostGameTick();
	}
}

// Called 1 time per frame
void GKWorld::OnPostFrame()
{
	GKJobSystem3* JobSystem = GetGameEngine()->GetJobSystem();
	// Todo: GKRegion::JobPostFrame
	//		- Calculate Region Visibility
	//			- Region becomes visible: set visible to all entities, MarkVisualDirty
	//			- Region becomes close: nothing
	//			- Region becomes invisible: set non visible to all entities, clear arrayVisualDirty. PostMessageGlobal(DeleteActor)
	JobSystem->ExecuteEventPostFrame(ArrayJobs);


	// Todo: GKRegion::EndOfFrame (non job)
	//		- ProcessMessageGlobal (idem as above). To deliver MessageDeleteActor to GKVisualManager
	for (GKEntityManager* Manager : ArrayEntityManagers)
	{
		Manager->OnEndOfFrame();
	}
}

GKEntity* GKWorld::FindEntity(uint32 EntityID)
{
	for (GKEntityManager* Manager : ArrayEntityManagers)
	{
		GKEntity* Entity = Manager->FindEntity(EntityID);
		if (Entity != nullptr) return Entity;
	}
	return nullptr;
}


//GKRegion* GKWorld::CreateRegion(const FIntVector& RegionPos)
//{
//	return new GKRegion(RegionPos);
//}

//GKRegion* GKWorld::GetRegion(const FIntVector& RegionPos)
//{
//	uint32 HashKey = GKRegion::RegionPosToHashKey(RegionPos);
//	GKRegion** Region = MapRegions.Find(HashKey);
//	return Region != nullptr ? *Region : nullptr;
//}

//bool firstTime = true;
//void GKWorld::Update(float DeltaTime)
//{
//	if (firstTime)
//	{
//		firstTime = false;
//		Regions.Log();
//	}
//
//}