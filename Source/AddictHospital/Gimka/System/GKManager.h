// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Entity/Identity/GKIdentity.h"

/**
 * 
 */
class GKManager : public GKIdentity
{
public:
	friend class GKGameEngine;

	GKManager(uint32 ID);
	virtual ~GKManager();

	virtual void OnCreate() {}

	virtual void OnStartGame() {}

	virtual void OnUpdateGame(float DeltaTime) {}

	virtual void OnEndGame() {}

	virtual void OnDestroy() {}
};
