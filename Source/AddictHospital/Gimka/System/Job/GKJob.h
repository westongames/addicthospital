// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/System/Job/GKJobSystem.h"
#include "Gimka/System/Job/GKJobSystem2.h"

#include <bitset>

/**
 * The GKJob will receive events from GKJobSystem. Each is in its own thread.
 * GKRegion, GKPathSystem, GKMessageSystem will all implement GKJob
 */
class GKJob
{
	// Allow access to OnGameMinute, etc.
	//friend class GKJobSystem;
	friend class GKJobSystem2;			// Allow GKJobSystem2 access to the bitEventRegistered

public:
	GKJob();
	virtual ~GKJob();

	// Both JobGameTick and ApplyChanges can be called 0 to several times within Actor::Tick per frame depending on gameSpeed
	virtual void OnJobGameTick(const GKJobSystem2::JobArgs& jobArgs) = 0;
	virtual void OnJobApplyChanges(const GKJobSystem2::JobArgs& jobArgs) = 0;

	// Called once per frame at after the multiple of GameTick & ApplyChanges.
	// To update region visibility and entities contained within it.
	virtual void OnJobPostFrame(const GKJobSystem2::JobArgs& jobArgs) = 0;

	// Called at the end of the Actor::Tick. Always called even if GameTick & ApplyChanges are not.
	// Will be used to update region/entities visibility, delete entities.
	//virtual void OnJobPostUpdate(const GKJobSystem2::JobArgs& jobArgs) = 0;

	//virtual void OnJobUpdateVisual(const GKJobSystem2::JobArgs& jobArgs) = 0;
	//virtual void OnJobPostProcess(const GKJobSystem2::JobArgs& jobArgs) = 0;

protected:
	/*virtual void OnJobGameMinute(int jobIndex) = 0;
	virtual void OnJobApplyChanges(int jobIndex) = 0;
	virtual void OnJobUpdateVisual(int jobIndex) = 0;
	virtual void OnJobPostProcess(int jobIndex) = 0;*/

	void RegisterForEvent(GKJobSystem2::Event aEvent);
	void UnregisterForEvent(GKJobSystem2::Event aEvent);

private:
	std::bitset<8> bitEventRegistered;		// See GKJobSystem::Event. Track multiple registered event in bit. Updated by GKJobSystem directly.
};
