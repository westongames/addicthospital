// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HAL/Runnable.h"

#include "HAL/RunnableThread.h"

class FRunnableThread;
class GKUEJobSystem;


class GKUEJob : public FRunnable
{
public:
	GKUEJob(int32 Calculation, GKUEJobSystem* System);

	bool bStopThread;

protected:

	/**
	 * Initializes the runnable object.
	 *
	 * This method is called in the context of the thread object that aggregates this, not the
	 * thread that passes this runnable to a new thread.
	 *
	 * @return True if initialization was successful, false otherwise
	 * @see Run, Stop, Exit
	 */
	virtual bool Init() override;

	/**
	 * Runs the runnable object.
	 *
	 * This is where all per object thread work is done. This is only called if the initialization was successful.
	 *
	 * @return The exit code of the runnable object
	 * @see Init, Stop, Exit
	 */
	virtual uint32 Run() override;

	/**
	 * Stops the runnable object.
	 *
	 * This is called if a thread is requested to terminate early.
	 * @see Init, Run, Exit
	 */
	virtual void Stop() override;

	/**
	 * Exits the runnable object.
	 *
	 * Called in the context of the aggregating thread to perform any cleanup.
	 * @see Init, Run, Stop
	 */
	//virtual void Exit() override;

private:
	int32 Calculation;
	int32 CalcCount;
	int32 CurrentCalculation;
	GKUEJobSystem* System;
};

/**
 * 
 */
class GKUEJobSystem
{
public:
	GKUEJobSystem();
	~GKUEJobSystem();

	int32 ProcessThread();

	void Initialize(uint32 workerThreads);
	void Initialize();

	void OnDestroy();
	TQueue<int32> ThreadQueue;

private:
	GKUEJob* Job = nullptr;
	FRunnableThread* CurrentRunningThread = nullptr;

	int32 ProcessedThread;
	int32 CurrentCalc;
};
