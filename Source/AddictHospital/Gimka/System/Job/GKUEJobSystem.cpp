// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/System/Job/GKUEJobSystem.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKUEJobSystem::GKUEJobSystem()
{
}

GKUEJobSystem::~GKUEJobSystem()
{
}

int32 GKUEJobSystem::ProcessThread()
{
	if (!ThreadQueue.IsEmpty() && ThreadQueue.Dequeue(ProcessedThread))
	{
		GKLog1("Processed Thread", ProcessedThread);
	}
	return int32();
}

void GKUEJobSystem::Initialize(uint32 workerThreads)
{
}

void GKUEJobSystem::Initialize()
{
	Job = new GKUEJob(222, this);
	CurrentRunningThread = FRunnableThread::Create(Job, TEXT("Thread #1"));
}

void GKUEJobSystem::OnDestroy()
{
	if (CurrentRunningThread && Job)
	{
		// Need to suspend first before can set bStopThread
		CurrentRunningThread->Suspend(true);
		Job->bStopThread = true;
		CurrentRunningThread->Suspend(false);
		CurrentRunningThread->Kill(false);
		CurrentRunningThread->WaitForCompletion();
		delete Job;
		Job = nullptr;
	}
}

GKUEJob::GKUEJob(int32 iCalculation, GKUEJobSystem* iSystem)
{
	Calculation = iCalculation;
	System = iSystem;
}

bool GKUEJob::Init()
{
	bStopThread = false;
	CalcCount = 0;
	return true;
}

uint32 GKUEJob::Run()
{
	while (!bStopThread)
	{
		if (CalcCount < Calculation)
		{
			CurrentCalculation += FMath::RandRange(20, 400);
			CurrentCalculation *= FMath::RandRange(2, 500);
			CurrentCalculation -= FMath::RandRange(10, 500);

			System->ThreadQueue.Enqueue(CurrentCalculation);
			++CalcCount;
		}
		else
		{
			bStopThread = true;
		}
	}
	return 0;
}

void GKUEJob::Stop()
{
}
