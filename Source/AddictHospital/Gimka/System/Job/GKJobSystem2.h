// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <atomic>
#include <functional>

#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * https://wickedengine.net/2018/11/24/simple-job-system-using-standard-c/
 * This implemention is following new version: https://github.com/turanszkij/WickedEngine/blob/master/WickedEngine/wiJobSystem.cpp
 * Todo: GKJobSystem should track processing time for every registered job and sort them based on longest first to optimize overall execution time.
 * We don't add the Dispatch and Execute as we don't use them.
 */

class GKJob;
struct GKJobConfig;

class GKJobSystem2
{
	GK_SINGLETON(GKJobSystem2);

public:
	enum Event { GameTick, PostFrame, Count };

	struct JobArgs
	{
		//GKJobSystem2::Event eventType;
		uint32_t jobIndex;		// job index relative to dispatch (like SV_DispatchThreadID in HLSL)
		uint32_t groupID;		// group index relative to dispatch (like SV_GroupID in HLSL)
		//uint32_t groupIndex;	// job index relative to group (like SV_GroupIndex in HLSL)
		//bool isFirstJobInGroup;	// is the current job the first one in the group?
		//bool isLastJobInGroup;	// is the current job the last one in the group?
		//void* sharedmemory;		// stack memory shared within the current group (jobs within a group execute serially)
		uint32_t groupJobOffset;
		uint32_t groupJobEnd;

	public:
		int32_t GroupIndex() { return jobIndex - groupJobOffset; }
		bool IsFirstJobInGroup() { return jobIndex == groupJobOffset; }
		bool IsLastJobInGroup() { return jobIndex == groupJobEnd - 1; }
	};

	// Defines a state of execution, can be waited on
	struct context
	{
		std::atomic<uint32_t> counter{ 0 };
	};

	void Initialize(uint32 workerThreads);

	static uint32_t CalcCoresCount();

	uint32_t GetThreadCount();

	// Check if any threads are working currently or not
	bool IsBusy(const context& ctx);

	// Wait until all threads become idle
	void Wait(const context& ctx);

	void RegisterForEvent(GKJob* job, Event aEvent);
	void UnregisterForEvent(GKJob* job, Event aEvent);
	void UnregisterForAllEvents(GKJob* job);

	void GetStats(int32& jobCount, int32& groupCount, int32& groupSize, int32& currentLabel);

	// EventGameTick, EventApplyChanges: for all jobs registered for GameTick
	void ExecuteEventGameTick();
	void ExecuteEventApplyChanges();

	// EventPostUpdate: for all jobs registered to PostUpdate
	void ExecuteEventPostFrame();

	void LogStats();

protected:
	// Divide a task onto multiple jobs and execute in parallel.
	//	jobCount	: how many jobs to generate for this task.
	//	groupSize	: how many jobs to execute per thread. Jobs inside a group execute serially. It might be worth to increase for small jobs
	//	task		: receives a wiJobArgs as parameter
	void ExecuteEvent(context& ctx, const std::function<void(GKJobConfig*)>& func, Event eventType);

	// Returns the amount of job groups that will be created for a set number of jobs and group size
	//uint32_t DispatchGroupCount(uint32_t jobCount, uint32_t groupSize);

private:
	// Array of jobs for each events.
	  // The Event::GameMinute includes for both OnGameMinute and OnApplyChanges
	TArray<GKJob*> ArrayJobs[Event::Count];

	// Number of threads allowed from 0 to NumCores(). If this is set to zero, then no more multithreading.
	uint32 numThreadsAllowed;

	context ctxGameTick;
};
