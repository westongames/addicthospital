// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <functional>

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Containers/ThreadSafe/GKThreadSafeRingBuffer.h"

// A Dispatched job will receive this as function argument:
struct GKJobDispatchArgs
{
	uint32 JobIndex;
	uint32 GroupIndex;
};

class GKJob;

/**
 * https://wickedengine.net/2018/11/24/simple-job-system-using-standard-c/
 * Todo: might want to integrate the updated version in Wicked Engine as referred in article: https://github.com/turanszkij/WickedEngine/blob/master/WickedEngine/wiJobSystem.cpp
 * Todo: GKJobSystem should track processing time for every registered job and sort them based on longest first to optimize overall execution time.
 */
//class GKJobSystem
//{
//    GK_SINGLETON(GKJobSystem);
//
//public:
//    enum Event { GameMinute, UpdateVisual, PostProcess, Count };
//
//    static uint32 NumCores();
//
//    // Create the internal resources such as worker threads, etc. Call it once when initializing the application.
//    void Initialize(uint32 WorkerThreads);
//
//    // Add a job to execute asynchronously. Any idle thread will execute this job.
//    void Execute(const std::function<void()>& job);
//
//    // Divide a job onto multiple jobs and execute in parallel.
//    //  jobCount    : how many jobs to generate for this task.
//    //  groupSize   : how many jobs to execute per thread. Jobs inside a group execute serially. It might be worth to increase for small jobs
//    //  func        : receives a JobDispatchArgs as parameter
//    void Dispatch(uint32_t jobCount, uint32_t groupSize, const std::function<void(const GKJobDispatchArgs&)>& job);
//
//    // Check if any threads are working currently or not
//    bool IsBusy();
//
//    // Wait until all threads become idle
//    void Wait();
//
//    // Event: do both DoGameMinute & DoApplyChanges. Multiple times if needed.
//    void DoTick(float DeltaTime);
//
//    // Event: Update Visual. Actors that have visual will create/destroy/update it here (animation, shape, transform, etc).
//    void DoUpdateVisual();
//
//    // Event: Long Post Process. This will not wait until next frame DoGameTick.
//    // Visual manager will use this to create/destroy/update shape, animation. I.E. Creating voxel data and render it, etc.
//    // Pathfinder manager will use this to calculate path request
//    void DoPostProcess();
//
//    void RegisterForEvent(GKJob* job, Event aEvent);
//    void UnregisterForEvent(GKJob* job, Event aEvent);
//    void UnregisterForAllEvents(GKJob* job);
//
//    void GetStats(int32& jobCount, int32& groupCount, int32& groupSize, int32& currentLabel);
//
//protected:
//    // Event: Update per frame. Tick & ApplyChanges can be called multiple times per frame, depending on the gamespeed.
//    // The GKRegion will manage further which entities need to be called every minute, hour, or day.
//    void DoGameMinute();                // Call all GKJob::OnGameMinute         
//    void DoApplyChanges();              // Call all GKJob::OnApplyChanges
//
//private:
//    // This little helper function will not let the system to be deadlocked while the main thread is waiting for something
//    void Poll();
//
//    int value[Event::Count];
//
//    // Array of jobs for each events.
//    // The Event::GameMinute includes for both OnGameMinute and OnApplyChanges
//    TArray<GKJob*> ArrayJobs[Event::Count];
// 
//    // Number of threads allowed from 0 to NumCores(). If this is set to zero, then no more multithreading.
//    uint32 numThreadsAllowed;       
//
//    //uint32 numThreads = 0;    // number of worker threads, it will be initialized in the Initialize() function
//    //GKThreadSafeRingBuffer<std::function<void()>, 256> jobPool;    // a thread safe queue to put pending jobs onto the end (with a capacity of 256 jobs). A worker thread can grab a job from the beginning
//    //std::condition_variable wakeCondition;    // used in conjunction with the wakeMutex below. Worker threads just sleep when there is no job, and the main thread can wake them up
//    //std::mutex wakeMutex;    // used in conjunction with the wakeCondition above
//    //uint64_t currentLabel = 0;    // tracks the state of execution of the main thread
//    //std::atomic<uint64_t> finishedLabel;    // track the state of execution across background worker threads
//
//};
//
