// Fill out your copyright notice in the Description page of Project Settings.


#include "GKJob.h"
#include "Gimka/System/Job/GKJobSystem2.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKJob::GKJob()
{
}

GKJob::~GKJob()
{
	//GKLog("destructor");
	GKJobSystem2* jobSystem;
	if (bitEventRegistered.any() && GKJobSystem2::TryGetInstance(jobSystem))
	{
		// Unregister if GKJobSystem is not yet destroyed
		jobSystem->UnregisterForAllEvents(this);
	}
}

void GKJob::RegisterForEvent(GKJobSystem2::Event aEvent)
{
	GKJobSystem2::GetInstance()->RegisterForEvent(this, aEvent);
}

void GKJob::UnregisterForEvent(GKJobSystem2::Event aEvent)
{
	GKJobSystem2::GetInstance()->UnregisterForEvent(this, aEvent);
}
