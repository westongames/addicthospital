// Fill out your copyright notice in the Description page of Project Settings.


#include "GKJobSystem.h"

#include <algorithm>    // std::max
#include <atomic>    // to use std::atomic<uint64_t>
#include <thread>    // to use std::thread
#include <condition_variable>    // to use std::condition_variable

#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/System/Job/GKJob.h"

//GKJobSystem* GKJobSystem::Instance;
//
//uint32 numThreads = 0;    // number of worker threads, it will be initialized in the Initialize() function
//GKThreadSafeRingBuffer<std::function<void()>, 256> jobPool;    // a thread safe queue to put pending jobs onto the end (with a capacity of 256 jobs). A worker thread can grab a job from the beginning
//std::condition_variable wakeCondition;    // used in conjunction with the wakeMutex below. Worker threads just sleep when there is no job, and the main thread can wake them up
//std::mutex wakeMutex;    // used in conjunction with the wakeCondition above
//uint64_t currentLabel = 0;    // tracks the state of execution of the main thread
//std::atomic<uint64_t> finishedLabel;    // track the state of execution across background worker threads
//
//GKJobSystem::GKJobSystem()
//{
//    uint32 numberCores = NumCores();
//    Initialize(numberCores);
//}
//
//GKJobSystem::~GKJobSystem()
//{
//    Instance = nullptr;
//}
//
//uint32 GKJobSystem::NumCores()
//{
//    // Retrieve the number of hardware threads in this system:
//    return std::thread::hardware_concurrency();
//}
//
//void GKJobSystem::Initialize(uint32 WorkerThreads)
//{
//    // Initialize the worker execution state to 0:
//    finishedLabel.store(0);
//
//    // Calculate the actual number of worker threads we want:
//    numThreads = std::max(1u, WorkerThreads);
//
//    GKLog1("GKJobSystem Worker Threads = ", WorkerThreads);
//
//    // Create all our worker threads while immediately starting them:
//    for (uint32_t threadID = 0; threadID < numThreads; ++threadID)
//    {
//        std::thread worker([] {
//
//            std::function<void()> job; // the current job for the thread, it's empty at start.
//
//            // This is the infinite loop that a worker thread will do 
//            while (true)
//            {
//                if (jobPool.pop_front(job)) // try to grab a job from the jobPool queue
//                {
//                    // It found a job, execute it:
//                    job(); // execute job
//                    finishedLabel.fetch_add(1); // update worker label state
//                }
//                else
//                {
//                    // no job, put thread to sleep
//                    std::unique_lock<std::mutex> lock(wakeMutex);
//                    wakeCondition.wait(lock);
//                }
//            }
//
//        });
//
//        // *****Here we could do platform specific thread setup...
//
//        worker.detach(); // forget about this thread, let it do it's job in the infinite loop that we created above
//    }
//}
//
//void GKJobSystem::Execute(const std::function<void()>& job)
//{
//    // The main thread label state is updated:
//    currentLabel += 1;
//
//    // Try to push a new job until it is pushed successfully:
//    while (!jobPool.push_back(job)) { Poll(); }
//
//    wakeCondition.notify_one(); // wake one thread
//}
//
//void GKJobSystem::Dispatch(uint32_t jobCount, uint32_t groupSize, const std::function<void(const GKJobDispatchArgs&)>& job)
//{
//    if (jobCount == 0 || groupSize == 0)
//    {
//        return;
//    }
//
//    // Calculate the amount of job groups to dispatch (overestimate, or "ceil"):
//    const uint32_t groupCount = (jobCount + groupSize - 1) / groupSize;
//
//    // The main thread label state is updated:
//    currentLabel += groupCount;
//
//    for (uint32_t groupIndex = 0; groupIndex < groupCount; ++groupIndex)
//    {
//        // For each group, generate one real job:
//        // Note: was auto& but cause error C4239
//        auto jobGroup = [jobCount, groupSize, job, groupIndex]() {
//
//            // Calculate the current group's offset into the jobs:
//            const uint32_t groupJobOffset = groupIndex * groupSize;
//            const uint32_t groupJobEnd = std::min(groupJobOffset + groupSize, jobCount);
//
//            GKJobDispatchArgs args;
//            args.GroupIndex = groupIndex;
//
//            // Inside the group, loop through all job indices and execute job for each index:
//            for (uint32_t i = groupJobOffset; i < groupJobEnd; ++i)
//            {
//                args.JobIndex = i;
//                job(args);
//            }
//        };
//
//        // Try to push a new job until it is pushed successfully:
//        while (!jobPool.push_back(jobGroup)) { Poll(); }
//
//        wakeCondition.notify_one(); // wake one thread
//    }
//}
//
//bool GKJobSystem::IsBusy()
//{
//    // Whenever the main thread label is not reached by the workers, it indicates that some worker is still alive
//    return finishedLabel.load() < currentLabel;
//}
//
//void GKJobSystem::Wait()
//{
//    while (IsBusy()) { Poll(); }
//}
//
//void GKJobSystem::DoTick(float DeltaTime)
//{
//    DoGameMinute();
//    Wait();
//    DoApplyChanges();
//    Wait();
//}
//
//void GKJobSystem::DoGameMinute()
//{
//    TArray<class GKJob*>* jobs = &ArrayJobs[Event::GameMinute];
//    uint32_t jobCount = jobs->Num();
//    if (jobCount == 0) return;
//
//    // Group count will equals to numThreads for now
//    const uint32_t groupCount = std::min(numThreads, jobCount);
//
//    // Calculate the group size (amount of jobs per thread). Overestimate.
//    // i.e. job = 5, groupCount = 2, groupSize = (5+2-1)/2 = 3. job = 4 => (4+2-1)/2 = 2. job = 6 => (6+2-1)/2 = 3
//    const uint32_t groupSize = (jobCount + groupCount - 1) / groupCount;        
//
//    for (uint32 i = 0; i < jobCount; ++i) GKLog1("Job ", (*jobs)[i]);
//
//    //GKLog4("DoGameMinute jobCount, groupCount, groupSize", jobCount, groupCount, groupSize, (int32)currentLabel);// , groupSize);// (int32)currentLabel);
//    //UE_LOG(LogTemp, Warning, TEXT("DoGameMinute jobCount, groupCount, groupSize, currentLabel %d, %d, %d, %d", )
//   // GKLog1("DoGameMinute currentLabel", currentLabel);
//    //GKLog1("Hello ", (int32)currentLabel);
//    // The main thread label state is updated
//    currentLabel += groupCount;
//
//    for (uint32_t groupIndex = 0; groupIndex < groupCount; ++groupIndex)
//    {
//        // for each group, generate one real job
//        auto jobGroup = [jobCount, groupSize, groupIndex, jobs]()
//        {
//            // calculate the current group's offset into the jobs:
//            const uint32_t groupJobOffset = groupIndex * groupSize;
//            const uint32_t groupJobEnd = std::min(groupJobOffset + groupSize, jobCount);
//
//            // inside the group, loop through all job indices and execute the event for each index
//            for (uint32_t i = groupJobOffset; i < groupJobEnd; ++i)
//            {
//                GKJob* theJob = (*jobs)[i];
//                theJob->OnJobGameMinute(i);
//            }
//        };
//
//
//        // Try to push a new job until it is pushed successfully:
//        while (!jobPool.push_back(jobGroup)) { Poll(); }
//
//        wakeCondition.notify_one(); // wake one thread
//    }
//}
//
//void GKJobSystem::DoApplyChanges()
//{
//    // Use the same array for now
//    TArray<class GKJob*>* jobs = &ArrayJobs[Event::GameMinute];
//    uint32_t jobCount = jobs->Num();
//    if (jobCount == 0) return;
//
//    // Group count will equals to numThreads for now
//    const uint32_t groupCount = std::min(numThreads, jobCount);
//
//    // Calculate the group size (amount of jobs per thread). Overestimate.
//    // i.e. job = 5, groupCount = 2, groupSize = (5+2-1)/2 = 3. job = 4 => (4+2-1)/2 = 2. job = 6 => (6+2-1)/2 = 3
//    const uint32_t groupSize = (jobCount + groupCount - 1) / groupCount;
//
//    GKLog4("DoApplyChanges jobCount, groupCount, groupSize, currentLabel", jobCount, groupCount, groupSize, currentLabel);
//
//    // The main thread label state is updated
//    currentLabel += groupCount;
//
//    for (uint32_t groupIndex = 0; groupIndex < groupCount; ++groupIndex)
//    {
//        // for each group, generate one real job
//        auto jobGroup = [jobCount, groupSize, groupIndex, jobs]()
//        {
//            // calculate the current group's offset into the jobs:
//            const uint32_t groupJobOffset = groupIndex * groupSize;
//            const uint32_t groupJobEnd = std::min(groupJobOffset + groupSize, jobCount);
//
//            // inside the group, loop through all job indices and execute the event for each index
//            for (uint32_t i = groupJobOffset; i < groupJobEnd; ++i)
//            {
//                GKJob* theJob = (*jobs)[i];
//                theJob->OnJobApplyChanges(i);
//            }
//        };
//
//
//        // Try to push a new job until it is pushed successfully:
//        while (!jobPool.push_back(jobGroup)) { Poll(); }
//
//        wakeCondition.notify_one(); // wake one thread
//    }
//}
//
//void GKJobSystem::DoUpdateVisual()
//{
//    TArray<class GKJob*>* jobs = &ArrayJobs[Event::UpdateVisual];
//    uint32_t jobCount = jobs->Num();
//    if (jobCount == 0) return;
//
//    // Group count will equals to numThreads for now
//    const uint32_t groupCount = std::min(numThreads, jobCount);
//
//    // Calculate the group size (amount of jobs per thread). Overestimate.
//    // i.e. job = 5, groupCount = 2, groupSize = (5+2-1)/2 = 3. job = 4 => (4+2-1)/2 = 2. job = 6 => (6+2-1)/2 = 3
//    const uint32_t groupSize = (jobCount + groupCount - 1) / groupCount;
//
//    GKLog4("DoUpdateVisual jobCount, groupCount, groupSize, currentLabel", jobCount, groupCount, groupSize, currentLabel);
//
//    // The main thread label state is updated
//    currentLabel += groupCount;
//
//    for (uint32_t groupIndex = 0; groupIndex < groupCount; ++groupIndex)
//    {
//        // for each group, generate one real job
//        auto jobGroup = [jobCount, groupSize, groupIndex, jobs]()
//        {
//            // calculate the current group's offset into the jobs:
//            const uint32_t groupJobOffset = groupIndex * groupSize;
//            const uint32_t groupJobEnd = std::min(groupJobOffset + groupSize, jobCount);
//
//            // inside the group, loop through all job indices and execute the event for each index
//            for (uint32_t i = groupJobOffset; i < groupJobEnd; ++i)
//            {
//                GKJob* theJob = (*jobs)[i];
//                theJob->OnJobUpdateVisual(i);
//            }
//        };
//
//
//        // Try to push a new job until it is pushed successfully:
//        while (!jobPool.push_back(jobGroup)) { Poll(); }
//
//        wakeCondition.notify_one(); // wake one thread
//    }
//}
//
//void GKJobSystem::DoPostProcess()
//{
//    TArray<class GKJob*>* jobs = &ArrayJobs[Event::PostProcess];
//    uint32_t jobCount = jobs->Num();
//    if (jobCount == 0) return;
//
//    // Group count will equals to numThreads for now
//    const uint32_t groupCount = std::min(numThreads, jobCount);
//
//    // Calculate the group size (amount of jobs per thread). Overestimate.
//    // i.e. job = 5, groupCount = 2, groupSize = (5+2-1)/2 = 3. job = 4 => (4+2-1)/2 = 2. job = 6 => (6+2-1)/2 = 3
//    const uint32_t groupSize = (jobCount + groupCount - 1) / groupCount;
//
//    GKLog4("DoPostProcess jobCount, groupCount, groupSize, currentLabel", jobCount, groupCount, groupSize, currentLabel);
//
//    // The main thread label state is updated
//    currentLabel += groupCount;
//
//    for (uint32_t groupIndex = 0; groupIndex < groupCount; ++groupIndex)
//    {
//        // for each group, generate one real job
//        auto jobGroup = [jobCount, groupSize, groupIndex, jobs]()
//        {
//            // calculate the current group's offset into the jobs:
//            const uint32_t groupJobOffset = groupIndex * groupSize;
//            const uint32_t groupJobEnd = std::min(groupJobOffset + groupSize, jobCount);
//
//            // inside the group, loop through all job indices and execute the event for each index
//            for (uint32_t i = groupJobOffset; i < groupJobEnd; ++i)
//            {
//                GKJob* theJob = (*jobs)[i];
//                theJob->OnJobUpdateVisual(i);
//            }
//        };
//
//
//        // Try to push a new job until it is pushed successfully:
//        while (!jobPool.push_back(jobGroup)) { Poll(); }
//
//        wakeCondition.notify_one(); // wake one thread
//    }
//}
//
//void GKJobSystem::RegisterForEvent(GKJob* job, Event aEvent)
//{
//    if (!job->bitEventRegistered[aEvent])
//    {
//        check(!ArrayJobs[aEvent].Contains(job));
//        ArrayJobs[aEvent].Add(job);
//        job->bitEventRegistered[aEvent] = true;
//    }
//}
//
//void GKJobSystem::UnregisterForEvent(GKJob* job, Event aEvent)
//{
//    if (job->bitEventRegistered[aEvent])
//    {
//        check(ArrayJobs[aEvent].Contains(job));
//        ArrayJobs[aEvent].RemoveSingleSwap(job);
//        job->bitEventRegistered[aEvent] = false;
//    }
//}
//
//void GKJobSystem::UnregisterForAllEvents(GKJob* job)
//{
//    if (job->bitEventRegistered.any())
//    {
//        GKLog("unregisteredForAllEvents");
//        auto bits = job->bitEventRegistered;
//        for (int i = 0; i < Event::Count; ++i)
//        {
//            if (bits[i])
//            {
//                ArrayJobs[i].RemoveSingleSwap(job);
//            }
//        }
//        job->bitEventRegistered.reset();
//    }
//}
//
//void GKJobSystem::GetStats(int32& jobCount, int32& groupCount, int32& groupSize, int32& aCurrentLabel)
//{
//    jobCount = ArrayJobs[Event::GameMinute].Num();
//    if (jobCount == 0) return;
//    groupCount = std::min(numThreads, (uint32)jobCount);
//    groupSize = (jobCount + groupCount - 1) / groupCount;
//    aCurrentLabel = currentLabel;
//}
//
//// This little helper function will not let the system to be deadlocked while the main thread is waiting for something
//void GKJobSystem::Poll()
//{
//    wakeCondition.notify_one(); // wake one worker thread
//    std::this_thread::yield(); // allow this thread to be rescheduled
//}
