// Fill out your copyright notice in the Description page of Project Settings.

#include "GKJobSystem2.h"

#include <thread>
#include <condition_variable>
#include <string>
#include <algorithm>
//#include <processthreadsapi.h>

#include "Gimka/System/Job/GKJob.h"
#include "Gimka/Containers/ThreadSafe/GKThreadSafeRingBuffer.h"
#include "Gimka/Tools/Macros/GKLogger.h"


struct GKJobConfig
{
	GKJobSystem2::context* ctx;
	//GKJobSystem2::Event eventType;				// Which event
	std::function<void(GKJobConfig*)> func;
	TArray<class GKJob*>* arrayJobs;
	uint32_t groupID;
	uint32_t groupJobOffset;
	uint32_t groupJobEnd;
	//uint32_t sharedmemory_size;
};

GKJobSystem2* GKJobSystem2::Instance;

uint32_t numberThreads = 0;
GKThreadSafeRingBuffer<GKJobConfig, 256> jobQueue;
std::condition_variable wakeCond;
std::mutex wakeMut;

GKJobSystem2::GKJobSystem2()
{
	GKLog("GKJobSystem2 construct");
	Initialize(CalcCoresCount() - 1);
	check(false);		// We're not using this right now
}

GKJobSystem2::~GKJobSystem2()
{
	Instance = nullptr;
}

// This function executes the next item from the job queue. Returns true if successful, false if there was no job available
inline bool work()
{
	GKJobConfig jobConfig;
	if (jobQueue.pop_front(jobConfig))
	{
		jobConfig.func(&jobConfig);
		jobConfig.ctx->counter.fetch_sub(1);

		return true;
	}
	return false;
}

void workGameTick(GKJobConfig* jobConfig)
{
	GKJobSystem2::JobArgs args;
	//args.eventType = job.eventType;
	args.groupID = jobConfig->groupID;
	/*if (job.sharedmemory_size > 0)
	{
		args.sharedmemory = alloca(job.sharedmemory_size);
	}
	else
	{
		args.sharedmemory = nullptr;
	}*/

	TArray<GKJob*>* arrayJobs = jobConfig->arrayJobs;

	for (uint32_t i = jobConfig->groupJobOffset; i < jobConfig->groupJobEnd; ++i)
	{
		args.jobIndex = i;
		//args.groupIndex = i - jobConfig->groupJobOffset;
		//args.isFirstJobInGroup = (i == jobConfig->groupJobOffset);
		//args.isLastJobInGroup = (i == jobConfig->groupJobEnd - 1);
		GKJob* theJob = (*arrayJobs)[i];
		theJob->OnJobGameTick(args);
		//job.task(args);
	}

}

void workApplyChanges(GKJobConfig* jobConfig)
{
	GKJobSystem2::JobArgs args;
	args.groupID = jobConfig->groupID;

	TArray<GKJob*>* arrayJobs = jobConfig->arrayJobs;

	for (uint32_t i = jobConfig->groupJobOffset; i < jobConfig->groupJobEnd; ++i)
	{
		args.jobIndex = i;
		GKJob* theJob = (*arrayJobs)[i];
		theJob->OnJobApplyChanges(args);
	}

}

void workPostFrame(GKJobConfig* jobConfig)
{
	GKJobSystem2::JobArgs args;
	args.groupID = jobConfig->groupID;

	TArray<GKJob*>* arrayJobs = jobConfig->arrayJobs;

	for (uint32_t i = jobConfig->groupJobOffset; i < jobConfig->groupJobEnd; ++i)
	{
		args.jobIndex = i;
		GKJob* theJob = (*arrayJobs)[i];
		theJob->OnJobPostFrame(args);
	}

}

void GKJobSystem2::Initialize(uint32 workerThreads)
{		
	// Calculate the actual number of worker threads we want (-1 main thread):
	numberThreads = std::max(1u, workerThreads);

	for (uint32_t threadID = 0; threadID < numberThreads; ++threadID)
	{
		std::thread worker([] {

			while (true)
			{
				if (!work())
				{
					// no job, put thread to sleep
					std::unique_lock<std::mutex> lock(wakeMut);
					wakeCond.wait(lock);
				}
			}

		});

//#ifdef _WIN32
//		// Do Windows-specific thread setup:
//		HANDLE handle = (HANDLE)worker.native_handle();
//
//		// Put each thread on to dedicated core:
//		DWORD_PTR affinityMask = 1ull << threadID;
//		DWORD_PTR affinity_result = SetThreadAffinityMask(handle, affinityMask);
//		check(affinity_result > 0);
//
//		//// Increase thread priority:
//		//BOOL priority_result = SetThreadPriority(handle, THREAD_PRIORITY_HIGHEST);
//		//assert(priority_result != 0);
//
//		// Name the thread:
//		std::wstring wthreadname = L"wiJobSystem_" + std::to_wstring(threadID);
//		HRESULT hr = SetThreadDescription(handle, wthreadname.c_str());
//		check(SUCCEEDED(hr));
//#endif // _WIN32

		worker.detach();
	}

	GKLog(("wiJobSystem Initialized with [" + std::to_string(CalcCoresCount()) + " cores] [" + std::to_string(numberThreads) + " threads]").c_str());
}

uint32_t GKJobSystem2::CalcCoresCount()
{
	// Retrieve the number of hardware threads in this system:
	return std::thread::hardware_concurrency();
}

uint32_t GKJobSystem2::GetThreadCount()
{
	return numberThreads;
}

void GKJobSystem2::ExecuteEvent(context& ctx, const std::function<void(GKJobConfig*)>& func, Event eventType)
{
	TArray<class GKJob*>* arrayJobs = &ArrayJobs[eventType];
	uint32_t jobCount = arrayJobs->Num();
	if (jobCount == 0) return;

	// Group count will equals to numThreads for now
	const uint32_t groupCount = std::min(numberThreads, jobCount);

	// Calculate the group size (amount of jobs per thread). Overestimate.
	// i.e. job = 5, groupCount = 2, groupSize = (5+2-1)/2 = 3. job = 4 => (4+2-1)/2 = 2. job = 6 => (6+2-1)/2 = 3
	const uint32_t groupSize = (jobCount + groupCount - 1) / groupCount;

	//for (uint32 i = 0; i < jobCount; ++i) GKLog1("Job ", (*arrayJobs)[i]);

	// Context state is updated:
	ctx.counter.fetch_add(groupCount);

	GKJobConfig job;
	job.ctx = &ctx;
	//job.eventType = eventType;
	job.func = func;
	job.arrayJobs = arrayJobs;
	//job.sharedmemory_size = (uint32_t)sharedmemory_size;

	for (uint32_t groupID = 0; groupID < groupCount; ++groupID)
	{
		// For each group, generate one real job:
		job.groupID = groupID;
		job.groupJobOffset = groupID * groupSize;
		job.groupJobEnd = std::min(job.groupJobOffset + groupSize, jobCount);

		// Try to push a new job until it is pushed successfully:
		while (!jobQueue.push_back(job)) { wakeCond.notify_all(); std::this_thread::yield(); }	// work();
	}

	// Wake any threads that might be sleeping:
	wakeCond.notify_all();
}

void GKJobSystem2::ExecuteEventGameTick()
{
	ExecuteEvent(ctxGameTick, workGameTick, Event::GameTick);// , 0, Event::GameMinute);
	Wait(ctxGameTick);
}

void GKJobSystem2::ExecuteEventApplyChanges()
{
	ExecuteEvent(ctxGameTick, workApplyChanges, Event::GameTick);// , 0, Event::GameMinute);
	Wait(ctxGameTick);
}

void GKJobSystem2::ExecuteEventPostFrame()
{
	ExecuteEvent(ctxGameTick, workPostFrame, Event::PostFrame);// , 0, Event::GameMinute);
	Wait(ctxGameTick);

}

//uint32_t GKJobSystem2::DispatchGroupCount(uint32_t jobCount, uint32_t groupSize)
//{
//	// Calculate the amount of job groups to dispatch (overestimate, or "ceil"):
//	return (jobCount + groupSize - 1) / groupSize;
//}

void GKJobSystem2::LogStats()
{
	GKLog1("GameTick", ArrayJobs[Event::GameTick].Num());
	GKLog1("PostFrame", ArrayJobs[Event::PostFrame].Num());

}

bool GKJobSystem2::IsBusy(const context& ctx)
{
	// Whenever the context label is greater than zero, it means that there is still work that needs to be done
	return ctx.counter.load() > 0;
}

void GKJobSystem2::Wait(const context& ctx)
{
	// Wake any threads that might be sleeping:
	wakeCond.notify_all();

	// Waiting will also put the current thread to good use by working on an other job if it can:
	while (IsBusy(ctx)) { work(); }
}

void GKJobSystem2::RegisterForEvent(GKJob* job, Event aEvent)
{
	if (!job->bitEventRegistered[aEvent])
	{
		check(!ArrayJobs[aEvent].Contains(job));
		ArrayJobs[aEvent].Add(job);
		job->bitEventRegistered[aEvent] = true;
	}
}

void GKJobSystem2::UnregisterForEvent(GKJob* job, Event aEvent)
{
	if (job->bitEventRegistered[aEvent])
	{
		check(ArrayJobs[aEvent].Contains(job));
		ArrayJobs[aEvent].RemoveSingleSwap(job);
		job->bitEventRegistered[aEvent] = false;
	}
}

void GKJobSystem2::UnregisterForAllEvents(GKJob* job)
{
	if (job->bitEventRegistered.any())
	{
		GKLog("unregisteredForAllEvents");
		auto bits = job->bitEventRegistered;
		for (int i = 0; i < Event::Count; ++i)
		{
			if (bits[i])
			{
				ArrayJobs[i].RemoveSingleSwap(job);
			}
		}
		job->bitEventRegistered.reset();
	}
}

void GKJobSystem2::GetStats(int32& jobCount, int32& groupCount, int32& groupSize, int32& aCurrentLabel)
{
	jobCount = ArrayJobs[Event::GameTick].Num();
	if (jobCount == 0) return;
	groupCount = std::min(numberThreads, (uint32)jobCount);
	groupSize = (jobCount + groupCount - 1) / groupCount;
	aCurrentLabel = ctxGameTick.counter.load();// currentLabel;
}
