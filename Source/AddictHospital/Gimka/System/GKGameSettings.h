// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
class GKGameSettings
{
//	GK_SINGLETON(GKGameSettings);

public:
	// From DeltaTime to GameTicks
	float CalculateGameTicks(float DeltaTime) const { return DeltaTime / GameTickDuration; }

	// From GameTicks to GameMinutes
	float CalculateGameMinutes(float GameTicks) const { return GameTicks / GameTickPerGameMinute; }

private:
	// Time			Update		gameMinute	gameHour	gameDay
	//				Per second	Duration	Duration	Duration
	// 				(gameTick)
	// Slow			30			1s			1m			24m
	// Normal		60			0.5s		30s			12m
	// Fast (x2)	120			0.25s		15s			6m
	// Ultra (x6)	360			0.0833s		5s			2m

	float GameTickDuration = 1.f / 60.f;			// Length of 1 game minute in real-time second. Normal is 1/60. Reduce to speed it up and vice versa.
	float GameTickPerGameMinute = 30.f;				// A gameMinutes has 30 gameTicks
};
