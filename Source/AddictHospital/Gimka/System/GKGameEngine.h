// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"

#include "Gimka/System/Time/GKGameTimer.h"
#include "Gimka/System/Job/GKJobSystem2.h"
#include "Gimka/System/Job/GKJobSystem3.h"

class GKManager;
class GKWorld;
class GKGameManager;
class GKVisualManager;
class GKGroundVisualManager;
class GKNetworkManager;
class UGKGameInstance;

/**
 * GKGameEngine is avaiable until throughout the duration of the App.
 * GKGameManager is only available during gameplay only (not available in mainmenu, etc).
 */
class GKGameEngine
{
	//GK_SINGLETON_NEW(GKGameEngine);
public:
	enum Manager
	{
		Game,
		Visual,
		GroundVisual,
		Network,
		Count
	};
	GKGameEngine(UGKGameInstance* GameInstance);
	~GKGameEngine();

	// GKGameEngine will call OnStartGame for GK objects
	// Start the game, provide the GameManager.
	void StartGame(GKGameManager* GameManager, 
		GKVisualManager* VisualManager, GKGroundVisualManager* GroundVisualManager,
		GKNetworkManager* NetworkManager);

	// Called every Actor::Tick (our own GameTick can be done 0 or more time)
	void UpdateGame(float DeltaTime);

	// GKGameEngine will call OnEndGame for GK objects
	// Destroy GameManager
	void EndGame();

	// After constructor, VGAGameSession will call this where any GK objects can find other GK objects.
	// World, i.e. the GKWorldRegion to use
	void Create();

	// Before GKGameEngine is destroyed.
	void OnDestroy();

	GKGameManager* GetGameManager() { return (GKGameManager*)Managers[Manager::Game]; }// return (GKGameManager*)ArrayManagers[Manager::Game];
	GKVisualManager* GetVisualManager() { return (GKVisualManager*)Managers[Manager::Visual]; }// return (GKGameManager*)ArrayManagers[Manager::Game];
	GKGroundVisualManager* GetGroundVisualManager() { return (GKGroundVisualManager*)Managers[Manager::GroundVisual]; }// return (GKGameManager*)ArrayManagers[Manager::Game];
	GKNetworkManager* GetNetworkManager() { return (GKNetworkManager*)Managers[Manager::Network]; }

	void PrintScreen(float Time, FColor DisplayColor, const FString& Message);

	void PrintSystemLog(const FString& Message);
	void PrintSystemWarning(const FString& Message);
	void PrintSystemError(const FString& Message);

private:
	// Always available until the app is closed. The ArrayJobs will be created in GKGameManager during gameplay only.
	//GKJobSystem3 JobSystem;

	GKManager* Managers[Manager::Count];

	GKJobSystem3* JobSystem;		// The JobSystem
	UGKGameInstance* GameInstance;

	// Available during GamePlay only
	//GKGameManager* GameManager = nullptr;

	// We have the VisualManager here to emphasize that the GameManager (World, Region, Entity) don't need the VisualManager to work.
	//GKVisualManager* VisualManager = nullptr;

	void DestroyManagers();

public:
	GK_GET(GKJobSystem3*, JobSystem);
	//GK_GET(UGKGameInstance*, GameInstance);

	//GKManager* ArrayManagers[Manager::Count];
	//GK_GET(GKGameManager*, GameManager);
	//GK_GET(GKVisualManager*, VisualManager);
};
