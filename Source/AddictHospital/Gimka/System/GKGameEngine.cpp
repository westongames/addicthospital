// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGameEngine.h"
#include "Gimka/System/Job/GKJobSystem.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/System/GKManager.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/UnrealEngine/GKGameInstance.h"
#include "Gimka/Visual/GKVisualManager.h"
#include "Gimka/Visual/GKGroundVisualManager.h"


//GKGameEngine* GKGameEngine::Instance;
//
GKGameEngine::GKGameEngine(UGKGameInstance* iGameInstance)
{
	////GKJobSystem::GetInstance();
	//GKLog1("GKGameEngine::GKGameEngine, instance = ", Instance);
	//check(Instance == nullptr);

	////GKJobSystem2::GetInstance();
	////new GKJobSystem3();
	GameInstance = iGameInstance;

	JobSystem = new GKJobSystem3();

	for (int i = 0; i < Manager::Count; ++i) Managers[i] = nullptr;
}

GKGameEngine::~GKGameEngine()
{
	GKLog("GKGameEngine::~GKGameEngine");
	DestroyManagers();
	//SAFE_DELETE(GameManager);
	/*for (int i = 0; i < Manager::Count; ++i)
	{
		delete ArrayManagers[i];
		ArrayManagers[i] = nullptr;
	}*/
}

void GKGameEngine::StartGame(GKGameManager* iGameManager, 
	GKVisualManager* iVisualManager, GKGroundVisualManager* iGroundVisualManager,
	GKNetworkManager* iNetworkManager)
{
	GKLog("GKGameEngine::StartGame()");
	
	// Note: VisualManager has to be after GameManager because we want to render after the entities have been updated
	check(Managers[Manager::Game] == nullptr);
	Managers[Manager::Game] = iGameManager;

	check(Managers[Manager::Visual] == nullptr);
	Managers[Manager::Visual] = iVisualManager;

	check(Managers[Manager::GroundVisual] == nullptr);
	Managers[Manager::GroundVisual] = iGroundVisualManager;

	check(Managers[Manager::Network] == nullptr);
	Managers[Manager::Network] = iNetworkManager;

	for (int i = 0; i < Manager::Count; ++i) Managers[i]->OnCreate();
	for (int i = 0; i < Manager::Count; ++i) Managers[i]->OnStartGame();

	//GameManager->OnCreate();
	//VisualManager->OnCreate();

	//GameManager->OnStartGame();
	//VisualManager->OnStartGame();

	//for (GKManager* Manager : ArrayManagers)
	//{
	//	Manager->OnStartGame();
	//}
}

void GKGameEngine::UpdateGame(float DeltaTime)
{
	//GKLog("GKGameEngine::UpdateGame()");
	/*for (GKManager* Manager : ArrayManagers)
	{
		Manager->OnUpdateGame(DeltaTime);
	}*/
	check(Managers[Manager::Game] != nullptr);
	check(Managers[Manager::Visual] != nullptr);
	check(Managers[Manager::GroundVisual] != nullptr);
	check(Managers[Manager::Network] != nullptr);

	GKLogger::SetPlayerID(GetGameManager()->GetPlayerID());
	GKLogger::SetGameTick(GetGameManager()->GetGameTick());

	Managers[Manager::Game]->OnUpdateGame(DeltaTime);

	// Rather than calling OnUpdateGame for other managers. 
	// We'll let VisualManager and NetworkManager to call their own from their Tick.
	// Their tick has also been ordered to occur after this tick via AddTickPrerequisiteActor.
	//for (int i = 0; i < Manager::Count; ++i) 
	//	Managers[i]->OnUpdateGame(DeltaTime);

	//GameManager->OnUpdateGame(DeltaTime);

	//// VisualManager is after GameManager
	//VisualManager->OnUpdateGame(DeltaTime);
}

void GKGameEngine::DestroyManagers()
{
	// Only delete GameManager. VisualManager is an actor and will be handled by UE.
	// Note: also assume VisualManager might already be destroyed by UE, so no more accessing it.
	Managers[Manager::Visual] = nullptr;
	Managers[Manager::GroundVisual] = nullptr;
	Managers[Manager::Network] = nullptr;

	for (int i = 0; i < Manager::Count; ++i)
	{
		if (Managers[i] != nullptr) Managers[i]->OnEndGame();
	}

	for (int i = 0; i < Manager::Count; ++i)
	{
		if (Managers[i] != nullptr) Managers[i]->OnDestroy();
	}

	for (int i = 0; i < Manager::Count; ++i)
	{
		SAFE_DELETE(Managers[i]);
	}


	//if (GameManager != nullptr) GameManager->OnEndGame();
	//if (VisualManager != nullptr) VisualManager->OnEndGame();

	//if (GameManager != nullptr) GameManager->OnDestroy();
	//if (VisualManager != nullptr) VisualManager->OnDestroy();

	//SAFE_DELETE(GameManager);
	//SAFE_DELETE(VisualManager);
}


void GKGameEngine::EndGame()
{
	// Note: EndGame causes crash. Possibly because GKJobSystem3 has been deallocated?
	// So we don't call it here.
	// Todo: maybe if we call this from OnTick or when change level.
	GKLog("GKGameEngine::EndGame()");
	//for (GKManager* Manager : ArrayManagers)
	//{
	//	//Manager->OnEndGame();
	//}
	DestroyManagers();

}

void GKGameEngine::Create()
{
	GKLog("GKGameEngine::OnCreate()");

	//ArrayManagers.AddUninitialized(Manager::Count);
	//ArrayManagers[Manager::Job] = new GKJobSystem3(Manager::Job);
	//ArrayManagers[Manager::Game] = new GKGameManager(Manager::Game, this, World);
	//GKLog1("ArrayManagers", ArrayManagers.Num());


	/*for (GKManager* Manager : ArrayManagers)
	{
		Manager->OnCreate();
	}*/

	//JobSystem.OnCreate();
}

void GKGameEngine::OnDestroy()
{
	// Similar issue like EndGame
	GKLog("GKGameEngine::OnDestroy()");
	/*for (GKManager* Manager : ArrayManagers)
	{
		Manager->OnDestroy();
	}*/
	DestroyManagers();
	SAFE_DELETE(JobSystem);


}

// We have this to avoid having the inner game deals with UGKGameInstance
void GKGameEngine::PrintScreen(float Time, FColor DisplayColor, const FString& Message)
{
	GameInstance->PrintScreen(Time, DisplayColor, Message);
}

void GKGameEngine::PrintSystemLog(const FString& Message)
{
	PrintScreen(5.f, FColor::Green, Message);
}

void GKGameEngine::PrintSystemWarning(const FString& Message)
{
	PrintScreen(5.f, FColor::Orange, Message);
}

void GKGameEngine::PrintSystemError(const FString& Message)
{
	PrintScreen(5.f, FColor::Red, Message);
}

