// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/System/Time/GKGameTimer.h"

/**
 * Store data for the current gaming session (not destroyed when changing map)
 */

class GKGameState
{
	//GK_SINGLETON(GKGameState);

public:
	void AdvanceGameTime(float DeltaTime);

private:
	GKGameTimer GameTime;
	int32 DeltaGameTick;			// DeltaGameTick for current frame, 0 or more

public:
	GK_GETCR(GKGameTimer, GameTime);
	GK_GET(int32, DeltaGameTick);

};
