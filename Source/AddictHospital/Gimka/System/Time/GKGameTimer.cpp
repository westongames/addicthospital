// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGameTimer.h"
#include "Gimka/System/GKGameSettings.h"

GKGameTimer::GKGameTimer()
{
}

GKGameTimer::~GKGameTimer()
{
}

//int GKGameTimer::AdvanceGameTicks(float gameTicks)
//{
//	float newGameTickTotal = gameTickTotal + gameTicks;
//
//	int32 prevGameTicks = (int32)gameTickTotal;
//	int32 newGameTicks = (int32)newGameTickTotal;
//
//	gameTickTotal = newGameTickTotal;
//
//	return newGameTicks - prevGameTicks;
//}

int GKGameTimer::PrecalcGameTicks(float gameTicks, float& fractionGameTicks)
{
	float NewGameTickTotal = gameTickTotal + gameTicks;

	int32 prevGameTicks = (int32)gameTickTotal;
	int32 newGameTicks = (int32)NewGameTickTotal;

	fractionGameTicks = NewGameTickTotal - newGameTicks;

	//GKLog2("PrecalcgameTick", prevGameTicks, newGameTicks);

	return newGameTicks - prevGameTicks;
}


void GKGameTimer::SetFractionGameTick(float fractionGameTicks)
{
	//GKLog2("SetGameTickTotal", gameTickTotal, NewGameTickTotal);
	// Because we're advancing one by one via Advance(). This is to ensure we don't lose any fraction of DeltaTime.
	// The NewGameTickTotal should have the same GameTick as the current one.

	// i.e. gameTickTotal was 2.8, new gameTicks was 0.3, so the new gameTickTotal is supposed to be 3.1
	// But GameTick as advanced by 1, so it becomes 3.8. This function will change the fraction so it's back to 3.1
	gameTickTotal = ((int32)gameTickTotal) + fractionGameTicks;
	
	//check((int32)gameTickTotal == (int32)(gameTickTotal + fractionGameTicks));
	/*if (!((int32)gameTickTotal == (int32)(gameTickTotal + fractionGameTicks)))
	{
		GKLog2("*************** gameTickTotal, fractionGameTick", gameTickTotal, fractionGameTicks);
	}
	gameTickTotal += fractionGameTicks;*/
}

void GKGameTimer::Advance()
{
	gameTickTotal += 1.f;
}