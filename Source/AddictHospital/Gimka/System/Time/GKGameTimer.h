// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class GKGameTimer
{
public:
	GKGameTimer();
	~GKGameTimer();

	// Advance the current total GameTicks and return the amount of Tick to perform for this frame
	//int AdvanceGameTicks(float DeltaGameTicks);

	int PrecalcGameTicks(float DeltaGameTicks, float& fractionGameTicks);

	void SetFractionGameTick(float fractionGameTicks);

	uint32 GetGameTick() const { return (uint32)gameTickTotal; }

	// Advance by 1 to follow the UpdateWorld
	void Advance();

private:
	float gameTickTotal = 0.f;				// How many gameTicks total
};
