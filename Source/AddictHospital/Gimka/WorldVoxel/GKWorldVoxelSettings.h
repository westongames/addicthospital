// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"

#define WVS(part, metric, subPart)						\
	int32 part ## Bit ## metric ## subPart;				\
	int32 part ## metric ## subPart;

/**
 * 
 */
struct GKWorldVoxelSettings
{
public:
	GKWorldVoxelSettings();
	~GKWorldVoxelSettings();

	void Initialize(int32 regionBitNumChunk, int32 chunkBitNumSections, int32 chunkBitNumTiles, int32 tileBitNumZVoxels);

	void SetVoxelBitNumMinis(int32 tileBitNumVoxels);


private:
	// Region contains 16x16 = 256 chunks ~ 16x16x16 (4k) sections ~ 256x256x16 (1m) tiles ~ 256x256x128 (8m) Voxels
	// Minis: 1024x1024x512 (0.5b, lo-res), 2048x2048x1024 (4b, hi-res), 4096x4096x2048 (32b, x-res)
	GK_DEF_BITAREA(int32, Region, Chunks);		// How many chunks per region (X & Y), i.e. 4 bits ~ 16 x 16 ~ 256
	GK_DEF_BITVOLZ(int32, Region, Tiles);		// How many tiles per region (X & Y, Z), i.e. 8 bits, 4 bits ~ 256*256x16 = 1m

	// Chunk contains 16 sections ~ 16x16x16 (4k) tiles ~ 16x16x128 (32k) voxels
	// Minis: 64x64x512 (2m, lo-res), 128x128x1024 (16m, hi-res), 256x256x2048 (128m, x-res)
	GK_DEF_BITNUM(int32, Chunk, Sections);		// How many sections per chunk, i.e. 4 bits ~ 16
	GK_DEF_BIT(int32, Chunk, Middle, Section);		// Middle section
	GK_DEF_BITVOLZ(int32, Chunk, Tiles);		// How many tiles per chunk (X & Y, Z), i.e. 4 bits, 4 bits ~ 16x16x16 ~ 256x16 ~ 4k

	// Section contains 16x16x1 (256) tiles ~ 16x16x8 (2k) voxels
	// Minis: 64x64x32 (0.125m, lo-res), 128x128x64 (1m, hi-res), 256x256x128 (8m, x-res)
	// Rendering is per section. An actor will contains the whole chunk (up to 16 sections).
	GK_DEF_BITAREA(int32, Section, Tiles);		// How many tiles per sections, i.e. 4 bits ~ 16

	// How many voxels per tile
	GK_DEF_BITNUMZ(int32, Tile, Voxels);		// How many voxels per tile (in height/ Z), i.e. 3 bits ~ 8

	// Voxel contains 16x16x16 minis (4k, x-res), 8x8x8 minis (0.5k, hi-res), 4x4x4 minis (0.0625k, lo-res)
	// Possibly support lower LOD like 2x2x2 minis (or just auto generate)
	GK_DEF_BITVOL(int32, Voxel, Minis);			// How many minis per voxel, i.e. 4 bits ~ 16x16x16 ~ 4k

public:	// Accessors
	GK_GET_BITAREA(int32, Region, Chunks);
	GK_GET_BITVOLZ(int32, Region, Tiles);
	GK_GET_BITNUM(int32, Chunk, Sections);
	GK_GET_BIT(int32, Chunk, Middle, Section);
	GK_GET_BITVOLZ(int32, Chunk, Tiles);
	GK_GET_BITAREA(int32, Section, Tiles);
	GK_GET_BITNUMZ(int32, Tile, Voxels);
	GK_GET_BITVOL(int32, Voxel, Minis);

};
