// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Game/GKWorld.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Math/GKIntVector.h"
#include "GKWorldVoxelSettings.h"

class GKChunk;
class GKRegion;

/**
 * 
 * 
 * New version (the layer version is obsolete?): The world contains multiple GKEntityManager.
 * In term of voxel game, the GKEntityManager is a chunk and is performed by a job.
 * Tile:
 *		- If extreme-res, contains 16x16x16 voxels = 4k voxels, with LOD 8x8x8 and 4x4x4.
 *		- If hi-res, contains 8x8x8 voxels = 0.5k voxels, with LOD 4x4x4 and 2x2x2.
 *		- If lo-res, contains 4x4x4 voxels = 0.0625k voxels, with LOD 2x2x2.
 *		- Note: LOD might goes automatic and go even at level of Section.
 * Section: (minecraft term 16x16x16, our height is only 8)
 *		- The intersection of layer and chunk. Size 16x16x8. Height of 8 should be enough for a floor.
 *		- Contains 16x16 x8 tiles = 2k tiles. 
 *			=> make wall double character high like in Sims, so if char height = 4 tiles then wall is 8 tiles.
 *			=> this will also allow giants.
 *		- If extreme-res, contains 2k x 4k = 8m voxels.
 *		- If hi-res, contains 2k x 0.5k = 1m voxels.
 *		- If lo-res, contains 2k x 0.0625k = 0.125m voxels.
 *		- Every section is an actor and so requires 1 draw call but they can use LOD. Most of time
 *			there will only be 1 or 2 section actors per chunk. This makes is easy to redraw too.
 *			The higher section needs to be calc lighting first before the bottom one can be rendered.
 *			Also rendering a section only needs 8m voxel buffer.
 *		- Every section also contains pathfinding data (might be empty).
 *			=> pathfinding data is stored in chunk because it's the lowest abstraction layer.
 *		- We're not keeping 2k tiles data per section, instead each section only has 16x16 tiles.
 *			The tiles might contains ground data and object data. This will simplify everything.
 * Chunk: (minecraft term 16x16x256)
 *		- Contains 32 sections, 16x16x256 tiles = 64k tiles.
 *		- If extreme-res, contains 64k x 4k = 256m voxels.
 *		- If hi-res, contains 64k x 0.5k = 32m voxel.
 *		- If lo-res, contains 64k x 0.0625k = 4m voxels.
 *		- It's an entity manager, has 1 job and its own messaging system.
 *			Better to keep this at chunk level because chunk always exist while section might be null.
 * Region: (minecraft term 32x32 chunks)
 *		- Contains 512x512x256 tiles = 64m tiles.
 *		- If extreme-res, contains 64m x 4k = 256b voxels.
 *		- If hi-res, contains 64m x 0.5k = 32b voxels.
 *		- If lo-res, contains 64m x 0.0625k = 4b voxels.
 *		- Used for generation and storing to files like minecraft. No actual data structure.
 * NEXT STEP is to not work on the voxel engine but just generating all of these and draw flat terrain with some obstacles and stairs.
 */
class GKWorldVoxel : public GKWorld
{
	//GK_SINGLETON(GKWorldRegion)
	//FIntVector RegionCount;
	//int32 RegionLandLevel;
	GKWorldVoxelSettings Settings;

public:
	GKWorldVoxel(GKGameManager* GameManager);
	virtual ~GKWorldVoxel();

	void Initialize(const GKWorldVoxelSettings& Settings);
		//int32 iRegionLandLevel,
		//const GKIntVector iRegionSize, const GKIntVector& iRegionCount);

	//GKChunk* GetChunk(const FIntPoint& RegionIndex) const;
	//GKChunk* GetChunk(int32 indexX, int32 indexY, int32 indexZ) const;

	virtual void GetEntityManagersForVisualUpdate(TArray<GKEntityManager*>& EntityManagers) override;

	virtual GKEntity* SpawnEntity(GKEntity::Type EntityType, uint32 EntityID,
		const FVector& Position, const FRotator& Rotation) override;

private:
	TMap<FIntPoint, GKRegion*> MapRegions;
	//virtual GKEntityManager* GKWorldRegion::CreateEntityManager() override;

	void UpdateRegionNeighbors(GKRegion* Region);

public:	// Accessors
	GK_GETCR_AND_PTR(GKWorldVoxelSettings, Settings);
	//FORCEINLINE const GKWorldVoxelSettings* GetSettingsPtr() const { return &Settings; }
};
