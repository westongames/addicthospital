// Fill out your copyright notice in the Description page of Project Settings.


#include "GKWorldVoxel.h"
#include "GKChunk.h"
#include "GKRegion.h"
#include "Gimka/Entity/Manager/GKEntityManager.h"
#include "Gimka/Entity/Entity/GKPawnEntity.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Tools/Macros/GKSource.h"

//GKWorldRegion* GKWorldRegion::Instance;
//
GKWorldVoxel::GKWorldVoxel(GKGameManager* iGameManager)
	: GKWorld(iGameManager)
{
}

GKWorldVoxel::~GKWorldVoxel()
{
}

//void GKEntityManager* GKWorldRegion::CreateEntityManager()
//{
//	return new GKRegion();
//}

void GKWorldVoxel::Initialize(const GKWorldVoxelSettings& iSettings)
	//int32 iRegionLandLevel,
	//const GKIntVector iRegionSize, const GKIntVector& iRegionCount)
{
	//GKChunk::InitializeStatic(iRegionSize);

	Settings = iSettings;

	//Settings.Initialize(3,					// sectionBitNumZTiles
	//	4, 4,								// chunkBitNumSections, chunkBitNumTiles
	//	4);									// regionBitNumChunk

	//Settings.SetTileBitNumVoxels(4);


	check(ArrayEntityManagers.Num() == 0);

	GKRegion* region = new GKRegion(this, FIntPoint::ZeroValue);
	MapRegions.Add(region->GetRegionPos(), region);
	// Create 16x16 regions
	//GK_ITERATE_XY(int32, 16, 16)
	//	GKEntityManager* manager = new GKChunk(this, FIntPoint(x * Settings.GetChunkNumTiles(), y * Settings.GetChunkNumTiles()));
	//	ArrayEntityManagers.Add(manager);
	//	ArrayJobs.Add(manager);
	//GK_ITERATE_XY_END


	//RegionCount = iRegionCount;
	//RegionLandLevel = iRegionLandLevel;
	//check(ArrayEntityManagers.Num() == 0);
	////ArrayEntityManagers.Empty();
	////checkSlow(ArrayEntityManagers.Num() == 0);
	////ArrayEntityManagers.AddUninitialized(RegionCount.Volume());

	////GKJobSystem2 * jobSystem = GKJobSystem2::GetInstance();
	//
	//// Create GKRegion instead of GKEntityManager
	//GK_ITERATE_XYZ(int32, RegionCount.X, RegionCount.Y, RegionCount.Z)
	//	GKEntityManager* manager = new GKChunk(this, GKIntVector(x, y, z), GameManager);
	//	ArrayEntityManagers.Add(manager);
	//	ArrayJobs.Add(manager);
	//	//jobSystem->RegisterForEvent(manager, GKJobSystem2::Event::GameTick);
	//	//jobSystem->RegisterForEvent(manager, GKJobSystem2::Event::PostFrame);
	//GK_ITERATE_XYZ_END
}

void GKWorldVoxel::UpdateRegionNeighbors(GKRegion* Region)
{
	FIntPoint RegionPos = Region->GetRegionPos();
	const int32 regionNumTiles = Settings.GetRegionNumTiles();

	GKRegion** right = MapRegions.Find(FIntPoint(RegionPos.X + regionNumTiles, RegionPos.Y));
	if (right) Region->SetNeighborRight(*right);

	GKRegion** forward = MapRegions.Find(FIntPoint(RegionPos.X, RegionPos.Y + regionNumTiles));
	if (forward) Region->SetNeighborForward(*forward);

	GKRegion** rightForward = MapRegions.Find(FIntPoint(RegionPos.X + regionNumTiles, RegionPos.Y + regionNumTiles));
	if (rightForward) Region->SetNeighborRightForward(*rightForward);

	GKRegion** left = MapRegions.Find(FIntPoint(RegionPos.X - regionNumTiles, RegionPos.Y));
	if (left) Region->SetNeighborLeft(*left);

	GKRegion** back = MapRegions.Find(FIntPoint(RegionPos.X, RegionPos.Y - regionNumTiles));
	if (back) Region->SetNeighborBack(*back);

	GKRegion** leftBack = MapRegions.Find(FIntPoint(RegionPos.X - regionNumTiles, RegionPos.Y - regionNumTiles));
	if (leftBack) Region->SetNeighborLeftBack(*leftBack);

	GKRegion** rightBack = MapRegions.Find(FIntPoint(RegionPos.X + regionNumTiles, RegionPos.Y - regionNumTiles));
	if (rightBack) Region->SetNeighborRightBack(*rightBack);

	GKRegion** leftForward = MapRegions.Find(FIntPoint(RegionPos.X - regionNumTiles, RegionPos.Y + regionNumTiles));
	if (leftForward) Region->SetNeighborLeftForward(*leftForward);
}


GKEntity* GKWorldVoxel::SpawnEntity(GKEntity::Type EntityType, uint32 EntityID, const FVector& Position, const FRotator& Rotation)
{
	switch (EntityType)
	{
		case GKEntity::Type::PawnEntity:
		{
			GKLog1("ArrayJobs:", ArrayJobs.Num());

			GKLog1("Create SpawnEntity", EntityID);
			UE_LOG(LogTemp, Warning, TEXT("Position %s, Rotation %s"),
				*Position.ToString(), *Rotation.ToString());
			GKChunk* Region = (GKChunk*)ArrayEntityManagers[0];// GetChunk(0, 0, 0);		// TODO: get region that should occupy the entity position
			GKPawnEntity* PawnEntity = new GKPawnEntity(Region, EntityID);
			PawnEntity->SetPosition(Position);
			PawnEntity->SetRotation(Rotation);
			if (Region->IsVisible())
			{
				PawnEntity->SetVisible(true);
			}
			//PawnEntity->SetVelocity(FVector(1, 0, 0));
			return PawnEntity;
			break;
		}
	}
	return nullptr;
}


void GKWorldVoxel::GetEntityManagersForVisualUpdate(TArray<GKEntityManager*>& EntityManagers)
{
	GKLog("GKWorldRegion::GetEntityManagersForVisualUpdate");
	// Todo: return regions close to camera. For now, just return all.
	EntityManagers.Empty();
	EntityManagers.Append(ArrayEntityManagers);
}
//
//
//GKChunk* GKWorldVoxel::GetChunk(const FIntVector& RegionIndex) const
//{
//	check(RegionIndex.X >= 0 && RegionIndex.X < RegionCount.X);
//	check(RegionIndex.Y >= 0 && RegionIndex.Y < RegionCount.Y);
//	check(RegionIndex.Z >= 0 && RegionIndex.Z < RegionCount.Z);
//	int32 regionIndex = RegionIndex.X + RegionIndex.Y * RegionCount.X + RegionIndex.Z * (RegionCount.X * RegionCount.Y);
//	return (GKChunk*)ArrayEntityManagers[regionIndex];
//}
//
//GKChunk* GKWorldVoxel::GetChunk(int32 indexX, int32 indexY, int32 indexZ) const
//{
//	check(indexX >= 0 && indexX < RegionCount.X);
//	check(indexY >= 0 && indexY < RegionCount.Y);
//	check(indexZ >= 0 && indexZ < RegionCount.Z);
//	int32 regionIndex = indexX + indexY * RegionCount.X + indexZ * (RegionCount.X * RegionCount.Y);
//	return (GKChunk*)ArrayEntityManagers[regionIndex];
//
//}
