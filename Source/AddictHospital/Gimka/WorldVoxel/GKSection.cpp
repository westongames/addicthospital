// Fill out your copyright notice in the Description page of Project Settings.


#include "GKSection.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "GKWorldVoxelSettings.h"
#include "GKChunk.h"

GKSection::GKSection(const GKChunk* iParent)
{
	Parent = iParent;
	Array2DTiles = new GKTile[Parent->GetWorldSettings()->GetSectionAreaTiles()];
	GenerateRandom();
}

GKSection::~GKSection()
{
	SAFE_DELETE_ARRAY(Array2DTiles);
}

inline GKTile& GKSection::GetTile(int32 x, int32 y)
{
	return Array2DTiles[x + y * Parent->GetWorldSettings()->GetSectionNumTiles()];
}

void GKSection::GenerateRandom()
{
	///int32 numChunkTiles = Parent->GetWorldSettings()->GetChunkNumTiles();
	int32 numTiles = Parent->GetWorldSettings()->GetSectionAreaTiles();
	for (int32 i = 0; i < numTiles; ++i)
	{
		bool blocked = FMath::RandHelper(8) == 1;
		if (!blocked) Array2DTiles[i].SetWalkable();
		//Array2DTiles[i].SetWalkable();
	}
}


