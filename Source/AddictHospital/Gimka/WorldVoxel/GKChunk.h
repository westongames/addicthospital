// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Misc/GKCompress.h"
#include "Gimka/Containers/Combo/GKArrayOfLists.h"
#include "Gimka/Entity/Manager/GKEntityManager.h"
#include "Gimka/Visual/GKGroundVisualManager.h"
#include "Gimka/Visual/GKGroundSpriteInfo.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Math/GKIntVector.h"
#include "GKWorldVoxel.h"
#include "GKRegion.h"
#include "GKSection.h"

class GKEntity;
/**
 * 
 */
class GKChunk : public GKEntityManager
{
public:
	GKChunk(const GKRegion* Region, const FIntPoint& ChunkPos);
	virtual ~GKChunk();

	//static void InitializeStatic(const GKIntVector& iRegionSize);

	uint32 GetHashKey();

	//static inline uint32 RegionPosToHashKey(GKIntVector Pos) { return GKCompress::IntVectorToUInt32(Pos); }
	//static inline GKIntVector HashKeyToRegionIndex(uint32 Hashkey) { return GKCompress::UInt32ToIntVector(Hashkey); }

	virtual void Tick(float DeltaTime);
	virtual void ApplyChanges();

	// For now return true if it's ON or Near
	bool IsVisible() { return ChunkVisibility != GKGroundVisualManager::ChunkVisibility::Off; }
	//void ClearDirtyGroundVisual() { SpriteInfo.C = false; }

protected:
	//GKArrayOfLists<GKEntity*, 60> ArrayListsUpdateMinutes;

	// Always called once at the end of the frame after the main loop: JobPostFrame, then EndOfFrame
	virtual void OnJobPostFrame(const GKJobSystem2::JobArgs& jobArgs) override;

private:
	// When RegionVisibility:
	//	- is On, region & entities will be rendered and bVisible set to true.
	//	- is Off, region & entities actor & mesh will be destroyed and bVisible set to false.
	//	- is Close, no changes. Region and entities that are already rendered will kept being rendered, but otherwise no.
	GKGroundVisualManager::ChunkVisibility ChunkVisibility = GKGroundVisualManager::ChunkVisibility::Off;
	//GKIntVector RegionIndex;
	FIntPoint ChunkPos;
	//static GKIntVector RegionSize;
	const GKRegion* Region;										// Ref to region
	const GKWorldVoxelSettings* WorldSettings;					// Ref to worldVoxelSettings

	GKSection** ArraySections;									// Array 1D of sections, might be nullptr if air

	// Chunk that need to be updated by GKGroundVisualManager
	// Note: it's possible for this chunk to wait for several frames before it gets updated by GroundVisualManager
	GKGroundSpriteInfo SpriteInfo;

public:
	GK_GETCR(FIntPoint, ChunkPos);
	//GK_GETCR(GKIntVector, RegionSize);
	GK_GET(GKGroundVisualManager::ChunkVisibility, ChunkVisibility);
	GKPTR_GET(const GKWorldVoxelSettings, WorldSettings);
	GK_GETR(GKGroundSpriteInfo, SpriteInfo);
	GKPTR_GETC(GKRegion, Region);

	//inline GKIntVector RegionPos() { return RegionIndex * RegionSize; }
//	inline GKIntVector RegionCenter() { return RegionIndex * RegionSize + RegionIndex * RegionSize / 2; }
	inline FIntPoint ChunkCenter() const
	{
		return ChunkPos + WorldSettings->GetChunkNumTiles()/2;
	}

	inline int32 GetChunkIndex() const
	{
		FIntPoint Index = GetChunkIndexInRegion();
		return Index.Y * WorldSettings->GetRegionNumChunks() + Index.X;
	}

	inline FIntPoint GetChunkIndexInRegion() const
	{
		return (ChunkPos - Region->GetRegionPos()) / WorldSettings->GetChunkNumTiles();
	}

	inline const GKChunk* GetNeighborRight() const;
	inline const GKChunk* GetNeighborForward() const;
	inline const GKChunk* GetNeighborRightForward() const;

	inline const GKChunk* GetNeighborLeft() const;
	inline const GKChunk* GetNeighborBack() const;
	inline const GKChunk* GetNeighborLeftBack() const;

	inline const GKChunk* GetNeighborRightBack() const;
	inline const GKChunk* GetNeighborLeftForward() const;

	inline GKSection* MiddleSection() const
	{
		return ArraySections[WorldSettings->GetChunkMiddleSection()];
	}
};

