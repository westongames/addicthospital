// Fill out your copyright notice in the Description page of Project Settings.


#include "GKRegion.h"
#include "GKChunk.h"
#include "Gimka/Tools/Misc/GKCompress.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "GKWorldVoxel.h"

GKRegion::GKRegion(GKWorldVoxel* iWorld, const FIntPoint& iRegionPos)
{
    World = iWorld;
    RegionPos = iRegionPos;
    WorldSettings = &World->GetSettings();

    ArrayChunks = new GKChunk*[WorldSettings->GetRegionAreaChunks()];

    const int32 chunkNumTiles = WorldSettings->GetChunkNumTiles();
    GK_ITERATE_XY(int32, 16, 16)
    GKChunk* chunk = new GKChunk(this, 
        FIntPoint(RegionPos.X + x * chunkNumTiles, RegionPos.Y + y * chunkNumTiles));
    World->AddEntityManager(chunk);
    GK_ITERATE_XY_END
}

GKRegion::~GKRegion()
{
    SAFE_DESTROY_ARRAY(ArrayChunks, WorldSettings->GetRegionAreaChunks());
}

//void GKRegion::SetNeighbors(GKRegion* Right, GKRegion* Forward, GKRegion* RightForward)
//{
//    SetNeighborRight(Right);
//    SetNeighborForward(Forward);
//    SetNeighborRightForward(RightForward);
//}

void GKRegion::SetNeighborRight(GKRegion* Right)
{
    if (NeighborRight != Right)
    {
        NeighborRight = Right;
        Right->SetNeighborLeft(this);
    }
}

void GKRegion::SetNeighborForward(GKRegion* Forward)
{
    if (NeighborForward != Forward)
    {
        NeighborForward = Forward;
        Forward->SetNeighborBack(this);
    }
}

void GKRegion::SetNeighborRightForward(GKRegion* RightForward)
{
    if (NeighborRightForward != RightForward)
    {
        NeighborRightForward = RightForward;
        RightForward->SetNeighborLeftBack(this);
    }
}

void GKRegion::SetNeighborLeft(GKRegion* Left)
{
    if (NeighborLeft != Left)
    {
        NeighborLeft = Left;
        Left->SetNeighborRight(this);
    }
}

void GKRegion::SetNeighborBack(GKRegion* Back)
{
    if (NeighborBack != Back)
    {
        NeighborBack = Back;
        Back->SetNeighborForward(this);
    }
}

void GKRegion::SetNeighborLeftBack(GKRegion* LeftBack)
{
    if (NeighborLeftBack != LeftBack)
    {
        NeighborLeftBack = LeftBack;
        LeftBack->SetNeighborRightForward(this);
    }
}

void GKRegion::SetNeighborRightBack(GKRegion* RightBack)
{
    if (NeighborRightBack != RightBack)
    {
        NeighborRightBack = RightBack;
        RightBack->SetNeighborLeftForward(this);
    }
}

void GKRegion::SetNeighborLeftForward(GKRegion* LeftForward)
{
    if (NeighborLeftForward != LeftForward)
    {
        NeighborLeftForward = LeftForward;
        LeftForward->SetNeighborRightBack(this);
    }
}

uint32 GKRegion::GetHashKey()
{
    return GKCompress::IntPointToUInt32(RegionPos);
}
