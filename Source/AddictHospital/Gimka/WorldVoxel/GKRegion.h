// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "GKWorldVoxelSettings.h"

class GKWorldVoxel;
class GKChunk;

/**
 * 
 */
class GKRegion
{
public:
	GKRegion(GKWorldVoxel* World, const FIntPoint& RegionPos);
	~GKRegion();

	uint32 GetHashKey();

	//void SetNeighbors(GKRegion* Right, GKRegion* Forward, GKRegion* RightForward);
	void SetNeighborRight(GKRegion* Right);
	void SetNeighborForward(GKRegion* Forward);
	void SetNeighborRightForward(GKRegion* RightForward);

	void SetNeighborLeft(GKRegion* Left);
	void SetNeighborBack(GKRegion* Back);
	void SetNeighborLeftBack(GKRegion* LeftBack);

	void SetNeighborRightBack(GKRegion* RightBack);
	void SetNeighborLeftForward(GKRegion* LeftForward);

private:
	FIntPoint RegionPos;

	GKWorldVoxel* World;								// Ref to World
	const GKWorldVoxelSettings* WorldSettings;				// Ref to worldVoxelSettings

	GKChunk** ArrayChunks;									// The 2D chunk arrays

	GKRegion* NeighborRight = nullptr;						// X + 1
	GKRegion* NeighborForward = nullptr;					// Y + 1
	GKRegion* NeighborRightForward = nullptr;				// X + 1, Y + 1

	GKRegion* NeighborLeft = nullptr;						// X - 1
	GKRegion* NeighborBack = nullptr;						// Y - 1
	GKRegion* NeighborLeftBack = nullptr;					// X - 1, Y - 1
	
	GKRegion* NeighborRightBack = nullptr;					// X + 1, Y - 1
	GKRegion* NeighborLeftForward = nullptr;				// X - 1, Y + 1

public:	// Accessor
	GK_GETCR(FIntPoint, RegionPos);
	GKPTR_GETC(GKWorldVoxel, World);
	GKPTR_GETC(GKWorldVoxelSettings, WorldSettings);

	GKPTR_GETC(GKRegion, NeighborRight);
	GKPTR_GETC(GKRegion, NeighborForward);
	GKPTR_GETC(GKRegion, NeighborRightForward);

	GKPTR_GETC(GKRegion, NeighborLeft);
	GKPTR_GETC(GKRegion, NeighborBack);
	GKPTR_GETC(GKRegion, NeighborLeftBack);

	GKPTR_GETC(GKRegion, NeighborRightBack);
	GKPTR_GETC(GKRegion, NeighborLeftForward);

	inline GKChunk* GetChunkFromIndex(int32 chunkIndex) const
	{
		return ArrayChunks[chunkIndex];
	}

	inline GKChunk* GetChunkFromIndex(int32 chunkIndexX, int32 chunkIndexY) const
	{
		return ArrayChunks[chunkIndexY * WorldSettings->GetRegionNumChunks() + chunkIndexX];
	}

};
