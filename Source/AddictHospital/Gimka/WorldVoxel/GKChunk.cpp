// Fill out your copyright notice in the Description page of Project Settings.


#include "GKChunk.h"
#include "Gimka/Tools/Misc/GKCompress.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"

//GKIntVector GKChunk::RegionSize;

GKChunk::GKChunk(const GKRegion* iRegion, const FIntPoint& iChunkPos)
	: GKEntityManager(iRegion->GetWorld()->GetGameManager())
{
	this->ChunkPos = iChunkPos;
	//this->RegionIndex = RegionIndex;
	this->Region = iRegion;
	this->WorldSettings = Region->GetWorldSettings();
	check(ChunkPos.X >= Region->GetRegionPos().X);
	check(ChunkPos.X + WorldSettings->GetChunkNumTiles() <= Region->GetRegionPos().X + WorldSettings->GetRegionNumTiles());
	check(ChunkPos.Y >= Region->GetRegionPos().Y);
	check(ChunkPos.Y + WorldSettings->GetChunkNumTiles() <= Region->GetRegionPos().Y + WorldSettings->GetRegionNumTiles());

	CREATE_ARRAY_ZERO(ArraySections, GKSection, WorldSettings->GetChunkNumSections());
	//ArraySections = new GKSection*[WorldSettings->GetChunkNumSections()];
	//FMemory::Memzero(ArraySections, 

	// Create the middle section
	ArraySections[WorldSettings->GetChunkMiddleSection()] = new GKSection(this);

	for (int i = 0; i < WorldSettings->GetChunkNumSections(); ++i)
		if (ArraySections[i] != nullptr)
		{
			GKLog1("Array is not null!", i);
		}
}

GKChunk::~GKChunk()
{
	SAFE_DESTROY_ARRAY(ArraySections, WorldSettings->GetChunkNumSections());
}

//void GKChunk::InitializeStatic(const GKIntVector& iRegionSize)
//{
//	//RegionSize = iRegionSize;
//}
uint32 GKChunk::GetHashKey()
{
	return GKCompress::IntPointToUInt32(ChunkPos);
}

void GKChunk::Tick(float DeltaTime)
{
	//GKEntityManager::Tick(DeltaTime);
}

void GKChunk::ApplyChanges()
{
}
void GKChunk::OnJobPostFrame(const GKJobSystem2::JobArgs& jobArgs)
{
	// TODO: no need to do this every turn. Only when camera changed.

	GKGroundVisualManager::ChunkVisibility NewChunkVisibility =
		GameManager->GetGameEngine()->GetGroundVisualManager()->CalculateChunkVisibility(ChunkCenter());

	if (ChunkVisibility != NewChunkVisibility)
	{
		GKLog2("RegionVisibility from ", ChunkVisibility, NewChunkVisibility);

		if (NewChunkVisibility == GKGroundVisualManager::ChunkVisibility::On)
		{
			// Make chunk visible (create actor)
			SpriteInfo.SetVisible(true);

			// Make all entities visible
			for (GKVisualEntity* Entity : ArrayVisualEntities)
			{
				Entity->SetVisible(true);
			}

			//bVisible = true;
		}
		else if (NewChunkVisibility == GKGroundVisualManager::ChunkVisibility::Off)
		{
			// Make chunk not visible (destroy actor)
			SpriteInfo.SetVisible(false);

			// Make all entities not visible (destroy actors)
			for (GKVisualEntity* Entity : ArrayVisualEntities)
			{
				Entity->SetVisible(false);
			}

			//bVisible = false;
		}

		ChunkVisibility = NewChunkVisibility;
	}
}

const GKChunk* GKChunk::GetNeighborRight() const
{
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.X + 1 < WorldSettings->GetRegionNumChunks())
	{
		return Region->GetChunkFromIndex(Index.X + 1, Index.Y);
	};

	const GKRegion* RegionRight = Region->GetNeighborRight();
	if (RegionRight)
	{
		return RegionRight->GetChunkFromIndex(0, Index.Y);
	}

	return nullptr;
}

const GKChunk* GKChunk::GetNeighborForward() const
{
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.Y + 1 < WorldSettings->GetRegionNumChunks())
	{
		return Region->GetChunkFromIndex(Index.X, Index.Y + 1);
	};

	const GKRegion* RegionForward = Region->GetNeighborForward();
	if (RegionForward)
	{
		return RegionForward->GetChunkFromIndex(Index.X, 0);
	}

	return nullptr;
}

const GKChunk* GKChunk::GetNeighborRightForward() const
{
	const int32 regionNumChunks = WorldSettings->GetRegionNumChunks();
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.X + 1 < regionNumChunks)
	{
		// X is inside
		if (Index.Y + 1 < regionNumChunks)
		{
			// X is inside, Y is inside
			return Region->GetChunkFromIndex(Index.X + 1, Index.Y + 1);
		}
		else
		{
			// X is inside, Y is outside
			const GKRegion* RegionForward = Region->GetNeighborForward();
			if (RegionForward)
			{
				return RegionForward->GetChunkFromIndex(Index.X + 1, 0);
			}

			return nullptr;
		}
	};

	// X is outside
	if (Index.Y + 1 < regionNumChunks)
	{
		// X is outside, Y is inside
		const GKRegion* RegionRight = Region->GetNeighborRight();
		if (RegionRight)
		{
			return RegionRight->GetChunkFromIndex(0, Index.Y + 1);
		}

		return nullptr;
	}

	// X is outside, Y is outside
	const GKRegion* RegionRightForward = Region->GetNeighborRightForward();
	if (RegionRightForward)
	{
		return RegionRightForward->GetChunkFromIndex(0, 0);
	}

	return nullptr;
}

inline const GKChunk* GKChunk::GetNeighborLeft() const
{
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.X - 1 >= 0)
	{
		return Region->GetChunkFromIndex(Index.X - 1, Index.Y);
	};

	const GKRegion* RegionLeft = Region->GetNeighborLeft();
	if (RegionLeft)
	{
		return RegionLeft->GetChunkFromIndex(WorldSettings->GetRegionNumChunks()-1, Index.Y);
	}

	return nullptr;
}

inline const GKChunk* GKChunk::GetNeighborBack() const
{
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.Y - 1 >= 0)
	{
		return Region->GetChunkFromIndex(Index.X, Index.Y - 1);
	};

	const GKRegion* RegionBack = Region->GetNeighborBack();
	if (RegionBack)
	{
		return RegionBack->GetChunkFromIndex(Index.X, WorldSettings->GetRegionNumChunks()-1);
	}

	return nullptr;
}

inline const GKChunk* GKChunk::GetNeighborLeftBack() const
{
	const int32 regionNumChunks = WorldSettings->GetRegionNumChunks();
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.X - 1 >= 0)
	{
		// X is inside
		if (Index.Y - 1 >= 0)
		{
			// X is inside, Y is inside
			return Region->GetChunkFromIndex(Index.X - 1, Index.Y - 1);
		}
		else
		{
			// X is inside, Y is outside
			const GKRegion* RegionBack = Region->GetNeighborBack();
			if (RegionBack)
			{
				return RegionBack->GetChunkFromIndex(Index.X - 1, regionNumChunks - 1);
			}

			return nullptr;
		}
	};

	// X is outside
	if (Index.Y - 1 >= 0)
	{
		// X is outside, Y is inside
		const GKRegion* RegionLeft = Region->GetNeighborLeft();
		if (RegionLeft)
		{
			return RegionLeft->GetChunkFromIndex(regionNumChunks - 1, Index.Y - 1);
		}

		return nullptr;
	}

	// X is outside, Y is outside
	const GKRegion* RegionLeftBack = Region->GetNeighborLeftBack();
	if (RegionLeftBack)
	{
		return RegionLeftBack->GetChunkFromIndex(regionNumChunks - 1, regionNumChunks - 1);
	}

	return nullptr;
}

inline const GKChunk* GKChunk::GetNeighborRightBack() const
{
	const int32 regionNumChunks = WorldSettings->GetRegionNumChunks();
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.X + 1 < regionNumChunks)
	{
		// X is inside
		if (Index.Y - 1 >= 0)
		{
			// X is inside, Y is inside
			return Region->GetChunkFromIndex(Index.X + 1, Index.Y - 1);
		}
		else
		{
			// X is inside, Y is outside
			const GKRegion* RegionBack = Region->GetNeighborBack();
			if (RegionBack)
			{
				return RegionBack->GetChunkFromIndex(Index.X + 1, regionNumChunks - 1);
			}

			return nullptr;
		}
	};

	// X is outside
	if (Index.Y - 1 >= 0)
	{
		// X is outside, Y is inside
		const GKRegion* RegionRight = Region->GetNeighborRight();
		if (RegionRight)
		{
			return RegionRight->GetChunkFromIndex(0, Index.Y - 1);
		}

		return nullptr;
	}

	// X is outside, Y is outside
	const GKRegion* RegionRightBack = Region->GetNeighborRightBack();
	if (RegionRightBack)
	{
		return RegionRightBack->GetChunkFromIndex(0, regionNumChunks - 1);
	}

	return nullptr;
}

inline const GKChunk* GKChunk::GetNeighborLeftForward() const
{
	const int32 regionNumChunks = WorldSettings->GetRegionNumChunks();
	FIntPoint Index = GetChunkIndexInRegion();
	if (Index.X - 1 >= 0)
	{
		// X is inside
		if (Index.Y + 1 < regionNumChunks)
		{
			// X is inside, Y is inside
			return Region->GetChunkFromIndex(Index.X - 1, Index.Y + 1);
		}
		else
		{
			// X is inside, Y is outside
			const GKRegion* RegionForward = Region->GetNeighborForward();
			if (RegionForward)
			{
				return RegionForward->GetChunkFromIndex(Index.X - 1, 0);
			}

			return nullptr;
		}
	};

	// X is outside
	if (Index.Y + 1 < regionNumChunks)
	{
		// X is outside, Y is inside
		const GKRegion* RegionLeft = Region->GetNeighborLeft();
		if (RegionLeft)
		{
			return RegionLeft->GetChunkFromIndex(regionNumChunks - 1, Index.Y + 1);
		}

		return nullptr;
	}

	// X is outside, Y is outside
	const GKRegion* RegionLeftForward = Region->GetNeighborLeftForward();
	if (RegionLeftForward)
	{
		return RegionLeftForward->GetChunkFromIndex(regionNumChunks - 1, 0);
	}

	return nullptr;
}