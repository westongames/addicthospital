// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/WorldVoxel/GKWorldVoxelSettings.h"
#include "Gimka/Tools/Macros/GKSource.h"

GKWorldVoxelSettings::GKWorldVoxelSettings()
{
}

GKWorldVoxelSettings::~GKWorldVoxelSettings()
{
}

void GKWorldVoxelSettings::Initialize(
	int32 regionBitNumChunks, int32 chunkBitNumSections, int32 chunkBitNumTiles, int32 tileBitNumZVoxels
	//int32 sectionBitNumZTiles, 
	//int32 chunkBitNumSections, int32 chunkBitNumTiles, 
	//int32 regionBitNumChunks
)
{
	/*	Settings.Initialize(
	*		4,					// regionBitNumChunk = 4
	*		4,					// chunkBitNumSections = 4
	*		4,					// chunkBitNumTiles = 4
	*		3					// tilenBitNumZVoxels = 4
	
	
	3,					// sectionBitNumZTiles
			4, 4,								// chunkBitNumSections, chunkBitNumTiles
			4);									// regionBitNumChunk*/

	// RegionBitNumChunks = 4, RegionNumChunks = 16
	// RegionBitAreaChunks = 8, RegionAreaChunks = 256
	GK_INIT_BITAREA(Region, Chunks, regionBitNumChunks);

	// regionBitNumTiles = 8, regionNumTiles = 256
	// regionBitAreaTiles = 16, regionAreaTiles = 64k
	// regionBitNumZTiles = 4, regionNumZTiles = 16
	// regionBitVolTiles = 16+4 = 20, regionBitTiles = 1m
	GK_INIT_BITVOLZ(Region, Tiles, (regionBitNumChunks + chunkBitNumTiles), chunkBitNumSections);

	// ChunkBitNumSections = 4, ChunkNumSections = 16
	GK_INIT_BITNUM(Chunk, Sections, chunkBitNumSections);
	GK_INIT_BIT(Chunk, Middle, Section, (chunkBitNumSections - 1));
	
	// ChunkBitNumTiles = 4, ChunkNumTiles = 16
	// ChunkBitAreaTiles = 8, ChunkAreaTiles = 256
	// ChunkBitNumZTiles = 4, ChunkNumZTiles = 16
	// ChunkBitVolTiles = 8+4 = 12, ChunkVolTiles = 4k
	GK_INIT_BITVOLZ(Chunk, Tiles, chunkBitNumTiles, (chunkBitNumSections + tileBitNumZVoxels));

	// SectionBitNumTiles = 4, SectionNumTiles = 16
	// SectionBitAreaTils = 8, SectionAreaTiles = 256
	GK_INIT_BITAREA(Section, Tiles, chunkBitNumTiles);

	// TileBitNumZVoxels = 3, TileNumZVoxels = 8
	GK_INIT_BITNUMZ(Tile, Voxels, tileBitNumZVoxels);
}

void GKWorldVoxelSettings::SetVoxelBitNumMinis(int32 voxelBitNumMinis)
{
	GK_INIT_BITVOL(Voxel, Minis, voxelBitNumMinis);
}
