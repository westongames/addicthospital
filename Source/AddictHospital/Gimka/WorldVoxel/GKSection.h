// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKTile.h"

class GKChunk;

/**
 * 
 */
struct GKSection
{
	GKTile* Array2DTiles;
	const GKChunk* Parent;

public:
	GKSection(const GKChunk* Parent);
	~GKSection();

	inline GKTile& GetTile(int32 x, int32 y);
	inline const GKTile* Tiles() const { return Array2DTiles; }

	void GenerateRandom();
};
