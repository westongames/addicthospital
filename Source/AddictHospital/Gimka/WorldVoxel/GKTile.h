// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Bits/GKBits.h"

/**
 * A tile is 1x1x8 tiles
 * If x-res: 16x16x128 voxels
 * If hi-res: 8x8x64 voxels
 * If lo-res: 4x4x32 voxels
 */
struct GKTile
{
	uint8 Status = 0;
public:
	GKTile();
	~GKTile();

	GKBIT_GETSET(Walkable, Status, 1);

	//FORCEINLINE SetWalkable

	//uint8 height;			// height in voxels, so yes different res will have different height
	//GKBits<uint8> status;
};
