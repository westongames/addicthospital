// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/System/GKManager.h"
#include "Gimka/UnrealEngine/GKNetworkSerialize.h"
#include "Gimka/Containers/Array/GKSortedArray.h"

class GKNetworkManager;

/**
 * 
 */
class GKNetworkPlayer
{
public:
	GKNetworkPlayer();
	virtual ~GKNetworkPlayer();

	void Initialize(GKNetworkManager* NetworkManager, bool IsServerPlayer, bool IsLocalPlayer);
	bool IsLocalPlayer() const { return LocalPlayer; }
	bool IsServerPlayer() const { return ServerPlayer; }
	void Disconnect();

	virtual void RPCClientToServer(FGKNetworkSerializeToServer Message) {};
	virtual void RPCClientToServerReliable(FGKNetworkSerializeToServer Message) {};

	virtual void RPCServerToClient(FGKNetworkSerializeToClient Message) {};
	virtual void RPCServerToClientReliable(FGKNetworkSerializeToClient Message) {};

	virtual void RPCServerToClientCommandReliable(FGKNetworkSerializeToClientCommand Message) {};

	// PlayerID = 0 (not assigned), 1 (server player), 2 - 255 (clients)
	virtual void SetPlayerID(uint8 Value) {}
	virtual uint8 GetPlayerID() const { return 0; }
	inline bool HasMissingCommand();

	void InitMissingCommandNum(uint32 iMissingCommandNum);
	bool GetMissingCommandNextIndexToReceiveAndAdvance(uint32& NextIndex);

	void RetrieveMessage(TArray<GKMessageNetworkToClient>& Inbox);
	void RetrieveMessage(GKSortedArray<uint32, GKMessageNetworkToClientCommand>& Inbox);

protected:
	// Inboxes have to be here because GameEngine might not have yet created/ linked when messages coming in.
	TArray<GKMessageNetworkToClient> InboxMessageNetworkToClient;
	TArray<GKMessageNetworkToClientCommand> InboxMessageNetworkToClientCommand;

	void OnReceiveMessage(const GKMessageNetworkToClient& iMessage);
	void OnReceiveMessage(const GKMessageNetworkToClientCommand& iMessage);

private:
	GKNetworkManager* NetworkManager;
	bool LocalPlayer;
	bool ServerPlayer;				// True if this is server's player

	/////////////////////////////////////////////////////////////////////////////////////////////
	// Valid on server only
	// Server uses these to track which missing messages to send to client.
	/////////////////////////////////////////////////////////////////////////////////////////////
	// Set to the ArchiveCommands.Num when player joined the server. Server will send missing messages (commands) up to the Num.
	uint32 MissingCommandNum = 0;

	// Track the last missing message index sent to this client
	uint32 MissingCommandNextIndexToReceive = 0;

public:
	GK_GET(GKNetworkManager*, NetworkManager);
};