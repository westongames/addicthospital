// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Network/GKNetworkPlayer.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKNetworkPlayer::GKNetworkPlayer()
{
}

GKNetworkPlayer::~GKNetworkPlayer()
{
}

void GKNetworkPlayer::Initialize(GKNetworkManager* iNetworkManager, bool IsServerPlayer, bool iIsLocalPlayer)
{
	NetworkManager = iNetworkManager;
	ServerPlayer = IsServerPlayer;
	LocalPlayer = iIsLocalPlayer;
	GKLog2("NetworkManagerClient networkManager, isLocalPlayer", (iNetworkManager != nullptr), iIsLocalPlayer);
}

void GKNetworkPlayer::Disconnect()
{
	NetworkManager = nullptr;
}

void GKNetworkPlayer::InitMissingCommandNum(uint32 iMissingCommandNum)
{
	check(MissingCommandNum == 0);
	check(MissingCommandNextIndexToReceive == 0);
	MissingCommandNum = iMissingCommandNum;
}

bool GKNetworkPlayer::GetMissingCommandNextIndexToReceiveAndAdvance(uint32& NextIndex)
{
	if (MissingCommandNextIndexToReceive < MissingCommandNum)
	{
		NextIndex = MissingCommandNextIndexToReceive++;
		GKLog3("Player ID, NextIndex, CommandNum", (uint32)GetPlayerID(), MissingCommandNextIndexToReceive, MissingCommandNum);
		return true;
	}
	return false;
}

void GKNetworkPlayer::OnReceiveMessage(const GKMessageNetworkToClient& iMessage)
{
	InboxMessageNetworkToClient.Add(iMessage);// GameEngine->GetGameManager()->PostGameMessage(iMessage);
}

void GKNetworkPlayer::OnReceiveMessage(const GKMessageNetworkToClientCommand& iMessage)
{
	//GKLog1("------------------------- Receive Message Command!", iMessage.GetGameTick());
	InboxMessageNetworkToClientCommand.Add(iMessage);
	//GameEngine->GetGameManager()->PostGameMessage(iMessage);
}

void GKNetworkPlayer::RetrieveMessage(TArray<GKMessageNetworkToClient>& Inbox)
{
	Inbox.Append(InboxMessageNetworkToClient);
	InboxMessageNetworkToClient.Empty();
}


void GKNetworkPlayer::RetrieveMessage(GKMessageNetworkToClientCommand::SortedArray& Inbox)
{
	if (InboxMessageNetworkToClientCommand.Num() > 0)
	{
		GKLog1("InboxMessageNetworkToClientCommand: ", InboxMessageNetworkToClientCommand.Num());
	}
	for (const GKMessageNetworkToClientCommand& Message : InboxMessageNetworkToClientCommand)
	{
		check(!Inbox.ContainID(Message.GetID()));
		Inbox.Add(Message);
	}
	//Inbox.Append(InboxMessageNetworkToClientCommand);
	InboxMessageNetworkToClientCommand.Empty();
}

