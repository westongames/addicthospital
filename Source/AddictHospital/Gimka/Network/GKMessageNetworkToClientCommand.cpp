// Fill out your copyright notice in the Description page of Project Settings.


#include "GKMessageNetworkToClientCommand.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKMessageNetworkToClientCommand::GKMessageNetworkToClientCommand(uint32 iType, uint32 GameTick)
	: GKMessageNetwork(iType, GameTick, true)
{
}

GKMessageNetworkToClientCommand::~GKMessageNetworkToClientCommand()
{
}

void GKMessageNetworkToClientCommand::Append(const GKMessageNetworkToClientCommand& Message)
{
	Commands.Append(Message.Commands);
}

bool GKMessageNetworkToClientCommand::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	GKLog1("GKMessageNetworkToClientCommand", bOutSuccess);

	if (!GKMessageNetwork::NetSerialize(Ar, Map, bOutSuccess)) return false;

	uint8 Num = (uint8)Commands.Num();
	Ar.Serialize(&Num, 1);

	if (Commands.Num() < Num)
	{
		Commands.AddDefaulted(Num - Commands.Num());
	}
	for (int32 i = 0, Count = Num; i < Count; ++i) 
	{
		Commands[i].Serialize(Ar);
	}

	GKLog1("GKMessageNetworkToClientCommandDone", bOutSuccess);
	bOutSuccess = true;
	return true;
}
