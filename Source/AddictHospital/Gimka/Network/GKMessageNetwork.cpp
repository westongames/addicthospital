// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Network/GKMessageNetwork.h"

GKMessageNetwork::GKMessageNetwork(uint32 iType, uint32 iGameTick, bool iReliable)
{
	MessageType = iType;
	GameTick = iGameTick;
	Reliable = iReliable;
}

GKMessageNetwork::~GKMessageNetwork()
{
}

bool GKMessageNetwork::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	// pack BaseType
	//Ar.SerializeBits(&BaseType, 2);
	//BaseType &= 3;				// Only the first 2 bits matter. If this was reading, make sure other bits are zeroed out.

	Ar.SerializeIntPacked(MessageType);

	Ar.SerializeIntPacked(GameTick);

	bOutSuccess = true;

	return true;
}
