// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Network/GKMessageNetwork.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Game/GKCommand.h"


/**
 * 
 */
struct GKMessageNetworkToServerCommand : public GKMessageNetwork
{
public:
	GKMessageNetworkToServerCommand() {}
	GKMessageNetworkToServerCommand(uint32 iType, uint32 GameTick, bool iReliable);
	~GKMessageNetworkToServerCommand();

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

private:
	GKCommand Command;			// Server will send PlayerInput so client can execute it.

};
