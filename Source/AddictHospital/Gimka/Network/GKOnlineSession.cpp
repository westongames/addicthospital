// Fill out your copyright notice in the Description page of Project Settings.


#include "GKOnlineSession.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/UnrealEngine/GKGameInstance.h"

#include "OnlineSubsystem.h"
#include "OnlineSessionSettings.h"

const static FName SESSION_NAME_KEY = TEXT("SessionName");

GKOnlineSession::GKOnlineSession(UGKGameInstance* iGameInstance)
{
	GameInstance = iGameInstance;
}

GKOnlineSession::~GKOnlineSession()
{
}

bool GKOnlineSession::CreateSessionInterface()
{
	check(SessionInterface == nullptr);

	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (!Subsystem)
	{
		GKLog("Found no subsystem!");
		return false;
	}

	FName name = Subsystem->GetSubsystemName();
	SubsystemName = name.ToString().ToLower();
	GKLog1("Found subsystem", name.ToString());
	SessionInterface = Subsystem->GetSessionInterface();
	if (!SessionInterface.IsValid())
	{
		GKLog("Can't get SessionInterface");
		return false;
	}

	// Don't know a better way to do this. For now just use this.
	if (name == STEAM_SUBSYSTEM)
		//SubsystemName.Equals(FString("STEAM"), ESearchCase::IgnoreCase))
	{
		Connection = ConnectionType::Steam;
	}
	else if (name == NULL_SUBSYSTEM)
	{
		Connection = ConnectionType::Null;
	}
	else
	{
		GKError1("Unknown Subsystem", SubsystemName);
		Connection = ConnectionType::Null;
	}

	GameInstance->OnFoundSubsystem(SubsystemName);
	
	return true;
}

bool GKOnlineSession::IsSessionInterfaceValid() const
{
	return SessionInterface.IsValid();
}

const FString& GKOnlineSession::GetSubsystemName() const
{
	return SubsystemName;
}

bool GKOnlineSession::ConnectionTypeIsNull() const
{
	return Connection == ConnectionType::Null;
}

void GKOnlineSession::ListenToNetworkFailure()
{
	// Chapter 76: https://www.udemy.com/course/unrealmultiplayer/learn/lecture/23996124#questions/12850327
	// If the server disconnect, the client freeze. This is to prevent that but it doesn't freeze on our version,
	// so we didn't implement this.
	GKEnsure(GEngine);
	GEngine->OnNetworkFailure().AddRaw(this, &GKOnlineSession::OnNetworkFailure);
}

void GKOnlineSession::OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString)
{
	GKLog1("OnNetworkFailure", ErrorString);
	// Simply go back to Main Menu or show error message
}

bool GKOnlineSession::CreateSession(const FString& SessionName)
{
	GKLog1("CreateSession", SessionName);
	if (!SessionInterface.IsValid())
	{
		if (!CreateSessionInterface()) return false;
	}

	AfterDestroy = AfterDestroyType::Create;// = true;
	if (DestroySession())
	{
		GKLog1("DestroySession successful, RecreateSessionName", SessionName);
		RecreateSessionName = SessionName;			// Save SessionName so if we need to destroy first, we can use this again!
		// Recreate the session if Destroy successful!
		return true;
	}
	AfterDestroy = AfterDestroyType::None;// bRecreateSessionAfterDestroy = false;

	GKLog("Creating Session");
	// There's really no reason to clear and re-add the callback but whatev.
	//	// https://www.udemy.com/course/unrealmultiplayer/learn/lecture/8111592#questions
	//	// TMulticastDelegate_TwoParams<void, FName, bool> FOnCreateSessionComplete
	//	// https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Delegates/Multicast/
	//	//FOnCreateSessionCompleteDelegate
	//	SessionInterface->OnCreateSessionCompleteDelegates.AddRaw(this, &GKOnlineSession::OnCreateSessionComplete);
	ClearCallbacks();
	SessionInterface->OnCreateSessionCompleteDelegates.AddRaw(this, &GKOnlineSession::OnCreateSessionComplete);

	FOnlineSessionSettings SessionSettings;

	if (Connection == ConnectionType::Steam)
	{
		SessionSettings.bIsLANMatch = false;
		SessionSettings.bUsesPresence = true;
	}
	else
	{
		SessionSettings.bIsLANMatch = true;
	}

	// This is how you set custom data, which can be advertised
	SessionSettings.Set(SESSION_NAME_KEY, SessionName, EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

	SessionSettings.NumPublicConnections = MaxPlayers;
	SessionSettings.bShouldAdvertise = true;

	// CreateSession is async
	SessionInterface->CreateSession(0, NAME_GameSession/*TEXT("GKSessionName")*/, SessionSettings);

	return true;
}

bool GKOnlineSession::FindSessions()
{
	if (!SessionInterface.IsValid())
	{
		if (!CreateSessionInterface()) return false;
	}

	// https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/SmartPointerLibrary/SharedPointer
	SessionSearch = MakeShareable(new FOnlineSessionSearch());
	if (!SessionSearch.IsValid()) return false;

	GKLog("Finding Sessions");

	if (Connection == ConnectionType::Steam)
	{
		// Note: because we're using a shared AppID (Configs/DefaultEngine.ini 480 = SpaceWars),
		// the result of the search will include other games. The parsing will fail because it's a different game.
		// We won't have this problem if we uses our own AppID.
		// But for now, We need to increase MaxSearchResults (by default it's only 1), so it will return multiple
		// results and hopefully some of them are this game.
		// Note: this seems to not return any session that is already full. Might want to return it but make it disable?
		SessionSearch->MaxSearchResults = 100;
		SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	}
	else
	{
		SessionSearch->bIsLanQuery = true;
	}

	// SessionSearch->QuerySettings. specific to type of onlinesubsystem
	ClearCallbacks();
	SessionInterface->OnFindSessionsCompleteDelegates.AddRaw(this, &GKOnlineSession::OnFindSessionComplete);

	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	return true;
}

bool GKOnlineSession::JoinSession(uint32 Index)
{
	if (!SessionInterface.IsValid()) return false;
	if (!SessionSearch.IsValid()) return false;
	if (Index >= (uint32)SessionSearch->SearchResults.Num()) return false;

	AfterDestroy = AfterDestroyType::Join;// = true;
	if (DestroySession())
	{
		GKLog1("DestroySession successful, RejoinSessionIndex", Index);
		RejoinSessionIndex = Index;		// Save Index so after destroy we can call this again
		//= SessionName;			// Save SessionName so if we need to destroy first, we can use this again!
		// Recreate the session if Destroy successful!
		return true;
	}
	AfterDestroy = AfterDestroyType::None;// bRecreateSessionAfterDestroy = false;

	/*auto ExistingSession = SessionInterface->GetNamedSession(NAME_GameSession);
	if (ExistingSession != nullptr)
	{
		GKLog("Session already existing!");
	}*/

	GKLog1("Join Session", Index);
	ClearCallbacks();
	SessionInterface->OnJoinSessionCompleteDelegates.AddRaw(this, &GKOnlineSession::OnJoinSessionComplete);
	SessionInterface->JoinSession(0, NAME_GameSession, SessionSearch->SearchResults[Index]);
	return true;
}

bool GKOnlineSession::DestroySession()
{
	if (!SessionInterface.IsValid())
	{
		// Nothing to destroy
		return true;
	}

	auto ExistingSession = SessionInterface->GetNamedSession(NAME_GameSession);
	if (ExistingSession == nullptr)
	{
		// No existingSession
		return false;
	}

	GKLog("Destroying Session");
	// There's really no reason to clear and re-add the callback but whatev.
	//bRecreateSessionAfterDestroy = false;		// Default to not recreate
	ClearCallbacks();
	SessionInterface->OnDestroySessionCompleteDelegates.AddRaw(this, &GKOnlineSession::OnDestroySessionComplete);

	// This is Async
	SessionInterface->DestroySession(NAME_GameSession);
	return true;
}

void GKOnlineSession::StartSession()
{
	check(SessionInterface.IsValid());
	SessionInterface->StartSession(NAME_GameSession);
}


void GKOnlineSession::OnCreateSessionComplete(FName SessionName, bool Success)
{
	// We are not keeping the callback just in case the GKOnlineSession is destroyed.
	ClearCallbacks();

	GameInstance->OnCreateSessionComplete(SessionName, Success);
}

void GKOnlineSession::OnFindSessionComplete(bool Success)
{
	GKLog1("Finished Find Sessions", Success);

	// We are not keeping the callback just in case the GKOnlineSession is destroyed.
	ClearCallbacks();

	// https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/TArrays
	if (!Success || !SessionSearch.IsValid())
	{
		// TODO: notify GameInstance?
		GameInstance->OnFindSessionsComplete(TArray<SessionData>(), false);
		return;
	}

	TArray<SessionData> Sessions;
	for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found session names: %s"), *SearchResult.GetSessionIdStr());
		SessionData Session;
		Session.TotalPlayers = SearchResult.Session.SessionSettings.NumPublicConnections;
		Session.CurrentPlayers = Session.TotalPlayers - SearchResult.Session.NumOpenPublicConnections;
		Session.HostUsername = SearchResult.Session.OwningUserName;
		Session.Ping = SearchResult.PingInMs;

		// This is how you get custom data
		FString SessionName;
		if (SearchResult.Session.SessionSettings.Get(SESSION_NAME_KEY, SessionName))
		{
			//GKLog1("Data found in settings", SessionName);
			Session.Name = SessionName;
		}
		else
		{
			//GKLog("Data not found in setting");
			Session.Name = SearchResult.GetSessionIdStr();
		}
		//GKLog1("Ping = ", Session.Ping);

		Sessions.Add(Session);

	}

	GKLog1("Find Session found: ", Sessions.Num());
	GameInstance->OnFindSessionsComplete(Sessions, true);
}

void GKOnlineSession::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{
	// We are not keeping the callback just in case the GKOnlineSession is destroyed.
	ClearCallbacks();

	if (Result != EOnJoinSessionCompleteResult::Success)
	{
		switch (Result)
		{
		case EOnJoinSessionCompleteResult::SessionIsFull:
			GKScreenError("Can't join session: session is full!");
			break;
		case EOnJoinSessionCompleteResult::SessionDoesNotExist:
			GKScreenError("Can't join session: session doesn't exist!");
			break;
		case EOnJoinSessionCompleteResult::CouldNotRetrieveAddress:
			GKScreenError("Can't join session: could not retrieve address!");
			break;
		case EOnJoinSessionCompleteResult::AlreadyInSession:
			GKScreenError("Can't join session: already in session!");
			break;
		case EOnJoinSessionCompleteResult::UnknownError:
			GKScreenError("Can't join session: unknown error!");
			break;
		}
		GameInstance->OnJoinSessionComplete(SessionName, FString(), false);
		return;
	}

	FString Address;
	if (!SessionInterface->GetResolvedConnectString(SessionName, Address))
	{
		GKScreenError1("Can't join session: couldn't get the Address.", Result);
		//GKLog("Couldn't get connect string.");
		GameInstance->OnJoinSessionComplete(SessionName, Address, false);
		//GameInstance->OnReopenMenu();
		return;
	}

	GameInstance->OnJoinSessionComplete(SessionName, Address, true);


	//GameInstance->OnMenuJoin(Address);
}


// This callback will create the SessionInterface and Host if successful.
void GKOnlineSession::OnDestroySessionComplete(FName SessionName, bool Success)
{
	GKLog1("Destroy Complete ", AfterDestroy);

	// We are not keeping the callback just in case the GKOnlineSession is destroyed.
	ClearCallbacks();

	if (AfterDestroy == AfterDestroyType::Create)
	{
		if (Success)
		{
			GKLog1("RecreateSession", RecreateSessionName);
			CreateSession(RecreateSessionName);
		}
		else
		{
			// Notify GameInstance it failed!
			GameInstance->OnCreateSessionComplete(NAME_GameSession, false);
		}
	}
	else if (AfterDestroy == AfterDestroyType::Join)
	{
		if (Success)
		{
			GKLog1("RejoinSession", RejoinSessionIndex);
			JoinSession(RejoinSessionIndex);
		}
		else
		{
			// Notify GameInstance it failed!
			GameInstance->OnJoinSessionComplete(SessionName, FString(), false);
		}
	} else
	{ 
		// Destroy successful. Done. Or notify GameInstance if needed.
	}
}

void GKOnlineSession::OnDestroy()
{
	ClearCallbacks();
	if (SessionInterface.IsValid())
	{
		SessionInterface.Reset();
	}
}

void GKOnlineSession::ClearCallbacks()
{
	if (SessionInterface.IsValid())
	{
		SessionInterface->OnCreateSessionCompleteDelegates.Clear();
		SessionInterface->OnFindSessionsCompleteDelegates.Clear();
		SessionInterface->OnDestroySessionCompleteDelegates.Clear();
		SessionInterface->OnJoinSessionCompleteDelegates.Clear();
	}
}