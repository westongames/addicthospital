// Fill out your copyright notice in the Description page of Project Settings.


#include "GKMessageNetworkToServer.h"
#include "Misc/Guid.h"

GKMessageNetworkToServer::GKMessageNetworkToServer(uint32 iType, uint32 iGameTick, bool iReliable)
	: GKMessageNetwork(iType, iGameTick, iReliable)
{
}

GKMessageNetworkToServer::~GKMessageNetworkToServer()
{
}

bool GKMessageNetworkToServer::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	if (!GKMessageNetwork::NetSerialize(Ar, Map, bOutSuccess)) return false;
	return true;
}

GKMessageNetworkToServer GKMessageNetworkToServer::CreateJoinGame()
{
	FGuid Guid = FGuid::NewGuid();

	return GKMessageNetworkToServer(MessageType::JoinGame, 0, true);
}
