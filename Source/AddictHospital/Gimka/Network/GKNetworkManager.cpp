// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Network/GKNetworkManager.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Network/GKNetworkPlayer.h"

GKNetworkManager::GKNetworkManager()
	: GKManager(1232)
{
	//GKLogger::Reset();
}

GKNetworkManager::~GKNetworkManager()
{
}

void GKNetworkManager::Initialize(GKGameEngine* iGameEngine)
{
	this->GameEngine = iGameEngine;
}

bool GKNetworkManager::IsServer()
{
	return GameEngine->GetGameManager()->HasAuthority();
}

void GKNetworkManager::OnStartGame()
{
	// Assign playerID to all existing players
	GKLog1("NetworkManager::OnStartGame ", IsServer());
	for (GKNetworkPlayer* Player : NetworkPlayers)
	{
		InitPlayer(Player);
	}
}

void GKNetworkManager::InitPlayer(GKNetworkPlayer* Player)
{
	if (IsServer())
	{
		check(Player->GetPlayerID() == 0);
		if (Player->IsServerPlayer())
		{
			// Server is always PlayerID = 1. Server will assign its own PlayerID by itself.
			Player->SetPlayerID(1);
			//GameEngine->GetGameManager()->SetPlayerID(1);
			GKLog(" ------- Set Player Server");
		}
		else
		{
			Player->SetPlayerID(GetNextPlayerID());
			GKLog1(" -------- Set Player Client ", Player->GetPlayerID());

			//Player->SetMissingCommandNum(GameEngine->GetGameManager)
		}
	}

	if (Player->IsLocalPlayer())
	{
		GKLog1("Setting PlayerID of ", Player->GetPlayerID());
		GKLogger::SetPlayerID(Player->GetPlayerID());
	}

	GameEngine->GetGameManager()->PlayerJoin(Player);
}



void GKNetworkManager::OnUpdateGame(float DeltaTime)
{
	if (GameEngine->GetGameManager()->HasAuthority())
	{
		OnUpdateServer(DeltaTime);
	}
	else
	{
		OnUpdateClient(DeltaTime);
	}
}

void GKNetworkManager::PostMessage(const GKMessageNetworkToServer& iMessage)
{
	OutboxMessageNetworkToServer.Add(iMessage);
}

void GKNetworkManager::PostMessage(const GKMessageNetworkToClient& iMessage, uint8 TargetClientID)
{
	if (TargetClientID == 0)
	{
		OutboxMessageNetworkToAll.Add(iMessage);
	}
	else
	{
		OutboxMessageNetworkToClient.Add(MessageNetworkToOneClient(iMessage, TargetClientID));
	}
}

void GKNetworkManager::PostMessage(const GKMessageNetworkToClientCommand& iMessage, uint8 TargetClientID)
{
	if (TargetClientID == 0)
	{
		OutboxMessageNetworkToAllCommand.Add(iMessage);
	}
	else
	{
		OutboxMessageNetworkToClientCommand.Add(MessageNetworkToOneClientCommand(iMessage, TargetClientID));
	}
}

//bool GKNetworkManager::IsServer()
//{
//	return GameEngine->GetGameManager()->HasAuthority();
//}

int32 GKNetworkManager::FindPlayer(uint8 PlayerID)
{
	for (int32 i = 0, count = NetworkPlayers.Num(); i < count; ++i)
	{
		if (NetworkPlayers[i]->GetPlayerID() == PlayerID) return i;
	}
	return -1;
}


uint8 GKNetworkManager::GetNextPlayerID()
{
	while (FindPlayer(NextPlayerID) != -1)
	{
		if (++NextPlayerID == 0) NextPlayerID = 2;
	}
	return NextPlayerID;
}



void GKNetworkManager::AddNetworkPlayer(GKNetworkPlayer* Player)
{
	check(!NetworkPlayers.Contains(Player));
	NetworkPlayers.Add(Player);

	GKLog1("AddNetworkPlayer: ", NetworkPlayers.Num());

	//GKLog1("Add Network, isServer = ", IsServer());
	if (Player->IsLocalPlayer())
	{
		check(LocalPlayer == nullptr);
		//GKLog1("SetLocalPlayer", IsServer());
		LocalPlayer = Player;
	}

	// GameEngine might not be avaiable yet, so we'll do this later in OnStartGame. But otherwise, set PlayerId here.
	if (GameEngine != nullptr)
	{
		InitPlayer(Player);
	}

	//if (IsServer())
	//{
	//	if (Player->IsLocalPlayer())
	//	{
	//		GKLog1("============================ SetPlayerID for server", 1);
	//		Player->SetPlayerID(1);		// Server is always player 1, this will be replicated to client
	//	}
	//	else
	//	{
	//		// Get the highest playerID now
	//		int32 HighestPlayerID = 1;
	//		for (GKNetworkPlayer* ItrPlayer : NetworkPlayers)
	//		{
	//			if (ItrPlayer->GetPlayerID() > HighestPlayerID)
	//			{
	//				HighestPlayerID = ItrPlayer->GetPlayerID();
	//			}
	//		}
	//		GKLog1("============================ SetPlayerID for client", HighestPlayerID + 1);
	//		Player->SetPlayerID(HighestPlayerID + 1);		// Server set the PlayerID, will be replicated to client
	//	}
	//}

	//bNetworkPlayerChanged = true;
}

void GKNetworkManager::RemoveNetworkPlayer(GKNetworkPlayer* Player)
{
	check(NetworkPlayers.Contains(Player));

	if (GameEngine != nullptr && GameEngine->GetGameManager() != nullptr)
	{
		GameEngine->GetGameManager()->PlayerDisconnected(Player);
	}

	if (Player->IsLocalPlayer())
	{
		check(LocalPlayer == Player);
		LocalPlayer = nullptr;
		//GKLogger::SetPlayerID(0);
	}

	NetworkPlayers.Remove(Player);

	GKLog2("RemoveNetworkPlayer: ", NetworkPlayers.Num(), (int32)Player->GetPlayerID());
}

void GKNetworkManager::OnEndPlay()
{
	// Disconnect all networkPlayers before the NetworkPlayer is getting destroyed and called GKNetworkManager::RemoveNetworkPlayer
	GKLog1("Disconnect all network players: ", NetworkPlayers.Num());
	for (GKNetworkPlayer* Player : NetworkPlayers)
	{
		if (GameEngine != nullptr && GameEngine->GetGameManager() != nullptr)
		{
			GameEngine->GetGameManager()->PlayerDisconnected(Player);
		}

		Player->Disconnect();
	}

	LocalPlayer = nullptr;
	NetworkPlayers.Empty();
}

void GKNetworkManager::OnReceiveMessage(const GKMessageNetworkToServer& iMessage)
{
	InboxMessageNetworkToServer.Add(iMessage);// ->GetGameManager()->PostGameMessage(iMessage);
}

void GKNetworkManager::OnReceiveMessage(const GKMessageNetworkToClient& iMessage)
{
	InboxMessageNetworkToClient.Add(iMessage);// GameEngine->GetGameManager()->PostGameMessage(iMessage);
}

void GKNetworkManager::OnReceiveMessage(const GKMessageNetworkToClientCommand& iMessage)
{
	GKLog1("------------------------- Receive Message Command!", iMessage.GetGameTick());
	InboxMessageNetworkToClientCommand.Add(iMessage);
	//GameEngine->GetGameManager()->PostGameMessage(iMessage);
}

GKNetworkPlayer* GKNetworkManager::FindPlayerPtr(uint8 PlayerID)
{
	for (GKNetworkPlayer* Player : NetworkPlayers)
	{
		if (Player->GetPlayerID() == PlayerID) return Player;
	}
	return nullptr;
}


void GKNetworkManager::OnUpdateServer(float DeltaTime)
{
	if (NetworkPlayers.Num() == 1)
	{
		check(NetworkPlayers[0]->IsLocalPlayer());

		// If alone, no need to broadcast
		OutboxMessageNetworkToAllCommand.Empty();
		OutboxMessageNetworkToClientCommand.Empty();
		OutboxMessageNetworkToAll.Empty();
		return;
	}

	// Send OutboxMessageNetworkToAllCommand
	FGKNetworkSerializeToClientCommand NetworkSerializeToClientCommand;
	for (const GKMessageNetworkToClientCommand& Message : OutboxMessageNetworkToAllCommand)
	{
		NetworkSerializeToClientCommand.SetMessage(Message);
		check(Message.IsReliable());
		RPCServerToAllCommandReliable(NetworkSerializeToClientCommand);
	}
	OutboxMessageNetworkToAllCommand.Empty();

	// Send OutboxMessageNetworkToClientCommand
	for (const MessageNetworkToOneClientCommand& Message : OutboxMessageNetworkToClientCommand)
	{
		NetworkSerializeToClientCommand.SetMessage(Message.Message);
		check(Message.Message.IsReliable());

		GKNetworkPlayer* Player = FindPlayerPtr(Message.TargetClientID);
		if (Player == nullptr) 
		{
			GKError1("Sending OutboxMessageNetworkToClientCommand, can't find player", Message.TargetClientID);
			continue;
		}
		GKLog2("Sending RPCServerToClientCommandReliable Message, to Player", Message.Message.GetMessageType(), (uint32)Player->GetPlayerID());
		Player->RPCServerToClientCommandReliable(NetworkSerializeToClientCommand);
		//RPCServerToAllCommandReliable(NetworkSerializeToClientCommand);
	}
	OutboxMessageNetworkToClientCommand.Empty();

	// Send OutboxMessageNetworkToAll
	FGKNetworkSerializeToClient NetworkSerializeToClient;
	for (const GKMessageNetworkToClient& Message : OutboxMessageNetworkToAll)
	{
		NetworkSerializeToClient.SetMessage(Message);
		if (Message.IsReliable())
		{
			RPCServerToAllReliable(NetworkSerializeToClient);
		}
		else
		{
			RPCServerToAll(NetworkSerializeToClient);	// GameTick);
		}
	}
	OutboxMessageNetworkToAll.Empty();

	// Send OutboxMessageNetworkToClient

}


void GKNetworkManager::OnUpdateClient(float DeltaTime)
{
	FGKNetworkSerializeToServer NetworkSerializeToServer;
	for (const GKMessageNetworkToServer& Message : OutboxMessageNetworkToServer)
	{
		GKLog1("Has Own:", HasOwner());
		NetworkSerializeToServer.SetMessage(Message);
		if (Message.IsReliable())
		{
			LocalPlayer->RPCClientToServerReliable(NetworkSerializeToServer);
		}
		else
		{
			LocalPlayer->RPCClientToServer(NetworkSerializeToServer);	// GameTick);
		}
	}

	OutboxMessageNetworkToServer.Empty();
}

void GKNetworkManager::RetrieveMessage(TArray<GKMessageNetworkToClient>& Inbox)
{
	LocalPlayer->RetrieveMessage(Inbox);
	Inbox.Append(InboxMessageNetworkToClient);
	InboxMessageNetworkToClient.Empty();
}

void GKNetworkManager::RetrieveMessage(TArray<GKMessageNetworkToServer>& Inbox)
{
	Inbox.Append(InboxMessageNetworkToServer);
	InboxMessageNetworkToServer.Empty();
}

void GKNetworkManager::RetrieveMessage(GKMessageNetworkToClientCommand::SortedArray& Inbox)
{
	if (InboxMessageNetworkToClientCommand.Num() > 0)
	{
		GKLog1("InboxMessageNetworkToClientCommand: ", InboxMessageNetworkToClientCommand.Num());
	}
	LocalPlayer->RetrieveMessage(Inbox);
	for (const GKMessageNetworkToClientCommand& Message : InboxMessageNetworkToClientCommand)
	{
		check(!Inbox.ContainID(Message.GetID()));
		Inbox.Add(Message);
	}
	//Inbox.Append(InboxMessageNetworkToClientCommand);
	InboxMessageNetworkToClientCommand.Empty();
}


//void GKNetworkManager::LocalClientSendMessageForServer()
//{
//	
//}
