// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Network/GKMessageNetwork.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Game/GKCommand.h"
#include "Gimka/Containers/Array/GKSortedArray.h"

/**
 * Message to all (including server)
 */
struct GKMessageNetworkToClientCommand : public GKMessageNetwork
{
public:
	enum MessageType
	{
		Empty,
		Command
	};

	GKMessageNetworkToClientCommand() {}
	GKMessageNetworkToClientCommand(uint32 iType, uint32 GameTick);
	virtual ~GKMessageNetworkToClientCommand();

	//virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

	void Add(const GKCommand& Command)
	{
		Commands.Add(Command);
	}

	// For sorting via GKSortedArray
	uint32 GetID() const { return GetGameTick(); }

	void Append(const GKMessageNetworkToClientCommand& Message);

	const TArray<GKCommand>& GetArray() const { return Commands; }

	typedef GKSortedArray<uint32, GKMessageNetworkToClientCommand> SortedArray;

private:
	TArray<GKCommand> Commands;			// Server will send PlayerInput so client can execute it.
	uint32 PrerequisiteCommandGameTick = 0;		// Make sure to do the previous Command message with the specified GameTick before this

public:
	GK_GETSET(uint32, PrerequisiteCommandGameTick);
};

template<>
struct TStructOpsTypeTraits<GKMessageNetworkToClientCommand> : public TStructOpsTypeTraitsBase2<GKMessageNetworkToClientCommand>
{
	enum
	{
		WithNetSerializer = true
	};
};