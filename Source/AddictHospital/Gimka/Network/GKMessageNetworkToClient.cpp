// Fill out your copyright notice in the Description page of Project Settings.


#include "GKMessageNetworkToClient.h"

GKMessageNetworkToClient::GKMessageNetworkToClient(uint32 iType, uint32 iGameTick, bool iReliable)
	: GKMessageNetwork(iType, iGameTick, iReliable)
{
}

GKMessageNetworkToClient::~GKMessageNetworkToClient()
{
}

bool GKMessageNetworkToClient::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	if (!GKMessageNetwork::NetSerialize(Ar, Map, bOutSuccess)) return false;

	Union1.Serialize(Ar);

	/*switch (GetMessageType())
	{
	case MessageType::StartGame:
		Ar.SerializeIntPacked(UInt1);
		break;
	}*/

	return true;
}

GKMessageNetworkToClient GKMessageNetworkToClient::CreateStartGame(uint32 PlayerID)
{
	GKMessageNetworkToClient Message = GKMessageNetworkToClient(MessageType::StartGame, 1, true);
	Message.Union1 = GKUnionStruct(PlayerID);
	return Message;
}

GKMessageNetworkToClient GKMessageNetworkToClient::CreateServerGameTick(uint32 iServerGameTick, uint32 PrerequisiteCommandGameTick)
{
	GKMessageNetworkToClient Message = GKMessageNetworkToClient(MessageType::ServerGameTick, iServerGameTick, true);
	Message.Union1 = GKUnionStruct(PrerequisiteCommandGameTick);
	return Message;
}

void GKMessageNetworkToClient::GetServerGameTick(uint32& ServerGameTick, uint32& PrerequisiteCommandGameTick) const
{
	ServerGameTick = GetGameTick();
	PrerequisiteCommandGameTick = Union1.GetUInt32();
}
