// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * 
 */
struct GKMessageNetwork
{
public:
	/*enum MessageBaseType : uint8
	{
		Empty = 0,
		ToServer = 1,
		ToClient = 2,
		ToAllStart = 3,
	};*/
	GKMessageNetwork() {}
	GKMessageNetwork(uint32 MessageType, uint32 iGameTick, bool iReliable);
	virtual ~GKMessageNetwork();

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess);

	// Need GetID function so we can use GKSortedArray
	uint32 GetID() const { return GameTick; }

protected:
	bool Reliable;				// True to use reliable channel, otherwise use unreliable
	//MessageBaseType BaseType = MessageBaseType::Empty;				// See MessageBaseType
	uint32 MessageType;
	uint32 GameTick;			// The GameTick when this message was sent

public:
	//GK_GET(MessageBaseType, BaseType);
	GK_GET(uint32, MessageType);
	GK_GETSET(uint32, GameTick)
	GK_SET(bool, Reliable);
	
	bool IsReliable() const { return Reliable; };
};
