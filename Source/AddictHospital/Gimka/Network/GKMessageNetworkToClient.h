// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Network/GKMessageNetwork.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Math/GKIntVector.h"
#include "Gimka/Containers/Struct/GKUnion.h"


/**
 * 
 */
struct GKMessageNetworkToClient : public GKMessageNetwork
{
public:
	enum MessageType
	{
		Empty,
		StartGame,						// Start game data, sent at GameTick 0, i.e. map seed, random seed, other playerinfo, etc
		ServerGameTick,					// Sent regularly to allow clients to advance to the Gametick (if client has executed all Commands before it). Otherwise clients will pause.
	};

	GKMessageNetworkToClient() {}
	GKMessageNetworkToClient(uint32 iType, uint32 iGameTick, bool iReliable);
	virtual ~GKMessageNetworkToClient();

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

	static GKMessageNetworkToClient CreateStartGame(uint32 PlayerID);

	// Specify the current ServerGameTick the client allows to advance to if the client already executed all Commands up to LastCommandGameTick
	static GKMessageNetworkToClient CreateServerGameTick(uint32 ServerGameTick, uint32 PrerequisiteCommandGameTick);
	void GetServerGameTick(uint32& ServerGameTIck, uint32& PrerequisiteCommandGameTick) const;

private:
	GKUnionStruct Union1;
};


/*
Client tracks:
- Array of ServerGameTicks: (if LastCommandGameTick is the same with the top one, just update the top one instead of adding entry)
	Element: ServerGameTick, LastCommandGameTick
- LastCommandGameTick: the last command executed's GameTick

To get the MaxGameTick client can advance to:
	Check the Array of ServerGameTicks first entry. If LastCommandGameTick >= entry's LastCommandGameTick, then set MaxGameTick to entry's ServerGameTick.
	Repeat until false.

*/