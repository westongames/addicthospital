// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/System/GKManager.h"

#include "Gimka/Network/GKMessageNetworkToServer.h"
#include "Gimka/Network/GKMessageNetworkToClient.h"
#include "Gimka/UnrealEngine/GKNetworkSerialize.h"
#include "Gimka/Containers/Array/GKSortedArray.h"

class GKGameEngine;
class GKNetworkPlayer;

/**
 * 
 */
class GKNetworkManager : public GKManager
{
public:
	GKNetworkManager();
	virtual ~GKNetworkManager();

	void Initialize(GKGameEngine* GameEngine);

	virtual void OnUpdateGame(float DeltaTime) override;

	void PostMessage(const GKMessageNetworkToServer& iMessage);
	void PostMessage(const GKMessageNetworkToClient& iMessage, uint8 TargetClientID = 0);
	void PostMessage(const GKMessageNetworkToClientCommand& iMessage, uint8 TargetClientID = 0);

	//void LocalClientSendMessageForServer();

	void AddNetworkPlayer(GKNetworkPlayer* Player);
	void RemoveNetworkPlayer(GKNetworkPlayer* Player);

	// Called by the Actor when OnEndPlay event
	void OnEndPlay();

	void OnReceiveMessage(const GKMessageNetworkToServer& iMessage);

	bool IsServer();

	void RetrieveMessage(TArray<GKMessageNetworkToClient>& Inbox);
	void RetrieveMessage(TArray<GKMessageNetworkToServer>& Inbox);
	void RetrieveMessage(GKMessageNetworkToClientCommand::SortedArray& Inbox);

	inline int NumPlayers() { return NetworkPlayers.Num(); }
	inline GKNetworkPlayer* GetPlayer(int index) { return NetworkPlayers[index]; }
	inline GKNetworkPlayer* FindPlayerPtr(uint8 PlayerID);

protected:
	struct MessageNetworkToOneClient
	{
		GKMessageNetworkToClient Message;
		uint8 TargetClientID;
		MessageNetworkToOneClient(const GKMessageNetworkToClient& iMessage, uint32 iTargetClientID)
			: Message(iMessage), TargetClientID(iTargetClientID) {}
	};

	struct MessageNetworkToOneClientCommand
	{
		GKMessageNetworkToClientCommand Message;
		uint8 TargetClientID;
		MessageNetworkToOneClientCommand(const GKMessageNetworkToClientCommand& iMessage, uint32 iTargetClientID)
			: Message(iMessage), TargetClientID(iTargetClientID) {}
	};

	virtual void OnStartGame() override;

	virtual bool HasOwner() { return false; }

	virtual void OnUpdateServer(float DeltaTime);
	virtual void OnUpdateClient(float DeltaTime);

	virtual void RPCServerToAll(FGKNetworkSerializeToClient Message) {}
	virtual void RPCServerToAllReliable(FGKNetworkSerializeToClient Message) {}
	virtual void RPCServerToAllCommandReliable(FGKNetworkSerializeToClientCommand Message) {}

	void OnReceiveMessage(const GKMessageNetworkToClient& iMessage);
	void OnReceiveMessage(const GKMessageNetworkToClientCommand& iMessage);

	TArray<GKMessageNetworkToServer> OutboxMessageNetworkToServer;
	TArray<MessageNetworkToOneClient> OutboxMessageNetworkToClient;
	TArray<GKMessageNetworkToClient> OutboxMessageNetworkToAll;

	TArray<MessageNetworkToOneClientCommand> OutboxMessageNetworkToClientCommand;
	TArray<GKMessageNetworkToClientCommand> OutboxMessageNetworkToAllCommand;

	// Inboxes have to be here because GameEngine might not have yet created/ linked when messages coming in.
	TArray<GKMessageNetworkToClient> InboxMessageNetworkToClient;
	TArray<GKMessageNetworkToClientCommand> InboxMessageNetworkToClientCommand;
	TArray<GKMessageNetworkToServer> InboxMessageNetworkToServer;

	// For logging
	uint8 GetPlayerID() { return LocalPlayer != nullptr ? LocalPlayer->GetPlayerID() : 0; }

	GKGameEngine* GameEngine;			// Ref back to GameEngine

	TArray<GKNetworkPlayer*> NetworkPlayers;
	GKNetworkPlayer* LocalPlayer = nullptr;

	int32 FindPlayer(uint8 PlayerID);

	//bool bNetworkPlayerChanged = false;

private:
	uint8 GetNextPlayerID();
	uint8 NextPlayerID = 2;

	void InitPlayer(GKNetworkPlayer* Player);

public:
	GK_GET(GKGameEngine*, GameEngine);

};
