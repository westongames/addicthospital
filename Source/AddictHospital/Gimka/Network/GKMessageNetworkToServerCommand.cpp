// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Network/GKMessageNetworkToServerCommand.h"

GKMessageNetworkToServerCommand::GKMessageNetworkToServerCommand(uint32 iType, uint32 GameTick, bool iReliable)
	: GKMessageNetwork(iType, GameTick, iReliable)
{
}

GKMessageNetworkToServerCommand::~GKMessageNetworkToServerCommand()
{
}

bool GKMessageNetworkToServerCommand::NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
{
	if (!GKMessageNetwork::NetSerialize(Ar, Map, bOutSuccess)) return false;
	return true;
}
