// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Network/GKMessageNetwork.h"
#include "Gimka/Tools/Macros/GKHeader.h"


/**
 * 
 */
struct GKMessageNetworkToServer : public GKMessageNetwork
{
public:
	enum MessageType
	{
		Empty,
		JoinGame,				// Sent by player when joined for first time, server will reply with StartGame data
	};
	GKMessageNetworkToServer() {}
	GKMessageNetworkToServer(uint32 iType, uint32 iGameTick, bool iReliable);
	virtual ~GKMessageNetworkToServer();

	static GKMessageNetworkToServer CreateJoinGame();

	virtual bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess) override;

};
