// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/OnlineSessionInterface.h"

class UGKGameInstance;

/**
 * Note: the Steam CreateSession and FindSession are not tested because we need 2 computers with 2 different Steam account.
 * It's probably already tested when we did Puzzle Platform the first time and it works. So it should be fine.
 * We'll test later after we focus on the actual game. Just follow Udemy C++ Multiplayer Master Chp 65 if needed.
 */
class GKOnlineSession
{
public:
	struct SessionData
	{
		FString Name;
		uint16 CurrentPlayers;
		uint16 TotalPlayers;
		FString HostUsername;
		int32 Ping;

	};


	GKOnlineSession(UGKGameInstance* GameInstance);
	~GKOnlineSession();

	bool FindSessions();
	bool CreateSession(const FString& SessionName);
	bool DestroySession();

	// This will start the session and make no more clients can join
	void StartSession();

	// Join after FindSession, Index specifies the index of SessionSearch.
	bool JoinSession(uint32 Index);

	void ListenToNetworkFailure();
	void OnNetworkFailure(UWorld* World, UNetDriver* NetDriver, ENetworkFailure::Type FailureType, const FString& ErrorString);

	void OnDestroy();

	bool IsSessionInterfaceValid() const;
	const FString& GetSubsystemName() const;
	
	bool ConnectionTypeIsNull() const;

private:
	UGKGameInstance* GameInstance = nullptr;			// Ref back to GameInstance
	IOnlineSessionPtr SessionInterface = nullptr;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;
	FString SubsystemName;
	uint16 MaxPlayers = 2;

	enum ConnectionType
	{
		Steam,
		Null,
		Count
	};
	ConnectionType Connection;


	void ClearCallbacks();

	enum AfterDestroyType
	{
		None,
		Create,
		Join
	};

	//bool bRecreateSessionAfterDestroy = false;
	AfterDestroyType AfterDestroy = AfterDestroyType::None;
	FString RecreateSessionName;			// Valid when AfterDestroy == Create
	uint32 RejoinSessionIndex;				// Valid when AfterDesoy == Join

	bool CreateSessionInterface();

	void OnCreateSessionComplete(FName SessionName, bool Success);
	void OnFindSessionComplete(bool Success);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);
	void OnDestroySessionComplete(FName SessionName, bool Success);

public:
	// Set to true to add fake sessions if empty (for ConnectionType::Null). So we can test the menu.
	static const bool bAddFakeSessions = true;

	GK_GETSET(uint16, MaxPlayers);

};
