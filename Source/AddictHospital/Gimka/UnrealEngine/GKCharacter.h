// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Gimka/Network/GKNetworkPlayer.h"
#include "Gimka/UnrealEngine/GKNetworkSerialize.h"

#include "GKCharacter.generated.h"

/*
* Not using this for AddictHospitalCharacter because pure virtual function crash.
*/

UCLASS()
class AGKCharacter : public ACharacter, public GKNetworkPlayer
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGKCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(Server, unreliable)
	virtual void RPCClientToServer(FGKNetworkSerializeToServer NetworkSerialize) override;

	UFUNCTION(Server, reliable)
	virtual void RPCClientToServerReliable(FGKNetworkSerializeToServer NetworkSerialize) override;

	//int CounterTest = 0;

	UFUNCTION(Client, unreliable)
	virtual void RPCServerToClient(FGKNetworkSerializeToClient NetworkSerialize) override;

	UFUNCTION(Client, reliable)
	virtual void RPCServerToClientReliable(FGKNetworkSerializeToClient NetworkSerialize) override;

	UFUNCTION(Client, reliable)
	virtual void RPCServerToClientCommandReliable(FGKNetworkSerializeToClientCommand NetworkSerialize) override;

	UFUNCTION()
	void OnButtonEscape();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	// PlayerID assigned by server, replicated to all clients
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_PlayerID, Category = GKNetwork, meta = (AllowPrivateAccess = "true"))
	uint8 PlayerID = 0;

	// Called when the PlayerID gets updated by server
	UFUNCTION()
	void OnRep_PlayerID();

public:
	virtual void SetPlayerID(uint8 Value) override { PlayerID = Value; }
	virtual uint8 GetPlayerID() const override { return PlayerID; }

};
