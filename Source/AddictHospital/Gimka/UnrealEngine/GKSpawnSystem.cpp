// Fill out your copyright notice in the Description page of Project Settings.


#include "GKSpawnSystem.h"
#include "Gimka/Tools/Macros/GKLogger.h"

#include "PuzzlePlatforms/MovingPlatform.h"


#include "Engine/StaticMeshActor.h"

UStaticMesh* StaticMesh;



AGKSpawnSystem::AGKSpawnSystem()
{
	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FObjectFinder<UStaticMesh> CylinderAsset(TEXT("/Game/Geometry/Meshes/1M_Cube"));
	StaticMesh = CylinderAsset.Object;

	if (CylinderAsset.Succeeded())
	{
		GKLog("Loaded cylinder");
	}
	else
	{
		GKLog("Not loaded 1m_cube");
	}
}

AGKSpawnSystem::~AGKSpawnSystem()
{
}

// Called when the game starts or when spawned
void AGKSpawnSystem::BeginPlay()
{
	Super::BeginPlay();

	FVector ActorLocation = FVector(0, 920.f, 260.f);
	//SpawnMovingPlatform(ActorLocation);
}

// Called every frame
void AGKSpawnSystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGKSpawnSystem::SpawnStaticMeshActor(const FVector& InLocation)
{
		///StarterContent/Shapes/Shape_Cylinder.Shape_Cylinder"));

	AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), InLocation, FRotator::ZeroRotator);
	//NewActor->SetActorLabel(TEXT("NewActorMe"));
	//, NAME_None, & InLocation);

	UStaticMeshComponent* MeshComponent = NewActor->GetStaticMeshComponent();	// CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TheMeshComponent"));
	GKLog1("MeshComponent", MeshComponent);

	if (MeshComponent != nullptr)
	{
		MeshComponent->SetStaticMesh(TheStaticMesh);
	}

	//NewActor->SetRootComponent(MeshComponent);

}

void AGKSpawnSystem::SpawnMovingPlatform(const FVector& InLocation)
{
	///StarterContent/Shapes/Shape_Cylinder.Shape_Cylinder"));

	AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AMovingPlatform::StaticClass(), InLocation, FRotator::ZeroRotator);
	//NewActor->SetActorLabel(TEXT("NewActorMe"));
	//, NAME_None, & InLocation);

	UStaticMeshComponent* MeshComponent = NewActor->GetStaticMeshComponent();	// CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TheMeshComponent"));
	GKLog1("MeshComponent", MeshComponent);

	if (MeshComponent != nullptr)
	{
		MeshComponent->SetStaticMesh(TheStaticMesh);
	}
//	(X = -50.000000, Y = 920.000000, Z = 260.000000)
	//NewActor->SetRootComponent(MeshComponent);

}
