// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/UnrealEngine/GKPawnWithCamera.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"


#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/UnrealEngine/GKNetworkManagerActor.h"
#include "EngineUtils.h"
#include "Net/UnrealNetwork.h"

//#include "GimkaSystem/VGANetworkManager.h"
#include "Gimka/System/GKGameEngine.h"
#include "Gimka/Game/GKGameManager.h"


// Sets default values
AGKPawnWithCamera::AGKPawnWithCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create our components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));

	// Attach our components
	StaticMeshComp->SetupAttachment(RootComponent);
	SpringArmComp->SetupAttachment(StaticMeshComp);
	CameraComp->SetupAttachment(SpringArmComp, USpringArmComponent::SocketName);

	// Assign SpringArm class variables
	SpringArmComp->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 10.0f), FRotator(-60.0f, 0.0f, 0.0f));
	SpringArmComp->TargetArmLength = 100.0f;// 400.0f;
	SpringArmComp->bEnableCameraLag = true;
	SpringArmComp->CameraLagSpeed = 3.0f;

	// Take control of the default Player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

}

// Called when the game starts or when spawned
void AGKPawnWithCamera::BeginPlay()
{
	Super::BeginPlay();

	GKLog1("Hello I'm AGKPawnWithCamera on Beginplay", GetOwner());
	GKLog1("Role", (uint32)GetLocalRole());
	TActorIterator<AGKNetworkManagerActor> ActorNetworkItr(GetWorld());

	// If this is main menu, it will not have AGKNetworkManager
	if (ActorNetworkItr)
	{
		//check(ActorNetworkItr);
		//NetworkManager = *ActorNetworkItr;
		AddTickPrerequisiteActor(*ActorNetworkItr);
		bool IsServer = HasAuthority() && IsLocallyControlled();
		GKLog2("HasAuthority, IsLocallyControlled", HasAuthority(), IsLocallyControlled());
		Initialize(*ActorNetworkItr, IsServer, IsLocallyControlled());
		(*ActorNetworkItr)->AddNetworkPlayer(this);
		/*if (HasAuthority() && PlayerID == 0)
		{
			PlayerID = 323;
		}*/
	}
}

void AGKPawnWithCamera::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GKLog("OnBeginDestroy");
	if (GetNetworkManager() != nullptr)
	{
		GetNetworkManager()->RemoveNetworkPlayer(this);
		Disconnect();
	}

	Super::EndPlay(EndPlayReason);
}


void AGKPawnWithCamera::OnResetVR()
{
	// If AddictHospital is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in AddictHospital.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

// Called every frame
void AGKPawnWithCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Zoom in if ZoomIn button is down, zoom back out if it's not
	if (bZoomingIn)
	{
		ZoomFactor += DeltaTime / 0.5f;		// Zoom in over half a second
	}
	else
	{
		ZoomFactor -= DeltaTime / 0.25f;	// Zoom out over a quarter of a second
	}
	ZoomFactor = FMath::Clamp<float>(ZoomFactor, 0.0f, 1.0f);

	// Blend our camera's FOV and our SpringArm's length based on ZoomFactor
	CameraComp->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, ZoomFactor);
	SpringArmComp->TargetArmLength = FMath::Lerp<float>(100.0f, 50.0f, ZoomFactor);// 400.0f, 300.0f, ZoomFactor);

	// Rotate our actor's yaw, which will turn our camera because we're attached to it
	{
		FRotator NewRotation = GetActorRotation();
		NewRotation.Yaw += CameraInput.X;
		SetActorRotation(NewRotation);
	}

	// Rotate our camera's pitch, but limit it so we're always looking downward
	{
		FRotator NewRotation = SpringArmComp->GetComponentRotation();
		NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.Y, -80.0f, -15.0f);
		SpringArmComp->SetWorldRotation(NewRotation);
	}

	// Handle movement based on our "MoxeX" and "MoveY" axes
	{
		if (!MovementInput.IsZero())
		{
			// Scale our movement input axis values by 100 units per second
			MovementInput = MovementInput.GetSafeNormal() * 100.0f;
			FVector NewLocation = GetActorLocation();
			NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
			NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
			SetActorLocation(NewLocation);
		}
	}
}

// Called to bind functionality to input
void AGKPawnWithCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Hook up events for "ZoomIn"
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &AGKPawnWithCamera::ZoomIn);
	InputComponent->BindAction("ZoomIn", IE_Released, this, &AGKPawnWithCamera::ZoomOut);

	// Hook up every-frame handling for our four axes
	InputComponent->BindAxis("MoveForward", this, &AGKPawnWithCamera::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AGKPawnWithCamera::MoveRight);
	InputComponent->BindAxis("LookUp", this, &AGKPawnWithCamera::PitchCamera);		// Was CameraPitch in tutorial
	InputComponent->BindAxis("Turn", this, &AGKPawnWithCamera::YawCamera);			// Was CameraYaw in tutorial
}


void AGKPawnWithCamera::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	// If we don't call this the client will become SimulatedProxy instead of AutonomousProxy
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGKPawnWithCamera, PlayerID);
}


void AGKPawnWithCamera::OnRep_PlayerID()
{
	GKLog1("=============================== PlayerID is assigned", PlayerID);
}

void AGKPawnWithCamera::MoveForward(float AxisValue)
{
	MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AGKPawnWithCamera::MoveRight(float AxisValue)
{
	MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void AGKPawnWithCamera::PitchCamera(float AxisValue)
{
	CameraInput.Y = AxisValue;
}

void AGKPawnWithCamera::YawCamera(float AxisValue)
{
	CameraInput.X = AxisValue;
}

void AGKPawnWithCamera::ZoomIn()
{
	bZoomingIn = true;
}

void AGKPawnWithCamera::ZoomOut()
{
	bZoomingIn = false;
}

void AGKPawnWithCamera::RPCClientToServer_Implementation(FGKNetworkSerializeToServer NetworkSerialize)
{
	//if (GameEngine == nullptr) return;
	uint32 GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToServer Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("================= Addict Server received ClientToServer at", GameTickLocal, Message.GetMessageType());
		Message.SetReliable(false);
		GetNetworkManager()->OnReceiveMessage(Message);
	}
	else
	{
		GKLog2("================= Addict Client received ClientToServer at", GameTickLocal, Message.GetMessageType());
	}
}


void AGKPawnWithCamera::RPCClientToServerReliable_Implementation(FGKNetworkSerializeToServer NetworkSerialize)
{
	//if (GameEngine == nullptr) return;
	uint32 GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToServer Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientToServerRel at", GameTickLocal, Message.GetMessageType());
		Message.SetReliable(true);
		GetNetworkManager()->OnReceiveMessage(Message);
	}
	else
	{
		GKLog2("============== Addict Client received ClientToServerRel at", GameTickLocal, Message.GetMessageType());
	}
}


void AGKPawnWithCamera::RPCServerToClient_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received Client at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received Client at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);
	}
}

void AGKPawnWithCamera::RPCServerToClientReliable_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientReliable at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received ClientReliable at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);

	}
}

void AGKPawnWithCamera::RPCServerToClientCommandReliable_Implementation(FGKNetworkSerializeToClientCommand NetworkSerialize)
{
	//GKNetworkManager* NetworkManager = GetNetworkManager();
	//if (NetworkManager == nullptr)
	//{
	//	TActorIterator<AVGANetworkManager> ActorNetworkItr(GetWorld());
	//	check(ActorNetworkItr);
	//	//NetworkManager = *ActorNetworkItr;
	//	AddTickPrerequisiteActor(*ActorNetworkItr);
	//	bool IsServer = HasAuthority() && IsLocallyControlled();
	//	GKLog2("HasAuthority, IsLocallyControlled", HasAuthority(), IsLocallyControlled());
	//	Initialize(*ActorNetworkItr, IsServer, IsLocallyControlled());
	//	(*ActorNetworkItr)->AddNetworkPlayer(this);
	//	NM = GetNetworkManager();
	//}
	//GKGameEngine* GameEngine = NM->GetGameEngine();
	//GKGameManager* GameManager = GameEngine->GetGameManager();
	//check(GameManager);
	//uint32 GameTickLocal = 0;
	//if (GetNetworkManager() != nullptr) GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToClientCommand Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientCommand at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received ClientCommand at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);

	}
}

