// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/UnrealEngine/GKCharacter.h"
#include "Gimka/UnrealEngine/GKNetworkManagerActor.h"
//#include "GimkaSystem/VGANetworkManager.h"

#include "EngineUtils.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AGKCharacter::AGKCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGKCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	GKLog1("Hello I'm AddictHospitalCharacter on Beginplay", GetOwner());
	GKLog1("Role", (uint32)GetLocalRole());
	TActorIterator<AGKNetworkManagerActor> ActorNetworkItr(GetWorld());
	check(ActorNetworkItr);
	//NetworkManager = *ActorNetworkItr;
	AddTickPrerequisiteActor(*ActorNetworkItr);
	bool IsServer = HasAuthority() && IsLocallyControlled();
	GKLog2("HasAuthority, IsLocallyControlled", HasAuthority(), IsLocallyControlled());
	Initialize(*ActorNetworkItr, IsServer, IsLocallyControlled());
	(*ActorNetworkItr)->AddNetworkPlayer(this);
	/*if (HasAuthority() && PlayerID == 0)
	{
		PlayerID = 323;
	}*/

}

// Called every frame
void AGKCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AGKCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Note: doesn't work for now - not get called even after we disable ESC to quit editor.
	PlayerInputComponent->BindAction("Escape", IE_Pressed, this, &AGKCharacter::OnButtonEscape);
}

void AGKCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	// If we don't call this the client will become SimulatedProxy instead of AutonomousProxy
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGKCharacter, PlayerID);
}


void AGKCharacter::OnRep_PlayerID()
{
	GKLog1("=============================== PlayerID is assigned", PlayerID);
}

void AGKCharacter::RPCClientToServer_Implementation(FGKNetworkSerializeToServer NetworkSerialize)
{
	//if (GameEngine == nullptr) return;
	uint32 GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToServer Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("================= Addict Server received ClientToServer at", GameTickLocal, Message.GetMessageType());
		Message.SetReliable(false);
		GetNetworkManager()->OnReceiveMessage(Message);
	}
	else
	{
		GKLog2("================= Addict Client received ClientToServer at", GameTickLocal, Message.GetMessageType());
	}
}


void AGKCharacter::RPCClientToServerReliable_Implementation(FGKNetworkSerializeToServer NetworkSerialize)
{
	//if (GameEngine == nullptr) return;
	uint32 GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToServer Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientToServerRel at", GameTickLocal, Message.GetMessageType());
		Message.SetReliable(true);
		GetNetworkManager()->OnReceiveMessage(Message);
	}
	else
	{
		GKLog2("============== Addict Client received ClientToServerRel at", GameTickLocal, Message.GetMessageType());
	}
}


void AGKCharacter::RPCServerToClient_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received Client at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received Client at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);
	}
}

void AGKCharacter::RPCServerToClientReliable_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientReliable at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received ClientReliable at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);

	}
}

void AGKCharacter::RPCServerToClientCommandReliable_Implementation(FGKNetworkSerializeToClientCommand NetworkSerialize)
{
	//GKNetworkManager* NetworkManager = GetNetworkManager();
	//if (NetworkManager == nullptr)
	//{
	//	TActorIterator<AVGANetworkManager> ActorNetworkItr(GetWorld());
	//	check(ActorNetworkItr);
	//	//NetworkManager = *ActorNetworkItr;
	//	AddTickPrerequisiteActor(*ActorNetworkItr);
	//	bool IsServer = HasAuthority() && IsLocallyControlled();
	//	GKLog2("HasAuthority, IsLocallyControlled", HasAuthority(), IsLocallyControlled());
	//	Initialize(*ActorNetworkItr, IsServer, IsLocallyControlled());
	//	(*ActorNetworkItr)->AddNetworkPlayer(this);
	//	NM = GetNetworkManager();
	//}
	//GKGameEngine* GameEngine = NM->GetGameEngine();
	//GKGameManager* GameManager = GameEngine->GetGameManager();
	//check(GameManager);
	//uint32 GameTickLocal = 0;
	//if (GetNetworkManager() != nullptr) GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToClientCommand Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientCommand at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received ClientCommand at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);

	}
}

void AGKCharacter::OnButtonEscape()
{
	GKLog("Button Escape pressed!");
}

