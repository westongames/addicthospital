// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/UnrealEngine/GKGameModeBase.h"
#include "GKGameModeBaseLobby.generated.h"

/**
 * GameMode exists only on the server, not clients!
 */
UCLASS()
class AGKGameModeBaseLobby : public AGKGameModeBase
{
	GENERATED_BODY()

public:
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;

private:
	void StartGame();

	uint32 NumberOfPlayers = 0;
	FTimerHandle GameStartTimer;
};
