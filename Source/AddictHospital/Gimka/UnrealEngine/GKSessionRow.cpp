// Fill out your copyright notice in the Description page of Project Settings.


#include "GKSessionRow.h"
#include "Components/TextBlock.h"
#include "Gimka/Tools/Macros/GKLogger.h"

#include "Gimka/Menu/GKMenuInterface.h"

#include "Components/Button.h"

void UGKSessionRow::SetSessionData(const GKOnlineSession::SessionData& Session)
{
	SessionName->SetText(FText::FromString(Session.Name));
	if (Session.TotalPlayers != 0)
	{
		HostUser->SetText(FText::FromString(Session.HostUsername));
		FString PlayerInfo = FString::Printf(TEXT("%d/%d"), Session.CurrentPlayers, Session.TotalPlayers);
		PlayerNum->SetText(FText::FromString(PlayerInfo));
		Ping->SetText(FText::FromString(FString::FromInt(Session.Ping)));
	}
	else
	{
		HostUser->SetText(FText());
		PlayerNum->SetText(FText());
		Ping->SetText(FText());
	}
}

void UGKSessionRow::Setup(class GKMenuInterface* iMenuInterface, uint32 iIndex)
{
	MenuInterface = iMenuInterface;
	Index = iIndex;

	ButtonRow->OnClicked.AddDynamic(this, &UGKSessionRow::OnClicked);
	ButtonRow->OnHovered.AddDynamic(this, &UGKSessionRow::OnHovered);
	ButtonRow->OnUnhovered.AddDynamic(this, &UGKSessionRow::OnUnhovered);
	//UpdateColor();
}

void UGKSessionRow::SetAsSelected(bool IsSelected)
{
	if (Selected != IsSelected)
	{
		Selected = IsSelected;
		UpdateColor();
	}
}

void UGKSessionRow::OnClicked()
{
	MenuInterface->SelectIndex(Index);
}

void UGKSessionRow::OnHovered()
{
	if (!Hovered)
	{
		Hovered = true;
		UpdateColor();
	}
	//GKLog1("OnHovered", Index);
	//GKLog1("IsHovered", IsHovered());
	//UpdateColor();
}

void UGKSessionRow::OnUnhovered()
{
	if (Hovered)
	{
		Hovered = false;
		UpdateColor();
	}
	//GKLog1("Unhovered", Index);
	//GKLog1("IsHovered", IsHovered());
	//UpdateColor();
}

void UGKSessionRow::UpdateColor()
{
	if (Selected)
	{
		SetColorAndOpacity(FLinearColor::Yellow);
	}
	else if (Hovered)
	{
		SetColorAndOpacity(FLinearColor(0.029f, 0.761f, 0.865f, 1.f));
	}
	else
	{
		SetColorAndOpacity(FLinearColor::White);
	}
	//const FLinearColor UnselectedColor = FLinearColor::White;
//const FLinearColor SelectedColor = FLinearColor::Green;

//const FLinearColor UnselectedHoveredColor = FLinearColor::Yellow;
//const FLinearColor SelectedHoveredColor = FLinearColor::Blue;
	//if (IsSelected())
	/*{
		if (IsHovered())
		{
			SessionName->SetColorAndOpacity(FLinearColor::Yellow);
		}
		else
		{
			SessionName->SetColorAndOpacity(FLinearColor::Green);

		}
	}*/
	/*else
	{
		if (IsHovered())
		{
			SessionName->SetColorAndOpacity(FLinearColor::Blue);
		}
		else
		{
			SessionName->SetColorAndOpacity(FLinearColor::White);

		}

	}*/
}


