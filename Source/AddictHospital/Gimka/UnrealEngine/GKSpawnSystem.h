// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GKSpawnSystem.generated.h"


/**
 * 
 */
UCLASS()
class ADDICTHOSPITAL_API AGKSpawnSystem : public AActor
{
	GENERATED_BODY()

public:
	AGKSpawnSystem();
	~AGKSpawnSystem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnStaticMeshActor(const FVector& InLocation);
	void SpawnMovingPlatform(const FVector& InLocation);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ore Types", meta = (AllowPrivateAccess = "true"))
	UStaticMesh* TheStaticMesh;
};
