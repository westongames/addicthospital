// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GKConnectionToServer.generated.h"

UCLASS()
class ADDICTHOSPITAL_API AGKConnectionToServer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGKConnectionToServer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(Server, reliable)
	void RPCClientToServerReliable(int32 Value);

	bool bFirstTime = true;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};
