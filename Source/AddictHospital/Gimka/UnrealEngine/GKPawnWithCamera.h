// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Gimka/Network/GKNetworkPlayer.h"
#include "Gimka/Unrealengine/GKNetworkSerialize.h"
#include "GKPawnWithCamera.generated.h"

class AVGANetworkManager;

UCLASS()
class ADDICTHOSPITAL_API AGKPawnWithCamera : public APawn, public GKNetworkPlayer
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArmComp;

	UPROPERTY(EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* CameraComp;

	UPROPERTY(EditAnywhere, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMeshComp;

	// Input variables
	FVector2D MovementInput;
	FVector2D CameraInput;
	float ZoomFactor;
	bool bZoomingIn;

public:
	// Sets default values for this pawn's properties
	AGKPawnWithCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


protected:
	UFUNCTION(Server, unreliable)
		virtual void RPCClientToServer(FGKNetworkSerializeToServer NetworkSerialize) override;

	UFUNCTION(Server, reliable)
		virtual void RPCClientToServerReliable(FGKNetworkSerializeToServer NetworkSerialize) override;

	//int CounterTest = 0;

	UFUNCTION(Client, unreliable)
		virtual void RPCServerToClient(FGKNetworkSerializeToClient NetworkSerialize) override;

	UFUNCTION(Client, reliable)
		virtual void RPCServerToClientReliable(FGKNetworkSerializeToClient NetworkSerialize) override;

	UFUNCTION(Client, reliable)
		virtual void RPCServerToClientCommandReliable(FGKNetworkSerializeToClientCommand NetworkSerialize) override;

private:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	// PlayerID assigned by server, replicated to all clients
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_PlayerID, Category = GKNetwork, meta = (AllowPrivateAccess = "true"))
		uint8 PlayerID = 0;

	// Called when the PlayerID gets updated by server
	UFUNCTION()
		void OnRep_PlayerID();

	// Input functions
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void PitchCamera(float AxisValue);
	void YawCamera(float AxisValue);
	void ZoomIn();
	void ZoomOut();

public:
	virtual void SetPlayerID(uint8 Value) override { PlayerID = Value; }
	virtual uint8 GetPlayerID() const override { return PlayerID; }

};
