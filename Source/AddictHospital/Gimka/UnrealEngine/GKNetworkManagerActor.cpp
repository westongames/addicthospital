// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/UnrealEngine/GKNetworkManagerActor.h"

// Sets default values
AGKNetworkManagerActor::AGKNetworkManagerActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Both bReplicates and bAlwaysRelevant need to be true for this to work.
	bReplicates = true;
	bAlwaysRelevant = true;

}

// Called when the game starts or when spawned
void AGKNetworkManagerActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGKNetworkManagerActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GKNetworkManager::OnEndPlay();
	Super::EndPlay(EndPlayReason);
}


void AGKNetworkManagerActor::OnStartGame()
{
	GKLog("AGKNetworkManagerActor::OnStartGame");
	GKNetworkManager::OnStartGame();
	/*if (HasAuthority())
	{
		GKLog("Create ConnectToServer");
		ConnectToServer = GetWorld()->SpawnActor<AActor>
			(AGKConnectionToServer::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
		ConnectToServer->SetActorLabel("ConnectToServer");
	}*/
}

// Called every frame
void AGKNetworkManagerActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	OnUpdateGame(DeltaTime);

	/*if (bNetworkPlayerChanged)
	{
		bNetworkPlayerChanged = false;
		GKLog2("LocalPlayer = ", NetworkPlayers.Num(), (int32)(LocalPlayer != nullptr));
		check(LocalPlayer != nullptr);
	}*/
}


void AGKNetworkManagerActor::RPCServerToAll_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	//check(GameEngine);
	//if (GameEngine == nullptr) return;
	//int32 GameTickLocal = GameEngine->GetGameManager()->GetGameTime().GetGameTick();

	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority()) {
		//GKLog1("Server Received at", Message.GetGameTick());
	}
	else {
		//GKLog2("Client Received at", GameTickLocal, Message.GetGameTick());

		// Note: might want to use message to send to gameManager?
		//GameEngine->GetGameManager()->SetServerGameTick(Message.GetGameTick());

		Message.SetReliable(false);
		OnReceiveMessage(Message);
	}
}

void AGKNetworkManagerActor::RPCServerToAllReliable_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	//check(GameTickLocal == 2);
	//check(GameEngine);
	//if (GameEngine == nullptr)
	//{
	//	GKLog1("GameEngie is null! GameTick", HasAuthority());
	//	GKLog1("Message", NetworkSerialize.GetMessage().GetMessageType());
	//}
	//if (GameEngine == nullptr) return;
	//int32 GameTickLocal = GameEngine->GetGameManager()->GetGameTime().GetGameTick();

	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority()) {
		//GKLog1("Server Received at", Message.GetGameTick());
	}
	else {
		//GKLog2("Client Received at", GameTickLocal, Message.GetGameTick());

		// Note: might want to use message to send to gameManager?
		//GameEngine->GetGameManager()->SetServerGameTick(Message.GetGameTick());

		Message.SetReliable(true);
		OnReceiveMessage(Message);
	}
}

void AGKNetworkManagerActor::RPCServerToAllCommandReliable_Implementation(FGKNetworkSerializeToClientCommand NetworkSerialize)
{
	//check(GameEngine);
	//if (GameEngine == nullptr) return;
	//int32 GameTickLocal = GameEngine->GetGameManager()->GetGameTime().GetGameTick();

	GKMessageNetworkToClientCommand Message = NetworkSerialize.GetMessage();
	if (HasAuthority()) {
		//GKLog1("Server Received at", Message.GetGameTick());
	}
	else {
		//GKLog2("Client Received at", GameTickLocal, Message.GetGameTick());

		// Note: might want to use message to send to gameManager?
		//GameEngine->GetGameManager()->SetServerGameTick(Message.GetGameTick());

		//Message.SetReliable(true);
		OnReceiveMessage(Message);
	}
}
