// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Network/GKOnlineSession.h"

#include "GKSessionRow.generated.h"

/**
 * 
 */
UCLASS()
class UGKSessionRow : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetSessionData(const GKOnlineSession::SessionData& Session);

	void Setup(class GKMenuInterface* MenuInterface, uint32 Index);

	UPROPERTY(BlueprintReadOnly)
	bool Selected = false;
	bool Hovered = false;

	void SetAsSelected(bool IsSelected);

private:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* SessionName;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* HostUser;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* PlayerNum;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* Ping;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonRow;

	class GKMenuInterface* MenuInterface;
	uint32 Index;

	UFUNCTION()
	void OnClicked();

	UFUNCTION()
	void OnHovered();

	UFUNCTION()
	void OnUnhovered();

	void UpdateColor();

	//const FLinearColor UnselectedColor = FLinearColor::White;
	//const FLinearColor SelectedColor = FLinearColor::Green;

	//const FLinearColor UnselectedHoveredColor = FLinearColor::Yellow;
	//const FLinearColor SelectedHoveredColor = FLinearColor::Blue;

public:
	GK_GET(uint32, Index);
	//GK_GETSET(UTextBlock, ServerName);
};
