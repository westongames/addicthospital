// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "GKSaveGame.generated.h"

/**
 * TODO:
 * - Use custom save as in https://www.youtube.com/watch?v=yOk1hk8uDlA (already downloaded project)
 * - But follow this: https://bluebubblebee.medium.com/lets-create-a-save-system-in-unreal-engine-4-276a24e66b2c
 * - Or https://www.tomlooman.com/save-system-unreal-engine-tutorial/
 */
UCLASS()
class UGKSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	// Sets default values
	UGKSaveGame();

public:
	// Variables to store
	UPROPERTY()						// If it doesn't have UPROPERTY, they won't be saved
	uint32 GameTick;

	// Save Slot Name
	UPROPERTY()
	FString SaveSlotName;

	// Save Slot User Index
	UPROPERTY()
	uint32 UserIndex;

	// Struct we can use to save our computer's local date, time, month, etc
	UPROPERTY()
	FDateTime SaveSlotDate;
};
