// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/UnrealEngine/GKSaveGame.h"

UGKSaveGame::UGKSaveGame()
{
	// Sets default values
	GameTick = 0;

	SaveSlotName = FString(TEXT("PlayerSaveSlot"));
	UserIndex = 0;
}