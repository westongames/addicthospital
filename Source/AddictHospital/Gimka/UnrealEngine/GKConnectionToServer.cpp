// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/UnrealEngine/GKConnectionToServer.h"
#include "GimkaSystem/VGANetworkManager.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "AddictHospitalCharacter.h"
#include "EngineUtils.h"

// Sets default values
AGKConnectionToServer::AGKConnectionToServer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGKConnectionToServer::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		SetReplicates(true);
		//SetReplicateMovement(true);
		bAlwaysRelevant = true;
	}
	else
	{
		GKLog("Hello I'm ConnectionToServer on client Beginplay");
		TActorIterator<AVGANetworkManager> ActorNetworkItr(GetWorld());
		//for (; ActorNetworkItr; ++ActorNetworkItr)
		//{
		//	GKLog("Found ACVGANetworkManager");
		//	break;
		//	//(*ActorItr)->SetOwner(this);
		//}
		for (TActorIterator<AAddictHospitalCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
		{
			GKLog1("Found AAddictHospitalCharacter", (*ActorItr)->IsLocallyControlled());
			if ((*ActorItr)->IsLocallyControlled())
			//(*ActorItr)->SetOwner(this);
				(*ActorNetworkItr)->SetOwner(*ActorItr);
			//SetOwner(*ActorItr);
		}
	}
}

// Called every frame
void AGKConnectionToServer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	GKLog1("AGKConnectionToServer: ", HasAuthority());
	if (bFirstTime)
	{
		bFirstTime = false;
		RPCClientToServerReliable(333);
	}
}

void AGKConnectionToServer::RPCClientToServerReliable_Implementation(int32 Value)
{
	if (HasAuthority()) 
	{
		GKLog1("========================  Server::AGKConnectionToServer", Value);
	}
	else
	{
		GKLog1("======================== Client::AGKConnectionToServer", Value);

	}
}
