// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "Gimka/Menu/GKMenuInterface.h"
#include "Gimka/Network/GKOnlineSession.h"

#include "GKGameInstance.generated.h"

class GKGameEngine;
class GKXMLManager;
//class UGKMenu;
//class GKOnlineSession;
//class UVGAMainMenu;

/**
 * 
 */
UCLASS()
class UGKGameInstance : public UGameInstance, public GKMenuInterface
{
	GENERATED_BODY()
	
public:
	UGKGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init() override;
	virtual void Shutdown() override;

	//UFUNCTION(exec)
	UFUNCTION(BlueprintCallable)
	void LoadMenuMain() { OnLoadMenuMain();  }

	UFUNCTION(BlueprintCallable)
	void LoadMenuInGame() { OnLoadMenuInGame(); }

	virtual void PrintScreen(float Time, FColor DisplayColor, const FString& Message) {}

	void ReturnToMainMenuHost();

	// Called by GKOnlineSession when session is created successfully
	virtual void OnCreateSessionComplete(const FName& SessionName, bool Success);

	// Called by GKOnlineSession when findSessions is completed
	virtual void OnFindSessionsComplete(const TArray<GKOnlineSession::SessionData>& Sessions, bool Success) {}

	// Called by GKOnlineSession when joinSession is completed
	virtual void OnJoinSessionComplete(const FName& SessionName, const FString& Address, bool Success) {}

	// Called by GKOnlineSession when a subsystem is found
	virtual void OnFoundSubsystem(const FString& SubsystemName) {}

protected:
	virtual void OnLoadMenuMain() {}
	virtual void OnLoadMenuInGame() {}

	virtual void StartHost() {}
	virtual void StartJoin(const FString& Address) {}
	virtual void StartPlay() {}

	//////////////////////////////////////////////////////////
	// GKMenuInterface
	//////////////////////////////////////////////////////////
	virtual void OnMenuPlay() override;
	virtual void OnMenuHost(const FString& SessionName) override;
	virtual void OnMenuJoin(const FString& Address) override;
	virtual void OnMenuJoin(uint32 Index) override;
	virtual void OnMenuQuit() override;
	virtual void RefreshServerList() override;
	//virtual void OnMenuClose() override;

	//UGKMenu* LoadMenu(GKMenuInterface* Owner, TSubclassOf<class UUserWidget> MenuClass);
	//void CloseMenu();

private:
	GKGameEngine* GameEngine = nullptr;
	GKOnlineSession* OnlineSession = nullptr;
	GKXMLManager* XMLManager = nullptr;

public:
	GK_GET(GKGameEngine*, GameEngine);
	GK_GET(GKOnlineSession*, OnlineSession);
	GK_GET(GKXMLManager*, XMLManager);

public:	
	// SaveGame: https://www.youtube.com/watch?v=yOk1hk8uDlA
	//void InitSaveGame(
};
