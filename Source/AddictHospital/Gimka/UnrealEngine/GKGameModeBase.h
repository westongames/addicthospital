// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"

#include "Gimka/System/GKGameEngine.h"
#include "Gimka/Tools/Macros/GKHeader.h"

#include "GKGameModeBase.generated.h"

class GKEntityManager;

/**
 * GameMode exists only on the server, not clients!
 */
UCLASS()
class AGKGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGKGameModeBase();
	
	// https://www.youtube.com/watch?v=2aUYBzmefpM
	//virtual class AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	//virtual bool ShouldSpawnAtStartSpot(AController* Player) override { return false; }

protected:
	virtual void StartPlay() override;

private:
	//GKGameEngine* GameEngine;

	//GKEntityManager* EntityManager;		// TODO: this will be inherited to GKRegion

public:
	//GKPTR_GET(GKGameEngine, GameEngine)
};
