// Fill out your copyright notice in the Description page of Project Settings.


#include "GKGameModeBaseLobby.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/UnrealEngine/GKGameInstance.h"
#include "TimerManager.h"


void AGKGameModeBaseLobby::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
	++NumberOfPlayers;
	GKLog1("Player login", NumberOfPlayers);
	if (NumberOfPlayers >= 2)
	{
		GKLog("Reached 2 players!");
		GetWorldTimerManager().SetTimer(GameStartTimer, this, &AGKGameModeBaseLobby::StartGame, 10);
	}

}

void AGKGameModeBaseLobby::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	--NumberOfPlayers;
	GKLog1("Player logout!", NumberOfPlayers);
}

void AGKGameModeBaseLobby::StartGame()
{
	UGKGameInstance* GameInstance = Cast<UGKGameInstance>(GetGameInstance());
	GKEnsure(GameInstance);

	GameInstance->GetOnlineSession()->StartSession();

	UWorld* World = GetWorld();
	GKEnsure(World);

	bUseSeamlessTravel = true;
	World->ServerTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap?listen");
}
