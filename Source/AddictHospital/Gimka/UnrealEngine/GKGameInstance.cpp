// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/UnrealEngine/GKGameInstance.h"

#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/System/GKGameEngine.h"
#include "GameFramework/GameModeBase.h"
#include "Gimka/Database/GKXMLManager.h"
//#include "Gimka/UnrealEngine/GKOnlineSession.h"

#include "Gimka/Menu/GKMenu.h"
#include "Blueprint/UserWidget.h"

/*
https://www.udemy.com/course/unrealmultiplayer/learn/lecture/8099224#questions
Reference Counting in Unreal:
- Use a TSharedPtr<AActor>
- Constructing increments the count (on the stack). Destructing decrements the count.
Garbage Collection in Unreal:
- All UObjects are automatically in the "set"
- Unreal starts from the "root set" (can set UObject as root set)
- Unreal walks all the UProperty pointer (from UObject)
- Any UObject not found can be deleted.
*/


UGKGameInstance::UGKGameInstance(const FObjectInitializer& ObjectInitializer)
{
	GKLog("UGKGameInstance constructor");
	/*ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/MenuSystem/WBPMainMenu"));
	if (!ensure(MenuBPClass.Class)) return;

	MenuClass = MenuBPClass.Class;*/

	//GKLog("Found WBPMainMenu!");
}

void UGKGameInstance::Init()
{
	Super::Init();
	//GKLog1("UGKGameInstance::Init Found class %s", MenuClass->GetName());

	if (GetEngine() == nullptr)
	{
		GKError("GetEngine is null");
	}
	GKLogger::Reset(GetEngine());

	if (GameEngine == nullptr)
	{
		GKLog("Created GKGameEngine");
		GameEngine = new GKGameEngine(this);
		GameEngine->Create();
	}

	if (OnlineSession == nullptr)
	{
		// Note: this doesn't create any OnlineSubsystem yet. It's not needed unless player do Host/Join.
		GKLog("Created GKOnlineSession");
		OnlineSession = new GKOnlineSession(this);
		//OnlineSession->OnInit();
	}

	if (XMLManager == nullptr)
	{
		GKLog("Created GKXMLManager");
		XMLManager = new GKXMLManager();
		XMLManager->Init();
		XMLManager->ParseAllXMLs();
	}
	
}

void UGKGameInstance::OnMenuHost(const FString& SessionName)
{
	GKMenuInterface::OnMenuHost(SessionName);

	if (!OnlineSession->CreateSession(SessionName))
	{
		GKScreenError("Can't create OnlineSession");
		ReopenMenu();
	}
}

void UGKGameInstance::OnCreateSessionComplete(const FName& SessionName, bool Success)
{
	GKLog1("Session created!", Success);
	if (Success)
	{
		StartHost();
	}
	else
	{
		GKScreenError("Can't create Session!");
		ReopenMenu();
	}
}

//void UGKGameInstance::OnFindSessionsComplete(const TArray<FString>& ServerNames, bool Success)
//{
//	SetServerList
//}

// Manual join using IPAddress
void UGKGameInstance::OnMenuJoin(const FString& Address)
{
	GKMenuInterface::OnMenuJoin(Address);

	StartJoin(Address);
}

// Join using FindSession
void UGKGameInstance::OnMenuJoin(uint32 Index)
{
	GKMenuInterface::OnMenuJoin(Index);

	// This will call OnMenuJoin(const FString& Address) if succesful
	if (!OnlineSession->JoinSession(Index))
	{
		GKScreenError("Can't join server!");
		ReopenMenu();
	}
}

void UGKGameInstance::OnMenuPlay()
{
	GKMenuInterface::OnMenuPlay();

	StartPlay();
}

void UGKGameInstance::OnMenuQuit()
{
	GKMenuInterface::OnMenuQuit();

	ReturnToMainMenuHost();


	/*APlayerController* PlayerController = GetFirstLocalPlayerController();
	GKEnsure(PlayerController);

	UWorld* World = PlayerController->GetWorld();
	GKEnsure(World);

	if (World->IsServer()) {
		AGameModeBase* GameMode = World->GetAuthGameMode<AGameModeBase>();
		if (GameMode) {
			GameMode->ReturnToMainMenuHost();
		}
	}
	else {
		if (PlayerController) {
			PlayerController->ClientReturnToMainMenu("Back to main menu");
		}
	}*/
}

void UGKGameInstance::RefreshServerList()
{
	OnlineSession->FindSessions();
}


void UGKGameInstance::Shutdown()
{
	if (GameEngine)
	{
		GameEngine->OnDestroy();
		//delete GameEngine;
		//GameEngine = nullptr;
	}

	if (OnlineSession)
	{
		OnlineSession->OnDestroy();
	}

	if (XMLManager)
	{
		XMLManager->OnDestroy();
	}
}

// https://cpp.hotexamples.com/examples/-/APlayerController/ClientReturnToMainMenu/cpp-aplayercontroller-clientreturntomainmenu-method-examples.html
void UGKGameInstance::ReturnToMainMenuHost()
{
	//FString RemoteReturnReason = NSLOCTEXT("NetworkErrors", "HostHasLeft", "Host has left the game.").ToString();

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	GKEnsure(PlayerController);

	UWorld* World = PlayerController->GetWorld();
	GKEnsure(World);

	// DestroySession regardless if it's server or client
	OnlineSession->DestroySession();

	if (World->IsServer()) 
	{
		// Make sure to destroy Session if host is quitting to main menu.
		//OnlineSession->DestroySession();
		AGameModeBase* GameMode = World->GetAuthGameMode<AGameModeBase>();
		if (GameMode) {
			GameMode->ReturnToMainMenuHost();
		}
	}
	else 
	{
		if (PlayerController) 
		{
			FString LocalReturnReason(TEXT(""));
			PlayerController->ClientReturnToMainMenu(LocalReturnReason);// "Back to main menu");
		}
	}

	//APlayerController* Controller = NULL;
	//PlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator();
	//for (; Iterator; ++Iterator)
	//{
	//	APlayerController* Controller1 = *Iterator;
	//	if (Controller && !Controller->IsLocalPlayerController() && Controller->IsPrimaryPlayer())
	//	{
	//		// Clients
	//		Controller->ClientReturnToMainMenu(RemoteReturnReason);
	//	}
	//}

	//Iterator.Reset();
	//for (; Iterator; ++Iterator)
	//{
	//	Controller = *Iterator;
	//	if (Controller && Controller->IsLocalPlayerController() && Controller->IsPrimaryPlayer())
	//	{
	//		Controller->ClientReturnToMainMenu(LocalReturnReason);
	//		break;
	//	}
	//}
}


//UGKMenu* UGKGameInstance::LoadMenu(GKMenuInterface* Owner, TSubclassOf<class UUserWidget> MenuClass)
//{
//	if (ActiveMenu != nullptr) return ActiveMenu;		// Was crashing so added this.
//	check(ActiveMenu == nullptr);
//
//	if (!ensure(MenuClass)) return nullptr;
//
//	ActiveMenu = CreateWidget<UGKMenu>(this, MenuClass);
//	if (!ensure(ActiveMenu)) return nullptr;;
//
//	bool Success = ActiveMenu->Setup();
//	check(Success);
//	ActiveMenu->SetMenuInterface(Owner);
//
//	return ActiveMenu;
//}

//void UGKGameInstance::CloseMenu()
//{
//	if (ActiveMenu != nullptr)
//	{
//		ActiveMenu->Teardown();
//		ActiveMenu = nullptr;			// Is this ok without delete?
//	}
//}

//void UGKGameInstance::OnMenuClose()
//{
//	CloseMenu();
//}


