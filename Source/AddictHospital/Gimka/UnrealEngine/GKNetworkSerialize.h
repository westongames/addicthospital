// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"

#include "Gimka/Network/GKMessageNetwork.h"
#include "Gimka/Network/GKMessageNetworkToServer.h"
#include "Gimka/Network/GKMessageNetworkToServerCommand.h"
#include "Gimka/Network/GKMessageNetworkToClient.h"
#include "Gimka/Network/GKMessageNetworkToClientCommand.h"

#include "GKNetworkSerialize.generated.h"

/**
 * A struct to encapsulate and serialize GKMessageNetwork and its subclass for transmitting over the net.
 */
USTRUCT()
struct FGKNetworkSerializeToServer
{
	GENERATED_USTRUCT_BODY()
public:
	FGKNetworkSerializeToServer() {}
	~FGKNetworkSerializeToServer() {}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		return Message.NetSerialize(Ar, Map, bOutSuccess);
	}

private:
	GKMessageNetworkToServer Message;

public:
	GK_GETSETCR(GKMessageNetworkToServer, Message);
};

USTRUCT()
struct FGKNetworkSerializeToServerCommand
{
	GENERATED_USTRUCT_BODY()
public:
	FGKNetworkSerializeToServerCommand() {}
	~FGKNetworkSerializeToServerCommand() {}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		return Message.NetSerialize(Ar, Map, bOutSuccess);
	}

private:
	GKMessageNetworkToServerCommand Message;

public:
	GK_GETSETCR(GKMessageNetworkToServerCommand, Message);
};

USTRUCT()
struct FGKNetworkSerializeToClient
{
	GENERATED_USTRUCT_BODY()
public:
	FGKNetworkSerializeToClient() {}
	~FGKNetworkSerializeToClient() {}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		return Message.NetSerialize(Ar, Map, bOutSuccess);
	}

private:
	GKMessageNetworkToClient Message;

public:
	GK_GETSETCR(GKMessageNetworkToClient, Message);
};

USTRUCT()
struct FGKNetworkSerializeToClientCommand
{
	GENERATED_USTRUCT_BODY()
public:
	FGKNetworkSerializeToClientCommand() {}
	~FGKNetworkSerializeToClientCommand() {}

	bool NetSerialize(FArchive& Ar, class UPackageMap* Map, bool& bOutSuccess)
	{
		return Message.NetSerialize(Ar, Map, bOutSuccess);
	}

private:
	GKMessageNetworkToClientCommand Message;

public:
	GK_GETSETCR(GKMessageNetworkToClientCommand, Message);
};

template<>
struct TStructOpsTypeTraits<FGKNetworkSerializeToServer> : public TStructOpsTypeTraitsBase2<FGKNetworkSerializeToServer>
{
	enum
	{
		WithNetSerializer = true
	};
};

template<>
struct TStructOpsTypeTraits<FGKNetworkSerializeToServerCommand> : public TStructOpsTypeTraitsBase2<FGKNetworkSerializeToServerCommand>
{
	enum
	{
		WithNetSerializer = true
	};
};

template<>
struct TStructOpsTypeTraits<FGKNetworkSerializeToClient> : public TStructOpsTypeTraitsBase2<FGKNetworkSerializeToClient>
{
	enum
	{
		WithNetSerializer = true
	};
};

template<>
struct TStructOpsTypeTraits<FGKNetworkSerializeToClientCommand> : public TStructOpsTypeTraitsBase2<FGKNetworkSerializeToClientCommand>
{
	enum
	{
		WithNetSerializer = true
	};
};
