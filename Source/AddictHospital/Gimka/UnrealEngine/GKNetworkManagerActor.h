// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Gimka/UnrealEngine/GKNetworkSerialize.h"
#include "Gimka/Network/GKNetworkManager.h"

#include "GKNetworkManagerActor.generated.h"

class GKNetworkPlayer;

UCLASS()
class AGKNetworkManagerActor : public AActor, public GKNetworkManager
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGKNetworkManagerActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


	virtual void OnStartGame() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Note: can't use const reference otherwise it won't be set when receiving
	UFUNCTION(NetMulticast, unreliable)
		virtual void RPCServerToAll(FGKNetworkSerializeToClient Message) override;

	// Note: Use reliable even for empty ServerGameTick because if higher unrealiable serverGameTick 
	// arrive before lower reliable gameTick with playerInput then client is messed up. 
	// So everything has to be reliable. Unreliable might be like sound/ animation if we have any.
	// Use reliable only for thing like sound, animation, etc.
	UFUNCTION(NetMulticast, reliable)
		virtual void RPCServerToAllReliable(FGKNetworkSerializeToClient Message) override;

	// Message with command always reliable
	UFUNCTION(NetMulticast, reliable)
		virtual void RPCServerToAllCommandReliable(FGKNetworkSerializeToClientCommand Message) override;


};
