// Fill out your copyright notice in the Description page of Project Settings.


#include "GKSortedArray.h"
#include "Gimka/Tools/Macros/GKLogger.h"

template<typename TID, typename T>
GKSortedArray<TID, T>::GKSortedArray()
{
}

template<typename TID, typename T>
GKSortedArray<TID, T>::~GKSortedArray()
{
}

// Note: insert sorted. ID doesn't need to be unique.
template<typename TID, typename T>
void GKSortedArray<TID, T>::Add(const T& item)
{
	// Check if empty
	int32 r = TArray<T>::Num() - 1;
	if (r < 0)
	{
		TArray<T>::Add(item);
		return;
	}

	TID ID = item.GetID();
	// Check if ID is smallest
	if (ID <= GetData()[0].GetID())
	{
		//checkSlow(ID != GetData()[0].GetID());
		TArray<T>::Insert(item, 0);
		return;
	}

	// Check if ID is highest
	if (ID >= GetData()[r].GetID())
	{
		//checkSlow(ID != GetData()[r].GetID());
		TArray<T>::Add(item);
		return;
	}

	// At this point, there are at least 2 items and the new item must be placed in between 0 to Num-1.
	// Note: ID doesn't need to be unique.
	int32 l = 1;
	while (l <= r)
	{
		int32 m = l + (r - l) / 2;

		if (ID >= GetData()[m - 1].GetID())
		{
			if (ID <= GetData()[m].GetID())
			{
				// Insert at m
				TArray<T>::Insert(item, m);
				return;
			}
			else
			{
				// Ignore left half
				l = m + 1;
			}
		}
		else
		{
			// Ignore right half
			r = m - 1;
		}
	}

	// impossible case
	GKError("Can't put item into sorted GetData() ");

	//do
	//{
	//	int32 m = l + (r - l) / 2;

	//	// Check if we should insert element at mid
	//	if (ID < GetData()[m].GetID() 

	//	if (GetData()[m].GetID() == ID) return m;

	//	// If ID is greater, ignore left half
	//	if (GetData()[m].GetID() < ID)
	//	{
	//		l = m + 1;
	//	}
	//	else
	//	{
	//		// If ID is smaller, ignore right half
	//		r = m - 1;
	//	}
	//} while (l <= r);

	//// At this point, there are at least 2 items in the GetData(). So index is at least 1.
	//int32 index = num / 2;
	//while (true)
	//{
	//	if (ID < GetData()[index].GetID())
	//	{
	//		if (ID >= GetData()[index].GetID())
	//		{
	//			checkSlow(ID != GetData()[index].GetID());
	//		}
	//	}
	//}
}

template<typename TID, typename T>
bool GKSortedArray<TID, T>::FindID(TID ID, int32& Index) const
{
	int32 l = 0, r = Num() - 1;
	while (l <= r)
	{
		Index = l + (r - l) / 2;

		// Check if ID is present at mid
		if (GetData()[Index].GetID() == ID) return true;

		// If ID is greater, ignore left half
		if (GetData()[Index].GetID() < ID)
		{
			l = Index + 1;
		}
		else
		{
			// If ID is smaller, ignore right half
			r = Index - 1;
		}
	}

	// If we reach here, then element was not present
	return false;
}

template<typename TID, typename T>
bool GKSortedArray<TID, T>::ContainID(TID ID) const
{
	int32 Index;
	return FindID(ID, Index);
}