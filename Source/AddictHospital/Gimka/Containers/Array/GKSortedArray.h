// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 - Array GKEntity in GKRegion should be double link list
 - GKVisualEntity has a pointer to a VisualNode which might be null (no visual) or 
			to a double link list node of GKVisualManager::ListVisual { GKEntity*, Actor*, lastUpdated, status, isModified }
 - GKRegion always do OnPreUpdate which is once per frame. 
	- It check with GKVisualManager to update its visible flag.
	- If it goes from visible to not visible, run through all of its entities, if entity has visualNode, post messageVisualDestroy(entity)
	- If it goes from not visible to visible, run through all of its entities, if entity doesn't have visualNode, post messageVisualCreate(entity)
 - In applyChanges:
	- if an entity moves from not visible region to visible region, post messageVisualCreate(entity)
	- If an enttiy moves from visible region to not visible region, it add messageVisual [delete after X gameTicks if still not visible]
 - After applyChanges, GKVisualManager run through all nearby visible GKRegion:
		- Mark GKRegion as visible or not visible.
 - When a GKRegion is mar
		- it check if GKRegion is visible and it doesn't have node, it add messageVisual [createNodeVisual]
 - After visualChange, GKVisualManager run through all entities of nearby visible GKRegion.
			For each entity, check if entity.visualNode is null then create actor, add to ListVisual and set the entity.visualNode
 - In single thread, go through all messageVisualChanges (the recipient will have the node ptr, which contains the actor):
			Apply messageVisualChanges - so the entity need to be aware of it.
 - In single thread, delete any visualNode if requested. Including the actor.
 - IN sing
 GKVisualManager (UCLASS) has TrackedArray = GKSortedArray<GKEntity.ID, [GKEntity*, Actor*]>
 - GKVisualManager iterate through all entities of nearby GKRegion. For each entity:
	- If it's not in TrackedArray, create and add it.
 */
template<typename TID, typename T>
class GKSortedArray : public TArray<T>
{
public:
	GKSortedArray();
	~GKSortedArray();

	void Add(const T& item);
	bool FindID(TID ID, int32& Index) const;
	bool ContainID(TID ID) const;
	//uint32 Num() { return array.Num(); }

	/*FORCEINLINE T& operator[](SizeType Index)
	{
		return array[Index];
	}

	FORCEINLINE const T& operator[](SizeType Index) const
	{
		return array[Index];
	}*/

//private:
	//TArray<T> array;
};
