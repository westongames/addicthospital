// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HAL/UnrealMemory.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * https://www.unrealengine.com/en-US/blog/optimizing-tarray-usage-for-performance
 */
template <typename TData, typename TIndex>
class GKArray
{
	TData* Array = nullptr;
	TIndex Capacity = 0;
	TIndex Count = 0;

public:
	/*GKArray();
	GKArray(TIndex initialCapacity);
	~GKArray();*/

	//FORCEINLINE TData& operator[](TIndex Index);
	//FORCEINLINE const TData& operator[](TIndex index) : const;

	//FORCEINLINE bool Find(const TData& Item, TIndex& Index) const;
	//TIndex Find(const TData& Item) const;

	//FORCEINLINE bool FindLast(const TData& Item, TIndex& Index) const;
	//TIndex FindLast(const TData& Item) const;

	//bool Contains(const TData& Item) const;

	////friend FArchive& operator<<(FArchive& ar, GKArray& A);
	////void CountBytes(FArchive& Ar) const;

	//TIndex AddUninitialized(TIndex Count = 1);
	//void InsertUnitialized(TIndex Index, TIndex Count = 1);
	//void InsertZeroed(TIndex index, TIndex Count = 1);
	//TData& InsertZeroed_GetRef(TIndex Index);


	//void Log() const;

private:
	//void EnsureCapacity(TIndex NewCapacity);

public:
	GK_GET(TIndex, Count);
	GK_GET(TIndex, Capacity);
};
