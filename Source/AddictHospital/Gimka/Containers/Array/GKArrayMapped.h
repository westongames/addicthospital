// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * Combine TMap and TArray
 */
template <typename TKey, typename TValue>
class GKArrayMapped
{
public:
	/*GKArrayMapped();
	~GKArrayMapped();*/

	FORCEINLINE void Add(TKey& HashKey, TValue& Value)
	{
		Map.Add(HashKey, Value);
		Array.Add(Value);
	}

	FORCEINLINE void Add(const TKey& HashKey, TValue& Value)
	{
		Map.Add(HashKey, Value);
		Array.Add(Value);
	}

	FORCEINLINE void Add(TKey& HashKey, const TValue& Value)
	{
		Map.Add(HashKey, Value);
		Array.Add(Value);
	}

	FORCEINLINE void Add(const TKey& HashKey, const TValue& Value)
	{
		Map.Add(HashKey, Value);
		Array.Add(Value);
	}

	FORCEINLINE void Emplace(TKey& HashKey, TValue& Value)
	{
		Map.Emplace(HashKey, Value);
		Array.Emplace(Value);
	}

	FORCEINLINE int32 FindValue(TValue Value) const
	{
		// Return INDEX_NONE if not found
		return Array.Find(Value);
	}

	FORCEINLINE bool TryFind(TKey HashKey, TValue& Value)
	{
		TValue* ValuePtr = Map.Find(HashKey);
		if (ValuePtr != nullptr)
		{
			Value = *ValuePtr;
			return true;
		}
		return false;
	}

	FORCEINLINE bool TryFind(TKey HashKey, const TValue& Value) const
	{
		TValue* ValuePtr = Map.Find(HashKey);
		if (ValuePtr != nullptr)
		{
			Value = *ValuePtr;
			return true;
		}
		return false;
	}

	FORCEINLINE TValue& Get(TKey HashKey)
	{
		return Map[HashKey];
	}

	FORCEINLINE const TValue& Get(TKey HashKey) const
	{
		return Map[HashKey];
	}

	FORCEINLINE TValue& operator[] (int32 Index)
	{
		return Array[Index];
	}

	FORCEINLINE const TValue& operator[] (int32 Index) const
	{
		return Array[Index];
	}
	
	// This RemoveAt should be the most performant
	TValue RemoveAt(int32 Index)
	{
		TValue Value = Array[Index];
		TKey HashKey = Value->GetHashKey();
		Array.RemoveAtSwap(Index);
		Map.Remove(HashKey);
		return Value;
	}

	TValue Remove(TKey HashKey)
	{
		if (Map.RemoveAndCopyValue(HashKey, TValue Value))
		{
			Array.RemoveSingleSwap(Value);
			return Value;
		}
		return nullptr;
	}

	int32 Remove(TValue Value)
	{
		int Index = Find(Value);
		if (Index == INDEX_NONE) return INDEX_NONE;
		TValue Value = Array[Index];
		TKey HashKey = Value->GetHashKey();
		Array.RemoveAtSwap(Index);
		Map.Remove(HashKey);
		return Index;
	}

	FORCEINLINE int32 Num() { return Array.Num(); } const

	FORCEINLINE void Empty()
	{
		Array.Empty();
		Map.Empty();
	}

	const TArray<TValue>& GetArray() { return Array; }

	void Log()
	{
		UE_LOG(LogTemp, Warning, TEXT("GKArrayMapped Size %d"), Array.Num());
	}

private:
	TArray<TValue> Array;
	TMap<TKey, TValue> Map;
};
