// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Container/GKContainerCache.h"
#include "Gimka/Containers/Container/GKContainer.h"

#define CONTAINER_DEFINE_ADD(VarType)										\
	void GKContainerCache::Add(uint16 ID, VarType Value)					\
	{																		\
		check(EntryIndexes[ID - IDStart] > EntryMaxIndex);					\
		EntryIndexes[ID - IDStart] = ContainerData->EntryCount;					\
		ContainerData->Add(ID, Value);											\
	}

#define CONTAINER_DEFINE_GETSET(VarType)								\
	void GKContainerCache::Set(uint16 ID, VarType Value)				\
	{																	\
		*(VarType*)GetAddress(ID) = Value;								\
	}																	\
	void GKContainerCache::Get(uint16 ID, VarType& Value) const			\
	{																	\
		Value = *(VarType*)GetAddress(ID);								\
	}

#define CONTAINER_DEFINE_GETSET_SAFE(VarType)							\
	void GKContainerCache::SetSafe(uint16 ID, VarType Value)			\
	{																	\
		char* Address = GetAddress(ID);									\
		if (Address)													\
		{																\
			*(VarType*)Address = Value;									\
		}																\
		else															\
		{																\
			Add(ID, Value);												\
		}																\
	}																	\
	bool GKContainerCache::GetSafe(uint16 ID, VarType& Value) const		\
	{																	\
		char* Address = GetAddress(ID);									\
		if (Address)													\
		{																\
			Value = *(VarType*)Address;									\
			return true;												\
		}																\
		Value = VarType();												\
		return false;													\
	}

#define CONTAINER_DEFINE(VarType)										\
	CONTAINER_DEFINE_ADD(VarType)										\
	CONTAINER_DEFINE_GETSET(VarType)									\
	CONTAINER_DEFINE_GETSET_SAFE(VarType)

GKContainerCache::GKContainerCache()
{
}

void GKContainerCache::Init(GKContainerData* iContainerData, uint16 iIDStart, uint16 iIDEnd)
{
	ContainerData = iContainerData;
	IDStart = iIDStart;
	int32 count = iIDEnd - IDStart + 1;
	check(count <= EntryMaxIndex + 1);
	EntryIndexes = new uint16[count];
	FMemory::Memset(EntryIndexes, 255, count * sizeof(uint16));
}

GKContainerCache::~GKContainerCache()
{
	if (EntryIndexes)
	{
		delete EntryIndexes;
		EntryIndexes = nullptr;
	}
}

CONTAINER_DEFINE(int16);
CONTAINER_DEFINE(uint16);
CONTAINER_DEFINE(int32);
CONTAINER_DEFINE(uint32);

#undef CONTAINER_DEFINE
#undef CONTAINER_DEFINE_ADD
#undef CONTAINER_DEFINE_GETSET
#undef CONTAINER_DEFINE_GETSET_SAFE


char* GKContainerCache::GetAddress(uint16 ID) const
{
	uint16 index = ID - IDStart;
	uint16 EntryIndex = EntryIndexes[index];
	check(EntryIndex != EntryNotFound);
	if (EntryIndex == EntryNotSearched)
	{
		int32 theIndex = ContainerData->Find(ID);
		if (theIndex == -1)
		{
			EntryIndexes[index] = EntryNotFound;
			return nullptr;
		}
		EntryIndexes[index] = EntryIndex = theIndex;
	}
	return ContainerData->Data + ContainerData->ArrayEntries[EntryIndex].Offset;
}

void GKContainerCache::Log() const
{
	ContainerData->Log();
}

bool GKContainerCache::Contains(uint16 ID) const
{
	uint16 index = ID - IDStart;
	uint16 EntryIndex = EntryIndexes[index];
	if (EntryIndex == EntryNotSearched)
	{
		int32 theIndex = ContainerData->Find(ID);
		if (theIndex == -1)
		{
			EntryIndexes[index] = EntryNotFound;
			return false;
		}
		EntryIndexes[index] = EntryIndex = theIndex;
	}
	return EntryIndex <= EntryMaxIndex;
}

uint16 GKContainerCache::operator[](int32 Index) const
{ 
	return ContainerData->ArrayEntries[Index].ID; 
}

uint16 GKContainerCache::Num() const
{ 
	return ContainerData->EntryCount; 
}

//char* GKContainerCache::operator[](uint16 ID)
//{
//	uint16 index = EntryIndexes[ID - IDStart];
//	if (index <= EntryMaxIndex)
//	{
//		return Container->Data + Container->ArrayEntries[index].Offset;
//	}
//	return nullptr;
//}

