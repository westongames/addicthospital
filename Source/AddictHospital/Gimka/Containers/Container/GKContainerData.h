// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Container/GKContainer.h"


#define CONTAINER_SUPPORT(VarType)										\
	virtual void SetSafe(uint16 ID, VarType Value) override;			\
	virtual bool GetSafe(uint16 ID, VarType& Value) const override;		\
	virtual void Set(uint16 ID, VarType Value) override;				\
	virtual void Get(uint16 ID, VarType& Value) const override;			\
	virtual void Add(uint16 ID, VarType Value) override;

/**
 * https://michaeljcole.github.io/wiki.unrealengine.com/Garbage_Collection_&_Dynamic_Memory_Allocation/
 * https://pzurita.wordpress.com/2015/08/26/adding-memory-tracking-features-to-unreal-engine-4/
 */
class GKContainerData : public GKContainer
{
	template<typename T>
	friend struct GKContainerArrayIterator;
	
	template<typename T>
	friend struct GKContainerIterator;

	friend class GKContainerCache;

public:
	GKContainerData(const GKContainerData& ContainerData);
	GKContainerData();
	virtual ~GKContainerData();

#include "Gimka/Containers/Container/GKContainerHeader.h"
	//CONTAINER_SUPPORT(int16);
	//CONTAINER_SUPPORT(uint16);
	//CONTAINER_SUPPORT(int32);
	//CONTAINER_SUPPORT(uint32);

	//void SetSafe(uint16 ID, uint16 Value);
	//bool GetSafe(uint16 ID, uint16& Value);
	//void Set(uint16 ID, uint16 Value);
	//void Get(uint16 ID, uint16& Value);
	//void Add(uint16 ID, uint16 Value);

	//void SetArraySafe(uint16 ID, uint16 Index, uint16 Value);
	//bool GetArraySafe(uint16 ID, uint16 Index, uint16& Value);
	//void AppendArraySafe(uint16 ID, uint16 Index, uint16 Value);
	FORCEINLINE virtual uint16 operator[](int32 Index) const override { return ArrayEntries[Index].ID; }
	FORCEINLINE virtual uint16 Num() const override { return EntryCount; }

	FORCEINLINE virtual bool Contains(uint16 ID) const override;
	virtual void Log() const;

	// Same like Find but return the address
	virtual char* GetAddress(uint16 ID) const override;

	//void Addo(uint16 ID, int32 Value);


private:
	// Find the ID in the ArrayEntries. Return -1 if not found!
	int32 Find(uint16 ID) const;

	// Called before adding a new entry
	void EnsureCapacity(uint32 MinDataCapacity);

	void ReallocArrayEntries(uint16 NewCapacity);
	void ReallocData(uint32 NewCapacity);

	// Custom Entry, i.e. GKContainerArrayIterator.
	// PlusOffset add to Entry.Offset to avoid having to add the HeaderSize to get to the data
	uint16 AddEntry(uint16 ID, uint32 EntryDataCapacity, int32 PlusOffset = 0);
	void ExpandEntryDataCapacity(uint16 EntryIndex, uint32 OldEntryDataCapacity, uint32 NewEntryDataCapacity);

	//uint16 ArrayAdd(uint16 ID, uint16 EntrySize, uint16 EntryCapacity);

	// Return the pointer to the Array Data
	//char* ArrayGetInfo(uint16 EntryIndex, uint16& ItemCount, uint16& ItemCapacity);

	//void ArrayEnsureCapacity(uint16 EntryIndex, uint16 MinEntryCapacity);
	//void ArraySetItemCount(uint16 EntryIndex, uint16 ItemCount);
	//void ArraySetItemCapacity(uint16 EntryIndex, uint16 ItemCapacity);
	//uint16 ArrayItemCount(uint16 EntryIndex) const;
	//uint16 ArrayItemCapacity(uint16 EntryIndex) const;


	struct Entry
	{
		uint16 ID;
		uint32 Offset;
	};

	// Array of Entries
	Entry* ArrayEntries = nullptr;
	uint16 EntryCount = 0;			// Current entry amount
	uint16 EntryCapacity = 0;		// Maximum number of entries

	// The Data
	char* Data = nullptr;
	uint32 DataSize = 0;			// The current data used in bytes. There may be unused fragments in the data, which can be cleared with a function call.
	uint32 DataCapacity = 0;		// Maximum data allocated
};

#undef CONTAINER_SUPPORT