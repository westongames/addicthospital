// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Container/GKContainerIterator.h"

template<typename T>
GKContainerIterator<T>::GKContainerIterator(GKContainer* iContainer, uint16 iID)
{
	Container = iContainer;
	ID = iID;
	EntryIndex = Container->Find(ID);
	if (EntryIndex != -1)
	{
		Value = *DataPtr();
	}
	else
	{
		Value = T();
	}
}

template<typename T>
GKContainerIterator<T>::~GKContainerIterator()
{
}

template<typename T>
GKContainerIterator<T>::operator T() const
{
	return Value;
}

template<typename T>
bool GKContainerIterator<T>::operator==(const T& Other) const
{
	return Value == Other;
}

template<typename T>
bool GKContainerIterator<T>::operator!=(const T& Other) const
{
	return Value != Other;
}

template<typename T>
bool GKContainerIterator<T>::Exists()
{
	return EntryIndex != -1;
}

template<typename T>
GKContainerIterator<T>& GKContainerIterator<T>::operator+=(const T& Other) const
{
	Value += Other;
	if (EntryIndex != -1)
	{
		*DataPtr() = Value;
	}
	else
	{
		EntryIndex = Container->EntryCount;
		Container->Add(ID, Value);
	}

	return *this;
}

template<typename T>
T* GKContainerIterator<T>::DataPtr()
{
	check(EntryIndex != -1);
	return (T*)(Container->Data + Container->ArrayEntries[EntryIndex].Offset);
}
