// Fill out your copyright notice in the Description page of Project Settings.


//#include "GKContainer.h"
//#include "HAL/UnrealMemory.h"
//#include "Gimka/Tools/Macros/GKLogger.h"
//
//#define CONTAINER_DEFINE_ADD(VarType)									\
//	void GKContainer::Add(uint16 ID, VarType Value)						\
//	{																	\
//		check(!Contains(ID));											\
//		uint32 NewDataSize = DataSize + sizeof(VarType);				\
//		EnsureCapacity(NewDataSize);									\
//		*((VarType*)(Data + DataSize)) = Value;							\
//		Entry& entry = ArrayEntries[EntryCount++];						\
//		entry.ID = ID;													\
//		entry.Offset = DataSize;										\
//		DataSize = NewDataSize;											\
//	}
//
//#define CONTAINER_DEFINE_GETSET(VarType)								\
//	void GKContainer::Set(uint16 ID, VarType Value)						\
//	{																	\
//		*(VarType*)GetAddress(ID) = Value;								\
//	}																	\
//	void GKContainer::Get(uint16 ID, VarType& Value) const				\
//	{																	\
//		Value = *(VarType*)GetAddress(ID);								\
//	}
//
//#define CONTAINER_DEFINE_GETSET_SAFE(VarType)							\
//	void GKContainer::SetSafe(uint16 ID, VarType Value)					\
//	{																	\
//		char* Address = GetAddress(ID);									\
//		if (Address)													\
//		{																\
//			*(VarType*)Address = Value;									\
//		}																\
//		else															\
//		{																\
//			Add(ID, Value);												\
//		}																\
//	}																	\
//	bool GKContainer::GetSafe(uint16 ID, VarType& Value) const			\
//	{																	\
//		char* Address = GetAddress(ID);									\
//		if (Address)													\
//		{																\
//			Value = *(VarType*)Address;									\
//			return true;												\
//		}																\
//		Value = VarType();												\
//		return false;													\
//	}
//
//#define CONTAINER_DEFINE(VarType)										\
//	CONTAINER_DEFINE_ADD(VarType)										\
//	CONTAINER_DEFINE_GETSET(VarType)									\
//	CONTAINER_DEFINE_GETSET_SAFE(VarType)
//
//GKContainer::GKContainer()
//{
//}
//
//GKContainer::~GKContainer()
//{
//	if (ArrayEntries)
//	{
//		FMemory::Free(ArrayEntries);
//		FMemory::Free(Data);
//		ArrayEntries = nullptr;
//		Data = nullptr;
//	}
//}
//
//CONTAINER_DEFINE(int16);
//CONTAINER_DEFINE(uint16);
//CONTAINER_DEFINE(int32);
//CONTAINER_DEFINE(uint32);
//
//#undef CONTAINER_DEFINE
//#undef CONTAINER_DEFINE_ADD
//#undef CONTAINER_DEFINE_GETSET
//#undef CONTAINER_DEFINE_GETSET_SAFE
//
////
////void GKContainer::SetSafe(uint16 ID, uint16 Value)
////{
////	char* Address = GetAddress(ID);
////	if (Address)
////	{
////		*(uint16*)Address = Value;
////	}
////	else
////	{
////		Add(ID, Value);
////	}
////}
////
////bool GKContainer::GetSafe(uint16 ID, uint16& Value) const
////{
////	char* Address = GetAddress(ID);
////	if (Address)
////	{
////		Value = *(uint16*)Address;
////		return true;
////	}
////	return false;
////}
////
////void GKContainer::Set(uint16 ID, uint16 Value)
////{
////	*(uint16*)GetAddress(ID) = Value;
////}
////
////void GKContainer::Get(uint16 ID, uint16& Value) const
////{
////	Value = *(uint16*)GetAddress(ID);
////}
////
////void GKContainer::Add(uint16 ID, uint16 Value)
////{
////	check(!Exist(ID));
////
////	uint32 NewDataSize = DataSize + sizeof(uint16);
////	EnsureCapacity(NewDataSize);
////
////	*((uint16*)(Data + DataSize)) = Value;
////
////	Entry& entry = ArrayEntries[EntryCount++];
////	entry.ID = ID;
////	entry.Offset = DataSize;
////
////	DataSize = NewDataSize;
////}
//
////void GKContainer::SetArraySafe(uint16 ID, uint16 Index, uint16 Value)
////{
////}
////
////bool GKContainer::GetArraySafe(uint16 ID, uint16 Index, uint16& Value)
////{
////	return false;
////}
////
////void GKContainer::AppendArraySafe(uint16 ID, uint16 Index, uint16 Value)
////{
////}
//
//bool GKContainer::Contains(uint16 ID) const
//{
//	return Find(ID) != -1;
//}
//
//int32 GKContainer::Find(uint16 ID) const
//{
//	int32 l = 0, r = EntryCount - 1;
//	while (l <= r)
//	{
//		int32 m = l + (r - l) / 2;
//
//		const Entry& entry = ArrayEntries[m];
//
//		// Check if ID is present at mid
//		if (entry.ID == ID) return m;
//
//		// If ID is greater, ignore left half
//		if (entry.ID < ID)
//		{
//			l = m + 1;
//		}
//		else
//		{
//			// If ID is smaller, ignore right half
//			r = m - 1;
//		}
//	}
//
//	// If we reach here, then element was not present
//	return -1;
//}
//
//char* GKContainer::GetAddress(uint16 ID) const
//{
//	int32 l = 0, r = EntryCount - 1;
//	while (l <= r)
//	{
//		int32 m = l + (r - l) / 2;
//
//		const Entry& entry = ArrayEntries[m];
//
//		// Check if ID is present at mid
//		if (entry.ID == ID) return Data + entry.Offset;
//
//		// If ID is greater, ignore left half
//		if (entry.ID < ID)
//		{
//			l = m + 1;
//		}
//		else
//		{
//			// If ID is smaller, ignore right half
//			r = m - 1;
//		}
//	}
//
//	// If we reach here, then element was not present
//	return nullptr;
//
//}
//
//void GKContainer::EnsureCapacity(uint32 MinDataCapacity)
//{
//	if (EntryCount >= EntryCapacity)
//	{
//		if (EntryCapacity > 0)
//		{
//			ReallocArrayEntries(EntryCapacity * 2);
//		}
//		else
//		{
//			ReallocArrayEntries(4);
//		}
//	}
//
//	if (MinDataCapacity > DataCapacity)
//	{
//		if (DataCapacity > 0)
//		{
//			ReallocData(FMath::Max(DataCapacity * 2, MinDataCapacity));
//		}
//		else
//		{
//			ReallocData(FMath::Max((uint32)32, MinDataCapacity));
//
//		}
//	}
//}
//
//void GKContainer::ReallocArrayEntries(uint16 NewCapacity)
//{
//    if (NewCapacity > EntryCapacity)
//    {
//        ArrayEntries = (Entry*)FMemory::Realloc(ArrayEntries, NewCapacity * sizeof(Entry));
//        EntryCapacity = NewCapacity;
//    }
//}
//
//void GKContainer::ReallocData(uint32 NewCapacity)
//{
//    if (NewCapacity > DataCapacity)
//    {
//		GKLog1("Realloc OldData", Data);
//        Data = (char*)FMemory::Realloc(Data, NewCapacity);
//		GKLog1("Realloc NewData", Data);
//        DataCapacity = NewCapacity;
//    }
//}
//
//uint16 GKContainer::AddEntry(uint16 ID, uint32 EntryDataCapacity, int32 PlusOffset)
//{
//	check(!Contains(ID));
//	
//	uint32 NewDataSize = DataSize + EntryDataCapacity;
//	EnsureCapacity(NewDataSize);
//	
//	Entry& entry = ArrayEntries[EntryCount++];
//	entry.ID = ID;
//	entry.Offset = DataSize + PlusOffset;
//	
//	DataSize = NewDataSize;
//	
//	return EntryCount - 1;
//}
//
//void GKContainer::ExpandEntryDataCapacity(uint16 EntryIndex, uint32 OldEntryDataCapacity, uint32 NewEntryDataCapacity)
//{
//	check(NewEntryDataCapacity > OldEntryDataCapacity);
//
//	uint32 DeltaCapacity = NewEntryDataCapacity - OldEntryDataCapacity;
//	uint32 NewDataSize = DataSize + DeltaCapacity;
//	GKLog2("Expand DeltaCap, NewDataSize", DeltaCapacity, NewDataSize);
//	EnsureCapacity(NewDataSize);
//
//	// If it's not the last entry, we need to shift down the data
//	GKLog2("EntryIndex, EntryCount", EntryIndex, EntryCount);
//	if (EntryIndex < EntryCount - 1)
//	{
//		const Entry& entry = ArrayEntries[EntryIndex];
//		char* OldDataEndPtr = Data + entry.Offset + OldEntryDataCapacity;
//
//		// Move the data below this array by DeltaCapacity
//		FMemory::Memmove(OldDataEndPtr + DeltaCapacity, OldDataEndPtr, DeltaCapacity);
//
//		// Shift down the offset of all entries below this entry
//		for (int32 Index = EntryIndex + 1; Index < EntryCount; ++Index)
//		{
//			ArrayEntries[Index].Offset += DeltaCapacity;
//		}
//	}
//
//	DataSize = NewDataSize;
//}
//
//
////uint16 GKContainer::ArrayAdd(uint16 ID, uint16 ItemSize, uint16 ItemCapacity)
////{
////	check(!Exist(ID));
////
////	// Array contains ItemCount, ItemCapacity then the array itself
////	uint32 ArrayDataSize = ARRAY_HEADER_SIZE + ItemSize * ItemCapacity;
////	uint32 NewDataSize = DataSize + ArrayDataSize;
////	EnsureCapacity(NewDataSize);
////
////	// Set ItemCount & ItemCapacity
////	uint16* ArrayInfoPtr = (uint16*)(Data + DataSize);
////	*ArrayInfoPtr = 0;
////	*(ArrayInfoPtr + 1) = ItemCapacity;
////
////	Entry& entry = ArrayEntries[EntryCount++];
////	entry.ID = ID;
////	entry.Offset = DataSize;
////
////	DataSize = NewDataSize;
////
////	return EntryCount - 1;
////}
////
////char* GKContainer::ArrayGetInfo(uint16 EntryIndex, uint16& ItemCount, uint16& ItemCapacity)
////{
////	uint16* ArrayInfoPtr = (uint16*)(Data + ArrayEntries[EntryIndex].Offset);
////	ItemCount = *ArrayInfoPtr;
////	ItemCapacity = *(ArrayInfoPtr + 1);
////	return (char*)(ArrayInfoPtr + 2);
////}
//
//
////void GKContainer::ArrayEnsureCapacity(uint16 EntryIndex, uint16 MinEntryCapacity)
////{
////	// Get ItemCount & ItemCapacity
////	const Entry& entry = ArrayEntries[EntryIndex];
////	uint16* ArrayInfoPtr = (uint16*)()
////	entry.ID = ID;
////	entry.Offset = DataSize;
////
////
////}
////
////void GKContainer::ArraySetItemCount(uint16 EntryIndex, uint16 ItemCount)
////{
////	*(uint16*)(Data + ArrayEntries[])
////}
////
////void GKContainer::ArraySetItemCapacity(uint16 EntryIndex, uint16 ItemCapacity)
////{
////}
////
////uint16 GKContainer::ArrayItemCount(uint16 EntryIndex) const
////{
////	return uint16();
////}
////
////uint16 GKContainer::ArrayItemCapacity(uint16 EntryIndex) const
////{
////	return uint16();
////}
////
//
//
//void GKContainer::Log() const
//{
//	UE_LOG(LogTemp, Warning, TEXT("Container Entry %d/%d, Data %d/%d"), EntryCount, EntryCapacity, DataSize, DataCapacity);
//	for (int i = 0; i < EntryCount; ++i)
//	{
//		const Entry& entry = ArrayEntries[i];
//		UE_LOG(LogTemp, Warning, TEXT("  %d. %d = %d"), i, entry.ID, *(uint16*)(Data + entry.Offset));
//	}
//}
//
//int32 GKContainer::Test()
//{
//	return int32();
//}
