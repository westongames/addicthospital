// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class GKContainerData;

/**
 * Note that adding to Array or adding new entry might move or realloc the Data, so only using 1 of this at a time.
 */
template <typename T>
struct GKContainerArrayIterator
{
public:
	GKContainerArrayIterator(GKContainerData* ContainerData, uint16 ID);
	~GKContainerArrayIterator();

	FORCEINLINE T& operator[](int32 Index)
	{
		return ArrayPtr()[Index];
	}

	FORCEINLINE const T& operator[](int32 Index) const
	{
		return ArrayPtr()[Index];
	}

	FORCEINLINE void Add(const T& Value);

	T RemoveAtSwap(int32 Index);
	T RemoveAt(int32 Index);

	FORCEINLINE int32 Find(const T& Value) const;
	FORCEINLINE bool Contains(const T& Value) const;

	bool RemoveSwap(const T& Value);
	bool Remove(const T& Value);

	FORCEINLINE int32 Num() { return ItemCount; }

	FORCEINLINE void Empty();

	void Log();

private:
	void UpdateContainerLink();
	void EnsureCapacity(uint16 MinItemCapacity);

	void Create(uint16 ItemCapacity);
	uint16 LoadItemCount();
	uint16 LoadItemCapacity();
	void SaveItemCount();
	void SaveItemCapacity();

	FORCEINLINE T* ArrayPtr();

	const uint8 HEADER_SIZE = 2 * sizeof(uint16);

	GKContainerData* ContainerData = nullptr;			// The container
	uint16 ID;									// The ID
	//T* Array = nullptr;							// Direct address to the array (the actual data), might be null if not created yet
	uint16 EntryIndex;							// The index to Container::ArrayEntries. Only valid if ItemCapacity > 0.
	uint16 ItemCount, ItemCapacity;				// The array info

};
