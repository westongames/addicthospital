// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Container/GKContainerArrayIterator.h"
#include "Gimka/Containers/Container/GKContainerData.h"
#include "Gimka/Tools/Macros/GKLogger.h"

template <typename T>
GKContainerArrayIterator<T>::GKContainerArrayIterator(GKContainerData* iContainerData, uint16 iID)
{
	ContainerData = iContainerData;
	ID = iID;
	UpdateContainerLink();
}

template <typename T>
GKContainerArrayIterator<T>::~GKContainerArrayIterator()
{
	// Nothing to delete
}

template<typename T>
void GKContainerArrayIterator<T>::Add(const T& Value)
{
	EnsureCapacity(ItemCount + 1);
	ArrayPtr()[ItemCount++] = Value;
	//Array[ItemCount++] = Value;
	SaveItemCount();
}

template<typename T>
T GKContainerArrayIterator<T>::RemoveAtSwap(int32 Index)
{
	check(Index < ItemCount);
	T* Array = ArrayPtr();
	T Value = Array[Index];
	Array[Index] = Array[--ItemCount];
	SaveItemCount();
	return Value;
}

template<typename T>
T GKContainerArrayIterator<T>::RemoveAt(int32 Index)
{
	check(Index < ItemCount);
	T* Array = ArrayPtr();
	T Value = Array[Index];
	if (Index + 1 < ItemCount)
	{
		FMemory::Memmove(Array + Index, Array + Index + 1, (ItemCount - Index - 1) * sizeof(T));
	}

	--ItemCount;
	SaveItemCount();
	return Value;
}

template<typename T>
int32 GKContainerArrayIterator<T>::Find(const T& Value) const
{
	T* Array = ArrayPtr();
	for (int32 Index = 0; Index < ItemCount; ++Index)
	{
		if (*Array == Value) return Index;
		++Array;
	}
	return -1;
}

template<typename T>
bool GKContainerArrayIterator<T>::Contains(const T& Value) const
{
	return Find(Value) != -1;
}

template<typename T>
bool GKContainerArrayIterator<T>::RemoveSwap(const T& Value)
{
	T* Array = ArrayPtr();
	for (int32 Index = 0; Index < ItemCount; ++Index)
	{
		if (*Array == Value)
		{
			*Array = ArrayPtr()[--ItemCount];
			SaveItemCount();
			return true;
		}
	}
	return false;
}

template<typename T>
bool GKContainerArrayIterator<T>::Remove(const T& Value)
{
	int32 Index = Find(Value);
	if (Index != -1)
	{
		RemoveAt(Index);
		return true;
	}
	return false;
}


template <typename T>
void GKContainerArrayIterator<T>::Create(uint16 iItemCapacity)
{
	//check(Array == nullptr);
	check(ItemCount == 0);
	check(ItemCapacity == 0);
	check(iItemCapacity != 0);
	uint32 DataCapacity = HEADER_SIZE + iItemCapacity * sizeof(T);
	EntryIndex = ContainerData->AddEntry(ID, DataCapacity, HEADER_SIZE);
	//Array = (T*)(Container->Data + Container->ArrayEntries[EntryIndex].Offset + HEADER_SIZE);
	ItemCapacity = iItemCapacity;
	SaveItemCount(); SaveItemCapacity();
}

template<typename T>
T* GKContainerArrayIterator<T>::ArrayPtr()
{
	// No need to add HEADER_SIZE because it's already added in Container->AddEntry
	return ((T*)(ContainerData->Data + ContainerData->ArrayEntries[EntryIndex].Offset));
}

template<typename T>
uint16 GKContainerArrayIterator<T>::LoadItemCount()
{
	//check(Array);
	return *((uint16*)(ContainerData->Data + ContainerData->ArrayEntries[EntryIndex].Offset - HEADER_SIZE));
}

template<typename T>
uint16 GKContainerArrayIterator<T>::LoadItemCapacity()
{
	//check(Array);
	return *(((uint16*)(ContainerData->Data + ContainerData->ArrayEntries[EntryIndex].Offset - HEADER_SIZE)) + 1);
	//return *(((uint16*)Array) - 1);
}

template<typename T>
void GKContainerArrayIterator<T>::SaveItemCount()
{
	//check(Array);
	//*(((uint16*)Array) - 2) = ItemCount;
	*((uint16*)(ContainerData->Data + ContainerData->ArrayEntries[EntryIndex].Offset - HEADER_SIZE)) = ItemCount;
}

template<typename T>
void GKContainerArrayIterator<T>::SaveItemCapacity()
{
	//check(Array);
	//*(((uint16*)Array) - 1) = ItemCapacity;
	*(((uint16*)(ContainerData->Data + ContainerData->ArrayEntries[EntryIndex].Offset - HEADER_SIZE)) + 1) = ItemCapacity;
}

template<typename T>
void GKContainerArrayIterator<T>::Empty()
{
	ItemCount = 0;
	SaveItemCount();
}

template <typename T>
void GKContainerArrayIterator<T>::Log()
{
	UE_LOG(LogTemp, Warning, TEXT("  Array EntryIndex %d, Item %d/%d"), EntryIndex, ItemCount, ItemCapacity);
	T* Array = ArrayPtr();// ((T*)(Container->Data + Container->ArrayEntries[EntryIndex].Offset + HEADER_SIZE));
	//GKLog1("ArrayLog", Array);
	//GKLog2("ArrayLog Now", Array[0], Array[1]);
	for (int i = 0; i < ItemCount; ++i)
	{
		UE_LOG(LogTemp, Warning, TEXT("    %d. %d"), i, Array[i]);
	}
}


template <typename T>
void GKContainerArrayIterator<T>::UpdateContainerLink()
{
	int32 index = ContainerData->Find(ID);
	if (index == -1)
	{
		// Not found
		//Array = nullptr;
		ItemCount = ItemCapacity = 0;
		EntryIndex = 65535;
	}
	else
	{
		EntryIndex = index;
		//Array = (T*)(Container->Data + Container->ArrayEntries[EntryIndex].Offset + HEADER_SIZE);
		ItemCount = LoadItemCount();
		ItemCapacity = LoadItemCapacity();
	}
}

template <typename T>
void GKContainerArrayIterator<T>::EnsureCapacity(uint16 MinItemCapacity)
{
	if (ItemCapacity > 0)
	{
		GKLog2("Ensure Capacity", ItemCapacity, MinItemCapacity);
		if (ItemCapacity < MinItemCapacity)
		{
			uint16 NewCapacity = FMath::Max((uint16)(ItemCapacity * 2), MinItemCapacity);
			uint32 OldDataCapacity = HEADER_SIZE + ItemCapacity * sizeof(T);
			uint32 NewDataCapacity = HEADER_SIZE + NewCapacity * sizeof(T);
			GKLog2("Expand ", OldDataCapacity, NewDataCapacity);
			//GKLog2("Array Pre", Array[0], Array[1]);
			//GKLog1("Array before", Array);
			ContainerData->ExpandEntryDataCapacity(EntryIndex, OldDataCapacity, NewDataCapacity);
			//Array = (T*)(Container->Data + Container->ArrayEntries[EntryIndex].Offset + HEADER_SIZE);
			//GKLog1("Array after", Array);
			//GKLog2("Array Post", Array[0], Array[1]);
			ItemCapacity = NewCapacity;
			SaveItemCapacity();
		}
	}
	else
	{
		Create(FMath::Max((uint16)4, MinItemCapacity));
	}
}

