// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Container/GKContainer.h"

#define CONTAINER_SUPPORT(VarType)										\
	virtual void SetSafe(uint16 ID, VarType Value) override;			\
	virtual bool GetSafe(uint16 ID, VarType& Value) const override;		\
	virtual void Set(uint16 ID, VarType Value) override;				\
	virtual void Get(uint16 ID, VarType& Value) const override;			\
	virtual void Add(uint16 ID, VarType Value) override;

class GKContainerData;

/**
 * Save all the EntryIndex that has been searched to avoid re-searching
 * GKContainerCache may contains all or a range of IDs of a GKContainer
 */
class GKContainerCache : public GKContainer
{
public:
	GKContainerCache();
	virtual ~GKContainerCache();

	void Init(GKContainerData* ContainerData, uint16 IDStart, uint16 IDEnd);

#include "Gimka/Containers/Container/GKContainerHeader.h"
	//CONTAINER_SUPPORT(int16);
	//CONTAINER_SUPPORT(uint16);
	//CONTAINER_SUPPORT(int32);
	//CONTAINER_SUPPORT(uint32);

	FORCEINLINE virtual uint16 operator[](int32 Index) const override;
	FORCEINLINE virtual uint16 Num() const override;
	virtual bool Contains(uint16 ID) const override;
	virtual char* GetAddress(uint16 ID) const override;
	virtual void Log() const override;

private:
	const uint16 EntryNotSearched = 65535;
	const uint16 EntryNotFound = 65534;
	const uint16 EntryMaxIndex = 65533;		// Max index because the top 2 are used for something else

	GKContainerData* ContainerData = nullptr;		// The container
	uint16* EntryIndexes = nullptr;			// might be EntryNotSearch 
	uint16 IDStart;


	//FORCEINLINE char* operator[](uint16 ID);

};

#undef CONTAINER_SUPPORT