// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Container/GKContainerWrapper.h"

#define CONTAINER_DEFINE_ADD(VarType)									\
	void GKContainerWrapper::Add(uint16 ID, VarType Value)				\
	{																	\
		check(!OriginalData->Contains(ID));								\
		NewData.Add(ID, Value);											\
	}

// Must contains ID but will add to NewData if needed
#define CONTAINER_DEFINE_GETSET(VarType)								\
	void GKContainerWrapper::Set(uint16 ID, VarType Value)				\
	{																	\
		char* Address = GKContainerData::GetAddress(ID);				\
		if (Address)													\
		{																\
			*(VarType*)Address = Value;									\
			return;														\
		}																\
		if (OriginalData->Contains(ID))									\
		{																\
			GKContainerData::Add(ID, Value);							\
			return;														\
		}																\
		Address = NewData.GetAddress(ID);								\
		*(VarType*)Address = Value;										\
	}

// Changes should be stored as NewData or in this ContainerData. No changes to OriginalData.
#define CONTAINER_DEFINE_GETSET_SAFE(VarType)							\
	void GKContainerWrapper::SetSafe(uint16 ID, VarType Value)			\
	{																	\
		char* Address = GKContainerData::GetAddress(ID);				\
		if (Address)													\
		{																\
			*(VarType*)Address = Value;									\
			return;														\
		}																\
		if (OriginalData->Contains(ID))									\
		{																\
			GKContainerData::Add(ID, Value);							\
			return;														\
		}																\
		Address = NewData.GetAddress(ID);								\
		if (Address)													\
		{																\
			*(VarType*)Address = Value;									\
			return;														\
		}																\
		Add(ID, Value);													\
	}

#define CONTAINER_DEFINE(VarType)										\
	CONTAINER_DEFINE_ADD(VarType)										\
	CONTAINER_DEFINE_GETSET(VarType)									\
	CONTAINER_DEFINE_GETSET_SAFE(VarType)

GKContainerWrapper::GKContainerWrapper(const GKContainerWrapper& ContainerWrapper)
	: GKContainerData(ContainerWrapper), OriginalData(ContainerWrapper.OriginalData), NewData(ContainerWrapper.NewData)
{

}


GKContainerWrapper::GKContainerWrapper(const GKContainer* iOriginalData)
{
    OriginalData = iOriginalData;
}

GKContainerWrapper::~GKContainerWrapper()
{
}

CONTAINER_DEFINE(int16);
CONTAINER_DEFINE(uint16);
CONTAINER_DEFINE(int32);
CONTAINER_DEFINE(uint32);

#undef CONTAINER_DEFINE
#undef CONTAINER_DEFINE_ADD
#undef CONTAINER_DEFINE_GETSET
#undef CONTAINER_DEFINE_GETSET_SAFE

uint16 GKContainerWrapper::operator[](int32 Index) const
{
    return Index < OriginalData->Num() ? (*OriginalData)[Index] : NewData[Index - OriginalData->Num()];
}

uint16 GKContainerWrapper::Num() const
{
    return OriginalData->Num() + NewData.Num();
}

bool GKContainerWrapper::Contains(uint16 ID) const
{
    return (NewData.Contains(ID) || OriginalData->Contains(ID));
}

char* GKContainerWrapper::GetAddress(uint16 ID) const
{
	char* Address = GKContainerData::GetAddress(ID);
	if (Address) return Address;
	Address = OriginalData->GetAddress(ID);
	if (Address) return Address;
	return NewData.GetAddress(ID);
}

void GKContainerWrapper::Log() const
{
	GKLog("Original Data");
	OriginalData->Log();

	GKLog("Modified Data");
	GKContainerData::Log();

	GKLog("New Data");
	NewData.Log();
}

//void GKContainerWrapper::SetSafes(uint16 ID, int32 Value)
//{
//	char* Address = GKContainerData::GetAddress(ID);
//	if (Address)
//	{
//		*(int32*)Address = Value;
//		return;
//	}
//	if (OriginalData->Contains(ID))
//	{
//		//GKLog1("ContainsID", ID);
//		GKContainerData::Add(ID, Value);
//		return;
//	}
//	//GKLog1("No ContainsID", ID);
//	/*	Address = NewData.GetAddress(ID);
//			if (Address)
//			{
//				* (int32*)Address = Value;
//				return;
//			}					*/
//			//Add(ID, Value);													
//}