// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class GKContainer;

/**
 * 
 */
template <typename T>
struct GKContainerIterator
{
public:
	GKContainerIterator(GKContainer* Container, uint16 ID);
	~GKContainerIterator();

	operator T() const;

	bool operator== (const T& Value) const;
	bool operator!= (const T& Value) const;

	bool Exists();

	GKContainerIterator& operator += (const T& Value) const;

private:
	GKContainer* Container = nullptr;			// The container
	uint16 ID;									// The ID
	int32 EntryIndex;							// The index to Container::ArrayEntries. -1 if not found.
	T Value;

	FORCEINLINE T* DataPtr();
};
