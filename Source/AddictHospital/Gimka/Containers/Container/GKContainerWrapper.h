// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Containers/Container/GKContainerData.h"

#define CONTAINER_SUPPORT(VarType)										\
	virtual void Set(uint16 ID, VarType Value) override;				\
	virtual void SetSafe(uint16 ID, VarType Value) override;			\
	virtual void Add(uint16 ID, VarType Value) override;
/**
 * Allow modifying data without changing the original.
 * Useful for graph traversal to keep only the changes.
 */
class GKContainerWrapper : public GKContainerData
{
public:
	GKContainerWrapper(const GKContainerWrapper& ContainerWrapper);
	GKContainerWrapper(const GKContainer* OriginalData);
	~GKContainerWrapper();

#include "Gimka/Containers/Container/GKContainerHeader.h"

	FORCEINLINE virtual uint16 operator[](int32 Index) const override;
	FORCEINLINE virtual uint16 Num() const override;

	FORCEINLINE virtual bool Contains(uint16 ID) const override;
	virtual char* GetAddress(uint16 ID) const override;
	virtual void Log() const override;

	//void SetSafes(uint16 ID, int32 Value);

private:
	// Modified data will be stored in this GKContainerData. NewData will be stored in NewData.
	// OriginalData won't be modified.
	const GKContainer* OriginalData;
	GKContainerData NewData;						
};

#undef CONTAINER_SUPPORT