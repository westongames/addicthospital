// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "HAL/UnrealMemory.h"

/**
* https://www.geeksforgeeks.org/binary-heap/
* https://www.geeksforgeeks.org/k-ary-heap/ - use this but do ascending instead and only 3
* - Peek: getMin: return the root element O(1)
* - Dequeue: extractMin: remove the min element (root). O (log n) for heapify after removing root. k-ary tree = more complex O (k log kn)
* - DecreaseKey (for delete, why?) time O(log n). k-ary tree = faster (O(log kn).
* - Insert: time O(log n)
* - Delete: time O(log n)
 * Root is always the minimum.
 * Using 3 children because it's faster than 2.
 * 
 */
// For Storing Pointer
// GetSortKey
template <typename TData, typename Comparator>
class GKPriorityQueue
{
	TArray<TData> ArrayData;

public:
	GKPriorityQueue(int32 initialCapacity = 4);
	GKPriorityQueue(std::initializer_list<TData> InitList);
	
	
	~GKPriorityQueue();

	void Append(std::initializer_list<TData> InitList);
	void Append(const TArray<TData>& Source);

	void Reserve(int32 Capacity);

	void Enqueue(const TData data);
	TData Dequeue();
	bool TryDequeue(TData& data);

	// Find exact match (i.e. by comparing pointer address) recursively.
	// Will use the sorting condition to speed up searching.
	int32 FindExact(const TData data, int32 startingIndex = 0);
	
	// After changing a node value, do a ReSortAt (use FindExact to find the index)
	void ReSortAt(int32 index);

	void Reset();

	void Log();

	FORCEINLINE int32 Num() { return ArrayData.Num(); }
	FORCEINLINE bool IsEmpty() { return ArrayData.Num() == 0; }
	FORCEINLINE bool HasItem() { return ArrayData.Num() > 0; }
	
private:
	void RestoreDown(int32 index);
	void RestoreUp(int32 index);
	void BuildHeap();
	void RestoreUpRangeExclusive(int32 startIndex, int32 endIndex);

	FORCEINLINE void Swap(int32 index1, int32 index2);

	FORCEINLINE int32 ParentOf(int32 index);
	FORCEINLINE int32 FirstChildOf(int32 index);
	FORCEINLINE int32 LastNonLeafNode();
};
