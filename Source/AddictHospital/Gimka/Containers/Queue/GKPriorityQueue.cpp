// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Queue/GKPriorityQueue.h"
#include "Gimka/Tools/Macros/GKLogger.h"

///#include "Util/IndexPriorityQueue.h"

template <typename TData, typename Comparator>
GKPriorityQueue<TData, Comparator>::GKPriorityQueue(int32 initialCapacity)
{
	ArrayData.Reserve(initialCapacity);
}

template<typename TData, typename Comparator>
GKPriorityQueue<TData, Comparator>::GKPriorityQueue(std::initializer_list<TData> InitList)
	: ArrayData(InitList)
{
	BuildHeap();
}

template <typename TData, typename Comparator>
GKPriorityQueue<TData, Comparator>::~GKPriorityQueue()
{
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Append(std::initializer_list<TData> InitList)
{
	int32 index = ArrayData.Num();
	ArrayData.Append(InitList);
	RestoreUpRangeExclusive(index, ArrayData.Num());
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Append(const TArray<TData>& Source)
{
	int32 index = ArrayData.Num();
	ArrayData.Append(Source);
	RestoreUpRangeExclusive(index, ArrayData.Num());
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Reserve(int32 Capacity)
{
	ArrayData.Reserve(Capacity);
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Enqueue(const TData data)
{
	ArrayData.Add(data);
	RestoreUp(ArrayData.Num() - 1);
}

template<typename TData, typename Comparator>
TData GKPriorityQueue<TData, Comparator>::Dequeue()
{
	int32 num = ArrayData.Num();
	check(num > 0);

	TData value = ArrayData[0];
	if (num == 1)
	{
		ArrayData.Reset();// .SetNumUnsafeInternal(0);
	}
	else
	{

		ArrayData[0] = ArrayData[--num];
		ArrayData.SetNumUnsafeInternal(num);
		RestoreDown(0);
	}
	return value;
}

template<typename TData, typename Comparator>
bool GKPriorityQueue<TData, Comparator>::TryDequeue(TData& data)
{
	if (ArrayData.Num() > 0)
	{
		data = Dequeue();
		return true;
	}
	return false;
}

template<typename TData, typename Comparator>
int32 GKPriorityQueue<TData, Comparator>::FindExact(const TData data, int32 startingIndex)
{
	int32 num = ArrayData.Num();
	check(num > 0);		// Don't call if empty

	if (data == ArrayData[startingIndex]) return startingIndex;
	int32 child1 = startingIndex * 3 + 1;
	int32 child2 = child1 + 1;
	int32 child3 = child1 + 2;

	int32 result;
	if (child3 < num)
	{
		// There are 3 childrens
		if ((result = FindExact(data, child1)) != -1) return result;
		if ((result = FindExact(data, child2)) != -1) return result;
		if ((result = FindExact(data, child3)) != -1) return result;
	}
	else if (child2 < num)
	{
		// There are 2 children
		if ((result = FindExact(data, child1)) != -1) return result;
		if ((result = FindExact(data, child2)) != -1) return result;
	}
	else if (child1 < num)
	{
		// Only 1 child
		if ((result = FindExact(data, child1)) != -1) return result;
	}

	return -1;		// Not found
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::ReSortAt(int32 index)
{
	// Check if it's smaller than parent then restoreUp
	if (index > 0 && Comparator::Compare(ArrayData[index], ArrayData[ParentOf(index)]))
	{
		RestoreUp(index);
		return;
	}

	// Otherwise restoreDown
	RestoreDown(index);
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Reset()
{
	ArrayData.SetNumUnsafeInternal(0);
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Log()
{
	int32 num = ArrayData.Num();
	if (num == 0) return;
	GKLog1("Priority Queue Num", Num());
	GKLog1("  Root: ", ArrayData[0]->GetActionIndex());
	for (int32 i = 1; i < num; i += 3)
	{
		UE_LOG(LogTemp, Warning, TEXT("  %d, %d, %d"),
			(i < num ? ArrayData[i]->GetActionIndex() : -1),
			(i + 1 < num ? ArrayData[i + 1]->GetActionIndex() : -1),
			(i + 2 < num ? ArrayData[i + 2]->GetActionIndex() : -1));
	}
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::Swap(int32 index1, int32 index2)
{
	TData temp = ArrayData[index1];
	ArrayData[index1] = ArrayData[index2];
	ArrayData[index2] = temp;
}

template<typename TData, typename Comparator>
int32 GKPriorityQueue<TData, Comparator>::ParentOf(int32 index)
{
	return index > 0 ? (index - 1) / 3 : -1;
}

template<typename TData, typename Comparator>
int32 GKPriorityQueue<TData, Comparator>::FirstChildOf(int32 index)
{
	return index * 3 + 1;
}

template<typename TData, typename Comparator>
int32 GKPriorityQueue<TData, Comparator>::LastNonLeafNode()
{
	// is (1-2)/3 == 0?
	return (ArrayData.Num() - 2) / 3;
}


// Heapify (or restore the min-heap property).
// This is used to build a 3-ary heap and in extractMin.
// Parent of the node at index (except root) = (index - 1) / 3
// Children of the node at index i are at index*3 + 1, index*3 + 2, index*3 + 3
// The last non-leaf node of size n is at (n-2)/3
template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::RestoreDown(int32 index)
{
	while (true)
	{
		int32 child1 = index * 3 + 1;
		int32 child2 = child1 + 1;
		int32 child3 = child1 + 2;
		int32 num = ArrayData.Num();
		int32 indexMin;
		if (child3 < num)
		{
			// There are 3 children, find the min
			const TData& data1 = ArrayData[child1];
			const TData& data2 = ArrayData[child2];
			const TData& data3 = ArrayData[child3];

			if (Comparator::Compare(data1, data2))
			{
				// data1 < data2
				if (Comparator::Compare(data1, data3))
				{
					// data1 < data2. data1 < data3. data1 < data2, data3
					indexMin = child1;
				}
				else
				{
					// data1 < data2. data1 >= data3. data3 <= data1 < data2
					indexMin = child3;
				}
			}
			else
			{
				// data1 >= data2
				if (Comparator::Compare(data1, data3) || Comparator::Compare(data2, data3))
				{
					// data1 >= data2. data1 < data3. data2 <= data1 <= data3 OR
					// data1 >= data2. data1 >= data3. data2 < data3. data2 < data3 <= data1. 
					indexMin = child2;
				}
				else
				{
					// data1 >= data2. data1 >= data3. data2 >= data3. data3 <= data2 <= data1
					indexMin = child3;
				}
			}
		}
		else if (child2 < num)
		{
			// There are 2 children, find the min
			indexMin = Comparator::Compare(ArrayData[child1], ArrayData[child2]) ? child1 : child2;
		}
		else if (child1 < num)
		{
			// Only 1 child
			indexMin = child1;
		}
		else
		{
			// No child, we're done!
			break;
		}

		// If current is lower than the min child then we're done!
		if (Comparator::Compare(ArrayData[index], ArrayData[indexMin])) break;

		// child is lower than current. Swap it and continue!
		Swap(index, indexMin);
		index = indexMin;
	}
}

// Restores a given node up in the heap. This is used in decreaseKey() and insert()
template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::RestoreUp(int32 index)
{
	check(index >= 0);
	int32 parent = ParentOf(index);

	// Loop should only run untilt he root node in case the element inserted is the lowest
	// restoreUp will send it to the root node.
	while (parent >= 0 && Comparator::Compare(ArrayData[index], ArrayData[parent]))
	{
		// Swap it
		Swap(parent, index);

		// And continue
		index = parent;
		parent = ParentOf(index);
	}
}

// Build of a heap of array
template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::BuildHeap()
{
	// Heapify all internal nodes starting from last non-leaf node all the way up to the root node
	for (int32 index = LastNonLeafNode(); i >= 0; --i)
	{
		RestoreDown(index);
	}
}

template<typename TData, typename Comparator>
void GKPriorityQueue<TData, Comparator>::RestoreUpRangeExclusive(int32 startIndex, int32 endIndex)
{
	for (; startIndex < endIndex; ++startIndex)
	{
		RestoreUp(startIndex);
	}
}
