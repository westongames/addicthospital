// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Set/GKBoolSet.h"

template <typename TID>
GKBoolSet<TID>::GKBoolSet(int32 initialCapacity)
{
	check(initialCapacity > 0);
	Capacity = initialCapacity;
	Array = new byte[initialCapacity];
}

template <typename TID>
GKBoolSet<TID>::GKBoolSet()
{
}

template <typename TID>
GKBoolSet<TID>::~GKBoolSet()
{
}

template <typename TID>
void GKBoolSet<TID>::Add(TID ID)
{
	check(!Contains(ID));

	EnsureCapacity(Count + 1);
	ArrayBool[Count++] = ID;
}

template <typename TID>
bool GKBoolSet<TID>::Contains(TID ID)
{
	return false;
}

template <typename TID>
void GKBoolSet<TID>::Remove(TID ID)
{
	TArray
}

template <typename TID>
void GKBoolSet<TID>::EnsureCapacity(int32 NewCapacity)
{
	if (NewCapacity > Capacity)
	{
		ArrayBool = (byte*)FMemory::Realloc(ArrayBool, NewCapacity);
		Capacity = NewCapacity;
	}
}