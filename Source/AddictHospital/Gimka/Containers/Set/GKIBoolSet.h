// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

template <typename TData>
class GKIBoolSetIterator
{
public:
	explicit GKIBoolSetIterator(TData* pData) : DataPtr(pData) {}

	// Conversion to bool returning true if the iterator is valid
	/*FORCEINLINE explicit operator bool() const
	{
		return CurrentNode != GKList<ElementType>::NodeType::EmptyNode;
	}*/

	FORCEINLINE GKListIterator& operator++()
	{
		//checkSlow((bool)this);
		//CurrentNode = List->Array[CurrentNode].GetNextNode();
		++DataPtr;
		return *this;
	}

	FORCEINLINE GKListIterator operator++(int)
	{
		auto Tmp = *DataPtr;
		++(*this);
		return Tmp;
	}

	FORCEINLINE GKListIterator& operator--()
	{
		//checkSlow((bool)this);
		//CurrentNode = List->Array[CurrentNode].GetPrevNode();
		--DataPtr;
		return *this;
	}

	FORCEINLINE GKListIterator operator--(int)
	{
		auto Tmp = *this;
		--(*this);
		return Tmp;
	}

	// Accessor
	FORCEINLINE TData* operator->() const
	{
		//checkSlow((bool)this);
		return DataPtr;// List->Array[CurrentNode].GetValue();
	}

	FORCEINLINE TData& operator*() const
	{
		//checkSlow((bool)this);
		return DataPtr;// List->Array[CurrentNode].GetValue();
	}


private:
	TData* DataPtr;
};

/**
 * T can be any integer type
 */
template <typename TID, typename TData>
class GKIBoolSet
{
public:
	GKIBoolSet(uint16 initialCapacity) {};
	GKIBoolSet();
	virtual ~GKIBoolSet() {};

	virtual void Add(TID ID) = 0;
	virtual bool Contains(TID ID) = 0;
	virtual void Remove(TID ID) = 0;
	
	//virtual iterator begin() = 0;
	//const_iterator begin() = 0;
};
