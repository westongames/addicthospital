// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKIBoolSet.h"
/**
 * 
 */
template <typename TID>
class GKBoolSet : public GKIBoolSet<TID, uint8>
{
public:
	GKBoolSet();
	GKBoolSet(int32 initializeCapacity);
	virtual ~GKBoolSet();

	virtual void Add(TID ID) override;
	virtual bool Contains(TID ID) override;
	virtual void Remove(TID ID) override;

private:
	uint8* ArrayBool = nullptr;
	uint16 Capacity = 0;
	uint16 Count = 0;

	void EnsureCapacity(int32 NewCapacity);
};


/*
template <typename TData>
GKISet
{
	void Add(TData)
	bool Contains(TDAta)
	void Remove(TData)		- implementation might keep it there but marked it as deleted, or it might be removed
}

GKSet : GKISet
{
	// Assume for small data
	// Add & Remove will shift the entire thing
}

GKSetArray : GKISet
{
	// Implemented as Array of int16. Each entry contains 16 values. So only toggle 0 or 1.
	// No iterator.
}

GKArrayBits

GKGoapBoolStates (sorted): GKSet<uint16>  => agent, action.condition, action.effects
GKGoapArrayBoolStates: TBitArray

*/