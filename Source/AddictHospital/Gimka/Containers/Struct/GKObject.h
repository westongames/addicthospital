// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"

class GKGameManager;

/**
 * 
 */
struct GKObject
{
	FName ID;

public:
	GKObject(const FName& ID);
	virtual ~GKObject();

	typedef TMap<FName, GKObject*> Map;

	virtual bool Init(GKGameManager* GameManager) { return false; }

public:	// Accessors
	GK_GETCR(FName, ID);
};
