// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Containers/Struct/GKUnion.h"

void GKUnionStruct::Serialize(FArchive& Ar)
{
	Ar.Serialize(&ByteSize, sizeof(ByteSize));
	if (ByteSize > 0) Ar.Serialize(&Union, ByteSize);
}