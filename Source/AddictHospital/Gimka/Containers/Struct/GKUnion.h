// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Math/GKIntVector.h"

#define UNI_SET(VarType, Variable)																		\
	GKUnion(VarType Value) { Variable[0] = Value; }														\
	GKUnion(VarType Value1, VarType Value2) { Variable[0] = Value1; Variable[1] = Value2; }				\
	GKUnion(VarType Value1, VarType Value2, VarType Value3)												\
		{ Variable[0] = Value1; Variable[1] = Value2; Variable[2] = Value3; }							\
	GKUnion(VarType Value1, VarType Value2, VarType Value3, VarType Value4)								\
		{ Variable[0] = Value1; Variable[1] = Value2; Variable[2] = Value3; Variable[3] = Value4; }		\
	VarType Get ## Variable() const { return Variable[0]; }												\
	void Get(VarType& Value) const { Value = Variable[0]; }												\
	void Get(VarType& Value1, VarType& Value2) const { Value1 = Variable[0]; Value2 = Variable[1]; }	\
	void Get(VarType& Value1, VarType& Value2, VarType& Value3) const									\
		{ Value1 = Variable[0]; Value2 = Variable[1]; Value3 = Variable[2];}							\
	void Get(VarType& Value1, VarType& Value2, VarType& Value3, VarType& Value4) const					\
		{ Value1 = Variable[0]; Value2 = Variable[1]; Value3 = Variable[2]; Value4 = Variable[3]; }							

#define UNI_STRUCT3(Struct, Member1, Member2, Member3, Variable)										\
	GKUnion(const Struct& Value) : GKUnion(Value.Member1, Value.Member2, Value.Member3) {};				\
	const Struct& Get ## Struct() const { return Struct(Variable[0], Variable[1], Variable[2]); }		\
	void Get(Struct& Value) const { Value.Member1 = Variable[0]; Value.Member2 = Variable[1]; Value.Member3 = Variable[3]; }
	

// Occupies 16 bytes
union GKUnion						// Occupy 16 bytes
{
public:
	int32 Int32[4];
	uint32 UInt32[4];
	float Float[4];

	GKUnion() {}

	UNI_SET(int32, Int32);
	UNI_SET(uint32, UInt32);
	UNI_SET(float, Float);

	UNI_STRUCT3(FVector, X, Y, Z, Float);
	UNI_STRUCT3(FRotator, Pitch, Roll, Yaw, Float);
	UNI_STRUCT3(GKIntVector, X, Y, Z, Int32);
	UNI_STRUCT3(FIntVector, X, Y, Z, Int32);
};

#define UNISTR_SET(VarType, Variable)																				\
	GKUnionStruct(VarType Value) { ByteSize = sizeof(VarType); Union.Variable[0] = Value; }							\
	GKUnionStruct(VarType Value1, VarType Value2)																	\
		{ ByteSize = 2 * sizeof(VarType); Union.Variable[0] = Value1; Union. Variable[1] = Value2; }				\
	GKUnionStruct(VarType Value1, VarType Value2, VarType Value3)													\
		{ ByteSize = 3 * sizeof(VarType); Union.Variable[0] = Value1; Union.Variable[1] = Value2;					\
		  Union.Variable[2] = Value3; }																				\
	GKUnionStruct(VarType Value1, VarType Value2, VarType Value3, VarType Value4)									\
		{ ByteSize = 4 * sizeof(VarType); Union.Variable[0] = Value1; Union.Variable[1] = Value2;					\
		  Union.Variable[2] = Value3; Union.Variable[3] = Value4; }													\
	VarType Get ## Variable() const { return Union.Variable[0]; }													\
	void Get(VarType& Value) const { check(ByteSize == sizeof(VarType)); Value = Union.Variable[0]; }				\
	void Get(VarType& Value1, VarType& Value2) const																\
		{ check(ByteSize == 2 * sizeof(VarType)); Value1 = Union.Variable[0]; Value2 = Union.Variable[1]; }			\
	void Get(VarType& Value1, VarType& Value2, VarType& Value3)	const												\
		{ check(ByteSize == 3 * sizeof(VarType)); Value1 = Union.Variable[0]; Value2 = Union.Variable[1];			\
		  Value3 = Union.Variable[2]; }																				\
	void Get(VarType& Value1, VarType& Value2, VarType& Value3, VarType& Value4) const								\
		{ check(ByteSize == 3 * sizeof(VarType)); Value1 = Union.Variable[0]; Value2 = Union.Variable[1];			\
		  Value3 = Union.Variable[2]; Value4 = Union.Variable[3]; }

#define UNISTR_STRUCT3(Struct, Member1, Member2, Member3, Variable)													\
	GKUnionStruct(const Struct& Value) : GKUnionStruct(Value.Member1, Value.Member2, Value.Member3) {};				\
	const Struct& Get ## Struct() const { return Struct(Union.Variable[0], Union.Variable[1], Union.Variable[2]); }	\
	void Get(Struct& Value) const { Value.Member1 = Union.Variable[0]; Value.Member2 = Union.Variable[1]; Value.Member3 = Union.Variable[2]; }


// Like GKUnion, but support serialization and basic checking to make sure it doesn't read uninitialized data.
// Occupies 17 bytes
struct GKUnionStruct
{
public:
	uint8 ByteSize = 0;				// Number of bytes to serialize
	GKUnion Union;

	GKUnionStruct() {}

	UNISTR_SET(int32, Int32);
	UNISTR_SET(uint32, UInt32);
	UNISTR_SET(float, Float);

	UNISTR_STRUCT3(FVector, X, Y, Z, Float);
	UNISTR_STRUCT3(FRotator, Pitch, Roll, Yaw, Float);
	UNISTR_STRUCT3(GKIntVector, X, Y, Z, Int32);
	UNISTR_STRUCT3(FIntVector, X, Y, Z, Int32);

	void Serialize(FArchive& Ar);
};
