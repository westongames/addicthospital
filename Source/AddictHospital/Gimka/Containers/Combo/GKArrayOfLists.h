// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Containers/List/GKList.h"


/**
 * 
 */
template <typename T, size_t ArrayCapacity>
class GKArrayOfLists
{
public:
	/*GKArrayOfList();
	~GKArrayOfList();*/
	FORCEINLINE void Add(int ArrayIndex, T Value)
	{
		ArrayLists[ArrayIndex].AddTailCheckUnique(Value);
	}

	FORCEINLINE void AddCheckUnique(int ArrayIndex, T Value)
	{
		ArrayLists[ArrayIndex].AddTailCheckUnique(Value);
	}

	FORCEINLINE bool Remove(int ArrayIndex, T Value)
	{
		ArrayLists[ArrayIndex].Remove(Value);
	}


private:
	GKList<T> ArrayLists[ArrayCapacity];
	//std::array<std::list<T>, capacity> Array;
};
