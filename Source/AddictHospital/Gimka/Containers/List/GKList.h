// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Kismet/KismetMathLibrary.h"

template <class ElementType>
class GKList;

template <class ElementType>
class GKListIterator
{
public:
	explicit GKListIterator(uint16 StartingNode, GKList<ElementType>* InList)
		: CurrentNode(StartingNode), List(InList)
	{}

	// Conversion to bool returning true if the iterator is valid
	FORCEINLINE explicit operator bool() const
	{
		return CurrentNode != GKList<ElementType>::NodeType::EmptyNode;
	}

	FORCEINLINE GKListIterator& operator++()
	{
		checkSlow((bool)this);
		CurrentNode = List->Array[CurrentNode].GetNextNode();
		return *this;
	}

	FORCEINLINE GKListIterator operator++(int)
	{
		auto Tmp = *this;
		++(*this);
		return Tmp;
	}

	FORCEINLINE GKListIterator& operator--()
	{
		checkSlow((bool)this);
		CurrentNode = List->Array[CurrentNode].GetPrevNode();
		return *this;
	}

	FORCEINLINE GKListIterator operator--(int)
	{
		auto Tmp = *this;
		--(*this);
		return Tmp;
	}

	// Accessor
	FORCEINLINE ElementType* operator->() const
	{
		checkSlow((bool)this);
		return List->Array[CurrentNode].GetValue();
	}

	FORCEINLINE ElementType& operator*() const
	{
		checkSlow((bool)this);
		return List->Array[CurrentNode].GetValue();
	}

	FORCEINLINE uint16 GetNode() const
	{
		return CurrentNode;
	}

	FORCEINLINE GKListIterator Next() const
	{
		checkSlow((bool)this);
		return GKListIterator(List->Array[CurrentNode].GetNextNode(), List);
	}

	FORCEINLINE GKListIterator Prev() const
	{
		checkSlow((bool)this);
		return GKListIterator(List->Array[CurrentNode].GetPrevNode(), List);
	}

	FORCEINLINE void InsertBefore(const ElementType& Value)
	{
		checkSlow((bool)this);
		List->InsertBefore(Value, *this);
	}

	FORCEINLINE void InsertAfter(const ElementType& Value)
	{
		checkSlow((bool)this);
		List->InsertAfter(Value, *this);
	}

	FORCEINLINE void Remove()
	{
		checkSlow((bool)this);
		List->Remove(*this);
	}

protected:
	uint16 CurrentNode;
	GKList<ElementType>* List;

	FORCEINLINE friend bool operator==(const GKListIterator& Lhs, const GKListIterator& Rhs) { return Lhs.CurrentNode == Rhs.CurrentNode; }
	FORCEINLINE friend bool operator!=(const GKListIterator& Lhs, const GKListIterator& Rhs) { return Lhs.CurrentNode != Rhs.CurrentNode; }
};

/*
	Double Linked list with extra features:
		- Can delete a node using iterator
*/
template <class ElementType>
class GKList
{
private:
	class NodeType
	{
	public:
		friend class GKList;

		static const uint16 EmptyNode = 65535;// std::numeric_limits<uint16>::max();

		// Constructor
		/*GKListNode(const ElementType& InValue)
			: Value(InValue), NextNode(0), PrevNode(0)
		{}*/

		NodeType(const ElementType& InValue, uint16 InNextNode, uint16 InPrevNode)
			: Value(InValue), NextNode(InNextNode), PrevNode(InPrevNode)
		{}

		FORCEINLINE const ElementType& GetValue() const
		{
			return Value;
		}

		FORCEINLINE ElementType& GetValue()
		{
			return Value;
		}

		FORCEINLINE uint16 GetNextNode() const
		{
			return NextNode;
		}

		FORCEINLINE uint16 GetPrevNode() const
		{
			return PrevNode;
		}

	protected:
		ElementType	Value;
		uint16 NextNode;
		uint16 PrevNode;
	};

public:
	friend GKListIterator<ElementType>;

	// Used to iterate over the elements of a linked list
	typedef GKListIterator<ElementType> Iterator;
	typedef GKListIterator<const ElementType> ConstIterator;

	// Constructors
	GKList()
		: HeadNode(NodeType::EmptyNode), TailNode(NodeType::EmptyNode), EmptyHeadNode(NodeType::EmptyNode), ListSize(0)
	{
	}

	void AddHead(const ElementType& Value)
	{
		uint16 Index = GetEmpty();

		// have an existing head node - change the head node to point to this one
		if (HeadNode != NodeType::EmptyNode)
		{
			Array[Index] = NodeType(Value, HeadNode, NodeType::EmptyNode);
			Array[HeadNode].PrevNode = Index;
			HeadNode = Index;
		}
		else
		{
			Array[Index] = NodeType(Value, NodeType::EmptyNode, NodeType::EmptyNode);
			HeadNode = TailNode = Index;
		}

		++ListSize;
	}

	void AddTail(const ElementType& Value)
	{
		uint16 Index = GetEmpty();

		// have an existing tail node - change the tail node to point to this one
		if (TailNode != NodeType::EmptyNode)
		{
			Array[Index] = NodeType(Value, NodeType::EmptyNode, TailNode);
			Array[TailNode].NextNode = Index;
			TailNode = Index;
		}
		else
		{
			Array[Index] = NodeType(Value, NodeType::EmptyNode, NodeType::EmptyNode);
			HeadNode = TailNode = Index;
		}

		++ListSize;
	}

	FORCEINLINE void AddHeadCheckUnique(const ElementType& Value)
	{
		checkSlow(!Contains(Value));
		AddHead(Value);
	}

	FORCEINLINE void AddTailCheckUnique(const ElementType& Value)
	{
		checkSlow(!Contains(Value));
		AddTail(Value);
	}

	/**
	 * Insert the specified value into the list at an arbitrary point.
	 *
	 * @param	Value				the value to insert into the list
	 * @param	NodeToInsertBefore	the new node will be inserted into the list at the current location of this node
	 *								if nullptr, the new node will become the new head of the list
	 * @return	whether the node was successfully added into the list
	 * @see Empty, RemoveNode
	 */
	void InsertBefore(const ElementType& Value, const Iterator& Iterator)
	{
		checkSlow(Iterator.List == this);

		uint16 IndexToInsertBefore = Iterator.GetNode();
		if (IndexToInsertBefore == NodeType::EmptyNode || IndexToInsertBefore == HeadNode)
		{
			AddHead(Value);
			return;
		}

		uint16 IndexToInsertAfter = Array[IndexToInsertBefore].PrevNode;

		uint16 Index = GetEmpty();
		Array[Index] = NodeType(Value, IndexToInsertBefore, IndexToInsertAfter);
		
		Array[IndexToInsertAfter].NextNode = Index;
		Array[IndexToInsertBefore].PrevNode = Index;

		++ListSize;
	}

	void InsertAfter(const ElementType& Value, const Iterator& Iterator)
	{
		checkSlow(Iterator.List == this);

		uint16 IndexToInsertAfter = Iterator.GetNode();
		if (IndexToInsertAfter == NodeType::EmptyNode || IndexToInsertAfter == TailNode)
		{
			AddTail(Value);
			return;
		}

		uint16 IndexToInsertBefore = Array[IndexToInsertAfter].NextNode;

		uint16 Index = GetEmpty();
		Array[Index] = NodeType(Value, IndexToInsertBefore, IndexToInsertAfter);

		Array[IndexToInsertAfter].NextNode = Index;
		Array[IndexToInsertBefore].PrevNode = Index;

		++ListSize;
	}
	
	void Remove(Iterator& InIterator)
	{
		checkSlow(Iterator.List == this);

		uint16 IndexToRemove = InIterator.GetNode();
		if (IndexToRemove != NodeType::EmptyNode)
		{
			// if we only have one node, just call Clear() so that we don't have to do lots of extra checks in the code below
			if (ListSize == 1)
			{
				checkSlow(IndexToRemove == HeadNode);
				Empty();
				return;
			}

			if (IndexToRemove == HeadNode)
			{
				HeadNode = Array[HeadNode].NextNode;
				Array[HeadNode].PrevNode = NodeType::EmptyNode;
			}
			else if (IndexToRemove == TailNode)
			{
				TailNode = Array[TailNode].PrevNode;
				Array[TailNode].NextNode = NodeType::EmptyNode;
			}
			else
			{
				// Not head nor tail. So it has next and prev.
				uint16 NextIndex = Array[IndexToRemove].NextNode;
				uint16 PrevIndex = Array[IndexToRemove].PrevNode;
				Array[NextIndex].PrevNode = PrevIndex;
				Array[PrevIndex].NextNode = NextIndex;
			}

			ReturnEmpty(IndexToRemove);
			--ListSize;

			InIterator = Iterator(NodeType::EmptyNode, this);
		}
	}

	bool Remove(const ElementType& Value)
	{
		if (TryFind(Value, Iterator & Iterator))
		{
			Remove(Iterator);
			return true;
		}
		return false;
	}

	/** Removes all nodes from the list. */
	void Empty()
	{
		if (ListSize > 0)
		{
			Array[TailNode].NextNode = EmptyHeadNode;
			EmptyHeadNode = HeadNode;
			HeadNode = TailNode = NodeType::EmptyNode;
			ListSize = 0;
		}
	}

	bool TryFind(const ElementType& Value, Iterator& Iterator)
	{
		uint16 Index = HeadNode;
		while (Index != NodeType::EmptyNode)
		{
			if (Array[Index].Value == Value)
			{
				Iterator = Iterator(Index, this);
				return true;
			}
			Index = Array[Index].NextNode;
		}
		return false;
	}

	Iterator Find(const ElementType& Value)
	{
		uint16 Index = HeadNode;
		while (Index != NodeType::EmptyNode) 
		{
			if (Array[Index].Value == Value) return Iterator(Index, this);
			Index = Array[Index].NextNode;
		}
		return Iterator(NodeType::EmptyNode, this);
	}

	bool contains(const ElementType& Value)
	{
		return (bool)Find(Value);
	}

	//bool InsertNode(TDoubleLinkedListNode* NewNode, TDoubleLinkedListNode* NodeToInsertBefore = nullptr)
	//{
	//	if (NewNode == nullptr)
	//	{
	//		return false;
	//	}

	//	if (NodeToInsertBefore == nullptr || NodeToInsertBefore == HeadNode)
	//	{
	//		return AddHead(NewNode);
	//	}

	//	NewNode->PrevNode = NodeToInsertBefore->PrevNode;
	//	NewNode->NextNode = NodeToInsertBefore;

	//	NodeToInsertBefore->PrevNode->NextNode = NewNode;
	//	NodeToInsertBefore->PrevNode = NewNode;

	//	SetListSize(ListSize + 1);
	//	return true;
	//}


	void Log()
	{
		uint16 Iterator = HeadNode;
		int Index = 0;
		UE_LOG(LogTemp, Warning, TEXT("Linked List Size %d/%d"), ListSize, Array.Num());
		while (Iterator != NodeType::EmptyNode) 
		{
			UE_LOG(LogTemp, Warning, TEXT("Linked List %d: %d"), Index++, Array[Iterator].Value);
			Iterator = Array[Iterator].NextNode;
		}
	}

	// Iterators
	Iterator Begin() { return Iterator(HeadNode, this); }
	//ConstIterator CBegin() const { return ConstIterator(HeadNode, this); }
	Iterator End() { return Iterator(TailNode, this); }

//private:
	//friend Iterator			Begin	(		ElementType& List) { return Iterator		(HeadNode, this); }
	//friend ConstIterator	Begin	(const	ElementType& List) { return ConstIterator(HeadNode, this); }
	//friend Iterator			End		(ElementType& List) { return Iterator		(TailNode, this); }
	//friend ConstIterator	End		(const	ElementType& List) { return ConstIterator(TailNode, this); }

protected:
	void ResizeArray(int32 NewSize) 
	{
		// Max size is 65535 - 1
		check(NewSize < NodeType::EmptyNode);

		int32 CountToAdd = NewSize - Array.Num();
		check(CountToAdd > 0);

		// Expand the array and initialize as a single linked list
		int32 OldNum = Array.AddUninitialized(CountToAdd);
		for (int Index = OldNum, NewSizeMin1 = NewSize - 1; Index < NewSizeMin1; ++Index) 
		{
			Array[Index].NextNode = Index + 1;
		}

		// Add to EmptyHeadNode
		Array[NewSize - 1].NextNode = EmptyHeadNode;
		EmptyHeadNode = OldNum;
	}

	FORCEINLINE uint16 GetEmpty()
	{
		if (EmptyHeadNode == NodeType::EmptyNode)
		{
			if (ListSize == 0) ResizeArray(4);
			else ResizeArray(FMath::Min(Array.Num() * 2, NodeType::EmptyNode - 1));
		}
		uint16 IndexNew = EmptyHeadNode;
		EmptyHeadNode = Array[EmptyHeadNode].GetNextNode();
		return IndexNew;
	}

	FORCEINLINE void ReturnEmpty(uint16 Index)
	{
		Array[Index].NextNode = EmptyHeadNode;
		EmptyHeadNode = Index;
	}

private:
	uint16 HeadNode = NodeType::EmptyNode;
	uint16 TailNode = NodeType::EmptyNode;
	uint16 EmptyHeadNode = NodeType::EmptyNode;
	int32 ListSize = 0;
	TArray<NodeType> Array;

	GKList(const GKList&);
	GKList& operator=(const GKList&);

	
};


/**
 * 
 */
//template <typename T>
//class GKList
//{
//public:
//	GKList();
//	virtual ~GKList();
//
//	/* Iterators */
//	typedef T* iterator;
//	typedef T* const const_iterator;
//	typedef TList<T>* NodeType;
//
//	inline void Add(T Value)
//	{
//		NodeType Node = new TList<T>(Value, Head);
//		Head = Node;
//	}
//
//	T& Front()
//	{
//		return Head->Element;
//	}
//
//	const T& Front() const
//	{
//		return Head->Element;
//	}
//
//	T& Pop()
//	{
//		NodeType PrevHead = Head;
//		T Value = Head->Element;
//		Head = Head->Next;
//		delete PrevHead;
//		return Value;
//	}
//
//	bool TryPop(T& Value)
//	{
//		if (Head != nullptr)
//		{
//			NodeType PrevHead = Head;
//			Value = Head->Element;
//			Head = Head->Next;
//			delete PrevHead;
//			return true;
//		}
//		return false;
//	}
//
//	//inline const 
//
//private:
//	NodeType Head = nullptr;
//};
