// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

#define GKBIT(bit) (((T)1) << bit)

#define GKBIT_SET(Name, VarName, BitValue)																\
	inline void Set ## Name(bool Value) { if (Value) VarName |= BitValue; else VarName &= ~BitValue; }	\
	inline void Set ## Name() { VarName |= BitValue; }											\
	inline void Clear ## Name() { VarName &= ~BitValue; }

#define GKBIT_GET(Name, VarName, BitValue)																\
	inline bool Is ## Name () const { return VarName & BitValue; }										\
	inline bool IsNot ## Name () const { return (VarName & BitValue) == 0; }

#define GKBIT_GETSET(Name, VarName, BitValue)															\
	GKBIT_GET(Name, VarName, BitValue)																	\
	GKBIT_SET(Name, VarName, BitValue)



template<typename T>
struct GKBits
{
	T Value = 0;
public:
	//GKBits();
	//~GKBits();

	inline bool SetBit(int32 bitIndex, bool on) { return SetBit(Value, bitIndex, on); }
	inline bool SetBitTrue(int32 bitIndex) { return SetBitTrue(Value, bitIndex); }
	inline bool SetBitFalse(int32 bitIndex) { return SetBitFalse(Value, bitIndex); }

	inline bool operator[](int32 bitIndex) const { return Value & GKBIT(bitIndex); }

	inline void SetTrue(int32 bitIndex) { return SetTrue(Value, bitIndex); }
	inline void SetFalse(int32 bitIndex) { return SetFalse(Value, bitIndex); }

	inline bool IsAnyTrue() const { return Value != 0; }
	inline bool AreAllFalse() const { return Value == 0; }

	inline bool IsTrue(int32 bitIndex) const { return IsTrue(Value, bitIndex); }
	inline bool IsFalse(int32 bitIndex) const { return IsFalse(Value, bitIndex); }

	inline void Reset() { Value = 0; }
	inline T GetValue() const { return Value; }

	/*
	* Static Functions
	*/
	inline static bool SetBit(T& Value, int32 bitIndex, bool on)
	{
		T bit = GKBIT(bitIndex);
		if (on)
		{
			if (Value & bit) return false;
			Value |= bit;
		}
		else
		{
			if ((Value & bit) == 0) return false;
			Value &= ~bit;
		}
		return true;
	}

	inline static bool SetBitTrue(T& Value, int32 bitIndex)
	{
		T bit = GKBIT(bitIndex);
		if (Value & bit) return false;
		Value |= bit;
		return true;
	}

	inline static bool SetBitFalse(T& Value, int32 bitIndex)
	{
		T bit = GKBIT(bitIndex);
		if ((Value & bit) == 0) return false;
		Value &= ~bit;
		return true;
	}

	inline static void Set(T& Value, int32 bitIndex, bool on)
	{
		if (on)
		{
			Value |= GKBIT(bitIndex);
		}
		else
		{
			Value &= ~GKBIT(bitIndex);
		}
	}

	inline static void SetTrue(T& Value, int32 bitIndex)
	{
		Value |= GKBIT(bitIndex);
	}

	inline static void SetFalse(T& Value, int32 bitIndex)
	{
		Value &= ~GKBIT(bitIndex);
	}

	inline static bool IsTrue(T Value, int32 bitIndex)
	{
		return (Value & GKBIT(bitIndex));
	}

	inline static bool IsFalse(T Value, int32 bitIndex)
	{
		return (Value & GKBIT(bitIndex)) == 0;
	}

	inline static uint32 GetSubValue(T& Value, int32 bitIndex, int32 bitCount)
	{
		return (Value >> bitIndex) & (GKBIT(bitIndex) - 1);
	}

	inline static void SetSubValue(T& Value, int32 bitIndex, int32 bitCount, uint32 subValue)
	{

	}
};

typedef GKBits<uint64> GKBits64;
typedef GKBits<uint32> GKBits32;
typedef GKBits<uint16> GKBits16;
typedef GKBits<uint8> GKBits8;

#undef GKBIT