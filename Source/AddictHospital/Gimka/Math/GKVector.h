// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
//struct GKVector : public FVector
//{
//public:
//	/** Vector's X component. */
//	float X;
//
//	/** Vector's Y component. */
//	float Y;
//
//	/** Vector's Z component. */
//	float Z;
//
//public:
//	static const GKVector Zero;			// 0, 0, 0
//	static const GKVector One;			// 1, 1, 1
//	static const GKVector Up;			// 0, 0, 1
//	static const GKVector Down;			// 0, 0, -1
//	static const GKVector Forward;		// 1, 0, 0
//	static const GKVector Backward;		// -1, 0, 0
//	static const GKVector Right;		// 0, 1, 0
//	static const GKVector Left;			// 0, -1, 0
//	static const GKVector XAxis;		// 1, 0, 0
//	static const GKVector YAxis;		// 0, 1, 0
//	static const GKVector ZAxis;		// 0, 0, 1
//
//public:
//#if ENABLE_NAN_DIAGNOSTIC
//	FORCEINLINE void DiagnosticCheckNaN() const
//	{
//		if (ContainsNaN())
//		{
//			logOrEnsureNanError(TEXT("GKVector contains NaN: %s"), *ToString());
//			*const_cast<GKVector*>(this) = Zero;
//		}
//	}
//
//	FORCEINLINE void DiagnosticCheckNaN(const TCHAR* Message) const
//	{
//		if (ContainsNaN())
//		{
//			logOrEnsureNanError(TEXT("%s: GKVector contains NaN: %s"), Message, *ToString());
//			*const_cast<GKVector*>(this) = Zero;
//		}
//	}
//#else
//	FORCEINLINE void DiagnosticCheckNaN() const {}
//	FORCEINLINE void DiagnosticCheckNaN(const TCHAR* Message) const {}
//#endif
//
//	FORCEINLINE GKVector();
//	~GKVector();
//	FVector
//};
