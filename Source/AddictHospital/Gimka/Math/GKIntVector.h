// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
struct GKIntVector
{
	/** Holds the point's x-coordinate. */
	int32 X;

	/** Holds the point's y-coordinate. */
	int32 Y;

	/**  Holds the point's z-coordinate. */
	int32 Z;

public:
	static const GKIntVector Zero;
	static const GKIntVector One;

	// Constructor
	GKIntVector();
	GKIntVector(int32 iX, int32 iY, int32 iZ);
	explicit GKIntVector(FVector iVector);
	explicit GKIntVector(FIntVector iVector);

	operator FIntVector() const { return FIntVector(X, Y, Z); }

public:
	/**
	 * Gets specific component of a point.
	 *
	 * @param ComponentIndex Index of point component.
	 * @return const reference to component.
	 */
	const int32& operator()(int32 ComponentIndex) const;

	/**
	 * Gets specific component of a point.
	 *
	 * @param ComponentIndex Index of point component.
	 * @return reference to component.
	 */
	int32& operator()(int32 ComponentIndex);

	/**
	 * Gets specific component of a point.
	 *
	 * @param ComponentIndex Index of point component.
	 * @return const reference to component.
	 */
	const int32& operator[](int32 ComponentIndex) const;

	/**
	 * Gets specific component of a point.
	 *
	 * @param ComponentIndex Index of point component.
	 * @return reference to component.
	 */
	int32& operator[](int32 ComponentIndex);

	/**
	 * Compares points for equality.
	 *
	 * @param Other The other int point being compared.
	 * @return true if the points are equal, false otherwise..
	 */
	bool operator==(const GKIntVector& Other) const;

	/**
	 * Compares points for inequality.
	 *
	 * @param Other The other int point being compared.
	 * @return true if the points are not equal, false otherwise..
	 */
	bool operator!=(const GKIntVector& Other) const;

	/**
	 * Scales this point.
	 *
	 * @param Scale What to multiply the point by.
	 * @return Reference to this point after multiplication.
	 */
	GKIntVector& operator*=(int32 Scale);

	/**
	 * Divides this point.
	 *
	 * @param Divisor What to divide the point by.
	 * @return Reference to this point after division.
	 */
	GKIntVector& operator/=(int32 Divisor);

	/**
	 * Adds to this point.
	 *
	 * @param Other The point to add to this point.
	 * @return Reference to this point after addition.
	 */
	GKIntVector& operator+=(const GKIntVector& Other);

	/**
	 * Subtracts from this point.
	 *
	 * @param Other The point to subtract from this point.
	 * @return Reference to this point after subtraction.
	 */
	GKIntVector& operator-=(const GKIntVector& Other);

	GKIntVector& operator*=(const GKIntVector& Other);
	GKIntVector& operator/=(const GKIntVector& Other);

	/**
	 * Assigns another point to this one.
	 *
	 * @param Other The point to assign this point from.
	 * @return Reference to this point after assignment.
	 */
	GKIntVector& operator=(const GKIntVector& Other);

	/**
	 * Gets the result of scaling on this point.
	 *
	 * @param Scale What to multiply the point by.
	 * @return A new scaled int point.
	 */
	GKIntVector operator*(int32 Scale) const;

	/**
	 * Gets the result of division on this point.
	 *
	 * @param Divisor What to divide the point by.
	 * @return A new divided int point.
	 */
	GKIntVector operator/(int32 Divisor) const;

	/**
	 * Gets the result of addition on this point.
	 *
	 * @param Other The other point to add to this.
	 * @return A new combined int point.
	 */
	GKIntVector operator+(const GKIntVector& Other) const;

	/**
	 * Gets the result of subtraction from this point.
	 *
	 * @param Other The other point to subtract from this.
	 * @return A new subtracted int point.
	 */
	GKIntVector operator-(const GKIntVector& Other) const;

	GKIntVector operator*(const GKIntVector& Other) const;
	GKIntVector operator/(const GKIntVector& Other) const;

	/**
	 * Shifts all components to the right.
	 *
	 * @param Shift The number of bits to shift.
	 * @return A new shifted int point.
	 */
	GKIntVector operator>>(int32 Shift) const;

	/**
	 * Shifts all components to the left.
	 *
	 * @param Shift The number of bits to shift.
	 * @return A new shifted int point.
	 */
	GKIntVector operator<<(int32 Shift) const;

	/**
	 * Component-wise AND.
	 *
	 * @param Value Number to AND with the each component.
	 * @return A new shifted int point.
	 */
	GKIntVector operator&(int32 Val) const;

	/**
	 * Component-wise OR.
	 *
	 * @param Value Number to OR with the each component.
	 * @return A new shifted int point.
	 */
	GKIntVector operator|(int32 Value) const;

	/**
	 * Component-wise XOR.
	 *
	 * @param Value Number to XOR with the each component.
	 * @return A new shifted int point.
	 */
	GKIntVector operator^(int32 Value) const;

	/**
	 * Is vector equal to zero.
	 * @return is zero
	*/
	bool IsZero() const;

public:
	/**
	 * Gets the maximum value in the point.
	 *
	 * @return The maximum value in the point.
	 */
	int32 GetMax() const;

	/**
	 * Gets the minimum value in the point.
	 *
	 * @return The minimum value in the point.
	 */
	int32 GetMin() const;

	/**
	 * Gets the distance of this point from (0,0,0).
	 *
	 * @return The distance of this point from (0,0,0).
	 */
	int32 Size() const;

	int32 Volume() const { return X * Y * Z; }
	int32 Area() const { return X * Y; }

	/**
	 * Get a textual representation of this vector.
	 *
	 * @return A string describing the vector.
	 */
	FString ToString() const;

public:

	/**
	 * Divide an int point and round up the result.
	 *
	 * @param lhs The int point being divided.
	 * @param Divisor What to divide the int point by.
	 * @return A new divided int point.
	 */
	static GKIntVector DivideAndRoundUp(GKIntVector lhs, int32 Divisor);
	static GKIntVector DivideAndRoundUp(GKIntVector lhs, GKIntVector Divisor);

	/**
	 * Gets the number of components a point has.
	 *
	 * @return Number of components point has.
	 */
	static int32 Num();

public:

	/**
	 * Serializes the Rectangle.
	 *
	 * @param Ar The archive to serialize into.
	 * @param Vector The vector to serialize.
	 * @return Reference to the Archive after serialization.
	 */
	friend FArchive& operator<<(FArchive& Ar, GKIntVector& Vector)
	{
		return Ar << Vector.X << Vector.Y << Vector.Z;
	}

	friend void operator<<(FStructuredArchive::FSlot Slot, GKIntVector& Vector)
	{
		FStructuredArchive::FRecord Record = Slot.EnterRecord();
		Record << SA_VALUE(TEXT("X"), Vector.X);
		Record << SA_VALUE(TEXT("Y"), Vector.Y);
		Record << SA_VALUE(TEXT("Z"), Vector.Z);
	}

	bool Serialize(FArchive& Ar)
	{
		Ar << *this;
		return true;
	}
};

