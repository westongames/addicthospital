// Fill out your copyright notice in the Description page of Project Settings.


#include "GKIntVector.h"

const GKIntVector GKIntVector::Zero(0, 0, 0);
const GKIntVector GKIntVector::One(1, 1, 1);

/* GKIntVector inline functions
 *****************************************************************************/

FORCEINLINE GKIntVector::GKIntVector()
{ }


FORCEINLINE GKIntVector::GKIntVector(int32 InX, int32 InY, int32 InZ)
	: X(InX)
	, Y(InY)
	, Z(InZ)
{ }


FORCEINLINE const int32& GKIntVector::operator()(int32 ComponentIndex) const
{
	return (&X)[ComponentIndex];
}


FORCEINLINE int32& GKIntVector::operator()(int32 ComponentIndex)
{
	return (&X)[ComponentIndex];
}


FORCEINLINE const int32& GKIntVector::operator[](int32 ComponentIndex) const
{
	return (&X)[ComponentIndex];
}


FORCEINLINE int32& GKIntVector::operator[](int32 ComponentIndex)
{
	return (&X)[ComponentIndex];
}

FORCEINLINE bool GKIntVector::operator==(const GKIntVector& Other) const
{
	return X == Other.X && Y == Other.Y && Z == Other.Z;
}


FORCEINLINE bool GKIntVector::operator!=(const GKIntVector& Other) const
{
	return X != Other.X || Y != Other.Y || Z != Other.Z;
}


FORCEINLINE GKIntVector& GKIntVector::operator*=(int32 Scale)
{
	X *= Scale;
	Y *= Scale;
	Z *= Scale;

	return *this;
}


FORCEINLINE GKIntVector& GKIntVector::operator/=(int32 Divisor)
{
	X /= Divisor;
	Y /= Divisor;
	Z /= Divisor;

	return *this;
}


FORCEINLINE GKIntVector& GKIntVector::operator+=(const GKIntVector& Other)
{
	X += Other.X;
	Y += Other.Y;
	Z += Other.Z;

	return *this;
}


FORCEINLINE GKIntVector& GKIntVector::operator*=(const GKIntVector& Other)
{
	X *= Other.X;
	Y *= Other.Y;
	Z *= Other.Z;

	return *this;
}

FORCEINLINE GKIntVector& GKIntVector::operator/=(const GKIntVector& Other)
{
	X /= Other.X;
	Y /= Other.Y;
	Z /= Other.Z;

	return *this;
}


FORCEINLINE GKIntVector& GKIntVector::operator-=(const GKIntVector& Other)
{
	X -= Other.X;
	Y -= Other.Y;
	Z -= Other.Z;

	return *this;
}

FORCEINLINE GKIntVector& GKIntVector::operator=(const GKIntVector& Other)
{
	X = Other.X;
	Y = Other.Y;
	Z = Other.Z;

	return *this;
}


FORCEINLINE GKIntVector GKIntVector::operator*(int32 Scale) const
{
	return GKIntVector(*this) *= Scale;
}


FORCEINLINE GKIntVector GKIntVector::operator/(int32 Divisor) const
{
	return GKIntVector(*this) /= Divisor;
}


FORCEINLINE GKIntVector GKIntVector::operator+(const GKIntVector& Other) const
{
	return GKIntVector(*this) += Other;
}

FORCEINLINE GKIntVector GKIntVector::operator-(const GKIntVector& Other) const
{
	return GKIntVector(*this) -= Other;
}

FORCEINLINE GKIntVector GKIntVector::operator*(const GKIntVector& Other) const
{
	return GKIntVector(*this) *= Other;
}

FORCEINLINE GKIntVector GKIntVector::operator/(const GKIntVector& Other) const
{
	return GKIntVector(*this) /= Other;
}

FORCEINLINE GKIntVector GKIntVector::operator>>(int32 Shift) const
{
	return GKIntVector(X >> Shift, Y >> Shift, Z >> Shift);
}

FORCEINLINE GKIntVector GKIntVector::operator<<(int32 Shift) const
{
	return GKIntVector(X << Shift, Y << Shift, Z << Shift);
}

FORCEINLINE GKIntVector GKIntVector::operator&(int32 Value) const
{
	return GKIntVector(X & Value, Y & Value, Z & Value);
}

FORCEINLINE GKIntVector GKIntVector::operator|(int32 Value) const
{
	return GKIntVector(X | Value, Y | Value, Z | Value);
}

FORCEINLINE GKIntVector GKIntVector::operator^(int32 Value) const
{
	return GKIntVector(X ^ Value, Y ^ Value, Z ^ Value);
}

FORCEINLINE GKIntVector GKIntVector::DivideAndRoundUp(GKIntVector lhs, int32 Divisor)
{
	return GKIntVector(FMath::DivideAndRoundUp(lhs.X, Divisor), FMath::DivideAndRoundUp(lhs.Y, Divisor), FMath::DivideAndRoundUp(lhs.Z, Divisor));
}

FORCEINLINE GKIntVector GKIntVector::DivideAndRoundUp(GKIntVector lhs, GKIntVector Divisor)
{
	return GKIntVector(FMath::DivideAndRoundUp(lhs.X, Divisor.X), FMath::DivideAndRoundUp(lhs.Y, Divisor.Y), FMath::DivideAndRoundUp(lhs.Z, Divisor.Z));
}


FORCEINLINE int32 GKIntVector::GetMax() const
{
	return FMath::Max(FMath::Max(X, Y), Z);
}


FORCEINLINE int32 GKIntVector::GetMin() const
{
	return FMath::Min(FMath::Min(X, Y), Z);
}


FORCEINLINE int32 GKIntVector::Num()
{
	return 3;
}


FORCEINLINE int32 GKIntVector::Size() const
{
	int64 X64 = (int64)X;
	int64 Y64 = (int64)Y;
	int64 Z64 = (int64)Z;
	return int32(FMath::Sqrt(float(X64 * X64 + Y64 * Y64 + Z64 * Z64)));
}

FORCEINLINE bool GKIntVector::IsZero() const
{
	return *this == Zero;
}


FORCEINLINE FString GKIntVector::ToString() const
{
	return FString::Printf(TEXT("X=%d Y=%d Z=%d"), X, Y, Z);
}

FORCEINLINE uint32 GetTypeHash(const GKIntVector& Vector)
{
	return FCrc::MemCrc_DEPRECATED(&Vector, sizeof(GKIntVector));
}
