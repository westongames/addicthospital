// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
struct GKDir
{
public:
	const int32 LEFT = 0;
	const int32 RIGHT = 1;
	const int32 BACK = 2;
	const int32 FORWARD = 3;
	const int32 LEFT_BACK = 4;
	const int32 LEFT_FORWARD = 5;
	const int32 RIGHT_BACK = 6;
	const int32 RIGHT_FORWARD = 7;
	const int32 DIR4_COUNT = 4;
	const int32 DIR8_COUNT = 8;

	GKDir();
	~GKDir();
};
