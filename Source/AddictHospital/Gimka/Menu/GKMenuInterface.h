// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
//#include "GKMenuInterface.generated.h"

class UGKMenu;

// This class does not need to be modified.
//UINTERFACE(MinimalAPI)
//class UGKMenuInterface : public UInterface
//{
//	GENERATED_BODY()
//};

/**
 * 
 */
class /*ADDICTHOSPITAL_API*/ GKMenuInterface
{
	//GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual ~GKMenuInterface() {}

	//////////////////////////////////////////////////////////
	// Common
	//////////////////////////////////////////////////////////
	virtual void OnMenuClose();

	//////////////////////////////////////////////////////////
	// Main Menu
	//////////////////////////////////////////////////////////
	virtual void OnMenuPlay();

	//////////////////////////////////////////////////////////
	// Multiplayer Menu
	//////////////////////////////////////////////////////////
	virtual void OnMenuHost(const FString& SessionName);
	virtual void OnMenuJoin(const FString& IPAddress);
	void OnMenuJoinSelectedServer();			// Join selected server
	virtual void OnMenuJoin(uint32 Index);

	//////////////////////////////////////////////////////////
	// Multiplayer Join Menu
	//////////////////////////////////////////////////////////
	virtual void RefreshServerList() {}

	//////////////////////////////////////////////////////////
	// In-Game Menu
	//////////////////////////////////////////////////////////
	virtual void OnMenuQuit();				// Back to main menu

	UGKMenu* LoadMenu(UGameInstance* GameInstance, TSubclassOf<class UUserWidget> MenuClass);

	void SelectIndex(uint32 Index);
	bool GetSelectedIndex(uint32& Index);
	void ResetSelectedIndex();

protected:
	void CloseMenu();
	void ReopenMenu();

	//TSubclassOf<Class UUserWidget> GetCurrentMenuClass() { return CurrentMenuClass; }
	UGKMenu* GetActiveMenu() { return ActiveMenu; }

private:
	UGKMenu* ActiveMenu = nullptr;			// Currently active menu
	TSubclassOf<class UUserWidget> CurrentMenuClass = nullptr;
	UGameInstance* CurrentGameInstance = nullptr;
	TOptional<uint32> SelectedIndex;

};
