// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Menu/GKMenuInterface.h"

#include "GKMenu.generated.h"

/**
 * Not using this for VGAMainMenu because pure virtual function crash.
 */
UCLASS()
class /*ADDICTHOSPITAL_API*/ UGKMenu : public UUserWidget
{
	GENERATED_BODY()

public:
	//UGKMainMenu(const FObjectInitializer& ObjectInitializer) {}
	
	bool Setup();
	void Teardown();
	//void Reopen();

	virtual void OnSelectIndex(uint32 Index) {}
	virtual void OnResetSelectedIndex(uint32 OldIndex) {}


protected:
	GKMenuInterface* MenuInterface;

public:
	GK_SET(GKMenuInterface*, MenuInterface);

};
