// Fill out your copyright notice in the Description page of Project Settings.


#include "GKMenuInterface.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Menu/GKMenu.h"

// Add default functionality here for any IGKMenuInterface functions that are not pure virtual.

void GKMenuInterface::OnMenuClose()
{
	CloseMenu();
}

void GKMenuInterface::OnMenuPlay()
{
	GKLog("OnPlay");
	CloseMenu();
}

void GKMenuInterface::OnMenuHost(const FString& SessionName)
{
	GKLog("OnHost");
	CloseMenu();
}

void GKMenuInterface::OnMenuJoin(const FString& IPAddress)
{
	GKLog("OnJoin");
	CloseMenu();
}

void GKMenuInterface::OnMenuJoin(uint32 Index)
{
	GKLog1("OnJoin Index", Index);
	CloseMenu();
}

void GKMenuInterface::OnMenuJoinSelectedServer()
{
	GKLog("OnJoinSelectedServer");
	uint32 Index;
	if (GetSelectedIndex(Index))
	{
		GKLog1("Selected Index", Index);
		OnMenuJoin(Index);
	}
	else
	{
		//GKLog("Selected Index not set.");
		GKScreenError("Please select a server to join!");
	}
}

void GKMenuInterface::OnMenuQuit()
{
	GKLog("OnQuit");
	CloseMenu();
}

void GKMenuInterface::CloseMenu()
{
	if (ActiveMenu != nullptr)
	{
		//ClosedMenu = ActiveMenu;
		ActiveMenu->Teardown();
		ActiveMenu = nullptr;			// Is this ok without delete?
	}
}

// For now reopen means recreating it again. Tried to just do Setup but it doesn't show anything.
void GKMenuInterface::ReopenMenu()
{
	if (ActiveMenu == nullptr && CurrentMenuClass != nullptr && CurrentGameInstance != nullptr)
	{
		LoadMenu(CurrentGameInstance, CurrentMenuClass);
	}
}

UGKMenu* GKMenuInterface::LoadMenu(UGameInstance* GameInstance, TSubclassOf<class UUserWidget> MenuClass)
{
	if (ActiveMenu != nullptr) return ActiveMenu;		// Was crashing so added this.
	check(ActiveMenu == nullptr);

	if (!ensure(MenuClass)) return nullptr;

	ActiveMenu = CreateWidget<UGKMenu>(GameInstance, MenuClass);
	if (!ensure(ActiveMenu)) return nullptr;;

	bool Success = ActiveMenu->Setup();
	check(Success);
	ActiveMenu->SetMenuInterface(this);

	CurrentMenuClass = MenuClass;
	CurrentGameInstance = GameInstance;
	return ActiveMenu;
}

void GKMenuInterface::SelectIndex(uint32 Index)
{
	SelectedIndex = Index;
	if (ActiveMenu != nullptr) 
	{
		ActiveMenu->OnSelectIndex(Index);
	}
}

bool GKMenuInterface::GetSelectedIndex(uint32& Index)
{
	if (SelectedIndex.IsSet())
	{
		Index = SelectedIndex.GetValue();
		return true;
	}
	return false;
}

void GKMenuInterface::ResetSelectedIndex()
{
	if (ActiveMenu != nullptr && SelectedIndex.IsSet())
	{
		ActiveMenu->OnResetSelectedIndex(SelectedIndex.GetValue());
	}
	SelectedIndex.Reset();
}
