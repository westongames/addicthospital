// Fill out your copyright notice in the Description page of Project Settings.


#include "GKMenu.h"

#include "Components/Button.h"
//
//bool UGKMainMenu::Initialize()
//{
//	if (!Super::Initialize()) return false;
//
//	if (!ensure(ButtonHost)) return false;
//	ButtonHost->OnClicked.AddDynamic(this, &UGKMainMenu::OnHostServer);
//
//	return true;
//}
//
//
//void UGKMainMenu::OnHostServer()
//{
//	GKLog("Host Server!");
//}


bool UGKMenu::Setup()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	GKEnsureB(PlayerController);

	GKLog1("Setup", PlayerController);

	//UVGAMainMenu* Menu = CreateWidget<UVGAMainMenu>(this, MenuClass);
		//if (!ensure(Menu)) return false;

	bIsFocusable = true;
	AddToViewport();

	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = true;

	return true;
}

void UGKMenu::Teardown()
{
	RemoveFromViewport();

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	GKEnsure(PlayerController);

	GKLog1("Teardown", PlayerController);

	FInputModeGameOnly InputModeData;
	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = false;
}

//void UGKMenu::Reopen()
//{
//	Setup();
//}
