// Fill out your copyright notice in the Description page of Project Settings.


#include "GKEntityManager.h"
#include "Gimka/Entity/Entity/GKEntity.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKEntityManager::GKEntityManager(const GKGameManager* iGameManager)
{
	GameManager = iGameManager;
}

GKEntityManager::~GKEntityManager()
{
}



int tracker = 0;
//void GKEntityManager::OnJobGameMinute(int jobIndex)
//{
//	GKLog1("GKEntityManager", tracker++);
//
//	// For now just do all entities. Later on, we can have separate array entities.
//	for (int i = 0, count = arrayEntities.Num(); i < count; ++i) 
//	{
//		GKEntity* entity = arrayEntities[i];
//		entity->OnGameMinute();
//	}
//}
//
//void GKEntityManager::OnJobApplyChanges(int jobIndex)
//{
//	// For now just do all entities. Later on, we can have separate array entities.
//	for (int i = 0, count = arrayEntities.Num(); i < count; ++i)
//	{
//		GKEntity* entity = arrayEntities[i];
//		entity->OnApplyChanges();
//	}
//}
//
//void GKEntityManager::OnJobUpdateVisual(int jobIndex)
//{
//}
//
//void GKEntityManager::OnJobPostProcess(int jobIndex)
//{
//}

void GKEntityManager::OnJobGameTick(const GKJobSystem2::JobArgs& jobArgs)
{
	//if (!GetGameManager()->HasAuthority())
		//GKLog1("GKEntityManager::OnJobGameTick", ArrayEntities.Num());// tracker++);

	// No changes here! All changes might be posted to ArrayChangeMessages.

	// 1. Process MessageInbox to entities here. Possibly message for GKRegion too if exists (inherit it)
	// Some entities might not have gameTick for this frame (updated only every gameMinute, or gameHour)
	// This ensures all entities will get their messages when there's a gameTick, regardless if they themselves have gameTick or not.
	// For instance, the message might to damage or ask the entity to self destroy, which the entity will send GKMessageChange::DestroySelf

	// 2. Entity::OnGameTick. For now just do all entities. Later on, we can have separate array entities.
	// If entity has multiple update (i.e. gameHour & gameMinute), it still will only be called once.
	for (int i = 0, count = ArrayEntities.Num(); i < count; ++i) 
	{
		GKEntity* entity = ArrayEntities[i];
		entity->OnGameTick();
	}
}

void GKEntityManager::OnJobApplyChanges(const GKJobSystem2::JobArgs& jobArgs)
{
	//GKLog1("GKEntityManager::OnJobApplyChanges", ArrayEntities.Num());// tracker++);

	// 1. Process ArrayChangeMessages, which are from JobGameTick, to update entity internal data.
	for (GKMessageChange& message : ArrayChangeMessages)
	{
		message.GetSender()->ProcessMessageChange(message);
	}

	ArrayChangeMessages.Empty();

	// 2. Proces ArrayRegionMessages, includes CreateEntity, DestroySelf
	
	// For now just do all entities. Later on, we can have separate array entities.
	//for (int i = 0, count = arrayEntities.Num(); i < count; ++i)
	//{
	//	GKEntity* entity = arrayEntities[i];
	//	entity->OnApplyChanges();
	//}
}

void GKEntityManager::OnPostGameTick()
{
	// Called 0 to multiple times depending on gameSpeed, in order of JobGameTick, JobApplyChanges, PostGameTick.
	// This one is non-job because we need to access other region's entities.

	// 1. Process MessageOutbox, deliver messages for entities in other region 
	//		(put into the region MessageInbox, includes for VisualManager)
	// 
	// 2. Process ArrayRegionExternalMessages, includes MoveEntityToOtherRegion

}

//void GKEntityManager::OnJobPostFrame(const GKJobSystem2::JobArgs& jobArgs)
//{
//	// Todo: update region visibility here and update entities visual as needed
//
//	// Todo: delete entities request here
//	GKLog("GKEntityManager::OnJobPostFrame");
//}

void GKEntityManager::OnEndOfFrame()
{
	// Todo: Process MessageOutbox, usually it's already empty unless OnJobPostFrame posted MessageDeleteActor for GKVisualManager
}


/*void GKEntityManager::OnJobUpdateVisual(const GKJobSystem2::JobArgs& jobArgs)
{
}

void GKEntityManager::OnJobPostProcess(const GKJobSystem2::JobArgs& jobArgs)
{
}*/

void GKEntityManager::Add(GKEntity* entity)
{
	ArrayEntities.Add(entity->GetID(), entity);

	if (entity->GetEntityType() >= GKEntity::Type::VisualEntity) 
	{
		ArrayVisualEntities.Add((GKVisualEntity*)entity);
	}
}

void GKEntityManager::ScheduleForVisualUpdate(GKVisualEntity* Entity)
{
	checkSlow(!ArrayDirtyVisualEntities.Contains(Entity));
	ArrayDirtyVisualEntities.Add(Entity);
	//GKLog1("Added Dirty VisualEntity to ", ArrayDirtyVisualEntities.Num());
}
