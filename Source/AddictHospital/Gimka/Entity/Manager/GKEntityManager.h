// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Containers/Array/GKArrayMapped.h"
#include "Gimka/Entity/Identity/GKIdentity.h"
#include "Gimka/System/Job/GKJob.h"
#include "Gimka/Entity/Message/GKMessageChange.h"
#include "Gimka/Entity/Message/GKMessageEntityManager.h"
#include "Gimka/Entity/Message/GKMessageEntityManagerNonJob.h"
#include "Gimka/Entity/Message/GKMessageGlobal.h"
#include "Gimka/Containers/Array/GKSortedArray.h"
#include "Gimka/Visual/GKSpriteInfo.h"
#include "Gimka/Tools/Macros/GKHeader.h"

class GKEntity;
class GKVisualEntity;
class GKGameManager;

/**
 * GKRegion will be derived from this.
 * 
 * We have CameraPosition and array3D of GKRegion. GKEntity has pointer to SpriteNode assigned from GKVisualManager.
 *  In GKEntity::ApplyPost
 *		- if changing position:
*				- If it has visual, SpriteNode.DirtyBits.SetTrue(Dirty.Position). Put into list GKRegion::EntitiesToUpdateVisual
 *		- if changing region, postMessagePostUpdate(ChangeRegion, Entity)
 *		- if deleting self, postMessageDestroyEntity(Entity)
 *  In GKRegion::OnDestroyEntityMessage (not multi threaded)
 *		- delete entity (put back to array), keep SpriteNode (it will be deleted by GKVisualManager)
 *		- If has visual, add the SpriteNode to EntitiesToUpdateVisual if not yet, set entity to nullptr
 *		- At the end of this function, combine the EntitiesToUpdateVisual to GKvisualManager::head. Reset current head to nullptr.
 *  In GKRegion::OnJobChangeEntityRegion (multi threaded)
*		- move entity to the other region.
*		- Move from entitiesToUpdateVisual if exist to other region.
*				
 *	In GKRegion::PostUpdate - calcEntityVisibility:
 *		- Visibility.On: run through all entities, those who don't have visual, put into list (actor is nullptr)
 *		- Visibility.Close: no changes
 *		- Visibility.Off: run through all entities, those who have visual, put into list and mark as to destroy visual
 *  
 * === If using separate
 *	
 */
class GKEntityManager : public GKJob
{
public:
	enum Event { GameMinute, UpdateVisual, Count };

	GKEntityManager(const GKGameManager* GameManager);
	virtual ~GKEntityManager();

	void Add(GKEntity* entity);

	// Put the Entity, belong to this Manager, into arrayDirtyVisualEntities
	void ScheduleForVisualUpdate(GKVisualEntity* Entity);
	void ClearArrayDirtyVisualEntities() { ArrayDirtyVisualEntities.Empty(); }

	inline void PostMessage(GKEntity* sender, GKMessageChange message)
	{
		message.SetSender(sender);
		ArrayChangeMessages.Add(message); 
	}

	//virtual void OnJob(const GKJobSystem2::JobArgs& jobArgs) override;
	virtual void OnPostGameTick();

	// Always called once at the end of the frame after JobPostFrame
	virtual void OnEndOfFrame();

	GKEntity* GetEntity(int32 index) { return ArrayEntities[index]; }

	GKEntity* FindEntity(uint32 EntityID) { GKEntity* Entity; return ArrayEntities.TryFind(EntityID, Entity) ? Entity : nullptr; }

protected:
	// Inherited via GKJob
	//virtual void OnJobGameMinute(int jobIndex) override;
	//virtual void OnJobApplyChanges(int jobIndex) override;
	//virtual void OnJobUpdateVisual(int jobIndex) override;
	//virtual void OnJobPostProcess(int jobIndex) override;

	const GKGameManager* GameManager;				// Ref back to GameManager

	GKArrayMapped<uint32, GKEntity*> ArrayEntities;				// All entities added here

	TArray<GKVisualEntity*> ArrayVisualEntities;			// VisualEntity or subclasses of it, so it can have Actor

	// The main game loop, called 0 to multiple times depending on gameSpeed: JobGameTick, JobApplyChanges, PostGameTick
	virtual void OnJobGameTick(const GKJobSystem2::JobArgs& jobArgs) override;
	virtual void OnJobApplyChanges(const GKJobSystem2::JobArgs& jobArgs) override;

	// Always called once at the end of the frame after the main loop: JobPostFrame, then EndOfFrame
	virtual void OnJobPostFrame(const GKJobSystem2::JobArgs& jobArgs) override {}


private:

	// VisualEntities that need to be updated by GKVisualManager
	// Note: it's possible for this chunk to wait for several frames before it gets updated by VisualManager
	TArray <GKVisualEntity*> ArrayDirtyVisualEntities;			

	// Update entity self data
	// Posted in GKEntity::JobGameTick
	// Processed in GKEntity::ProcessMessageChange
	TArray<GKMessageChange> ArrayChangeMessages;						// array message for ApplyChanges

	// Messages for entities in this region
	TArray<GKMessageGlobal> ArrayInboxMessages;

	// Messages for region: CreateEntity, DestroySelf (entity)
	// Posted in GKEntity::JobGameTick, GKEntity::ProcessMessageChange
	// Processed in GKEntityManager::JobApplyChanges
	TArray<GKMessageEntityManager> ArrayRegionMessages;

	// Messages for region non job for external: MoveEntityToOtherRegion
	// Posted in GKEntity::ProcessMessageChange
	// Processed in GKEntityManager::PostGameTick
	TArray<GKMessageEntityManagerNonJob> ArrayRegionNonJobMessages;

	// Messages for entities in other regions (to be put into the other region arrayInboxMessages),
	// or for other like GKVisualManager (i.e. DeleteActor)
	// Posted in GKEntity::JobGameTick, GKEntity::ProcessMessageChange
	// Process in GKEntityManager::JobGameTick, GKVisualManager::OnUpdate
	TArray<GKMessageGlobal> ArrayOutboxMessages;

public:
	GK_GETCR(TArray<GKVisualEntity*>, ArrayDirtyVisualEntities);
	GKPTR_GETC(GKGameManager, GameManager);
};
