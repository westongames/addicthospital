// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKMessageHeader.h"
#include "GKMessage.h"

/**
 * Change messages for self
 */
struct GKMessageChange : public GKMessage
{
public:
	GKMessageChange();
	~GKMessageChange();

	enum Msg 
	{
		Position,
		PositionAndVelocity,
		Acceleration
	};

	MSG_CR(FVector, Position, Vector);
	MSG2_CR(PositionAndVelocity, FVector, Position, Vector, FVector, Velocity, Vector2);
	MSG_CR(FVector, Acceleration, Vector);

private:
	//GKEntity* SenderEntity;				// Direct to entity because this will be applied to local computer (not send over network)
};
