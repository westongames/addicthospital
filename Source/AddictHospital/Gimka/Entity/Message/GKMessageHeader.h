// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

#define MSG_CR(VarType, VarName, IntVar)																\
	FORCEINLINE void Set ## VarName (const VarType& Value) { Type = Msg::VarName; IntVar = Value; }		\
	FORCEINLINE const VarType& Get ## VarName () const { return IntVar; }

#define MSG2_CR(MsgName, VarType1, VarName1, IntVar1, VarType2, VarName2, IntVar2)						\
	FORCEINLINE void Set ## MsgName (const VarType1& Value1, const VarType2& Value2)					\
		{ Type = Msg::MsgName; IntVar1 = Value1; IntVar2 = Value2; }									\
	FORCEINLINE void Get ## MsgName (VarType1& Value1, VarType2 Value2) const							\
		{ Value1 = IntVar1; Value2 = IntVar2; }
