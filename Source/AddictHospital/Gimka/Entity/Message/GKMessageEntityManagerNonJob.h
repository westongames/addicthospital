// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKMessage.h"

/**
 * 
 */
class GKMessageEntityManagerNonJob : public GKMessage
{
public:
	GKMessageEntityManagerNonJob();
	~GKMessageEntityManagerNonJob();
};
