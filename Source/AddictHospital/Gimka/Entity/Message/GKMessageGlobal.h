// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKMessage.h"

/**
 * 
 */
struct GKMessageGlobal : public GKMessage
{
public:
	GKMessageGlobal();
	~GKMessageGlobal();
};
