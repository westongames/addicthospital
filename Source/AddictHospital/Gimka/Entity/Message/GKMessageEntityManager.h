// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GKMessage.h"

/**
 * 
 */
struct GKMessageEntityManager : public GKMessage
{
public:
	GKMessageEntityManager();
	~GKMessageEntityManager();
};
