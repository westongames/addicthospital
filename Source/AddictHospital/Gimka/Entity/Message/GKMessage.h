// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"

class GKEntity;

/**
 * 
 */
struct GKMessage
{
public:
	GKMessage();
	~GKMessage();

protected:
	int32 Type;
	FVector Vector, Vector2;
	FRotator Rotation;

private:
	GKEntity* Sender;

public:
	GK_GETSET(GKEntity*, Sender);
	GK_GET(int32, Type);

};
