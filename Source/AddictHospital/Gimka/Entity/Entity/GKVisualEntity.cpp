// Fill out your copyright notice in the Description page of Project Settings.


#include "GKVisualEntity.h"
#include "Gimka/Entity/Manager/GKEntityManager.h"

GKVisualEntity::GKVisualEntity(GKEntityManager* manager, uint32 ID, Type type)
	: GKStaticEntity(manager, ID, type)
{
	SpriteInfo.Initialize(this);

}

GKVisualEntity::~GKVisualEntity()
{
}

void GKVisualEntity::ScheduleForVisualUpdate()
{
	GetManager()->ScheduleForVisualUpdate(this);
}

void GKVisualEntity::SetVisible(bool visible)
{
	SpriteInfo.SetVisible(visible);
}

