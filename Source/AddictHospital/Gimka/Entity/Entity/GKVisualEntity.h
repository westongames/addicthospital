// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Entity/Entity/GKStaticEntity.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Visual/GKSpriteInfo.h"

/**
 * GKVisualEntity:
 * - Has unique visual, separated from GKRegion. Usually as an actor in Unreal.
 */
class GKVisualEntity : public GKStaticEntity
{
public:
	GKVisualEntity(GKEntityManager* manager, uint32 ID, Type type = Type::VisualEntity);
	virtual ~GKVisualEntity();

	void ScheduleForVisualUpdate();
	
	// When regionVisibility changed, setVisible will be called to create/ destroy Actor.
	// No change if actor is already created/ destroyed.
	void SetVisible(bool visible);

private:
	GKSpriteInfo SpriteInfo;			// If has actor, this will link this Entity to its matching SpriteNode. Otherwise it's null.

public:
	GK_SETCR_OVERRIDE(FVector, Position) 
	{ 
		GKStaticEntity::SetPosition(Value); 
		SpriteInfo.SetDirtyPosition();
	}

	GK_SETCR_OVERRIDE(FRotator, Rotation)
	{
		GKStaticEntity::SetRotation(Value);
		SpriteInfo.SetDirtyRotation();
	}

	GK_GETR(GKSpriteInfo, SpriteInfo);

	/*FORCEINLINE virtual void SetPosition(const VarType& Value) override
	{ 
		GKStaticEntity::SetPosition()
		VarName = Value; 
	}*/
};
