// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Entity/Identity/GKIdentity.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Entity/Message/GKMessageChange.h"

class GKEntityManager;
class GKGameManager;
/**
 * GKEntity might be an item in Inventory, it doesn't have location in the world.
 *	- Has an ID
 *	- Has an owner, which might be null. 
 *	- Has a GKEntityManager, i.e. the GKRegion
 *	- Can receive and send message
 *	- Can register for GKJob events.
 */
class GKEntity : public GKIdentity
{
	friend class GKEntityManager;

public:
	enum Type { Entity, StaticEntity, VisualEntity, MovingEntity, PawnEntity, CharacterEntity };

	GKEntity(GKEntityManager* manager, uint32 ID, Type type = Type::Entity);
	virtual ~GKEntity();

protected:
	virtual void OnGameTick() {}

	virtual void ProcessMessageChange(const GKMessageChange& message);

	virtual FORCEINLINE void PostMessage(const GKMessageChange& message);

private:
	Type EntityType;
	GKEntityManager* Manager;		// Point back to manager, never null

public:
	GK_GET(Type, EntityType);
	GK_GET(GKEntityManager*, Manager);

	inline const GKGameManager* GetGameManager();
};
