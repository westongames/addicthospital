// Fill out your copyright notice in the Description page of Project Settings.


#include "GKEntity.h"
#include "Gimka/Entity/Manager/GKEntityManager.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKEntity::GKEntity(GKEntityManager* manager, uint32 ID, Type type)
	: GKIdentity(ID)
{
	this->Manager = manager;
	this->EntityType = type;
	manager->Add(this);
}

GKEntity::~GKEntity()
{
}

void GKEntity::PostMessage(const GKMessageChange& message)
{
	Manager->PostMessage(this, message);
}

void GKEntity::ProcessMessageChange(const GKMessageChange& message)
{
	GKError("Unprocessed message");
}

const GKGameManager* GKEntity::GetGameManager()
{ 
	return Manager->GetGameManager(); 
}
