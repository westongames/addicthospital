// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Entity/Entity/GKPawnEntity.h"

/**
 * GKCharacterEntity: complex character that can do combat, i.e. the patient, doctor.
 * - Able to do combat. Has TargettingSystem, CombatSystem.
 */
class GKCharacterEntity : public GKPawnEntity
{
public:
	GKCharacterEntity(GKEntityManager* manager, int32 ID, Type type = Type::CharacterEntity);
	virtual ~GKCharacterEntity();

private:

public:
};
