// Fill out your copyright notice in the Description page of Project Settings.


#include "GKMovingEntity.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Game/GKGameManager.h"

GKMovingEntity::GKMovingEntity(GKEntityManager* manager, uint32 ID, Type type)
	: GKVisualEntity(manager, ID, type)
{
}

GKMovingEntity::~GKMovingEntity()
{
}

void GKMovingEntity::OnGameTick() 
{
	GKVisualEntity::OnGameTick();

	//GKLog1("Velocity = ", Velocity.X);
	/*if (!GetGameManager()->HasAuthority())
	{
		// Temp Solution. Following Udemy Tutorial. No moving unless server.
		return;
	}*/

	// Add acceleration
	if (!Acceleration.IsNearlyZero())
	{
		FVector NewVelocity = Velocity + Acceleration;

		// Todo: send message to update velocity

		//Velocity = NewVelocity;

		FVector NewPosition = GetPosition() + Velocity;
		
		// Todo: send message to update Position
		//SetPosition(NewPosition);

		GKMessageChange message;
		message.SetPositionAndVelocity(NewPosition, NewVelocity);
		PostMessage(message);
	}
	else if (!Velocity.IsNearlyZero())
	{
		FVector NewPosition = GetPosition() + Velocity;

		//if (!GetGameManager()->HasAuthority())
		/*{
			GKLog2("Client is moving ", Velocity.X, NewPosition.X);
		}*/

		// Todo: send message to update Position
		//SetPosition(NewPosition);
		//GKLog1("NewPos", NewPosition.X);

		GKMessageChange message;
		message.SetPosition(NewPosition);
		PostMessage(message);
	}
}

void GKMovingEntity::ProcessMessageChange(const GKMessageChange& message)
{
	switch (message.GetType())
	{
		case GKMessageChange::Msg::PositionAndVelocity:
		{
			FVector NewPosition, NewVelocity;
			message.GetPositionAndVelocity(NewPosition, NewVelocity);
			SetPosition(NewPosition);
			SetVelocity(NewVelocity);
			GKLog1("Msg::PositionAndVelocity", NewPosition.X);
			break;
		}
		case GKMessageChange::Msg::Acceleration:
		{
			SetAcceleration(message.GetAcceleration());
			break;
		}
		default:
		{
			GKVisualEntity::ProcessMessageChange(message);
			break;
		}
	}
}
