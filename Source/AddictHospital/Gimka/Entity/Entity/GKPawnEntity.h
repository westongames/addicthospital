// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Entity/Entity/GKMovingEntity.h"
#include "Gimka/Containers/Container/GKContainerData.h"

/**
 * GKPawnEntity: a simple AI entity, i.e. animals, persons who don't attack (if exists)
 *  - Has Force/Power which affect movement (Velocity, Acceleration).
 *  - Has Sense to receive input from the world.
 *  - Has Memory to search, chase, follow other Pawns.
 *	- Has variable data, stored in flexible, expandable, dictionary.
 *  - Doesn't do combat. But can be attacked and die. Has HP. As specified in GKStaticEntity.
 *	- Visual can play animation, i.e. walking, running, jumping, dancing.
 */
class GKPawnEntity : public GKMovingEntity
{
public:
	GKPawnEntity(GKEntityManager* manager, uint32 ID, Type type = Type::PawnEntity);
	virtual ~GKPawnEntity();

protected:
	//virtual void OnGameMinute() override;
	GKContainerData Data;

};
