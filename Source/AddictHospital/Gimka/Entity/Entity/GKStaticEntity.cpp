// Fill out your copyright notice in the Description page of Project Settings.


#include "GKStaticEntity.h"

GKStaticEntity::GKStaticEntity(GKEntityManager* manager, uint32 ID, Type type)
	: GKEntity(manager, ID, type)
{
}

GKStaticEntity::~GKStaticEntity()
{
}

void GKStaticEntity::ProcessMessageChange(const GKMessageChange& message)
{
	switch (message.GetType())
	{
	case GKMessageChange::Position:
	{
		SetPosition(message.GetPosition());
		break;
	}
	default:
	{
		GKEntity::ProcessMessageChange(message);
		break;
	}
	}
}
