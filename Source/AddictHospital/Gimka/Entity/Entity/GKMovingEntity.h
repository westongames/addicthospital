// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Entity/Entity/GKVisualEntity.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * GKMovingEntity: can move, i.e. Projectiles, leaves falling.
 * - Has velocity, acceleration.
 * - Has unique visual. For voxel games, it won't be rendered together with the GKRegion.
 */
class GKMovingEntity : public GKVisualEntity
{
public:
	GKMovingEntity(GKEntityManager* manager, uint32 ID, Type type = Type::MovingEntity);
	virtual ~GKMovingEntity();

protected:
	virtual void OnGameTick() override;
	virtual void ProcessMessageChange(const GKMessageChange& message) override;

private:
	FVector Velocity = FVector::ZeroVector;
	FVector Acceleration = FVector::ZeroVector;

public:
	GK_GETSETCR(FVector, Velocity);				// Velocity per tick
	GK_GETSETCR(FVector, Acceleration);			// Acceleration per tick
};
