// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Entity/Entity/GKEntity.h"
#include "Gimka/Math/GKIntVector.h"

/**
 * GKActor: a simple static in the world (doesn't move), i.e. a rock, a tree.
 * - Has location in the world.
 * - Might use Rotation or it default to zero.
 * - No separate visual. For voxel games, it's rendered together with the region.
 * - Has HP. Can be destroyed.
 * - Not for ground. Ground data will be specified in GKRegion. 
 */
class GKStaticEntity : public GKEntity
{
public:
	GKStaticEntity(GKEntityManager* manager, uint32 ID, Type type = Type::StaticEntity);
	virtual ~GKStaticEntity();

protected:
	virtual void ProcessMessageChange(const GKMessageChange& message) override;

private:
	FVector Position = FVector::ZeroVector;
	FRotator Rotation = FRotator::ZeroRotator;

public:
	GK_GETSETCR_VIRTUAL(FVector, Position)
	GK_GETSETCR_VIRTUAL(FRotator, Rotation)
};
