// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * Identity an Entity. See GKEntity.
 */
struct GKIdentity
{
public:
	GKIdentity(uint32 ID);
	virtual ~GKIdentity();

	inline bool operator==(const GKIdentity& other) const { return ID == other.ID; }
	inline bool operator!=(const GKIdentity& other) const { return ID != other.ID; }

	inline bool Equals(const GKIdentity& other) const { return ID == other.ID; }

	inline uint32 GetTypeHash(const GKIdentity& Other) { return ID; }
	//{ return FCrc::MemCrc32(&Other, sizeof(GKIdentity)); }	
	// GetTypeHash(ID) + GetTypeHash(other.ID);


private:
	uint32 ID;			// Can store type into ID. Also will use player number combined with some number generating to make sure it's unique. Or let the server generate ID (can't because take time).

public:
	GK_GET(uint32, ID);
};


