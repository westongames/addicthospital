// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "GKXMLCollection.h"

/**
 * 
 */
class GKXMLManager
{
public:
	GKXMLManager();
	~GKXMLManager();

	void Init();
	void ParseAllXMLs();

	void OnDestroy();

private:
	struct ModInfo
	{
		FName ID;
		FString Label;
		FString Author;
		FString URL;
		FString Description;
		FString Path;

		void DebugPrint()
		{
			GKLog4("Mod", ID.ToString(), Label, Author, URL);
			GKLog2("Path", Description, Path);
		}
	};

	// Core data is never removed so we don't need to re-parse
	GKXMLCollection CollectionCore;

	void Clear();

	void GetModList(TArray<ModInfo>& Mods);
	void GetModFiles(TArray<FString>& Files, const FString& ModDirName, const FString& ModSubDirName);

	void LoadMod(GKXMLCollection& xmlCollection, const FString& ModDirName);

	void DebugPrint(const FString& prn, const TArray<FString>& Files);

	TArray<ModInfo> ArrayMods;
};
