// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class FXmlNode;

/**
 * Relative path: https://forums.unrealengine.com/t/data-files-and-paths/62558
 * Parsing XML: https://www.reddit.com/r/unrealengine/comments/eeiwej/parsing_xml/
 */
struct GKXML
{
public:
	GKXML();
	~GKXML();

	// The ID. For top XML, examples: "Mod", "Color".
	FName ID;

	// Optional content
	FString Content;

	// Optional linked list of children.
	// IDs can be same like multiple <State> in Country.
	// To inject children, go through the tree, i.e. Mod.Core.Label
	GKXML* ChildNode = nullptr;

	GKXML* NextNode = nullptr;

	// Parsing the root, the ID is in the attribute ID.
	void ParseWithID(const FXmlNode* Node);

	int32 ChildNum();

	void DebugPrint();

private:
	// Parsing a node, might create 1 or 2 tiers node.
	void Parse(const FXmlNode* Node);

	void ParseChildren(const FXmlNode* Node);
};
