// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"

struct GKXML;
class FXmlNode;

/**
 * 
 */
class GKXMLClass
{
public:
	GKXMLClass(const FName& ID);
	~GKXMLClass();

	// TypeNode must have ID
	void AddOrReplace(const FXmlNode* TypeNode);

	void DebugPrint();

private:
	FName ID;
	FString Content;

	TMap<FName, GKXML*> MapXMLTypes;		// All different types, i.e. State.Alabama, State.California

public:	// accessors
	GK_GETCR(FName, ID);
	GK_GETCR(FString, Content);
};
