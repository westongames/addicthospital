// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Database/GKXMLClass.h"

GKXMLClass::GKXMLClass(const FName& iID)
	: ID(iID)
{
}

GKXMLClass::~GKXMLClass()
{
}

void GKXMLClass::AddOrReplace(const FXmlNode* TypeNode)
{
	GKXML* xml = new GKXML();
	xml->ParseWithID(TypeNode);

	GKXML **xmlType = MapXMLTypes.Find(xml->ID);
	if (xmlType)
	{
		// For now, simply replace
		MapXMLTypes[xml->ID] = xml;
		delete* xmlType;
	}
	else
	{
		// Add it
		MapXMLTypes.Add(xml->ID, xml);
	}
}


void GKXMLClass::DebugPrint()
{
	GKLog3("  ", ID.ToString(), FString::FromInt(MapXMLTypes.Num()), Content);
	for (auto pair : MapXMLTypes)
	{
		GKXML* xml = pair.Value;
		xml->DebugPrint();
	}
}