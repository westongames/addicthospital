// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Struct/GKObject.h"
#include "Gimka/Tools/Macros/GKHeader.h"

/**
 * Has ID and index to the database's ArrayItems
 * T should derive from GKDatabaseItem because it needs both ID and Index
 */
template <typename T>
struct GKDatabaseItem : public GKObject
{
	T Index;

	// GKDatabase needs to set Index directly
	template<typename OtherT>
	friend class GKDatabase;

public:
	GKDatabaseItem(const FName& ID);
	virtual ~GKDatabaseItem();

public:	// Accessors
	GK_GET(T, Index);
};

typedef GKDatabaseItem<uint64> GKDatabaseItem64;
typedef GKDatabaseItem<uint32> GKDatabaseItem32;
typedef GKDatabaseItem<uint16> GKDatabaseItem16;
typedef GKDatabaseItem<uint8> GKDatabaseItem8;
