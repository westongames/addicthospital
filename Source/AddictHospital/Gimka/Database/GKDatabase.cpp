// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Database/GKDatabase.h"
#include "Gimka/Tools/Macros/GKLogger.h"

template <typename T>
GKDatabase<T>::GKDatabase(const FName& ID)
    : GKObject(ID)
{
}

template <typename T>
GKDatabase<T>::~GKDatabase()
{
}

template<typename T>
bool GKDatabase<T>::Init(GKGameManager* GameManager)
{
    GKLog("GKDatabase::Init");
    if (IsInitialized) return true;          // It's already initialized
    if (!CanInitialize()) return false;     // Not ready to initialize. Init dependent databases first!
    OnParseData(GameManager);
    IsInitialized = true;
    return true;
}

template<typename T>
void GKDatabase<T>::Reserve(int32 capacity)
{
    ArrayItems.Reserve(capacity);
    ArrayIDs.Reserve(capacity);
}

template<typename T>
const T& GKDatabase<T>::operator[](int32 index) const
{
    return ArrayItems[index];
}

template<typename T>
const T& GKDatabase<T>::operator[](const FName& iID) const
{
    return ArrayItems[ArrayIDs[iID]];
}

template<typename T>
void GKDatabase<T>::Add(const T& item)
{
    check(!ArrayIDs.Contains(item.GetID()));
    int32 num = ArrayItems.Num();
    //item.Index = ArrayItems.Num();
    ArrayIDs.Add(item.GetID(), num);
    ArrayItems.Add(item);
    ArrayItems[num].Index = num;


    //GKLog1("Add Item ", item.GetID().ToString());
    //GKLog1("  Item added ", ArrayItems[ArrayItems.Num() - 1].GetID().ToString());
}

template<typename T>
int32 GKDatabase<T>::Find(const FName& iID) const
{
    return ArrayIDs[iID];
}

template<typename T>
int32 GKDatabase<T>::TryFind(const FName& ID) const
{
    int32* Value = ArrayIDs.Find(ID);
    return Value ? *Value : -1;
}

template<typename T>
const FName& GKDatabase<T>::GetItemID(int32 index) const
{
    // TODO: insert return statement here
    return ArrayItems[index].GetID();
}

template<typename T>
int32 GKDatabase<T>::Num() const
{
    return ArrayItems.Num();
}

template<typename T>
void GKDatabase<T>::Log(GKGameManager* GameManager) const
{
    //UE_LOG(LogTemp, Warning, TEXT("Database %s num %d"), &(GetID().ToString()), Num());
    GKLog1("Database", ID.ToString());
    for (int i = 0; i < Num(); ++i)
    {
        ArrayItems[i].Log(GameManager);
    }
}
