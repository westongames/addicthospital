// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Database/GKXML.h"
#include "XmlParser.h"

GKXML::GKXML()
{
}

GKXML::~GKXML()
{
	while (ChildNode)
	{
		GKXML* Next = ChildNode->NextNode;
		delete ChildNode;
		ChildNode = Next;
	}
}

void GKXML::ParseWithID(const FXmlNode* Node)
{
	ID = FName(Node->GetAttribute("id"));
	check(ID.GetStringLength() > 0);

	Content = Node->GetContent();

	ParseChildren(Node);
}

//
//GKXML* GKXML::Parse(FXmlNode* Node)
//{
//	return nullptr;
//}
//
//

void GKXML::ParseChildren(const FXmlNode* Node)
{
	const TArray<FXmlNode*>& ChildrenNodes = Node->GetChildrenNodes();
	for (int i = ChildrenNodes.Num() - 1; i >= 0; --i)
	{
		GKXML* xml = new GKXML();
		xml->Parse(ChildrenNodes[i]);
		xml->NextNode = ChildNode;
		ChildNode = xml;
	}
}

void GKXML::Parse(const FXmlNode* Node)
{
	ID = FName(Node->GetTag());
	check(ID.GetStringLength() > 0);

	// If has attribute ID then create 2 tiers, otherwise only 1 tier
	FString AttribID = Node->GetAttribute("id");

	if (AttribID.Len() > 0)
	{
		// It has attribute ID so create 2 tiers
		ChildNode = new GKXML();
		ChildNode->ID = FName(ID);
		ChildNode->Content = Node->GetContent();
		ChildNode->ParseChildren(Node);
	}
	else
	{
		// No ID, create 1 tier
		Content = Node->GetContent();
		ParseChildren(Node);
	}
}

int32 GKXML::ChildNum()
{
	int32 count = 0;
	GKXML* child = ChildNode;
	while (child)
	{
		++count;
		child = child->NextNode;
	}
	return count;
}


void GKXML::DebugPrint()
{
	GKLog3("    ", ID.ToString(), FString::FromInt(ChildNum()), Content);
}
