// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Database/GKXMLCollection.h"
#include "GKXML.h"
#include "GKXMLClass.h"
#include "XmlParser.h"

GKXMLCollection::GKXMLCollection()
{
}

GKXMLCollection::~GKXMLCollection()
{
}

void GKXMLCollection::Add(FXmlNode* RootNode)
{
	TArray<FXmlNode*> ChildNodes = RootNode->GetChildrenNodes();
	if (ChildNodes.Num() == 0) return;		// Nothing to add

	const FXmlNode* TypeNode = RootNode->GetFirstChildNode();
	FName ClassID = FName(TypeNode->GetTag());
	check(ClassID.GetStringLength() > 0);

	GKXMLClass** xmlClass = MapXMLClass.Find(ClassID);
	while (true)
	{
		if (xmlClass)
		{
			// Add the new type to existing class
			(*xmlClass)->AddOrReplace(TypeNode);
		}
		else
		{
			// Create a new class to contain the type!
			// Note Node->GetContent is for the subclass.
			GKXMLClass* newXMLClass = new GKXMLClass(ClassID);
			MapXMLClass.Add(ClassID, newXMLClass);
			newXMLClass->AddOrReplace(TypeNode);
		}

		TypeNode = TypeNode->GetNextNode();
		if (!TypeNode) break;		// We're done!

		ClassID = FName(TypeNode->GetTag());
		check(ClassID.GetStringLength() > 0);
		if (!xmlClass || ((*xmlClass)->GetID() != ClassID))
		{
			xmlClass = MapXMLClass.Find(ClassID);
		}
	}



//	const FString& tag = RootNode->GetTag();
//	check(tag.Len() > 0);
//	FName ClassID = FName(tag);
//	
//	GKXMLClass** xmlClass = MapXMLClass.Find(ClassID);
//	if (xmlClass)
//	{
//		// Add the new type to existing class
//		(*xmlClass)->AddOrReplace(RootNode);
//	} else
//	{
//		// Create a new class to contain the type!
//		// Note RootNode->GetContent is for the subclass.
//		GKXMLClass* newXMLClass = new GKXMLClass(ClassID);
//		MapXMLClass.Add(ClassID, newXMLClass);
//		newXMLClass->AddOrReplace(RootNode);
//	}
}

void GKXMLCollection::Add(const GKXMLCollection& other)
{
}

void GKXMLCollection::DebugPrint()
{
	//GKLog1("XMLCollection", ID);
	GKLog1("XMLCollection", MapXMLClass.Num());
	for (auto pair : MapXMLClass)
	{
		GKXMLClass* xmlClass = pair.Value;
		xmlClass->DebugPrint();
	}
}