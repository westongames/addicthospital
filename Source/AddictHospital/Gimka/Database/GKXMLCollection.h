// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "XmlParser.h"

struct GKXML;

/**
 * 
 */
class GKXMLCollection
{
public:
	GKXMLCollection();
	~GKXMLCollection();

	void Add(FXmlNode* RootNode);

	// Add and replace if same ID
	void Add(const GKXMLCollection& other);

	void DebugPrint();


private:
	// The XML classes from Tag, i.e. Color, State, BodyPart.
	// Each will have Map of subTypes
	TMap<FName, GKXMLClass*> MapXMLClass;
};
