// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"

class GKGameManager;
/**
 * 
 */
template <typename T>
class GKDatabase : public GKObject
{
	TArray<T> ArrayItems;
	TMap<FName, int32> ArrayIDs;				// ID to index to ArrayItems
	bool IsInitialized = false;					// true if it has been initialized!

public:
	GKDatabase(const FName& ID);
	virtual ~GKDatabase();

	// Return false if dependent databases have not been loaded.
	// GameManager will retry Init again later.
	virtual bool Init(GKGameManager* GameManager) override;

	FORCEINLINE const T& operator[](int32 index) const;
	FORCEINLINE const T& operator[](const FName& ID) const;

	FORCEINLINE void Reserve(int32 capacity);
	FORCEINLINE int32 Find(const FName& ID) const;
	FORCEINLINE int32 TryFind(const FName& ID) const;
	FORCEINLINE const FName& GetItemID(int32 index) const;
	FORCEINLINE int32 Num() const;

	virtual void Log(GKGameManager* GameManager) const;

protected:
	void Add(const T& item);
	virtual bool CanInitialize() { return true; }
	virtual void OnParseData(GKGameManager* GameManager) {}

public: // accessor
	GK_GET(bool, IsInitialized);
};
