// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Database/GKXMLManager.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "XmlParser.h"

GKXMLManager::GKXMLManager()
{
}

GKXMLManager::~GKXMLManager()
{
}

void GKXMLManager::Init()
{
	GKLog("GKXMLManager::Init");
	GetModList(ArrayMods);
	GKLog1("Mod found", ArrayMods.Num());
	for (int i = 0; i < ArrayMods.Num(); ++i)
	{
		GKLog1("Mod ", ArrayMods[i].Path);
	}
	GKLog1("Mod ", ArrayMods[0].Path);

	LoadMod(CollectionCore, "Core");
	//DebugPrint("ArrayMods", ArrayMods);

	/*TArray<FString> Files;
	GetModFiles(Files, "Core");
	DebugPrint("ArrayFiles", Files);*/
}

// https://unrealcommunity.wiki/file-and-folder-management-create-find-delete-et2g64gx
// https://www.fatalerrors.org/a/text-file-profile-json-file-xml-file-read-and-write-in-ue4.html
// https://unrealcommunity.wiki/file-and-folder-management-create-find-delete-et2g64gx
// GameAgnosticSavedDir: "C:/Users/pabri/AppData/Local/UnrealEngine/4.27/Saved/"
// GameDevelopersDir: "../../../../../Google Drive/Projects/AddictHospital/Content/Developers/"
// GameSourceDir: "../../../../../Google Drive/Projects/AddictHospital/Source/"
// GameUserDeveloperDir: "../../../../../Google Drive/Projects/AddictHospital/Content/Developers/pabri/"
// GameUserDeveloperFolderName: "pabri"
// ProjectContentDir: "../../../../../Google Drive/Projects/AddictHospital/Content/"
// EngineContentDir: "../../../Engine/Content/"
void GKXMLManager::ParseAllXMLs()
{
	GKLog("ParseAllXMLs");
	//GKLog1("ProjectContentDir", FPaths::ProjectContentDir());
	//GKLog1("EngineContentDir", FPaths::EngineContentDir());
	//GKLog1("GameUserDeveloperDir", FPaths::GameUserDeveloperDir());
	//GKLog1("GameUserDeveloperFolderName", FPaths::GameUserDeveloperFolderName());

	//FString Path = FPaths::ProjectContentDir()+"Mods/Core/";
	//GKLog1("Path", Path);
	//FPaths::NormalizeDirectoryName(Path);
	//GKLog1("Path after normalize", Path);
	////Path = FPaths::ConvertRelativePathToFull(Path);
	////GKLog1("Path converted", Path);

	//TArray<FString> Files;
	//IFileManager& FileManager = IFileManager::Get();
	////FileManager.FindFiles(Files, *Path, NULL);

	//// This gets all folders recursively
	////FileManager.FindFilesRecursive(Files, *Path, TEXT("*"), false, true, true);

	//FileManager.FindFiles(Files, *Path, false, true);

	////FileManager.FindFilesRecursive(Files, *Path, TEXT("*.xml"), true, false, true);


	//GKLog1("Files found", Files.Num());
	//for (int i = 0; i < Files.Num(); ++i)
	//{
	//	GKLog1("File found: ", Files[i]);
	//}
}

struct DirectoryVisitor : public IPlatformFile::FDirectoryVisitor
{
	TArray<FString> Files;

	// This function is called for every file or directory it finds.
	bool Visit(const TCHAR* FilenameOrDirectory, bool bIsDirectory) override
	{
		// Did we find a Directory or a File?
		if (bIsDirectory)
		{
			Files.Add(FilenameOrDirectory);
		}
		//else
		//{
		//	GKLog1("File", FString(FilenameOrDirectory));
		//}
		return true;
	}
};

void GKXMLManager::GetModList(TArray<ModInfo>& Mods)
{
	FString path = FPaths::ProjectModsDir();// +"*";// +"Mods/*";
	IPlatformFile& FileManager = FPlatformFileManager::Get().GetPlatformFile();
	DirectoryVisitor Visitor;
if (FileManager.IterateDirectory(*path, Visitor))
{
	const TArray<FString>& Files = Visitor.Files;
	GKLog1("We found", Files.Num());
	Mods.Reserve(Files.Num());
	for (int i = 0, count = Files.Num(); i < count; ++i)
	{
		ModInfo modInfo;
		FString filePath = modInfo.Path = Files[i];

		// Get mod info
		FXmlFile xml(filePath + "/About/About.xml");
		if (!xml.IsValid()) continue;
		FXmlNode* RootNode = xml.GetRootNode();;
		if (RootNode->GetTag() == "Mod")
		{
			GKLog1("RootNodeT", RootNode->GetTag());
			GKLog1("RootNodeC", RootNode->GetContent());

			FString id = RootNode->GetAttribute("id");
			GKLog1("ID = ", id);
			modInfo.ID = FName(id);

			TArray<FXmlNode*> ChildNodes = RootNode->GetChildrenNodes();
			GKLog1("ChildrenNodes", ChildNodes.Num());
			for (int j = 0; j < ChildNodes.Num(); ++j)
			{
				FXmlNode* ChildNode = ChildNodes[j];
				GKLog1("ChildNodeT", ChildNode->GetTag());
				GKLog1("ChildNodeC", ChildNode->GetContent());
				const FString& tag = ChildNode->GetTag();
				if (tag == "label")
				{
					//GKLog1("Found name: ", ChildNode->GetContent());
					modInfo.Label = ChildNode->GetContent();
				}
				else if (tag == "author")
				{
					//GKLog1("Found author: ", ChildNode->GetContent());
					modInfo.Author = ChildNode->GetContent();
				}
				else if (tag == "url")
				{
					//GKLog1("Found url: ", ChildNode->GetContent());
					modInfo.URL = ChildNode->GetContent();
				}
				else if (tag == "description")
				{
					//GKLog1("Found desc: ", ChildNode->GetContent());
					modInfo.Description = ChildNode->GetContent();
				}
				else
				{
					GKError2("Unknown tag in About.xml", filePath, tag);
				}
			}
			modInfo.DebugPrint();
			//FXmlNode* node = ChildNodes[3];
		}

		Mods.Add(modInfo);
	}
}


/* This one works too!
* But decided to use PlatformFile to follow the https://unrealcommunity.wiki/file-and-folder-management-create-find-delete-et2g64gx
  Also it doesn't need to use "*"
FString path = FPaths::ProjectModsDir()+"*";
TArray<FString> Files;
IFileManager& FileManager = IFileManager::Get();
FileManager.FindFiles(Files, *path, false, true);
GKLog1("We found", Files.Num());
Mods.Reserve(Files.Num());
for (int i = 0, count = Files.Num(); i < count; ++i)
{
	ModInfo modInfo;
	modInfo.Path = Files[i];
	Mods.Add(modInfo);
}*/
}

void GKXMLManager::GetModFiles(TArray<FString>& Files, const FString& ModDirName, const FString& ModSubDirName)
{
	FString path = FPaths::ProjectModsDir() + ModDirName + "/" + ModSubDirName;// "Mods/" + Mod;
	//IPlatformFile& FileManager = FPlatformFileManager::Get().GetPlatformFile();
	IFileManager& FileManager = IFileManager::Get();
	Files.Empty();
	FileManager.FindFilesRecursive(Files, *path, TEXT("*.xml"), true, false, true);
}

void GKXMLManager::LoadMod(GKXMLCollection& xmlCollection, const FString& ModDirName)
{
	TArray<FString> Files;
	GetModFiles(Files, ModDirName, "Add");
	DebugPrint("FilesAdd", Files);
	for (const FString& file : Files)
	{
		FXmlFile xml(file);
		if (!xml.IsValid())
		{
			GKError1("Invalid XML", file);
			continue;
		}
		FXmlNode* RootNode = xml.GetRootNode();;
		GKLog1("RootNodeT", RootNode->GetTag());
		GKLog1("RootNodeC", RootNode->GetContent());
		TArray<FXmlNode*> ChildNodes = RootNode->GetChildrenNodes();
		GKLog1("ChildrenNodes", ChildNodes.Num());

		xmlCollection.Add(RootNode);

		/*const FXmlNode* NextNode = RootNode->GetNextNode();
		GKLog1("NextNodeT", NextNode->GetTag());
		GKLog1("NextNodeC", NextNode->GetContent());
		ChildNodes = NextNode->GetChildrenNodes();
		GKLog1("ChildrenNodes", ChildNodes.Num());*/

	}
	xmlCollection.DebugPrint();

}


void GKXMLManager::DebugPrint(const FString& prn, const TArray<FString>& Files)
{
	GKLog1("Files", prn);
	GKLog1("Files Found", Files.Num());
	for (int i = 0; i < Files.Num(); ++i)
	{
		GKLog1("File found: ", Files[i]);
	}
}


void GKXMLManager::OnDestroy()
{
	Clear();
}

void GKXMLManager::Clear()
{

}