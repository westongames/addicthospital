// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Math/GKIntVector.h"
#include "Gimka/System/GKManager.h"
#include "Gimka/Entity/Entity/GKVisualEntity.h"

//class GKEntityManager;

/**
 * 
 */
class GKVisualManager : public GKManager
{
	//GK_SINGLETON(GKVisualManager)
public:

	GKVisualManager();
	virtual ~GKVisualManager();

	void Initialize(GKGameEngine* GameEngine);

	// Has an array to search sprite based on GKEntity ID.
	// Go through all nearby GKRegions's entities and create or update its sprite.
	// Go through all sprites, if it's not updated for a while, destroy it.

	virtual void OnUpdateGame(float DeltaTime) override;

protected:
	virtual int CreateActor(const GKVisualEntity& VisualEntity) { return 0; }
	virtual void SetActorPosition(int Actor, const FVector& Position) {}

private:
	GKGameEngine* GameEngine;			// Ref back to Game Engine

	//TArray<GKEntityManager*> TempArrayEntityManagers;
};
