// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Bits/GKBits.h"
#include "Gimka/Tools/Macros/GKHeader.h"

class GKVisualEntity;

/**
 * 
 */
struct GKSpriteInfo
{
public:
	//friend class GKVisualManager;

	enum Dirty 
	{ 
		CreateActor,
		DestroyActor,
		Position, 
		Rotation, 
		Animation 
	};
	GKSpriteInfo();
	~GKSpriteInfo();

	inline void Initialize(GKVisualEntity* iOwner)
	{
		this->Owner = iOwner;
	}

	inline void SetDirtyPosition();
	inline void SetDirtyRotation();
	inline void SetDirtyAnimation();

	void SetVisible(bool visible);

	void SetActor(int iActor);
	inline void ResetDirtyBits() { DirtyBits.Reset();  }

private:
	const int32 ActorNone = -1;

	GKVisualEntity* Owner;			// Ref back to the entity itself
	GKBits16 DirtyBits;
	int32 Actor = ActorNone;					// The actor index or -1 if doesn't have actor yet (i.e. just created or far from camera)
									
									//GKSpriteInfo* Next, * Prev;
public:	// Accessors
	GK_GETCR(GKBits16, DirtyBits);
	GK_GET(int32, Actor);
};
