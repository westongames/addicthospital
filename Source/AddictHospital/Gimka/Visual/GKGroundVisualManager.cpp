// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Visual/GKGroundVisualManager.h"
//#include "GKVisualManager.h"
#include "Gimka/Game/GKWorld.h"
#include "Gimka/WorldVoxel/GKChunk.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/Tools/Macros/GKlogger.h"

GKGroundVisualManager::GKGroundVisualManager()
	: GKManager(555)
{
}

GKGroundVisualManager::~GKGroundVisualManager()
{
}

void GKGroundVisualManager::Initialize(GKGameEngine* iGameEngine)
{
	this->GameEngine = iGameEngine;
}

void GKGroundVisualManager::OnUpdateGame(float DeltaTime)
{
	// 2. Go through all nearby regions and process those ArrayVisualGroundDirty. For now just do all.
	const TArray<GKEntityManager*>& ArrayEntityManagers = GameEngine->GetGameManager()->GetWorld()->GetArrayEntityManagers();
	for (GKEntityManager* EntityManager : ArrayEntityManagers)
	{
		GKChunk* Chunk = (GKChunk*)EntityManager;
		if (Chunk->GetChunkVisibility() != ChunkVisibility::Off)
		{
			GKGroundSpriteInfo& Sprite = Chunk->GetSpriteInfo();
			if (!Sprite.IsDirty()) continue;

			const GKBits16& DirtyBits = Sprite.GetDirtyBits();

			// 1. Process CreateActor
			if (DirtyBits.IsTrue(GKGroundSpriteInfo::CreateActor))
			{
				int Actor = CreateChunkActor(*Chunk);
				Sprite.SetActor(Actor);

				GKLog2("Create GroundSprite", Chunk->GetChunkPos().X, Chunk->GetChunkPos().Y);

				// Note: Sprite.SetActor will call DirtyBits.Clear();
				continue;
			}

			// 2. Process redraw
			if (DirtyBits.IsTrue(GKGroundSpriteInfo::UpdateActor))
			{

			}

			Sprite.ResetDirtyBits();
			//if (SpriteInfo.)

			//{
			//	
			//	// Check groundSpriteInfo and do it
			//	//GKLog2("Render")
			//}
			// It's visible or close
			//const TArray<GKVisualEntity*> ArrayDirtyVisualEntities = EntityManager->GetArrayDirtyVisualEntities();
			//for (GKVisualEntity* VisualEntity : ArrayDirtyVisualEntities)
			//{
			//	GKSpriteInfo& Sprite = VisualEntity->GetSpriteInfo();

			//	// 1. Process CreateActor
			//	if (Sprite.DirtyBits.IsTrue(GKSpriteInfo::CreateActor))
			//	{
			//		int Actor = CreateActor(*VisualEntity);
			//		Sprite.SetActor(Actor);

			//		// Note: Sprite.SetActor will call DirtyBits.Clear
			//		continue;
			//	}

			//	// 2. Process change to Position, Rotation, Animation
			//	if (Sprite.DirtyBits.IsTrue(GKSpriteInfo::Position))
			//	{
			//		SetActorPosition(Sprite.Actor, VisualEntity->GetPosition());
			//	}

			//	// No need to process further. We assume the new Position, Rotation, Animation has been set correctly.
			//	Sprite.DirtyBits.Reset();


			//	/*
			//	FVector Pos = VisualEntity->GetPosition();
			//	Pos.Y += 2000.5f;
			//	VisualEntity->SetPosition(Pos);
			//	GKLog1("VisualEntity Position is now ", Pos.Y);*/
			//}
			//EntityManager->ClearArrayDirtyVisualEntities();
		}
	}
}
