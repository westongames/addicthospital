// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/System/GKManager.h"

//#include "Gimka/WorldVoxel/GKChunk.h"

class GKChunk;

/**
 * 
 */
class GKGroundVisualManager : public GKManager
{
public:
	// ChunkVisibility status: 
	enum ChunkVisibility
	{
		Off,		// Not close to camera, destroy any rendered regionMesh and actors.
		Close,		// Close to a region that is visible, no changes (keep regionMesh and actors rendered or not rendered)
		On,			// Close to camera, regionMesh and actors will be rendered.
	};

	GKGroundVisualManager();
	virtual ~GKGroundVisualManager();

	void Initialize(GKGameEngine* GameEngine);

	virtual void OnUpdateGame(float DeltaTime) override;

	// Will be called in GKRegion::OnJobPostUpdate (so after the camera moved)
	ChunkVisibility CalculateChunkVisibility(const FIntPoint& regionPos) const
	{
		// For now just return Yes
		return ChunkVisibility::On;
	}

protected:
	virtual int CreateChunkActor(const GKChunk& Chunk) { return 0; }

private:
	GKGameEngine* GameEngine;				// Ref back to Game Engine
};
