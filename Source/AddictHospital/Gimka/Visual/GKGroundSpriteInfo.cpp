// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Visual/GKGroundSpriteInfo.h"

GKGroundSpriteInfo::GKGroundSpriteInfo()
{
}

GKGroundSpriteInfo::~GKGroundSpriteInfo()
{
}

inline void GKGroundSpriteInfo::SetDirtyUpdateActor()
{
	/*if (Actor != ActorNone && DirtyBits.AreAllFalse())
	{
	}*/
	DirtyBits.SetTrue(Dirty::UpdateActor);
}

void GKGroundSpriteInfo::SetVisible(bool visible)
{
	// Notes: multiple gameTicks can be called per frame.
	// This GroundSpriteInfo might already been scheduled for CreateActor for make sure to avoid duplicate by using Dirty::CreateActor
	if (visible)
	{
		if (Actor == ActorNone && DirtyBits.SetBitTrue(Dirty::CreateActor))
		{
			// Schedule to create actor. Nothing?
		}
	}
	else
	{
		if (Actor != ActorNone && DirtyBits.SetBitTrue(Dirty::DestroyActor))
		{

		}
	}
}

void GKGroundSpriteInfo::SetActor(int iActor)
{
	// Actor has been set by GKGroundVisualManager. Reset DirtyBits because everything has just been created!
	Actor = iActor;
	DirtyBits.Reset();
}
