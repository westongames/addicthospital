// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Bits/GKBits.h"
#include "Gimka/Tools/Macros/GKHeader.h"

class GKChunk;

/**
 * 
 */
struct GKGroundSpriteInfo
{
public:
	enum Dirty
	{
		CreateActor,
		UpdateActor,
		DestroyActor
	};
	GKGroundSpriteInfo();
	~GKGroundSpriteInfo();

	inline void Initialize(GKChunk* iOwner)
	{
		this->Owner = iOwner;
	}

	inline void SetDirtyUpdateActor();

	inline bool IsDirty() { return DirtyBits.IsAnyTrue(); }

	void SetVisible(bool visible);
	void SetActor(int iActor);
	inline void ResetDirtyBits() { DirtyBits.Reset(); }

private:
	const int32 ActorNone = -1;

	GKChunk* Owner;					// Ref back to the chunk itself
	GKBits16 DirtyBits;
	int32 Actor = ActorNone;			// The actor index or -1 if doesn't have actor yet (i.e. just created or far from camera)

public:		// Accessors
	GK_GETCR(GKBits16, DirtyBits);
	GK_GET(int32, Actor);
};
