// Fill out your copyright notice in the Description page of Project Settings.


#include "GKSpriteInfo.h"
#include "Gimka/Tools/Macros/GKLogger.h"

GKSpriteInfo::GKSpriteInfo()
{
}

GKSpriteInfo::~GKSpriteInfo()
{
}

void GKSpriteInfo::SetDirtyPosition() 
{ 
	if (Actor != ActorNone && DirtyBits.AreAllFalse())
	{
		Owner->ScheduleForVisualUpdate();
	}
	DirtyBits.SetTrue(Dirty::Position);
}

void GKSpriteInfo::SetDirtyRotation() 
{ 
	if (Actor != ActorNone && DirtyBits.AreAllFalse())
	{
		Owner->ScheduleForVisualUpdate();
	}
	DirtyBits.SetTrue(Dirty::Rotation);
}

void GKSpriteInfo::SetDirtyAnimation() 
{ 
	if (Actor != ActorNone && DirtyBits.AreAllFalse())
	{
		Owner->ScheduleForVisualUpdate();
	}
	DirtyBits.SetTrue(Dirty::Animation);
}

void GKSpriteInfo::SetActor(int iActor)
{
	// Actor has been set by GKVisualManager. Reset DirtyBits because everything has just been created!
	Actor = iActor;
	DirtyBits.Reset();
}

void GKSpriteInfo::SetVisible(bool visible)
{
	// Notes: multiple gameTicks can be called per frame. 
	// This SpriteInfo might already been scheduled for CreateActor so make sure to avoid duplicate by using Dirty::CreateActor)
	if (visible)
	{
		if (Actor == ActorNone && DirtyBits.SetBitTrue(Dirty::CreateActor))
		{
			// Schedule to create actor. 
			GKLog("schedule to create actor");
			Owner->ScheduleForVisualUpdate();
		}
		else { GKLog("No schedule to create actor"); }
	}
	else
	{
		if (Actor != ActorNone && DirtyBits.SetBitTrue(Dirty::DestroyActor))
		{
			// TODO: send message to destroyActor
			//Owner->ScheduleForVisualUpdate();
		}
	}
}


