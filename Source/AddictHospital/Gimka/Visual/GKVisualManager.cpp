// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/Visual/GKVisualManager.h"
#include "GKGroundVisualManager.h"
#include "Gimka/Math/GKIntVector.h"
#include "Gimka/Game/GKWorld.h"
#include "Gimka/WorldVoxel/GKChunk.h"
#include "Gimka/Entity/Entity/GKVisualEntity.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/Tools/Macros/GKHeader.h"

//GKVisualManager* GKVisualManager::Instance;

GKVisualManager::GKVisualManager()
	: GKManager(323)
{
}

GKVisualManager::~GKVisualManager()
{
}

void GKVisualManager::Initialize(GKGameEngine* iGameEngine)
{
	this->GameEngine = iGameEngine;
}


void GKVisualManager::OnUpdateGame(float DeltaTime)
{
	// 1. Process message, i.e. DeleteActor from GKRegion

	// 2. Go through all nearby regions and process those ArrayVisualEntityDirty. For now just do all.
	const TArray<GKEntityManager*>& ArrayEntityManagers = GameEngine->GetGameManager()->GetWorld()->GetArrayEntityManagers();
	for (GKEntityManager* EntityManager : ArrayEntityManagers)
	{
		GKChunk* Chunk = (GKChunk*)EntityManager;
		// TODO: not correct because to destroy entity the chunkvibility will be off
		if (Chunk->GetChunkVisibility() != GKGroundVisualManager::ChunkVisibility::Off)
		{
			// It's visible or close
			const TArray<GKVisualEntity*> ArrayDirtyVisualEntities = EntityManager->GetArrayDirtyVisualEntities();
			for (GKVisualEntity* VisualEntity : ArrayDirtyVisualEntities)
			{
				GKSpriteInfo& Sprite = VisualEntity->GetSpriteInfo();
				const GKBits16& DirtyBits = Sprite.GetDirtyBits();

				// 1. Process CreateActor
				if (DirtyBits.IsTrue(GKSpriteInfo::CreateActor))
				{
					int Actor = CreateActor(*VisualEntity);
					Sprite.SetActor(Actor);

					// Note: Sprite.SetActor will call DirtyBits.Clear
					continue;
				}

				// 2. Process change to Position, Rotation, Animation
				if (DirtyBits.IsTrue(GKSpriteInfo::Position))
				{
					SetActorPosition(Sprite.GetActor(), VisualEntity->GetPosition());
				}

				// No need to process further. We assume the new Position, Rotation, Animation has been set correctly.
				Sprite.ResetDirtyBits();


				/*
				FVector Pos = VisualEntity->GetPosition();
				Pos.Y += 2000.5f;
				VisualEntity->SetPosition(Pos);
				GKLog1("VisualEntity Position is now ", Pos.Y);*/
			}
			EntityManager->ClearArrayDirtyVisualEntities();
		}
	}

	// Todo: determine which region to update
	// Note: currently need to check all because destroyActor will need to be processed too while it's far from camera
	// For destroyActor send message to GKVisualManager
	//GKIntVector regionFrom(0, 0, 0);
	//GKIntVector regionTo(2, 2, 2);

	
	// For now do all regions, later limit to nearest only
	/*GKWorld* World = GKWorld::GetInstance();
	World->GetEntityManagersForVisualUpdate(TempArrayEntityManagers);
	GKLog1("GKVisualManager.OnUpdate", TempArrayEntityManagers.Num());
	for (GKEntityManager* EntityManager : TempArrayEntityManagers)
	{
		TArray <GKVisualEntity*> ArrayDirtyVisualEntities = EntityManager->GetArrayDirtyVisualEntities();

		if (ArrayDirtyVisualEntities.Num() > 0)
		{
			GKLog1("ArrayDirtyVisualEntities", ArrayDirtyVisualEntities.Num());
		}

		ArrayDirtyVisualEntities.Empty();
	}*/
	//for (int i = 0, count = TempArrayEntityManagers.Num(); i < count; ++i)
	//{
	//	/*GKRegion* Region = ArrayRegions[i];
	//	TArrayGKEntityVisual*/


	//}
	//GK_ITERATE_XYZ_MINMAX(int32, regionFrom.X, regionTo.X, regionFrom.Y, regionTo.Y, regionFrom.Z, regionTo.Z)
	//GK_ITERATE_XYZ(int32, 
		//GKRegion* region = word->GetRegion(x, y, z);


	//GK_ITERATE_XYZ_END
}

