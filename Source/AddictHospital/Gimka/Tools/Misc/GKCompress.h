// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Math/GKIntVector.h"

/**
 * 
 */
class GKCompress
{
public:
	GKCompress();
	~GKCompress();

	// X,Y is 12 bits (-2,048 to 2,047); Z is 8 bits (-128 to 127)
	static inline uint32 IntVectorToUInt32(const GKIntVector& Pos)
	{
		// X is 0 to 99,999
		// Y is 0 to 999,990,000

		//check(Pos.X >= -2048 && Pos.X <= 2047);
		//check(Pos.Y >= -2048 && Pos.Y <= 2047);
		//check(Pos.Z >= -128 && Pos.Z <= 127);
		return (Pos.X + 2048) + ((Pos.Y + 2048) << 12) + (((uint32)(Pos.Z + 128)) << 24);
	}

	// X,Y is 12 bits (-2,048 to 2,047); Z is 8 bits (-128 to 127)
	static inline GKIntVector UInt32ToIntVector(uint32 Pos)
	{
		GKIntVector Vector;
		Vector.X = (Pos & 4095) - 2048; Pos >>= 12;
		Vector.Y = (Pos & 4095) - 2048; Pos >>= 12;
		Vector.Z = Pos - 128;
		return Vector;
	}

	// X,Y is 16 bits
	static inline uint32 IntPointToUInt32(const FIntPoint& Pos)
	{
		// X is 0 to 99,999
		// Y is 0 to 999,990,000


		//check(Pos.X >= -32768 && Pos.X <= 32767);
		//check(Pos.Y >= -32768 && Pos.Y <= 32767);
		return (Pos.X + 32768) + (((uint32)(Pos.Y + 32768)) << 16);
	}

	// X,Y is 16 bits
	static inline FIntPoint UInt32ToIntPoint(uint32 Pos)
	{
		FIntPoint Point;
		Point.X = (Pos & 65535) - 32768; Pos >>= 16;
		Point.Y = (Pos & 65535) - 32768;
		return Point;
	}
};
