// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

struct GKIntVector;

#define GK_GET(VarType, VarName) FORCEINLINE VarType Get ## VarName () const { return VarName; }
#define GK_SET(VarType, VarName) FORCEINLINE void Set ## VarName (VarType Value) { VarName = Value; }
#define GK_GETSET(VarType, VarName) GK_GET(VarType, VarName) GK_SET(VarType, VarName)

// Const Ref
#define GK_GETCR(VarType, VarName) FORCEINLINE const VarType& Get ## VarName () const { return VarName; }
#define GK_SETCR(VarType, VarName) FORCEINLINE void Set ## VarName (const VarType& Value) { VarName = Value; }
#define GK_GETSETCR(VarType, VarName) GK_GETCR(VarType, VarName) GK_SETCR(VarType, VarName)

#define GK_GETCR_AND_PTR(VarType, VarName)				\
		GK_GETCR(VarType, VarName);						\
		FORCEINLINE const VarType* Get ## VarName ## Ptr () const { return &VarName; }

#define GK_SETCR_VIRTUAL(VarType, VarName) FORCEINLINE virtual void Set ## VarName (const VarType& Value) { VarName = Value; }
#define GK_GETSETCR_VIRTUAL(VarType, VarName) GK_GETCR(VarType, VarName) GK_SETCR_VIRTUAL(VarType, VarName)
#define GK_SETCR_OVERRIDE(VarType, VarName) FORCEINLINE virtual void Set ## VarName (const VarType& Value) override

#define GK_GETR(VarType, VarName) FORCEINLINE VarType& Get ## VarName () { return VarName; }

#define GKPTR_GET(VarType, VarName) FORCEINLINE VarType* Get ## VarName () const { return VarName; }
#define GKPTR_SET(VarType, VarName) FORCEINLINE void Set ## VarName (VarType* Value) { VarName = Value; }
#define GKPTR_GETC(VarType, VarName) FORCEINLINE const VarType* Get ## VarName () const { return VarName; }


#define GK_DEF_BIT(VarType, VarName, Metric, PartName)		\
	VarType VarName ## Bit ## Metric ## PartName;			\
	VarType VarName ## Metric ## PartName;

#define GK_DEF_BITNUM(VarType, VarName, PartName)			\
	GK_DEF_BIT(VarType, VarName, Num, PartName);

#define GK_DEF_BITNUMZ(VarType, VarName, PartName)			\
	GK_DEF_BIT(VarType, VarName, NumZ, PartName);

#define GK_DEF_BITAREA(VarType, VarName, PartName)			\
	GK_DEF_BITNUM(VarType, VarName, PartName);				\
	GK_DEF_BIT(VarType, VarName, Area, PartName);

#define GK_DEF_BITVOL(VarType, VarName, PartName)			\
	GK_DEF_BITAREA(VarType, VarName, PartName);				\
	GK_DEF_BIT(VarType, VarName, Vol, PartName);

#define GK_DEF_BITVOLZ(VarType, VarName, PartName)			\
	GK_DEF_BITAREA(VarType, VarName, PartName);				\
	GK_DEF_BITNUMZ(VarType, VarName, PartName);				\
	GK_DEF_BIT(VarType, VarName, Vol, PartName);

#define GK_GET_BIT(VarType, VarName, Metric, PartName)		\
	GK_GET(VarType, VarName ## Bit ## Metric ## PartName)	\
	GK_GET(VarType, VarName ## Metric ## PartName)

#define GK_GET_BITNUM(VarType, VarName, PartName)			\
	GK_GET_BIT(VarType, VarName, Num, PartName);

#define GK_GET_BITNUMZ(VarType, VarName, PartName)			\
	GK_GET_BIT(VarType, VarName, NumZ, PartName);

#define GK_GET_BITAREA(VarType, VarName, PartName)			\
	GK_GET_BITNUM(VarType, VarName, PartName);				\
	GK_GET_BIT(VarType, VarName, Area, PartName);

#define GK_GET_BITVOL(VarType, VarName, PartName)			\
	GK_GET_BITAREA(VarType, VarName, PartName);				\
	GK_GET_BIT(VarType, VarName, Vol, PartName);

#define GK_GET_BITVOLZ(VarType, VarName, PartName)			\
	GK_GET_BITAREA(VarType, VarName, PartName);				\
	GK_GET_BITNUMZ(VarType, VarName, PartName);				\
	GK_GET_BIT(VarType, VarName, Vol, PartName);

// Singleton has to be created explicitly via CreateInstance. It can't have the previous Instance.
#define GK_SINGLETON_NEW(VarType)									\
	public:															\
		static VarType* GetInstance()								\
		{															\
			check(Instance != nullptr);								\
			return Instance;										\
		}															\
		static bool IsInstanceAvailable()							\
		{															\
			return Instance != nullptr;								\
		}															\
		static bool TryGetInstance(VarType*& theInstance)			\
		{															\
			theInstance = Instance;									\
 			return false;											\
		}															\
		static bool DestroyInstance()								\
		{															\
			if (Instance != nullptr) { delete Instance; Instance = nullptr; return true; }			\
			return false;											\
		}															\
		static VarType* CreateInstance()							\
		{															\
			check(Instance == nullptr);								\
			Instance = new VarType();								\
			return Instance;										\
		}															\
	protected:														\
		VarType();													\
		virtual ~VarType();											\
	private:														\
		static VarType* Instance;


#define GK_SINGLETON(VarType)										\
	public:															\
		static VarType* GetInstance()								\
		{															\
			if (Instance == nullptr) { Instance = new VarType(); }	\
			return Instance;										\
		}															\
		static bool IsInstanceAvailable()							\
		{															\
			return Instance != nullptr;								\
		}															\
		static bool TryGetInstance(VarType*& theInstance)			\
		{															\
			theInstance = Instance;									\
 			return false;											\
		}															\
		static bool DestroyInstance()								\
		{															\
			if (Instance != nullptr) { delete Instance; Instance = nullptr; return true; }			\
			return false;											\
		}															\
	protected:														\
		VarType();													\
		virtual ~VarType();											\
	private:														\
		static VarType* Instance;


//#define GKPROPERTY_PTR_GET(varType, varName, category)		\
//	private:											\
//		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = category, meta = (AllowPrivateAccess = "true"))				\
//		class varType* varName;				\
//	public:									\
//		FORCEINLINE varType* Get ## varName () const { return varName; }

//#define GKPROPERTY_PTR_GET(varType, varName, category)		\
//		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = category, meta = (AllowPrivateAccess = "true"))				\
//		class varType* varName;				\
//	public:									\
//		FORCEINLINE varType* Get ## varName () const { return varName; }

#define GKSINGLETON_ACTOR(varType)									\
	public:															\
		static varType* GetInstance()								\
		{															\
			check(Instance);										\
			return Instance;										\
		}															\
	private:														\
		static varType* Instance;