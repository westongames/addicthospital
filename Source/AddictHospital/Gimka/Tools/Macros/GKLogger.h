// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/Engine.h"


// Can't add the 4th VarType for instance+line. Seems it's the macro limit?
#define logger(LogType, VarType, Format)																					\
	static void Log ## LogType(const char* text, VarType Val1) { UE_LOG(LogTemp, LogType,									\
		TEXT("%s: " # Format), *FString(text), Val1); }																		\
	static void Log ## LogType(const char* text, VarType Val1, VarType Val2) { UE_LOG(LogTemp, LogType,						\
		TEXT("%s: " # Format ", " Format), *FString(text), Val1, Val2); }													\
	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3) { UE_LOG(LogTemp, LogType,		\
		TEXT("%s: " # Format ", " Format ", " Format), *FString(text), Val1, Val2, Val3); }									\
	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4) { UE_LOG(LogTemp, LogType,	\
		TEXT("%s: " # Format ", " Format ", " Format ", " Format), *FString(text), Val1, Val2, Val3, Val4); }				
//\
//	static void Log(const char* text, VarType Val1, const FString& instance, int line) { UE_LOG(LogTemp, Warning,			\
//		TEXT("%s(%d)\t\t%s: " # Format), *instance, line, *FString(text), Val1); }											\
//	static void Log(const char* text, VarType Val1, VarType Val2, const FString& instance, int line)						\
//		{ UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: " # Format ", " Format),												\
//		*instance, line, *FString(text), Val1, Val2); }																		\
//	static void Log(const char* text, VarType Val1, VarType Val2, VarType Val3, const FString& instance, int line)			\
//		{ UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: " # Format ", " Format ", " Format),									\
//		*instance, line, *FString(text), Val1, Val2, Val3); }

#define Logger(LogType, VarType, Format)																					\
	static void Log ## LogType ## Info(const char* text, VarType Val1, const FString& info) { UE_LOG(LogTemp, LogType,				\
		TEXT("%s %s: " # Format), *info, *FString(text), Val1); }													\
	static void Log ## LogType ## Info(const char* text, VarType Val1, VarType Val2, const FString& info) { UE_LOG(LogTemp, LogType,								\
		TEXT("%s %s: " # Format ", " Format), *info, *FString(text), Val1, Val2); }													\
	static void Log ## LogType ## Info(const char* text, VarType Val1, VarType Val2, VarType Val3, const FString& info) { UE_LOG(LogTemp, LogType,					\
		TEXT("%s %s: " # Format ", " Format ", " Format), *info, *FString(text), Val1, Val2, Val3); }									\
	static void Log ## LogType ## Info(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4, const FString& info) { UE_LOG(LogTemp, LogType,	\
		TEXT("%s %s: " # Format ", " Format ", " Format ", " Format), *info, *FString(text), Val1, Val2, Val3, Val4); }				


#define screen(ScrType, VarType, Format, time, color)																			\
	static void Screen ## ScrType(const char* text, VarType Val1) { if (Engine) Engine->AddOnScreenDebugMessage(INDEX_NONE,		\
		time, FColor::color, FString::Printf(TEXT("%s: " # Format), *FString(text), Val1)); }									\
	static void Screen ## ScrType(const char* text, VarType Val1, VarType Val2) { if (Engine) Engine->AddOnScreenDebugMessage(	\
		INDEX_NONE, time, FColor::color, FString::Printf(TEXT("%s: " # Format ", " # Format), *FString(text), Val1, Val2)); }	\
	static void Screen ## ScrType(const char* text, VarType Val1, VarType Val2, VarType Val3) { if (Engine)						\
		Engine->AddOnScreenDebugMessage(INDEX_NONE, time, FColor::color, FString::Printf(										\
		TEXT("%s: " # Format ", " # Format ", " # Format), *FString(text), Val1, Val2, Val3)); }								\
	static void Screen ## ScrType(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4) { if (Engine)		\
		Engine->AddOnScreenDebugMessage(INDEX_NONE, time, FColor::color, FString::Printf(										\
		TEXT("%s: " # Format ", " # Format ", " # Format ", " # Format), *FString(text), Val1, Val2, Val3, Val4)); }


#define loggerFString(LogType, VarType, Format)																					\
	static void Log ## LogType(const char* text, VarType Val1) { UE_LOG(LogTemp, LogType,									\
		TEXT("%s: " # Format), *FString(text), *Val1); }																		\
	static void Log ## LogType(const char* text, VarType Val1, VarType Val2) { UE_LOG(LogTemp, LogType,						\
		TEXT("%s: " # Format ", " Format), *FString(text), *Val1, *Val2); }													\
	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3) { UE_LOG(LogTemp, LogType,		\
		TEXT("%s: " # Format ", " Format ", " Format), *FString(text), *Val1, *Val2, *Val3); }									\
	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4) { UE_LOG(LogTemp, LogType,	\
		TEXT("%s: " # Format ", " Format ", " Format ", " Format), *FString(text), *Val1, *Val2, *Val3, *Val4); }				

#define LoggerFString(LogType, VarType, Format)																					\
	static void Log ## LogType ## Info(const char* text, VarType Val1, const FString& info) { UE_LOG(LogTemp, LogType,				\
		TEXT("%s %s: " # Format), *info, *FString(text), *Val1); }													\
	static void Log ## LogType ## Info(const char* text, VarType Val1, VarType Val2, const FString& info) { UE_LOG(LogTemp, LogType,								\
		TEXT("%s %s: " # Format ", " Format), *info, *FString(text), *Val1, *Val2); }													\
	static void Log ## LogType ## Info(const char* text, VarType Val1, VarType Val2, VarType Val3, const FString& info) { UE_LOG(LogTemp, LogType,					\
		TEXT("%s %s: " # Format ", " Format ", " Format), *info, *FString(text), *Val1, *Val2, *Val3); }									\
	static void Log ## LogType ## Info(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4, const FString& info) { UE_LOG(LogTemp, LogType,	\
		TEXT("%s %s: " # Format ", " Format ", " Format ", " Format), *info, *FString(text), *Val1, *Val2, *Val3, *Val4); }				

#define screenFString(ScrType, VarType, Format, time, color)																			\
	static void Screen ## ScrType(const char* text, VarType Val1) { if (Engine) Engine->AddOnScreenDebugMessage(INDEX_NONE,		\
		time, FColor::color, FString::Printf(TEXT("%s: " # Format), *FString(text), *Val1)); }									\
	static void Screen ## ScrType(const char* text, VarType Val1, VarType Val2) { if (Engine) Engine->AddOnScreenDebugMessage(	\
		INDEX_NONE, time, FColor::color, FString::Printf(TEXT("%s: " # Format ", " # Format), *FString(text), *Val1, *Val2)); }	\
	static void Screen ## ScrType(const char* text, VarType Val1, VarType Val2, VarType Val3) { if (Engine)						\
		Engine->AddOnScreenDebugMessage(INDEX_NONE, time, FColor::color, FString::Printf(										\
		TEXT("%s: " # Format ", " # Format ", " # Format), *FString(text), *Val1, *Val2, *Val3)); }								\
	static void Screen ## ScrType(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4) { if (Engine)		\
		Engine->AddOnScreenDebugMessage(INDEX_NONE, time, FColor::color, FString::Printf(										\
		TEXT("%s: " # Format ", " # Format ", " # Format ", " # Format), *FString(text), *Val1, *Val2, *Val3, *Val4)); }

//#define LoggerID(LogType, VarType, Format)																					\
//	static void Log ## LogType(const char* text, VarType Val1, const FString& info) { UE_LOG(LogTemp, LogType,				\
//		TEXT("%s %s: " # Format), *info, *FString(text), Val1); }													\
//	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, const FString& info) { UE_LOG(LogTemp, LogType,								\
//		TEXT("%s %s: " # Format ", " Format), *info, *FString(text), Val1, Val2); }													\
//	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3, const FString& info) { UE_LOG(LogTemp, LogType,					\
//		TEXT("%s %s: " # Format ", " Format ", " Format), *info, *FString(text), Val1, Val2, Val3); }									\
//	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4, const FString& info) { UE_LOG(LogTemp, LogType,	\
//		TEXT("%s %s: " # Format ", " Format ", " Format ", " Format), *info, *FString(text), Val1, Val2, Val3, Val4); }				


//#define Logger(LogType, VarType, Format)																					\
//	static void Log ## LogType(const char* text, VarType Val1, const FString& instance, int line) { UE_LOG(LogTemp, LogType,			\
//		TEXT("%s(%d)\t\t%s: " # Format), *instance, line, *FString(text), Val1); }											\
//	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, const FString& instance, int line)						\
//		{ UE_LOG(LogTemp, LogType, TEXT("%s(%d)\t\t%s: " # Format ", " Format),												\
//		* instance, line, *FString(text), Val1, Val2); }																	\
//	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3, const FString& instance, int line)			\
//		{ UE_LOG(LogTemp, LogType, TEXT("%s(%d)\t\t%s: " # Format ", " Format ", " Format),									\
//		* instance, line, *FString(text), Val1, Val2, Val3); }																\
//	static void Log ## LogType(const char* text, VarType Val1, VarType Val2, VarType Val3, VarType Val4, const FString& instance, int line)	\
//		{ UE_LOG(LogTemp, LogType, TEXT("%s(%d)\t\t%s: " # Format ", " Format ", " Format "," Format),						\
//			* instance, line, *FString(text), Val1, Val2, Val3, Val4); }



#define Logg(VarType, Format)							\
	logger(Warning, VarType, Format);					\
	logger(Error, VarType, Format);						\
	Logger(Warning, VarType, Format);					\
	Logger(Error, VarType, Format);						\
	screen(Log, VarType, Format, 5.f, Green);			\
	screen(Warn, VarType, Format, 5.f, Yellow);			\
	screen(Error, VarType, Format, 5.f, Red);

/**
/**
 * 
 */
static class GKLogger
{
public:

	// Use the GKLog instead
	static void LogWarning(const char* text) { UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(text)); }
	static void LogWarningInfo(const char* text, const FString& info) { UE_LOG(LogTemp, Warning, TEXT("%s %s"), *info, *FString(text)); }
	//static void LogWarning(const char* text, const FString& file, uint32 line) { UE_LOG(LogTemp, Warning, TEXT("%s\t\t[% s % d]"), *FString(text), *file, line); }

	static void LogError(const char* text) { UE_LOG(LogTemp, Error, TEXT("%s"), *FString(text)); }
	static void LogErrorInfo(const char* text, const FString& info) { UE_LOG(LogTemp, Error, TEXT("%s %s"), *info, *FString(text)); }
	//static void LogError(const char* text, const FString& file, uint32 line) { UE_LOG(LogTemp, Error, TEXT("%s\t\t[% s % d]"), *FString(text), *file, line); }

	static void ScreenLog(const char* text) { if (Engine) Engine->AddOnScreenDebugMessage(INDEX_NONE, 5.f, FColor::Green, FString(text)); }// , true, }
	static void ScreenWarn(const char* text) { if (Engine) Engine->AddOnScreenDebugMessage(INDEX_NONE, 5.f, FColor::Yellow, FString(text)); }// , true, }
	static void ScreenError(const char* text) { if (Engine) Engine->AddOnScreenDebugMessage(INDEX_NONE, 5.f, FColor::Red, FString(text)); }// , true, }
	
	static void Reset(UEngine* iEngine) 
	{
		Engine = iEngine;// nullptr;
		PlayerID = 0; 
		GameTick = 0; 
		//if (Engine == nullptr) LogError("Engine is nullptr");
	}
	static void SetPlayerID(uint8 iPlayerID) { /*check(PlayerID == 0);  */PlayerID = iPlayerID; }
	static void SetGameTick(uint32 Value) { GameTick = Value; }

	static uint8 GetPlayerID() { return PlayerID; }

	//static void Log(const char* text, int32 val) { UE_LOG(LogTemp, Warning, TEXT("%s: %d"), *FString(text), val); }
	//static void Log(const char* text, uint32 val) { UE_LOG(LogTemp, Warning, TEXT("%s: %d"), *FString(text), val); }
	//static void Log(const char* text, float val) { UE_LOG(LogTemp, Warning, TEXT("%s: %f"), *FString(text), val); }
	//static void Log(const char* text, double val) { UE_LOG(LogTemp, Warning, TEXT("%s: %lf"), *FString(text), val); }
	//static void Log(const char* text, char val) { UE_LOG(LogTemp, Warning, TEXT("%s: %c"), *FString(text), val); }
	//static void Log(const char* text, wchar_t val) { UE_LOG(LogTemp, Warning, TEXT("%s: %lc"), *FString(text), val); }
	//static void Log(const char* text, bool val) { UE_LOG(LogTemp, Warning, TEXT("%s: %d"), *FString(text), val); }
	//static void Log(const char* text, const char* val) { UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *FString(text), *FString(val)); }
	//static void Log(const char* text, const FString& val) { UE_LOG(LogTemp, Warning, TEXT("%s: %s"), *FString(text), *val); }
	//static void Log(const char* text, void* val) { UE_LOG(LogTemp, Warning, TEXT("%s: %d"), *FString(text), val); }

	Logg(int32, "%d");
	Logg(uint8, "%u");
	Logg(uint32, "%u");
	Logg(float, "%f");
	Logg(double, "%lf");
	Logg(char, "%c");
	Logg(wchar_t, "%lc");
	Logg(bool, "%d");
	Logg(const void*, "%d");
	//Logg(const char*, "%s"); This doesn't work. Use LoggerFString.

	loggerFString(Warning, const FString&, "%s");
	loggerFString(Error, const FString&, "%s");
	LoggerFString(Warning, const FString&, "%s");
	LoggerFString(Error, const FString&, "%s");
	screenFString(Log, const FString&, "%s", 5.f, Green);
	screenFString(Warn, const FString&, "%s", 5.f, Yellow);
	screenFString(Error, const FString&, "%s", 5.f, Red);

	//screen(Log, int32, "%d", 5.f, Green);

	//LOGG(int32, "%d");
	static FString File(const char* fileStr)
	{
		return FString(std::strrchr(fileStr, '/')+1);
			//15, fileStr + std::strlen(fileStr) - 15);
	}

	static FString File(const char* fileStr, uint32 line)
	{
		int len = std::strlen(fileStr);
		FString str(fileStr + len - 20);
		if (line < 10) str.Append("   ");
		else if (line < 100) str.Append("  ");
		else if (line < 1000) str.Append(" ");
		str.AppendInt(line);
		if (PlayerID == 1)
		{
			str.Append(" [SV:");
		} else
		if (PlayerID != 0)
		{
			str.Append(" [P");
			str.AppendInt(PlayerID);
			str.Append(":");
		}
		else
		{
			str.Append(" [  :");
		}

		if (GameTick < 10) str.Append("   ");
		else if (GameTick < 100) str.Append("  ");
		else if (GameTick < 1000) str.Append(" ");
		str.AppendInt(GameTick);
		str.Append("] ");

		return str;

		//if (line < 10) return FString(fileStr + len - 20).AppendInt(line);
		//FString str(fileStr);
		//str.Append(line);
		//return FString(std::strrchr(fileStr, '/') + 1);
		//15, fileStr + std::strlen(fileStr) - 15);
	}

	//static FString File(const char* fileStr, uint32 line)
	//{
	//	int len = std::strlen(fileStr);
	//	FString str(fileStr + len - 20);
	//	if (line < 10) str.Append("   ");
	//	else if (line < 100) str.Append("  ");
	//	else if (line < 1000) str.Append(" ");
	//	str.AppendInt(line);
	//	str.Append(" ");
	//	str.AppendInt(PlayerID);
	//	str.Append(": ");
	//	return str;

	//	//if (line < 10) return FString(fileStr + len - 20).AppendInt(line);
	//	//FString str(fileStr);
	//	//str.Append(line);
	//	//return FString(std::strrchr(fileStr, '/') + 1);
	//	//15, fileStr + std::strlen(fileStr) - 15);
	//}


	//static void Log(const char* text, int32 val1, int32 val2) { UE_LOG(LogTemp, Warning, TEXT("%s: %d, %d"), *FString(text), val1, val2); }
	//static void Log(const char* text, int32 val1, int32 val2, int32 val3) { UE_LOG(LogTemp, Warning, TEXT("%s: %d, %d, %d"), *FString(text), val1, val2, val3); }
	//static void Log(const char* text, int32 val1, int32 val2, int32 val3, int32 val4) { UE_LOG(LogTemp, Warning, TEXT("%s: %d, %d, %d, %d"), *FString(text), val1, val2, val3, val4); }

	//static void Log(const char* text, bool val1, bool val2) { UE_LOG(LogTemp, Warning, TEXT("%s: %d, %d"), *FString(text), val1, val2); }
	//static void Log(const char* text, bool val1, bool val2, bool val3) { UE_LOG(LogTemp, Warning, TEXT("%s: %d, %d, %d"), *FString(text), val1, val2, val3); }
	//static void Log(const char* text, bool val1, bool val2, bool val3, bool val4) { UE_LOG(LogTemp, Warning, TEXT("%s: %d, %d, %d, %d"), *FString(text), val1, val2, val3, val4); }

//	static void LogWarning(const char* text, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s"), *instance, line, *FString(text)); }
	//static void Log(const char* text, int32 val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %d"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, uint32 val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %d"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, float val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %f"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, double val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %lf"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, char val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %c"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, wchar_t val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %lc"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, bool val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %d"), *instance, line, *FString(text), val); }
	//static void Log(const char* text, const char* val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %s"), *instance, line, *FString(text), *FString(val)); }
	//static void Log(const char* text, const FString& val, const FString& instance, int line) { UE_LOG(LogTemp, Warning, TEXT("%s(%d)\t\t%s: %s"), *instance, line, *FString(text), *val); }
	static uint8 PlayerID;
	static uint32 GameTick;
	static UEngine* Engine;
};

// The uppercase version will always log the instance name
//#define GKLOG(text) GKLogger::LogWarning(text, GetName(), __LINE__)
//#define GKLOG1(text, val) GKLogger::LogWarning(text, val, GetName(), __LINE__)
//#define GKLOG2(text, val1, val2) GKLogger::LogWarning(text, val1, val2, GetName(), __LINE__)
//#define GKLOG3(text, val1, val2, val3) GKLogger::LogWarning(text, val1, val2, val3, GetName(), __LINE__)
//#define GKLOG4(text, val1, val2, val3, val4) GKLogger::LogWarning(text, val1, val2, val3, val4, GetName(), __LINE__)
//
//// The lowercase version will never log the instance name, especially useful for non unreal class
//#define GKLog(text) GKLogger::LogWarning(text)
//#define GKLog1(text, val) GKLogger::LogWarning(text, val, GKLogger::File(__FILE__), __LINE__)
//#define GKLog2(text, val1, val2) GKLogger::LogWarning(text, val1, val2)
//#define GKLog3(text, val1, val2, val3) GKLogger::LogWarning(text, val1, val2, val3)
//#define GKLog4(text, val1, val2, val3, val4) GKLogger::LogWarning(text, val1, val2, val3, val4)

// The lowercase version will depend on the LOG_INSTANCE_NAME
#define LOG_VERBOSE
#ifdef LOG_VERBOSE

	#define GKLog(text) GKLogger::LogWarningInfo(text, GKLogger::File(__FILE__, __LINE__))
	#define GKLog1(text, val) GKLogger::LogWarningInfo(text, val, GKLogger::File(__FILE__, __LINE__))
	#define GKLog2(text, val1, val2) GKLogger::LogWarningInfo(text, val1, val2, GKLogger::File(__FILE__, __LINE__))
	#define GKLog3(text, val1, val2, val3) GKLogger::LogWarningInfo(text, val1, val2, val3, GKLogger::File(__FILE__, __LINE__))
	#define GKLog4(text, val1, val2, val3, val4) GKLogger::LogWarningInfo(text, val1, val2, val3, val4, GKLogger::File(__FILE__, __LINE__))

	//#define GKLogID(text) GKLogger::LogWarning(text, GKLogger::File(__FILE__, __LINE__, GetPlayerID()))
	//#define GKLogID1(text, val) GKLogger::LogWarning(text, val, GKLogger::File(__FILE__, __LINE__, GetPlayerID()))
	//#define GKLogID2(text, val1, val2) GKLogger::LogWarning(text, val1, val2, GKLogger::File(__FILE__, __LINE__, GetPlayerID()))
	//#define GKLogID3(text, val1, val2, val3) GKLogger::LogWarning(text, val1, val2, val3, GKLogger::File(__FILE__, __LINE__, GetPlayerID()))
	//#define GKLogID4(text, val1, val2, val3, val4) GKLogger::LogWarning(text, val1, val2, val3, val4, GKLogger::File(__FILE__, __LINE__, GetPlayerID()))

	//#define GKLog(text) GKLogger::LogWarning(text, GetName(), __LINE__)
	//#define GKLog1(text, val) GKLogger::LogWarning(text, val, GetName(), __LINE__)
	//#define GKLog2(text, val1, val2) GKLogger::LogWarning(text, val1, val2, GetName(), __LINE__)
	//#define GKLog3(text, val1, val2, val3) GKLogger::LogWarning(text, val1, val2, val3, GetName(), __LINE__)
	//#define GKLog4(text, val1, val2, val3, val4) GKLogger::LogWarning(text, val1, val2, val3, val4, GetName(), __LINE__)

	#define GKError(text) GKLogger::LogErrorInfo(text, GKLogger::File(__FILE__, __LINE__))
	#define GKError1(text, val) GKLogger::LogErrorInfo(text, val, GKLogger::File(__FILE__, __LINE__))
	#define GKError2(text, val1, val2) GKLogger::LogErrorInfo(text, val1, val2, GKLogger::File(__FILE__, __LINE__))
	#define GKError3(text, val1, val2, val3) GKLogger::LogErrorInfo(text, val1, val2, val3, GKLogger::File(__FILE__, __LINE__))
	#define GKError4(text, val1, val2, val3, val4) GKLogger::LogErrorInfo(text, val1, val2, val3, val4, GKLogger::File(__FILE__, __LINE__))

	#define GKScreen(text) GKLogger::ScreenLog(text)
	#define GKScreen1(text, val) GKLogger::ScreenLog(text, val)
	#define GKScreen2(text, val1, val2) GKLogger::ScreenLog(text, val1, val2)
	#define GKScreen3(text, val1, val2, val3) GKLogger::ScreenLog(text, val1, val2, val3)
	#define GKScreen4(text, val1, val2, val3, val4) GKLogger::ScreenLog(text, val1, val2, val3, val4)

	#define GKScreenWarn(text) GKLogger::ScreenWarn(text)
	#define GKScreenWarn1(text, val) GKLogger::ScreenWarn(text, val)
	#define GKScreenWarn2(text, val1, val2) GKLogger::ScreenWarn(text, val1, val2)
	#define GKScreenWarn3(text, val1, val2, val3) GKLogger::ScreenWarn(text, val1, val2, val3)
	#define GKScreenWarn4(text, val1, val2, val3, val4) GKLogger::ScreenWarn(text, val1, val2, val3, val4)

	#define GKScreenError(text) GKLogger::ScreenError(text)
	#define GKScreenError1(text, val) GKLogger::ScreenError(text, val)
	#define GKScreenError2(text, val1, val2) GKLogger::ScreenError(text, val1, val2)
	#define GKScreenError3(text, val1, val2, val3) GKLogger::ScreenError(text, val1, val2, val3)
	#define GKScreenError4(text, val1, val2, val3, val4) GKLogger::ScreenError(text, val1, val2, val3, val4)


#else
	#define GKLog(text) GKLogger::LogWarning(text)
	#define GKLog1(text, val) GKLogger::LogWarning(text, val)
	#define GKLog2(text, val1, val2) GKLogger::LogWarning(text, val1, val2)
	#define GKLog2(text, val1, val2, val3) GKLogger::LogWarning(text, val1, val2, val3)
	#define GKLog2(text, val1, val2, val3, val4) GKLogger::LogWarning(text, val1, val2, val3, val4)

	#define GKError(text) GKLogger::LogError(text)
	#define GKError1(text, val) GKLogger::LogError(text, val)
	#define GKError2(text, val1, val2) GKLogger::LogError(text, val1, val2)
	#define GKError3(text, val1, val2, val3) GKLogger::LogError(text, val1, val2, val3)
	#define GKError4(text, val1, val2, val3, val4) GKLogger::LogError(text, val1, val2, val3, val4)

#endif
