// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define GK_ITERATE_XYZ(varType, countX, countY, countZ)					\
	for (varType z = 0, index = 0; z < countZ; ++z)						\
	{																	\
		for (varType y = 0; y < countY; ++y)							\
		{																\
			for (varType x = 0; x < countX; ++x, ++index)				\
			{

#define GK_ITERATE_XYZ_MINMAX(varType, startX, endX, startY, endY, startZ, endZ)					\
	for (varType z = startZ; z <= endZ; ++z)							\
	{																	\
		for (varType y = startY; y <= endY; ++y)						\
		{																\
			for (varType x = startX; x < endX; ++x)						\
			{

#define GK_ITERATE_XYZ_END												\
			}															\
		}																\
	}

#define GK_ITERATE_XY(varType, countX, countY)							\
		for (varType y = 0, index = 0; y < countY; ++y)					\
		{																\
			for (varType x = 0; x < countX; ++x, ++index)				\
			{

#define GK_ITERATE_XY_END												\
		}																\
	}


#define SAFE_DELETE(ptr)	if (ptr) { delete ptr; ptr = nullptr; }
#define SAFE_DELETE_ARRAY(array)		if (array) { delete[] array; array = nullptr; }

#define SAFE_DESTROY_ARRAY(array, count)			\
	if (array)										\
	{												\
		for (int iii = 0, ccc = count; iii < ccc; ++iii) delete array[iii];				\
		delete[] array;								\
		array = nullptr;							\
	}

#define CREATE_ARRAY_ZERO(Var, VarType, count)				\
	Var = new VarType*[(count)];							\
	FMemory::Memzero(Var, (count)*sizeof(VarType*));

#define GKEnsure(ptr)		if (!ensure(ptr)) return;
#define GKEnsureB(ptr)		if (!ensure(ptr)) return false;

#define GK_INIT_BIT(VarName, Metric, PartName, Value)		\
	VarName ## Bit ## Metric ## PartName = Value;			\
	VarName ## Metric ## PartName = 1 << Value;				\
	GKLog2("Init", (VarName ## Bit ## Metric ## PartName), (VarName ## Metric ## PartName))

#define GK_INIT_BITNUM(VarName, PartName, Value)			\
	GK_INIT_BIT(VarName, Num, PartName, Value)

#define GK_INIT_BITNUMZ(VarName, PartName, Value)			\
	GK_INIT_BIT(VarName, NumZ, PartName, Value)

#define GK_INIT_BITAREA(VarName, PartName, Value)			\
	GK_INIT_BITNUM(VarName, PartName, Value);				\
	GK_INIT_BIT(VarName, Area, PartName, (Value*2));

#define GK_INIT_BITVOL(VarName, PartName, Value)			\
	GK_INIT_BITAREA(VarName, PartName, Value);				\
	GK_INIT_BIT(VarName, Vol, PartName, (Value*3));

#define GK_INIT_BITVOLZ(VarName, PartName, Value, ValueZ)	\
	GK_INIT_BITAREA(VarName, PartName, Value);				\
	GK_INIT_BITNUMZ(VarName, PartName, ValueZ);				\
	GK_INIT_BIT(VarName, Vol, PartName, (Value*2+ValueZ));
