// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
GKGraph contains list of GKGraphNode

GKMap derived from GKGraph 
	- stored GKGraphNode in array 2D but accessible as single array too
	- NOPE: list of cost to other GKMap, from its edge node to every other edge node that is connected to other GKMap
GKMapNode derivedfrom GKGraphNode, contains:
	- list of GKMapNode that it can reaches (bigraph: cost may be different). Includes jumping? 
		For specific entitySize and jumping range. Maybe just for specific entitySize, we're not making
		different jump range. Prob some configuration.
		The node on map edge can point to other GKMapNode on the 26 neighbors.
	- list of gateway for hierarchical

Logic:
- From target, spread using djisktra the cost to every node inside the current map
- Gateway node (at the edge) will have edge to neighbor map all gateway node, so spread to those
- Repeat until we reach source map, then spread node again until we reach source

So included node edge to use during pathfinding:
- TargetMap: all edges pointing inside and other map
- SourceMap: all edges pointing inside only
- OtherMap: all edges pointing to other map only

When a node modified:
- Update the map, if it's on edge update neighbor map

==== GOAP


 */
class ADDICTHOSPITAL_API GKGraph
{
public:
	GKGraph();
	~GKGraph();
};
