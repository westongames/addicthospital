// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Containers/Container/GKContainerData.h"
/**
 * 
 */

struct GKGOAPNode
{
	GKGOAPNode* parent;
	float cost;
	GKContainer* states;
	GKGOAPAction* action;

	GKGOAPNode(GKGOAPNode* iParent, float iCost, GKContainer* iStates, GKGOAPAction* iAction)
	{
		parent = iParent;
		cost = iCost;
		states = iStates;
		action = iAction;
	}
};
