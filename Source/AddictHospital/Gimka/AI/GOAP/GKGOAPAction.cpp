// Fill out your copyright notice in the Description page of Project Settings.


#include "Gimka/AI/GOAP/GKGOAPAction.h"

GKGOAPAction::GKGOAPAction()
{
	
}

GKGOAPAction::~GKGOAPAction()
{
}

bool GKGOAPAction::IsAchievableGiven(const GKContainer* Conditions) const
{
	for (int32 index = 0, count = Preconditions.Num(); index < count; ++index)
	{
		uint16 ID = Preconditions[index];
		if (!Conditions->Contains(ID)) return false;
		// TODO: might also check if the value requirement satisfied or not
	}
	return true;
}
