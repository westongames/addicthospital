// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Containers/Container/GKContainerData.h"
#include "Gimka/AI/GOAP/GKGOAPWorldStates.h"

class GKEntity;

/**
 * 
 */
class GKGOAPAction
{
public:
	GKGOAPAction();
	virtual ~GKGOAPAction();

	virtual bool IsAchievable() const { return true; }

	virtual bool IsAchievableGiven(const GKContainer* Conditions) const;

	virtual bool PrePerform() = 0;
	virtual bool PostPerform() = 0;

private:
	FName Name = TEXT("Action");
	float Cost = 1.f;						// Agent will choose lowest cost, based on dislike, time, resources, etc
	GKEntity* Target;
	// public GameObject targetTag;							// If no target, use tag
	int32 DelayDuration = 0;					// How long in GameTick before performing the action

	// In Udemy, they use array to enter from editor and then copied them the Dictionary in Awake.
	// Here, we're just add directly to array.
	GKContainerData Preconditions;
	GKContainerData Effects;
	// public NavMeshAgent agent;					// in Awake, = this.gameObject.GetComponent<NavMeshAgent>();

	// public Dictionary<string, int> preconditions;
	// public Dictionary<string, int> effects;
	//GKContainer Preconditions;
	//GKContainer Effects;

	GKContainerData AgentBeliefs;
	bool running = false;

public:
	GK_GETCR(FName, Name);
	GK_GETCR(GKContainerData, Effects);
	GK_GET(float, Cost);

};
