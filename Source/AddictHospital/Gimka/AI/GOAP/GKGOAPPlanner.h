// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/AI/GOAP/GKGOAPAgent.h"
#include "Gimka/AI/GOAP/GKGOAPNode.h"
#include "Gimka/Containers/List/GKList.h"
#include "Gimka/Containers/Container/GKContainerWrapper.h"

/**
 * 
 */
class GKGOAPPlanner
{
public:
	//GKGOAPPlanner();
	~GKGOAPPlanner();

	bool Plan(TArray<GKGOAPAction*>& arrayActions, GKContainer* goal, GKContainer* worldStates, 
		TQueue<GKGOAPAction*>& queueActions)
	{
		TArray<GKGOAPAction*> usableActions;
		for (GKGOAPAction* action : arrayActions)
		{
			if (action->IsAchievable())
				usableActions.Add(action);
		}

		TArray<GKGOAPNode> leaves;

		// (GKGOAPNode* iParent, float iCost, const ArrayGOAPWorldStates& iStates, GKGOAPAction* iAction)
		GKGOAPNode start(nullptr, 0, worldStates, nullptr);

		bool success = BuildGraph(&start, leaves, usableActions, goal);
		if (!success)
		{
			GKLog("No plan!");
			return false;
		}

		// Find cheapest node
		check(leaves.Num() > 0);			// If zero then no point going here should just return
		GKGOAPNode* cheapest = &(leaves[0]);
		for (int32 index = 1, count = leaves.Num(); index < count; ++index)
		{
			GKGOAPNode& leaf = leaves[index];
			if (leaf.cost < cheapest->cost) cheapest = &leaf;
		}

		// Traverse up and add all actions
		TArray<GKGOAPAction*> result;
		GKGOAPNode* node = cheapest;
		while (node != nullptr)
		{
			if (node->action != nullptr)
			{
				result.Add(node->action);
			}
			node = node->parent;
		}

		// Move result to QueueActions, reversing it. Parent's action (at bottom of array) should be performed first.
		queueActions.Empty();
		GKLog("The plan is");
		for (int32 index = result.Num() - 1; index >= 0; --index)
		{
			queueActions.Enqueue(result[index]);
			GKLog1("   ", result[index]->GetName().ToString());//UE_LOG(LogTemp, Warning, TEXT("   %s"), result[index]->GetName());
		}

		return true;
	}

	bool BuildGraph(GKGOAPNode* parent, TArray<GKGOAPNode>& leaves, TArray<GKGOAPAction*>& usableActions, const GKContainer* Goal)
	{
		bool foundPath = false;
		for (GKGOAPAction* action : usableActions)
		{
			if (action->IsAchievableGiven(parent->states))
			{
				// Todo: make mini containerData that doesn't use heap but limited in buffer. Then we can use mini wrapper.
				GKContainerWrapper wrapper(parent->states);

				const GKContainerData& effects = action->GetEffects();
				for (int32 i = 0, count = effects.Num(); i < count; ++i)
				{
					uint16 EffectID = effects[i];
					wrapper.SetSafe(EffectID, 1);		// For now no using EffectValue


					GKGOAPNode Node(parent, parent->cost + action->GetCost(), &wrapper, action);

					//if (GoalAchieved(goal, &wrapper))
					//{
					//	leaves.Add(Node);
					//	foundPath = true;
					//}
					//else
					//{
					//	// todo: for removing action, just mark it as unusable then when return can mark it usable again.
					//}

					/*int32 EffectValue;
					effects.Get(EffectID, EffectValue);
					
					int32 Value;
					wrapper.GetSafe(EffectID, Value);
					Value += EffectValue;
					wrapper.SetSafe(EffectID, Value);*/
				}
			}
		}
		return false;
		//for (GKGOAPAction)
	}

private:
	
};
