// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/Containers/Container/GKContainerData.h"
#include "Gimka/Containers/Container/GKContainerCache.h"

#include "Gimka/AI/GOAP/GKGOAPWorldState.h"

/**
 * 
 */
class GKGOAPWorldStates
{
public:
	enum Key
	{
		Start = 1000,
		Var1 = Start,
		Var2,
		End = Var2
	};

	GKGOAPWorldStates();
	~GKGOAPWorldStates();

	inline bool HasState(Key stateKey)
	{
		return States.Contains(stateKey);
	}

	inline void AddState(Key stateKey, int32 value)
	{
		States.Add(stateKey, value);
	}

	inline void SetState(Key stateKey, int32 value)
	{
		States.SetSafe(stateKey, value);
	}

	inline void ModifyState(Key stateKey, int32 value)
	{
		int32 currentValue;
		States.GetSafe(stateKey, currentValue);
		currentValue += FMath::Max(0, value);
		States.Set(stateKey, value);
	}

	inline void RemoveState(Key stateKey)
	{
		if (States.Contains(stateKey))
		{
			States.Set(stateKey, 0);
		}
	}

	//inline GKContainerCache* GetStates() { return &States; }
	inline GKContainer* GetStates() { return &States; }

private:
	GKContainerData StateData;
	GKContainerCache States;

//public:
	//GK_GETR(GKContainerCache, States);
};


