// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/AI/GOAP/GKGOAPWorldStates.h"

/**
 * 
 */
class GKGOAPWorld
{
public:
	GKGOAPWorld();
	~GKGOAPWorld();

private:
	GKGOAPWorldStates WorldStates;

public:
	GK_GETR(GKGOAPWorldStates, WorldStates);
};
