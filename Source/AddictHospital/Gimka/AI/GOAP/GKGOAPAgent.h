// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Tools/Macros/GKHeader.h"
#include "Gimka/AI/GOAP/GKGOAPWorldStates.h"
#include "Gimka/AI/GOAP/GKGOAPAction.h"
#include "Gimka/AI/GOAP/GKGOAPPlanner.h"
#include "Gimka/Containers/Container/GKContainer.h"

// In Udemy, it's a class
// Note: goals can be removed if it's not relevant anymore. For instance, patient that has been cured, doesn't need goal GettingTreatment.
// Goals that are repeated like Nurse resting, shouldn't be removed.
struct GKGOAPSubGoal
{
	GKGOAPSubGoal(GKGOAPWorldStates::Key stateKey, int32 value, bool iRemove)
	{
		//SubGoals.Add(GKGOAPWorldState(stateKey, value));
		SubGoals.Add(stateKey, value);
		bRemove = iRemove;
	}

private:
	GKContainerData SubGoals;
	bool bRemove;
};

/**
 * 
 */
class GKGOAPAgent
{
public:

	GKGOAPAgent();
	~GKGOAPAgent();

	void LateUpdate()
	{

	}

private:
	TArray<GKGOAPAction> ArrayActions;
	TArray<GKGOAPSubGoal> ArrayGoals;

	GKGOAPPlanner Planner;
	TQueue<GKGOAPAction*> QueueActions;
	int32 CurrentActionIndex;
	int32 CurrentGoalIndex;


};
