// Copyright Epic Games, Inc. All Rights Reserved.

#include "AddictHospitalCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/UnrealEngine/GKNetworkManagerActor.h"
#include "EngineUtils.h"
#include "Net/UnrealNetwork.h"

//#include "GimkaSystem/VGANetworkManager.h"
#include "Gimka/System/GKGameEngine.h"
#include "Gimka/Game/GKGameManager.h"
//#include "EngineUtils.h"
//#include "Net/UnrealNetwork.h"


//////////////////////////////////////////////////////////////////////////
// AAddictHospitalCharacter

AAddictHospitalCharacter::AAddictHospitalCharacter()
{
	GKLog("AAddictHospitalCharacter constructor");
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAddictHospitalCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AAddictHospitalCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAddictHospitalCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAddictHospitalCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAddictHospitalCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AAddictHospitalCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AAddictHospitalCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AAddictHospitalCharacter::OnResetVR);
}


void AAddictHospitalCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	GKLog1("Hello I'm AddictHospitalCharacter on Beginplay", GetOwner());
	GKLog1("Role", (uint32)GetLocalRole());
	TActorIterator<AGKNetworkManagerActor> ActorNetworkItr(GetWorld());
	
	// If this is main menu, it will not have AGKNetworkManager
	if (ActorNetworkItr)
	{
		//check(ActorNetworkItr);
		//NetworkManager = *ActorNetworkItr;
		AddTickPrerequisiteActor(*ActorNetworkItr);
		bool IsServer = HasAuthority() && IsLocallyControlled();
		GKLog2("HasAuthority, IsLocallyControlled", HasAuthority(), IsLocallyControlled());
		Initialize(*ActorNetworkItr, IsServer, IsLocallyControlled());
		(*ActorNetworkItr)->AddNetworkPlayer(this);
		/*if (HasAuthority() && PlayerID == 0)
		{
			PlayerID = 323;
		}*/
	}
}

void AAddictHospitalCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	GKLog("OnBeginDestroy");
	if (GetNetworkManager() != nullptr)
	{
		GetNetworkManager()->RemoveNetworkPlayer(this);
		Disconnect();
	}

	Super::EndPlay(EndPlayReason);
}


void AAddictHospitalCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	//GKLog("AddictHospitalCharacter::Tick");

	//if (!HasAuthority() && IsLocallyControlled()) 
	//{
	//	//GKLog("Found a local character!");
	//	NetworkManager->LocalClientSendMessageForServer();
	//}
}

void AAddictHospitalCharacter::OnResetVR()
{
	// If AddictHospital is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in AddictHospital.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AAddictHospitalCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AAddictHospitalCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AAddictHospitalCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAddictHospitalCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AAddictHospitalCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		//if (!HasAuthority() && IsLocallyControlled())
		//{
		//	// This works doing it from the client
		//	//RPCClientToServerReliable(332);
		//}

		////if (HasAuthority())// && !IsLocallyControlled())
		//{
		//	GKLog1("ServerToClient LabelID", GetActorLabel());
		//	TActorIterator<AVGANetworkManager> ActorNetworkItr(GetWorld());
		//	GKLog1("AVGNetworkManager HasAuthority", (*ActorNetworkItr)->HasAuthority());
		//	GKLog1("ServerToClient HasAuthority", HasAuthority());
		//	GKLog1("ServerToClient Local Role", GetLocalRole());
		//	GKLog1("ServerToClient Remote Role", GetRemoteRole());
		//	GKLog1("ServerToClient IsLocallyControlled", IsLocallyControlled());
		//	RPCClientToServerReliable(332);
		//	//RPCServerToClientReliable(122);
		//	//RPCServerToAllReliable(333);
		//}

		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AAddictHospitalCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AAddictHospitalCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	// If we don't call this the client will become SimulatedProxy instead of AutonomousProxy
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AAddictHospitalCharacter, PlayerID);
}


void AAddictHospitalCharacter::OnRep_PlayerID()
{
	GKLog1("=============================== PlayerID is assigned", PlayerID);
}

void AAddictHospitalCharacter::RPCClientToServer_Implementation(FGKNetworkSerializeToServer NetworkSerialize)
{
	//if (GameEngine == nullptr) return;
	uint32 GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToServer Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("================= Addict Server received ClientToServer at", GameTickLocal, Message.GetMessageType());
		Message.SetReliable(false);
		GetNetworkManager()->OnReceiveMessage(Message);
	}
	else
	{
		GKLog2("================= Addict Client received ClientToServer at", GameTickLocal, Message.GetMessageType());
	}
}


void AAddictHospitalCharacter::RPCClientToServerReliable_Implementation(FGKNetworkSerializeToServer NetworkSerialize)
{
	//if (GameEngine == nullptr) return;
	uint32 GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToServer Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientToServerRel at", GameTickLocal, Message.GetMessageType());
		Message.SetReliable(true);
		GetNetworkManager()->OnReceiveMessage(Message);
	}
	else
	{
		GKLog2("============== Addict Client received ClientToServerRel at", GameTickLocal, Message.GetMessageType());
	}
}


void AAddictHospitalCharacter::RPCServerToClient_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received Client at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received Client at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);
	}
}

void AAddictHospitalCharacter::RPCServerToClientReliable_Implementation(FGKNetworkSerializeToClient NetworkSerialize)
{
	GKMessageNetworkToClient Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientReliable at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received ClientReliable at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);

	}
}

void AAddictHospitalCharacter::RPCServerToClientCommandReliable_Implementation(FGKNetworkSerializeToClientCommand NetworkSerialize)
{
	//GKNetworkManager* NetworkManager = GetNetworkManager();
	//if (NetworkManager == nullptr)
	//{
	//	TActorIterator<AVGANetworkManager> ActorNetworkItr(GetWorld());
	//	check(ActorNetworkItr);
	//	//NetworkManager = *ActorNetworkItr;
	//	AddTickPrerequisiteActor(*ActorNetworkItr);
	//	bool IsServer = HasAuthority() && IsLocallyControlled();
	//	GKLog2("HasAuthority, IsLocallyControlled", HasAuthority(), IsLocallyControlled());
	//	Initialize(*ActorNetworkItr, IsServer, IsLocallyControlled());
	//	(*ActorNetworkItr)->AddNetworkPlayer(this);
	//	NM = GetNetworkManager();
	//}
	//GKGameEngine* GameEngine = NM->GetGameEngine();
	//GKGameManager* GameManager = GameEngine->GetGameManager();
	//check(GameManager);
	//uint32 GameTickLocal = 0;
	//if (GetNetworkManager() != nullptr) GameTickLocal = GetNetworkManager()->GetGameEngine()->GetGameManager()->GetGameTime().GetGameTick();
	GKMessageNetworkToClientCommand Message = NetworkSerialize.GetMessage();
	if (HasAuthority())
	{
		GKLog2("============== Addict Server received ClientCommand at", Message.GetGameTick(), Message.GetMessageType());
	}
	else
	{
		GKLog2("============== Addict Client received ClientCommand at", Message.GetGameTick(), Message.GetMessageType());
		Message.SetReliable(true);
		OnReceiveMessage(Message);

	}
}

