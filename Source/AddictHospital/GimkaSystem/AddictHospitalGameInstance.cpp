// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/AddictHospitalGameInstance.h"
#include "GimkaSystem/VGAMainMenu.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"

#include "Gimka/Menu/GKMenu.h"

// Test
#include "Gimka/Containers/Container/GKContainerData.h"
#include "Gimka/Containers/Container/GKContainerWrapper.h"
#include "Gimka/Containers/Container/GKContainerArrayIterator.h"
#include "Gimka/Containers/Queue/GKPriorityQueue.h"

//#include "HAL/MemoryBase.h"
//#include "Gimka/Network/GKOnlineSession.h"

#include "UObject/ConstructorHelpers.h"

UAddictHospitalGameInstance::UAddictHospitalGameInstance(const FObjectInitializer& ObjectInitializer)
	: UGKGameInstance(ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> MenuMainBPClass(TEXT("/Game/MenuSystem/WBPMainMenu"));
	GKEnsure(MenuMainBPClass.Class);

	MenuMainClass = MenuMainBPClass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> MenuInGameBPClass(TEXT("/Game/MenuSystem/WBPInGameMenu"));
	GKEnsure(MenuInGameBPClass.Class);

	MenuInGameClass = MenuInGameBPClass.Class;

}

struct Node
{
	int value;
	bool operator< (const Node& other) const 
	{ 
		GKLog("Operator < is called!");
		return value < other.value; 
	}
};

struct Comparison
{
	static bool Compare(const Node* lhs, const Node* rhs)
	{
		return lhs->value <= rhs->value;
	}
};

void UAddictHospitalGameInstance::Init()
{
	//bool MallocThreadSafe = FMalloc::IsInternallyThreadSafe();
	//GKLog1("Malloc Thread Safe", MallocThreadSafe);

	Super::Init();
	GetOnlineSession()->SetMaxPlayers(4);

	GKGoapState goap;

	/*GKPriorityQueue<Node*, Comparison> Queue;
	Node Node1, Node2, Node3, Node4, Node5;
	Node1.value = 33;
	Node2.value = 66;
	Node3.value = 44;
	Node4.value = 22;
	Node5.value = 55;
	Queue.Enqueue(&Node1);
	Queue.Enqueue(&Node2);
	Queue.Enqueue(&Node3);
	Queue.Enqueue(&Node4);
	Queue.Enqueue(&Node5);

	Queue.Log();*/


//	planner.Plan()

	//GKGoapGoal goalHasWeapon;
	//goalHasWeapon.GetStates().Add(GKGoapState::HasWeapon);







	//if (Comparison::Compare(&Bla, &Bla))
	//{

	//}

	//GKContainerData Container;
	//for (int i = 0; i < 10; ++i) Container.Add(i * 2, i * 4);
	//Container.Log();

	//GKContainerWrapper ConWrap(&Container);
	//ConWrap.Log();

	//ConWrap.Add(20, 40);
	//ConWrap.Set(2, 5);
	//ConWrap.SetSafe(21, 232);
	////char* Address = ConWrap.GetAddress(2);
	////GKLog1("Address", Address);
	//ConWrap.Log();
	//int32 Val;
	//ConWrap.Get(21, Val);
	//GKLog1("Value of key 2", Val);


	//GKContainerWrapper Data2(ConWrap);
	//Data2.Log();

	//GKContainerArrayIterator<int32> ArrayInt(&Container, 100);
	//ArrayInt.Log();

	//for (int i = 0; i < 5; ++i) ArrayInt.Add(512 + i*10);
	//ArrayInt[1] = 1024;
	//ArrayInt.Log();

	//ArrayInt.RemoveAt(2);
	////ArrayInt.RemoveSwap(512);

	//for (int i = 10; i < 20; ++i) Container.Add(i * 2, i * 4);

	//Container.Log();
	//ArrayInt.Log();
}

void UAddictHospitalGameInstance::Host()
{
	StartHost();
}

void UAddictHospitalGameInstance::Join(const FString& Address)
{
	StartJoin(Address);
}

void UAddictHospitalGameInstance::StartHost()
{
	GKScreen("Hosting");
	UWorld* World = GetWorld();
	GKEnsure(World);

	World->ServerTravel("/Game/Lobby/Lobby?listen");
	//World->ServerTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap?listen");
}

void UAddictHospitalGameInstance::StartJoin(const FString& Address)
{
	GKScreen1("Joining", Address);
	//"232.31.44");
		//(*Address));
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	GKEnsure(PlayerController);

	PlayerController->ClientTravel(Address, ETravelType::TRAVEL_Absolute);
}

void UAddictHospitalGameInstance::Quity()
{
	GKScreen("Quitting");
	UWorld* World = GetWorld();
	GKEnsure(World);

	World->ServerTravel("/Game/Lobby/Lobby?listen");
}

void UAddictHospitalGameInstance::StartPlay()
{
	CloseMenu();
	GKLog("MenuPlay");

	UWorld* World = GetWorld();
	GKEnsure(World);

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	GKEnsure(PlayerController);

	// virtual bool UEngine::LoadMap(FWorldContext & WorldContext, FURL URL, class UPendingNetGame* Pending,
	//	 FString& Error)

	//virtual EBrowseReturnVal::Type UEngine::Browse(FWorldContext & WorldContext, FURL URL, FString & Error)
	//FWorldContext* WorldContext = GetWorldContext();
	FString Error;
	GetEngine()->Browse(*WorldContext, FURL(TEXT("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap")), Error);

	// void APlayerController:ClientTravel(const FString & URL, enum ETravelType TravelType, bool bSeamless, 
	//	FGuid MapPackageGuid)
	//PlayerController->ClientTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap", ETravelType::TRAVEL_Absolute);

	// virtual bool UWorld::ServerTravel(const FString & InURL, bool bAbsolute, bool bShouldSkipGameNotify)
	//World->ServerTravel("/Game/ThirdPersonCPP/Maps/ThirdPersonExampleMap");
}

//void UAddictHospitalGameInstance::OnMenuHost()
//{
//	GKLog("OnHost");
//	CloseMenu();
//
//	Host();
//}

//void UAddictHospitalGameInstance::OnMenuJoin(const FString& Address)
//{
//	GKLog("OnJoin");
//	CloseMenu();
//
//	StartJoin(Address);
//}

// Called by GKOnlineSession when findSessions is completed
void UAddictHospitalGameInstance::OnFindSessionsComplete(const TArray<GKOnlineSession::SessionData>& Sessions, bool Success)
{
	// Make sure it's still the main menu. If not then ignore.
	UVGAMainMenu* MainMenu = dynamic_cast<UVGAMainMenu*>(GetActiveMenu());
	GKEnsure(MainMenu);

	if (Success)
	{
		if (Sessions.Num() > 0)
		{
			MainMenu->SetSessionList(Sessions);
			MainMenu->EnableServerList();
			ResetSelectedIndex();
		}
		else
		{
			if (GKOnlineSession::bAddFakeSessions && GetOnlineSession()->ConnectionTypeIsNull())
			{
				TArray<GKOnlineSession::SessionData> FakeSessions;
				for (int i = 0; i < 3; ++i)
				{
					GKOnlineSession::SessionData Session;
					Session.Name = FString::Printf(TEXT("Fake Session %d"), (i + 1));
					Session.TotalPlayers = 0;		// If zero, SessionRow will set other field empty.
					FakeSessions.Add(Session);
				}
				MainMenu->SetSessionList(FakeSessions);
				MainMenu->EnableServerList();
				ResetSelectedIndex();
			}
			else
			{
				MainMenu->DisableServerList("Session not found!");
			}
		}
	}
	else
	{
		MainMenu->DisableServerList("Error searching for session!");
	}
}

// Called by GKOnlineSession when joinSession is completed
void UAddictHospitalGameInstance::OnJoinSessionComplete(const FName& SessionName, const FString& Address, bool Success)
{
	if (Success)
	{
		StartJoin(Address);
	}
	else
	{
		ReopenMenu();
	}

	//FString Address;
	//if (!SessionInterface->GetResolvedConnectString(SessionName, Address))
	//{
	//	GKLog("Couldn't get connect string.");
	//	GameInstance->OnReopenMenu();
	//	return;
	//}

	//GameInstance->OnMenuJoin(Address);

}

// Called by GKOnlineSession when a subsystem is found
void UAddictHospitalGameInstance::OnFoundSubsystem(const FString& SubsystemName)
{
	// If it's main menu, add the name of subsystem to the title. Otherwise just ignore.
	// MainMenu might be null if user choosen Host and the menu has been destroyed.
	UVGAMainMenu* MainMenu = dynamic_cast<UVGAMainMenu*>(GetActiveMenu());
	if (MainMenu == nullptr) return;

	MainMenu->SetSubsystemName(SubsystemName);
}


void UAddictHospitalGameInstance::OnLoadMenuMain()
{
	if (LoadMenu(this, MenuMainClass) != nullptr)
	{
		// If it's main menu, add the name of subsystem to the title. Otherwise just ignore.
		UVGAMainMenu* MainMenu = dynamic_cast<UVGAMainMenu*>(GetActiveMenu());
		GKEnsure(MainMenu);

		if (GetOnlineSession()->IsSessionInterfaceValid())
		{
			MainMenu->SetSubsystemName(GetOnlineSession()->GetSubsystemName());
		}
	}
}

void UAddictHospitalGameInstance::OnLoadMenuInGame()
{
	LoadMenu(this, MenuInGameClass);
}


void UAddictHospitalGameInstance::PrintScreen(float Time, FColor DisplayColor, const FString& Message)
{
	GetEngine()->AddOnScreenDebugMessage(INDEX_NONE, Time, DisplayColor, Message);
}
