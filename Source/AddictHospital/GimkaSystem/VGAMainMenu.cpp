// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAMainMenu.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"

#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "UObject/ConstructorHelpers.h"
#include "Gimka/UnrealEngine/GKSessionRow.h"


UVGAMainMenu::UVGAMainMenu(const FObjectInitializer& ObjectInitializer)
	: UGKMenu(ObjectInitializer)
{
	GKLog("UVGAMainMenu constructor");
	/*ConstructorHelpers::FClassFinder<UUserWidget> MenuBPClass(TEXT("/Game/MenuSystem/WBPMainMenu"));
	if (!ensure(MenuBPClass.Class)) return;

	MenuClass = MenuBPClass.Class;
	GKLog1("UVGAMainMenu class:", MenuClass);*/

	ConstructorHelpers::FClassFinder<UUserWidget> SessionRowBPClass(TEXT("/Game/MenuSystem/WBPSessionRow"));
	GKEnsure(SessionRowBPClass.Class);

	SessionRowClass = SessionRowBPClass.Class;
}

bool UVGAMainMenu::Initialize()
{
	if (!Super::Initialize()) return false;

	///////////////////////////////////////////////////////////////////////////////
	/// Main Menu
	///////////////////////////////////////////////////////////////////////////////
	GKEnsureB(ButtonPlay);
	ButtonPlay->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonPlay);

	GKEnsureB(ButtonMultiplayer);
	ButtonMultiplayer->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenMultiplayerMenu);

	GKEnsureB(ButtonExitGame);
	ButtonExitGame->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonExitGame);

	///////////////////////////////////////////////////////////////////////////////
	/// Multiplayer Menu
	///////////////////////////////////////////////////////////////////////////////
	GKEnsureB(ButtonMPHost);
	ButtonMPHost->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenHostMenu);

	GKEnsureB(ButtonMPJoin);
	ButtonMPJoin->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenJoinMenu);

	GKEnsureB(ButtonMPJoinManual);
	ButtonMPJoinManual->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenJoinManualMenu);

	GKEnsureB(ButtonMPClose);
	ButtonMPClose->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenMainMenu);

	///////////////////////////////////////////////////////////////////////////////
	/// Host Menu
	///////////////////////////////////////////////////////////////////////////////
	GKEnsureB(ButtonMPHostContinue);
	ButtonMPHostContinue->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonHostServer);

	GKEnsureB(ButtonMPHostClose);
	ButtonMPHostClose->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenMultiplayerMenu);

	ETextBoxSessionName->OnTextChanged.AddDynamic(this, &UVGAMainMenu::OnValidateSessionName);
	ButtonMPHostContinue->SetIsEnabled(false);

	///////////////////////////////////////////////////////////////////////////////
	/// Join Menu Manual
	///////////////////////////////////////////////////////////////////////////////
	GKEnsureB(ButtonMPJoinManContinue);
	ButtonMPJoinManContinue->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonJoinServer);

	GKEnsureB(ButtonMPJoinManClose);
	ButtonMPJoinManClose->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenMultiplayerMenu);

	ETextBoxIPAddressField->OnTextChanged.AddDynamic(this, &UVGAMainMenu::OnValidateIPAddress);
	ButtonMPJoinManContinue->SetIsEnabled(false);

	///////////////////////////////////////////////////////////////////////////////
	/// Join Menu
	///////////////////////////////////////////////////////////////////////////////
	GKEnsureB(ButtonMPJoinListContinue);
	ButtonMPJoinListContinue->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonJoinServerList);

	GKEnsureB(ButtonMPJoinListRefresh);
	ButtonMPJoinListRefresh->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonJoinRefresh);

	GKEnsureB(ButtonMPJoinListClose);
	ButtonMPJoinListClose->OnClicked.AddDynamic(this, &UVGAMainMenu::OnButtonOpenMultiplayerMenu);

	//TextMPJoin->Text = FText::FromString("Hello you!");

	return true;
}

void UVGAMainMenu::OnButtonPlay()
{
	if (MenuInterface) MenuInterface->OnMenuPlay();
}


void UVGAMainMenu::OnButtonExitGame()
{
#if WITH_EDITOR
	GetWorld()->GetFirstPlayerController()->ConsoleCommand("quit");
#else
	FGenericPlatformMisc::RequestExit(false);
#endif
}

//void UVGAMainMenu::SetMenuInterface(IGKMenuInterface* iMenuInterface)
//{
//	this->MenuInterface = iMenuInterface;
//}



void UVGAMainMenu::OnButtonHostServer()
{
	GKLog("Host Server!");
	if (MenuInterface)
	{
		GKEnsure(ETextBoxSessionName);
		const FString& SessionName = ETextBoxSessionName->GetText().ToString();
		if (SessionName.Len() == 0)
		{
			// This is impossible case because the Continue button is disabled if SessionName is empty
			GKScreenError("Please enter a session name!");
			return;
		}
		MenuInterface->OnMenuHost(SessionName);
	}
}

void UVGAMainMenu::OnButtonJoinServer()
{
	GKLog("Join Server!");
	if (MenuInterface)
	{
		GKEnsure(ETextBoxIPAddressField);
		const FString& IPAddress = ETextBoxIPAddressField->GetText().ToString();
		if (IPAddress.Len() == 0)
		{
			// This is impossible case because the Continue button is disabled if IPAddress is empty
			GKScreenError("Please enter IP Address!");
			return;
		}
		MenuInterface->OnMenuJoin(IPAddress);
	}
}

void UVGAMainMenu::EnableServerList()
{
	ScrollBoxServerList->SetIsEnabled(true);
	ButtonMPJoinListContinue->SetIsEnabled(true);
	ButtonMPJoinListRefresh->SetIsEnabled(true);
}

void UVGAMainMenu::DisableServerList(const FString& Reason, bool EnableRefresh)
{
	TArray<GKOnlineSession::SessionData> Sessions;
	GKOnlineSession::SessionData Session;
	Session.Name = Reason;
	Session.TotalPlayers = 0;				// If zero then SessionRow will show other field as empty
	Sessions.Add(Session);
	SetSessionList(Sessions);

	ScrollBoxServerList->SetIsEnabled(false);
	ButtonMPJoinListContinue->SetIsEnabled(false);
	ButtonMPJoinListRefresh->SetIsEnabled(EnableRefresh);

	SessionHeader->SetVisibility(ESlateVisibility::Hidden);
}


void UVGAMainMenu::SetSessionList(const TArray<GKOnlineSession::SessionData>& Sessions)
{
	UWorld* World = GetWorld();
	GKEnsure(World);

	SessionHeader->SetVisibility(ESlateVisibility::Visible);

	ScrollBoxServerList->ClearChildren();

	for (uint32 i = 0, count = Sessions.Num(); i < count; ++i)
	{
		UGKSessionRow* SessionRow = CreateWidget<UGKSessionRow>(World, SessionRowClass);
		GKEnsure(SessionRow);

		SessionRow->SetSessionData(Sessions[i]);
		SessionRow->Setup(MenuInterface, i);

		ScrollBoxServerList->AddChild(SessionRow);
	}
}

void UVGAMainMenu::SetSubsystemName(const FString& SubsystemName)
{
	GKEnsure(TextMPJoin);
	GKLog2("Change Text from ", TextMPJoin->Text.ToString(), SubsystemName);
	
	if (SubsystemName != FString("NULL"))
	{
		FString Name = FString::Printf(TEXT("Join a Game (%s)"), *SubsystemName);
		TextMPJoin->SetText(FText::FromString(Name));

		Name = FString::Printf(TEXT("Host Game (%s)"), *SubsystemName);
		TextMPHost->SetText(FText::FromString(Name));
	}
}

void UVGAMainMenu::OnSelectIndex(uint32 Index)
{
	for (auto Child : ScrollBoxServerList->GetAllChildren())
	{
		auto SessionRow = Cast<UGKSessionRow>(Child);
		if (SessionRow)
		{
			bool IsSelected = SessionRow->GetIndex() == Index;
			SessionRow->SetAsSelected(IsSelected);
		}
	}
}

void UVGAMainMenu::OnResetSelectedIndex(uint32 OldIndex)
{
	for (auto Child : ScrollBoxServerList->GetAllChildren())
	{
		auto SessionRow = Cast<UGKSessionRow>(Child);
		if (SessionRow)
		{
			SessionRow->SetAsSelected(false);
		}
	}
}

void UVGAMainMenu::OnValidateSessionName(const FText& Text)
{
	int32 len = Text.ToString().Len();
	if (len > 0)
	{
		ButtonMPHostContinue->SetIsEnabled(true);
	}
	else
	{
		ButtonMPHostContinue->SetIsEnabled(false);
	}
}


void UVGAMainMenu::OnButtonJoinServerList()
{
	//SetServerList(
		//{});
		//{ "LineA", "Line Bk" });
	GKLog("JoinServerList");
	if (MenuInterface)
	{
		MenuInterface->OnMenuJoinSelectedServer();
	}
	
}

void UVGAMainMenu::OnButtonJoinRefresh()
{
	MenuInterface->RefreshServerList();
	DisableServerList("Searching...", false);
}

void UVGAMainMenu::OnButtonOpenMainMenu()
{
	GKEnsure(MenuSwitcher);
	GKEnsure(MenuMain);
	MenuSwitcher->SetActiveWidget(MenuMain);
}

void UVGAMainMenu::OnButtonOpenMultiplayerMenu()
{
	GKEnsure(MenuSwitcher);
	GKEnsure(MenuMPJoin);
	MenuSwitcher->SetActiveWidget(MenuMultiplayer);
}

void UVGAMainMenu::OnButtonOpenHostMenu()
{
	GKEnsure(MenuSwitcher);
	GKEnsure(MenuMPHost);
	MenuSwitcher->SetActiveWidget(MenuMPHost);
	//OnValidateSessionName();
}

void UVGAMainMenu::OnButtonOpenJoinMenu()
{
	GKEnsure(MenuSwitcher);
	GKEnsure(MenuMPJoin);

	MenuSwitcher->SetActiveWidget(MenuMPJoin);

	MenuInterface->RefreshServerList();
	DisableServerList("Searching...", false);
}

void UVGAMainMenu::OnValidateIPAddress(const FText& Text)
{
	int32 len = Text.ToString().Len();
	if (len > 0)
	{
		ButtonMPJoinManContinue->SetIsEnabled(true);
	}
	else
	{
		ButtonMPJoinManContinue->SetIsEnabled(false);
	}
}

void UVGAMainMenu::OnButtonOpenJoinManualMenu()
{
	GKEnsure(MenuSwitcher);
	GKEnsure(MenuMPJoinManual);
	MenuSwitcher->SetActiveWidget(MenuMPJoinManual);
	//OnValidateIPAddress();
}