// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAPawnWithCamera.h"

// Sets default values
AVGAPawnWithCamera::AVGAPawnWithCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVGAPawnWithCamera::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVGAPawnWithCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AVGAPawnWithCamera::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

