// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAChunkVoxelActor.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/WorldVoxel/GKChunk.h"
#include "Gimka/WorldVoxel/GKSection.h"
#include "Gimka/WorldVoxel/GKTile.h"

// Sets default values
AVGAChunkVoxelActor::AVGAChunkVoxelActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AVGAChunkVoxelActor::SetOwner(const GKChunk* iOwner)
{
	GKLog1("Set ChunkVoxelActor Owner", Owner);
	Owner = iOwner;

	FIntPoint ChunkPos = Owner->GetChunkPos();
	FIntPoint RegionPos = Owner->GetRegion()->GetRegionPos();

#if WITH_EDITOR
	FString folderString = "/Region_" + FString::FromInt(RegionPos.X) + "_" + FString::FromInt(RegionPos.Y);
	this->SetFolderPath(*folderString);

	FString string = FString::FromInt(ChunkPos.X) + "_" + FString::FromInt(ChunkPos.Y);
	//string = FString::FromInt(Owner->GetChunkIndex());
	this->SetActorLabel("Chunk_" + string);
#endif

	UpdateMesh();
}


// Called when the game starts or when spawned
void AVGAChunkVoxelActor::BeginPlay()
{
	Super::BeginPlay();
	GKLog("ChunkVoxelActor::BeginPlay");
}

// This is called before SetOwner
// Sequence: OnConstruction, BeginPlay, SetOwner, UpdateMesh, GroundVisualManager::CreateGroundActor, GroundVisualManager::Create GroundSprite
void AVGAChunkVoxelActor::OnConstruction(const FTransform& Transform)
{
	SetActorEnableCollision(false);

	GKLog("ChunkVoxelActor::OnConstruction");

	FString string = "Section_0";// +string;
	FName name = FName(*string);
	ProceduralComponent = NewObject<UProceduralMeshComponent>(this, name);
	ProceduralComponent->RegisterComponent();
	ProceduralComponent->SetMobility(EComponentMobility::Type::Static);
	//ProceduralComponent->bUseC

	RootComponent = ProceduralComponent;
	RootComponent->SetWorldTransform(Transform);		// set position of root component

	Super::OnConstruction(Transform);
}

// Called every frame
void AVGAChunkVoxelActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

const int32 QuadTriangles[] = { 2, 1, 0, 0, 3, 2 };
const FVector2D QuadUVs[] = { FVector2D(0, 0), FVector2D(0, 1), FVector2D(1, 1), FVector2D(1, 0) };
const FVector QuadNormals0[] = { FVector(0, 0, 1), FVector(0, 0, 1), FVector(0, 0, 1), FVector(0, 0, 1) };		// top

void AVGAChunkVoxelActor::UpdateMesh()
{
	// TODO: this should be generated in a seperate module: VoxelEngine
	GKLog("ChunkVoxelActor::UpdateMesh");
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FProcMeshTangent> Tangents;
	TArray<FColor> VertexColors;

	int32 ChunkNumTiles = Owner->GetWorldSettings()->GetChunkNumTiles();

	const GKSection* MidSection = Owner->MiddleSection();
	const GKTile* Tiles = MidSection->Tiles();
	const GKTile* CurrentTile = Tiles;

	int32 NumTriangles = 0;
	for (int y = 0, index = 0; y < ChunkNumTiles; ++y)
	{
		for (int x = 0; x < ChunkNumTiles; ++x, ++index, ++CurrentTile)
		{
			if (CurrentTile->IsNotWalkable()) continue;
			bool walkable = true;		// Todo: get the data here walkable or blocked
			
			for (int i = 0; i < 6; ++i)
			{
				Triangles.Add(QuadTriangles[i] + NumTriangles);
			}
			NumTriangles += 4;				// Add 4 vertices to next triangle

			// Add top
			Vertices.Add(FVector(x, y + 1, 0));
			Vertices.Add(FVector(x, y, 0));
			Vertices.Add(FVector(x + 1, y, 0));
			Vertices.Add(FVector(x + 1, y + 1, 0));

			Normals.Append(QuadNormals0, UE_ARRAY_COUNT(QuadNormals0));

			UVs.Append(QuadUVs, UE_ARRAY_COUNT(QuadUVs));

			FColor color = CurrentTile->IsWalkable() ? FColor(255, 255, 255, 255) : FColor(255, 0, 0, 255);// ::White;
			color = FColor(255, 0, 0, 255);
			color = FColor(255, 255, 255, 255);
			VertexColors.Add(color);
			VertexColors.Add(color);
			VertexColors.Add(color);
			VertexColors.Add(color);
		}
	}

	ProceduralComponent->ClearAllMeshSections();
	ProceduralComponent->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, VertexColors, Tangents, true);

}

