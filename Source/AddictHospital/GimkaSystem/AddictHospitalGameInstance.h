// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/UnrealEngine/GKGameInstance.h"
#include "AddictHospitalGameInstance.generated.h"

class UGKMenu;

/**
 * EXEC Compatible Classes: https://www.udemy.com/course/unrealmultiplayer/learn/lecture/7829432#questions
 *		PlayerControllers, Possessed Pawns, HUDs, Cheat Managers, Game Modes, Game Instances
 */
UCLASS()
class ADDICTHOSPITAL_API UAddictHospitalGameInstance : public UGKGameInstance
{
	GENERATED_BODY()

public:
	UAddictHospitalGameInstance(const FObjectInitializer& ObjectInitializer);
	virtual void Init() override;


	virtual void OnLoadMenuMain() override;
	virtual void OnLoadMenuInGame() override;

	virtual void PrintScreen(float Time, FColor DisplayColor, const FString& Message) override;

	// Called by GKOnlineSession when findSessions is completed
	virtual void OnFindSessionsComplete(const TArray<GKOnlineSession::SessionData>& Sessions, bool Success) override;

	// Called by GKOnlineSession when joinSession is completed
	virtual void OnJoinSessionComplete(const FName& SessionName, const FString& Address, bool Success) override;

	// Called by GKOnlineSession when a subsystem is found
	virtual void OnFoundSubsystem(const FString& SubsystemName) override;

protected:
	virtual void StartHost() override;
	virtual void StartJoin(const FString& Address) override;
	virtual void StartPlay() override;

	//////////////////////////////////////////////////////////
	// EXEC - use tilde then type the function name
	//////////////////////////////////////////////////////////
	UFUNCTION(exec)
	void Host();

	UFUNCTION(exec)
	void Join(const FString& Address);

	// Testing ability to go back to lobby
	UFUNCTION(exec)
	void Quity();

	//////////////////////////////////////////////////////////
	// GKMenuInterface
	//////////////////////////////////////////////////////////
	//virtual void OnMenuPlay() override;
	//virtual void OnMenuHost() override;
	//virtual void OnMenuJoin(const FString& Address) override;
	//virtual void OnMenuQuit() override;

private:
	TSubclassOf<class UUserWidget> MenuMainClass;
	TSubclassOf<class UUserWidget> MenuInGameClass;

	//UGKMenu* MenuMain;
	//UGKMenu* MenuInGame;

};
