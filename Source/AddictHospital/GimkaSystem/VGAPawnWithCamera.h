// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
//#include "GameFramework/Pawn.h"
#include "Gimka/UnrealEngine/GKPawnWithCamera.h"
#include "VGAPawnWithCamera.generated.h"

UCLASS()
class ADDICTHOSPITAL_API AVGAPawnWithCamera : public AGKPawnWithCamera
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVGAPawnWithCamera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


};
