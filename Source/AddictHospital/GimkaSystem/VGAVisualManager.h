// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Gimka/Visual/GKVisualManager.h"
#include "Gimka/Entity/Entity/GKVisualEntity.h"

#include "VGAVisualManager.generated.h"

UCLASS()
class ADDICTHOSPITAL_API AVGAVisualManager : public AActor, public GKVisualManager
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVGAVisualManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual int CreateActor(const GKVisualEntity& VisualEntity) override;
	virtual void SetActorPosition(int Actor, const FVector& Position) override;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Actor Meshes", meta = (AllowPrivateAccess = "true"))
	UStaticMesh* TheStaticMesh;

	TArray<AActor*> ArrayActors;
};