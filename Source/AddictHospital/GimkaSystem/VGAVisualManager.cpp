// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAVisualManager.h"
#include "PuzzlePlatforms/MovingPlatform.h"
#include "Gimka/Tools/Macros/GKLogger.h"

// Sets default values
AVGAVisualManager::AVGAVisualManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVGAVisualManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AVGAVisualManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	OnUpdateGame(DeltaTime);

}

int AVGAVisualManager::CreateActor(const GKVisualEntity& VisualEntity)
{
	AStaticMeshActor* NewActor = GetWorld()->SpawnActor<AStaticMeshActor>
		(AMovingPlatform::StaticClass(), VisualEntity.GetPosition(), VisualEntity.GetRotation());
	//NewActor->SetActorLabel(FString::Printf(TEXT("Actor %d"), VisualEntity.GetID()));

	UStaticMeshComponent* MeshComponent = NewActor->GetStaticMeshComponent();
	if (MeshComponent != nullptr)
	{
		MeshComponent->SetStaticMesh(TheStaticMesh);
	}

	int actor = ArrayActors.Add(NewActor);
	GKLog1("CreateActor ", actor);
	UE_LOG(LogTemp, Warning, TEXT("Position %s, Rotation %s"), 
		*VisualEntity.GetPosition().ToString(), *VisualEntity.GetRotation().ToString());
	return actor;
}

void AVGAVisualManager::SetActorPosition(int ActorIndex, const FVector& Position)
{
	AActor* Actor = ArrayActors[ActorIndex];
	Actor->SetActorLocation(Position);
	//GKLog1("SetActorLocation ", Position.X);
}


