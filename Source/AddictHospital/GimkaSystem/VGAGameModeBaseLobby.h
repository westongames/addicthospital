// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/UnrealEngine/GKGameModeBaseLobby.h"
#include "VGAGameModeBaseLobby.generated.h"

/**
 * 
 */
UCLASS()
class ADDICTHOSPITAL_API AVGAGameModeBaseLobby : public AGKGameModeBaseLobby
{
	GENERATED_BODY()
public:
	AVGAGameModeBaseLobby();
};
