// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Gimka/UnrealEngine/GKNetworkManagerActor.h"

#include "VGANetworkManager.generated.h"

UCLASS()
class ADDICTHOSPITAL_API AVGANetworkManager : public AGKNetworkManagerActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVGANetworkManager();

protected:
	//virtual bool HasOwner() override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//virtual void EndPlay() override;

	//virtual void OnStartGame() override;

	//// Called every frame
	//virtual void Tick(float DeltaTime) override;

	/*UFUNCTION(Server, unreliable)
	virtual void RPCClientToServer(FGKNetworkSerializeToServer Message) override;

	UFUNCTION(Server, reliable)
	virtual void RPCClientToServerReliable(FGKNetworkSerializeToServer Message) override;*/

	//UFUNCTION(Client, unreliable)
	//virtual void RPCServerToClient(FGKNetworkSerializeToServer Message) override;

	//UFUNCTION(Client, reliable)
	//virtual void RPCServerToClientReliable(FGKNetworkSerializeToServer Message) override;



private:
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Helper Actors", meta = (AllowPrivateAccess = "true"))
	//AActor* ConnectToServer = nullptr;		// Valid on server only so it's not garbage collected.
	//int32 LastGameTickSent = -1;

};
