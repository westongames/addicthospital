// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Menu/GKMenu.h"
#include "VGAInGameMenu.generated.h"

/**
 * 
 */
UCLASS()
class ADDICTHOSPITAL_API UVGAInGameMenu : public UGKMenu
{
	GENERATED_BODY()

protected:
	virtual bool Initialize();

private:
	///////////////////////////////////////////////////////////////////////////////
	/// InGame Menu
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonClose;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonQuit;

	UFUNCTION()
	void OnButtonCloseMenu();

	UFUNCTION()
	void OnButtonQuitToMainMenu();

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Levels", meta = (AllowPrivateAccess = "true"))
	//TAssetPtr<UWorld> LevelLobby;

};
