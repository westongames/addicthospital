// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAInGameMenu.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"

bool UVGAInGameMenu::Initialize()
{
	if (!Super::Initialize()) return false;

	GKEnsureB(ButtonClose);
	ButtonClose->OnClicked.AddDynamic(this, &UVGAInGameMenu::OnButtonCloseMenu);

	GKEnsureB(ButtonQuit);
	ButtonQuit->OnClicked.AddDynamic(this, &UVGAInGameMenu::OnButtonQuitToMainMenu);

	return true;
}

void UVGAInGameMenu::OnButtonCloseMenu()
{
	if (MenuInterface) MenuInterface->OnMenuClose();
}

void UVGAInGameMenu::OnButtonQuitToMainMenu()
{
	if (MenuInterface) MenuInterface->OnMenuQuit();
}