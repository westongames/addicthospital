// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Gimka/Visual/GKGroundVisualManager.h"

#include "VGAGroundVisualManager.generated.h"

class AVGAChunkVoxelActor;

UCLASS()
class ADDICTHOSPITAL_API AVGAGroundVisualManager : public AActor, public GKGroundVisualManager
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVGAGroundVisualManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual int CreateChunkActor(const GKChunk& Chunk) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	TArray<AVGAChunkVoxelActor*> ArrayActors;

};
