// Fill out your copyright notice in the Description page of Project Settings.


#include "VGAGameSession.h"
//#include "Gimka/JobSystem/GKJobSystem.h"
#include "Gimka/System/Job/GKJob.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "Gimka/Game/Simulation/GKGameManagerSimulationServer.h"
#include "Gimka/Game/Simulation/GKGameManagerSimulationClient.h"
#include "Gimka/Game/PlayerControlled/GKGameManagerPCServer.h"
#include "Gimka/Game/PlayerControlled/GKGameManagerPCClient.h"
#include "Gimka/WorldVoxel/GKChunk.h"
#include "Gimka/WorldVoxel/GKWorldVoxel.h"
#include "Gimka/System/GKGameEngine.h"
#include "Gimka/Entity/Entity/GKPawnEntity.h"
#include "Gimka/Visual/GKVisualManager.h"
#include "Gimka/UnrealEngine/GKGameInstance.h"

#include "VGAVisualManager.h"
#include "VGAGroundVisualManager.h"
#include "VGANetworkManager.h"

#include "Gimka/Goap/States/DB/GKGoapStateBoolDB.h"
#include "Gimka/Goap/States/DB/GKGoapStateIntDB.h"
#include "Gimka/Goap/Goals/DB/GKGoapGoalDB.h"
#include "Gimka/Goap/Actions/DB/GKGoapActionDB.h"


//bool bFirstTime = true;

//class GKJobTest : public GKJob
//{
//public:
//	// Inherited via GKJob
//	virtual void OnJobGameMinute(int jobIndex) override
//	{
//		Value = jobIndex;
//	}
//	virtual void OnJobApplyChanges(int jobIndex) override
//	{
//	}
//	virtual void OnJobUpdateVisual(int jobIndex) override
//	{
//	}
//	virtual void OnJobPostProcess(int jobIndex) override
//	{
//	}
//
//	virtual ~GKJobTest() {}
//
//	int Value;
//};

// Sets default values
AVGAGameSession::AVGAGameSession()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GKLog("AVGAGameSession::AVGAGameSession");
}

void AVGAGameSession::PreInitializeComponents()
{
	Super::PreInitializeComponents();
	GKLog("AVGAGameSession::PreInitializeComponents");

}

//void AVGAGameSession::InitializeComponent()
//{
//	Super::InitializeComponent();
//	GKLog("AVGAGameSession::InitializeComponents");
//
//}

void AVGAGameSession::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	GKLog("AVGAGameSession::PostInitializeComponents");

}



// Called when the game starts or when spawned
void AVGAGameSession::BeginPlay()
{
	Super::BeginPlay();
	GKLog("AVGAGameSession::BeginPlay");
	//bFirstTime = true;


	VisualManager->AddTickPrerequisiteActor(this);
	GroundVisualManager->AddTickPrerequisiteActor(this);
	NetworkManager->AddTickPrerequisiteActor(this);

	/*
	bFirstTime = true;

	// Destroy existing jobSystem and recreate
	GKLog("AVGAJobSystem::BeginPlay");

	// Instead of GKEntityManager, we use GKRegion
	GKIntVector RegionSize(32, 32, 8);
	GKIntVector RegionCount(3, 3, 3);		// Starts with 3x3x3 region
	int32 TotalRegionCount = RegionCount.Volume();
	GKEntityManager** ArrayEntityManagers = new GKEntityManager * [TotalRegionCount];
	GK_ITERATE_XYZ(int32, RegionCount.X, RegionCount.Y, RegionCount.Z)
		ArrayEntityManagers[index] = new GKRegion(GKIntVector(x, y, z));
	GK_ITERATE_XYZ_END

	GKGameManager* GameManager = GKGameManager::GetInstance();
	GameManager->Initialize();//
	
	// We are using GKWorldRegion. Make sure GKWorld is not instanced.
	checkSlow(!GKWorld::IsInstanceAvailable());
	GKWorldRegion* WorldRegion = GKWorldRegion::GetInstance();

	// Let's start with 3 worlds
	WorldRegion->Initialize(1, GKIntVector(32, 32, 8), GKIntVector(3, 3, 3));
	

	*/
	//GKJobSystem::DestroyInstance();
	///GKJobSystem* jobSystem = GKJobSystem::GetInstance();


	/*if (bFirstTime)
	{
		bFirstTime = false;
		GKJobSystem* jobSystem = GKJobSystem::GetInstance();

		const int num = 10;
		GKJobTest* jobTests[num];
		for (int i = 0; i < num; ++i)
		{
			jobTests[i] = new GKJobTest();
			jobSystem->RegisterForEvent(jobTests[i], GKJobSystem::Event::GameMinute);
		}

		jobSystem->DoTick(1.0f);
		//jobSystem->Wait();

		for (int i = 0; i < num; ++i)
		{
			GKLog1("Job ", jobTests[i]->Value);
			delete jobTests[i];
		}
	}*/
}

// Called every frame
void AVGAGameSession::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// If firstTime
	if (GameEngine == nullptr)
	{
		GKLog("AVGAGameSession::Tick firstTime");

		UGKGameInstance* GameInstance = (UGKGameInstance*)GetGameInstance();
		GameEngine = GameInstance->GetGameEngine();
		// Make sure to destroy the prev instance first if available
		//GKGameEngine::DestroyInstance();

		//GKGameEngine::CreateInstance();

		//GameEngine = GKGameEngine::GetInstance();

		//GameEngine = new GKGameEngine();
		//GameEngine->Create();

		GKGameManager* GameManager;
		if (HasAuthority())
		{
			GameManager = new GKGameManagerSimulationServer(123, GameEngine, true);
			//GameManager = new GKGameManagerPCServer(123, GameEngine, true);
		}
		else
		{
			GameManager = new GKGameManagerSimulationClient(123, GameEngine, false);
			//GameManager = new GKGameManagerPCClient(123, GameEngine, false);
		}

		GKGoapStateBoolDB* GoapStateBoolDB = new GKGoapStateBoolDB();
		GKGoapStateIntDB* GoapStateIntDB = new GKGoapStateIntDB();
		GKGoapGoalDB* GoapGoalDB = new GKGoapGoalDB(GoapStateBoolDB, GoapStateIntDB);
		GKGoapActionDB* GoapActionDB = new GKGoapActionDB(GoapStateBoolDB, GoapStateIntDB);

		// Order doesn't matter
		GameManager->AddDatabase(GoapStateBoolDB);
		GameManager->AddDatabase(GoapStateIntDB);
		GameManager->AddDatabase(GoapGoalDB);
		GameManager->AddDatabase(GoapActionDB);

		GKWorldVoxelSettings Settings;
		Settings.Initialize(
			4,						// regionBitNumChunk = 4
			4,						// chunkBitNumSections = 4
			4,						// chunkBitNumTiles = 4
			3						// tileBitNumZVoxels = 3
		);
			
			//3,					// sectionBitNumZTiles
			//4, 4,								// chunkBitNumSections, chunkBitNumTiles
			//4);									// regionBitNumChunk
		Settings.SetVoxelBitNumMinis(4);

		GKWorldVoxel* WorldRegion = new GKWorldVoxel(GameManager);
		WorldRegion->Initialize(Settings);// 1, GKIntVector(32, 32, 8), GKIntVector(3, 3, 3));

		GameManager->Initialize(WorldRegion);



		//	//Region->Add(Pawn);
		//	Pawn->SetVelocity(FVector::ZeroVector);
		//	Pawn->SetPosition(FVector::ZeroVector);
		//	Pawn->SetVelocity(FVector(1.0f, 0.f, 0.f));

		//GKVisualManager* gkVisualManager = new GKVisualManager();
		VisualManager->Initialize(GameEngine);
		GroundVisualManager->Initialize(GameEngine);
		NetworkManager->Initialize(GameEngine);
		GameEngine->StartGame(GameManager, VisualManager, GroundVisualManager, NetworkManager);

		return;
	}

	++TickCount;
	GameEngine->UpdateGame(DeltaTime);

	// Test the pawn movement
	//GKGameManager* GameManager = GameEngine->GetGameManager();
	//GKWorld* World = GameManager->GetWorld();
	//GKEntityManager* EntityManager = World->GetEntityManager(1);
	//GKEntity* Entity = EntityManager->GetEntity(0);
	////GKPawnEntity* Pawn = (GKPawnEntity*)GameEngine->GetGameManager()->GetWorld()->GetEntityManager(1)->GetEntity(0);
	//GKPawnEntity* Pawn = (GKPawnEntity*)Entity;
	//EntityPosition = Pawn->GetPosition();

	//GKGameManager* GameManager = GKGameManager::GetInstance();
	//GameManager->AdvanceGame(DeltaTime);

	//GKJobSystem2* jobSystem2 = GKJobSystem2::GetInstance();
	//jobSystem2->GetStats(JobCount, GroupCount, GroupSize, CurrentLabel);

	//GKLog1("AVGAGameSession::Tick", GameManager);



	//GKLog1("VGAJobSystem:: Tick", DeltaTime);
	//GKJobSystem* jobSystem = GKJobSystem::GetInstance();
	//jobSystem->DoTick(DeltaTime);

	//int32 NewLabel;
	//jobSystem->GetStats(JobCount, GroupCount, GroupSize, NewLabel);
	//DeltaLabel = NewLabel - CurrentLabel;
	//CurrentLabel = NewLabel;

	//jobSystem->

	//CurrentLabel = 

}


void AVGAGameSession::BeginDestroy()
{
	Super::BeginDestroy();
	GKLog1("AVGAGameSession::BeginDestroy()", GameEngine);

	if (GameEngine != nullptr)
	{
		GameEngine->EndGame();

		//GameEngine->OnDestroy();

		//delete GameEngine;
		//GameEngine = nullptr;
		//	GameEngine->DestroyInstance();
	}
}

bool AVGAGameSession::IsReadyForFinishDestroy()
{
	GKLog("AVGAGameSession::IsReadyForFinishDestroy()");
	return Super::IsReadyForFinishDestroy();
}

void AVGAGameSession::FinishDestroy()
{
	Super::FinishDestroy();
	GKLog("AVGAGameSession::FinishDestroy()");
}
