// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gimka/Menu/GKMenu.h"
#include "Gimka/Network/GKOnlineSession.h"


#include "VGAMainMenu.generated.h"

/**
 * 
 */
UCLASS()
class ADDICTHOSPITAL_API UVGAMainMenu : public UGKMenu
{
	GENERATED_BODY()
	
public:
	UVGAMainMenu(const FObjectInitializer& ObjectInitializer);
	//void SetMenuInterface(IGKMenuInterface* MenuInterface);

	void SetSessionList(const TArray<GKOnlineSession::SessionData>& Sessions);
	void EnableServerList();
	void DisableServerList(const FString& Reason, bool EnableRefresh = true);

	void SetSubsystemName(const FString& SubsystemName);

	// Called by GKMenuInterface
	virtual void OnSelectIndex(uint32 Index) override;
	virtual void OnResetSelectedIndex(uint32 OldIndex) override;

protected:
	virtual bool Initialize();

private:
	///////////////////////////////////////////////////////////////////////////////
	/// Common
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;

	///////////////////////////////////////////////////////////////////////////////
	/// Main Menu
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UWidget* MenuMain;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonPlay;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMultiplayer;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonExitGame;

	UFUNCTION()
	void OnButtonOpenMainMenu();

	UFUNCTION()
	void OnButtonPlay();

	UFUNCTION()
	void OnButtonExitGame();

	///////////////////////////////////////////////////////////////////////////////
	/// Multiplayer Menu
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UWidget* MenuMultiplayer;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPHost;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoin;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoinManual;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPClose;

	UFUNCTION()
	void OnButtonOpenMultiplayerMenu();

	UFUNCTION()
	void OnButtonOpenHostMenu();

	///////////////////////////////////////////////////////////////////////////////
	/// Host Menu
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UWidget* MenuMPHost;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextMPHost;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPHostContinue;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPHostClose;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* ETextBoxSessionName;

	UFUNCTION()
	void OnValidateSessionName(const FText& Text);

	UFUNCTION()
	void OnButtonHostServer();

	///////////////////////////////////////////////////////////////////////////////
	/// Join Menu
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UWidget* MenuMPJoin;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TextMPJoin;

	UPROPERTY(meta = (BindWidget))
	class UWidget* SessionHeader;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoinListContinue;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoinListRefresh;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoinListClose;

	UPROPERTY(meta = (BindWidget))
	class UPanelWidget* ScrollBoxServerList;

	TSubclassOf<class UUserWidget> SessionRowClass;

	UFUNCTION()
	void OnButtonOpenJoinMenu();

	UFUNCTION()
	void OnButtonJoinServerList();

	UFUNCTION()
	void OnButtonJoinRefresh();

	///////////////////////////////////////////////////////////////////////////////
	/// Join Menu Manual
	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(meta = (BindWidget))
	class UWidget* MenuMPJoinManual;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoinManContinue;

	UPROPERTY(meta = (BindWidget))
	class UButton* ButtonMPJoinManClose;

	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* ETextBoxIPAddressField;

	UFUNCTION()
	void OnValidateIPAddress(const FText& Text);

	UFUNCTION()
	void OnButtonOpenJoinManualMenu();

	UFUNCTION()
	void OnButtonJoinServer();



	//TSubclassOf<class UUserWidget> MenuClass;
};
