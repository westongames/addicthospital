// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAGroundVisualManager.h"
#include "Gimka/Tools/Macros/GKLogger.h"
#include "Gimka/WorldVoxel/GKChunk.h"
#include "Gimka/WorldVoxel/GKWorldVoxelSettings.h"
#include "Gimka/Tools/Macros/GKSource.h"
#include "VGAChunkVoxelActor.h"


// Sets default values
AVGAGroundVisualManager::AVGAGroundVisualManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVGAGroundVisualManager::BeginPlay()
{
	Super::BeginPlay();
	
}

int AVGAGroundVisualManager::CreateChunkActor(const GKChunk& Chunk)
{
	const FIntPoint& ChunkPos = Chunk.GetChunkPos();
	//const FIntPoint& RegionPos = Chunk.GetRegion()->GetRegionPos();

	AVGAChunkVoxelActor* NewActor = GetWorld()->SpawnActor<AVGAChunkVoxelActor>(
		FVector(ChunkPos.X, ChunkPos.Y, 0.0f), FRotator::ZeroRotator);

	NewActor->SetOwner(&Chunk);

	//NewActor->SetFolderPath("/TestFolder");

	int actor = ArrayActors.Add(NewActor);
	GKLog1("CreateGroundActor ", actor);
	return actor;

	//TArray<FVector> Vertices;
	//TArray<int32> Triangles;
	//TArray<FVector> Normals;
	//TArray<FColor> VertexColors;

	//const GKWorldVoxelSettings* Settings = Chunk.GetWorldSettings();
	//int32 chunkNumTiles = Settings->GetChunkNumTiles();

	//// TODO: add section to chunk and process all sections

	//for (int32 y = 0, index = 0; y < chunkNumTiles; ++y)
	//{
	//	for (int32 x = 0; x < chunkNumTiles; ++x, ++index)
	//	{
	//		Triangles.Add()
	//	}
	//}

	//return 0;
}

// Called every frame
void AVGAGroundVisualManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	OnUpdateGame(DeltaTime);
}

