// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"

#include "VGAChunkVoxelActor.generated.h"

class GKChunk;

UCLASS()
class ADDICTHOSPITAL_API AVGAChunkVoxelActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVGAChunkVoxelActor();

	void SetOwner(const GKChunk* Owner);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	virtual void OnConstruction(const FTransform& Transform) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


private:
	UPROPERTY()
	UProceduralMeshComponent* ProceduralComponent;

	const GKChunk* Owner;

	void UpdateMesh();
};
