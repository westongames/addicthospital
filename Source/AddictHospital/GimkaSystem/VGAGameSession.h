// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"


#include "VGAGameSession.generated.h"

class GKGameEngine;
class GKPawnEntity;

class AVGAVisualManager;
class AVGAGroundVisualManager;
class AVGANetworkManager;


UCLASS()
class ADDICTHOSPITAL_API AVGAGameSession : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AVGAGameSession();

protected:

	virtual void PreInitializeComponents() override;
	//virtual void InitializeComponent() override;
	virtual void PostInitializeComponents() override;


	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void BeginDestroy() override;
	virtual bool IsReadyForFinishDestroy() override;
	virtual void FinishDestroy() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Managers", meta = (AllowPrivateAccess = "true"))
	AVGAVisualManager* VisualManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Managers", meta = (AllowPrivateAccess = "true"))
	AVGAGroundVisualManager* GroundVisualManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Managers", meta = (AllowPrivateAccess = "true"))
	AVGANetworkManager* NetworkManager;

	// For test
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Entity Info", meta = (AllowPrivateAccess = "true"))
	FVector EntityPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GKSetting", meta = (AllowPrivateAccess = "true"))
	int32 TickCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GKSetting", meta = (AllowPrivateAccess = "true"))
	int32 JobCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GKSetting", meta = (AllowPrivateAccess = "true"))
	int32 GroupCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GKSetting", meta = (AllowPrivateAccess = "true"))
	int32 GroupSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GKSetting", meta = (AllowPrivateAccess = "true"))
	int32 CurrentLabel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GKSetting", meta = (AllowPrivateAccess = "true"))
	int32 DeltaLabel;

	GKGameEngine* GameEngine = nullptr;
};
