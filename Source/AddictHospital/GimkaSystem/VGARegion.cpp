// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGARegion.h"
#include "Gimka/WorldVoxel/GKChunk.h"
#include "Gimka/Entity/Entity/GKPawnEntity.h"
#include "Gimka/Game/GKGameManager.h"
#include "Gimka/Game/GKWorld.h"

// Sets default values
AVGARegion::AVGARegion()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AVGARegion::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AVGARegion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// In BeginPlay, this created twice
	//if (Region == nullptr)
	//{
	//	//Region = new GKRegion(FIntVector::ZeroValue);
	//	Region = GKWorldRegion::GetInstance()->GetRegion(FIntVector::ZeroValue);
	//	//RegionIndex
	//	GKGameManager* GameManager = GKGameManager::GetInstance();
	//	Pawn = new GKPawnEntity(Region, GameManager->GenerateUniqueIDForLocalPlayer());
	//	//Region->Add(Pawn);
	//	Pawn->SetVelocity(FVector::ZeroVector);
	//	Pawn->SetPosition(FVector::ZeroVector);
	//	Pawn->SetVelocity(FVector(1.0f, 0.f, 0.f));
	//}
	//GKGameEngine* GameEngine = GKGameEngine::GetInstance();
//	if (GameEngine != nullptr)
	//{
	//	GKEntityManager* EntityManager = GameEngine->GetGameManager()->GetWorld()->GetEntityManager(1);
	//}


	//EntityPosition = Pawn->GetPosition();
	//Pawn->SetPosition(EntityPosition + Pawn->GetVelocity());

}

