// Fill out your copyright notice in the Description page of Project Settings.


#include "GimkaSystem/VGAGameModeBaseLobby.h"

AVGAGameModeBaseLobby::AVGAGameModeBaseLobby()
{
	// set default pawn class to our Blueprinted character
	GKLog("Constructor");
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}