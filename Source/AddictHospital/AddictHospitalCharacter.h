// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Gimka/Network/GKNetworkPlayer.h"
#include "Gimka/UnrealEngine/GKNetworkSerialize.h"

//#include "Gimka/UnrealEngine/GKCharacter.h"

#include "AddictHospitalCharacter.generated.h"

/*
ClientToServer:
- Called from Character on client: !HasAuthority && IsLocallyControlled.
	!AVGANetworkManager.HasAuthority, !HasAuthority, Local Role 2 (Autonomous), Remote Role 3 (Authority), IsLocallyControlled
- It's only happening on 1 client character, it doesn't affect the other character.
- If called from character owned by server, it will just call the Server side:
	AVGANEtowrkManager.HasAuthority, HasAuthority, Local Role 3 (Authority), Remote Role 1 (Simulated), IsLocallyControlled

ServerToClient:
- Called from Character on a client: !HasAuthority && IsLocallyControlled
	!AVGANetworkManager.HasAuthority, !HasAuthority, Local Role 2 (Autonomous), Remote Role 3 (Authority), IsLocallyControlled
- This seems to be a different character than the ClientToServer. The counter doesn't add to each other.
- If called from Character owned by server, it will just call the Server side:
	AVANetowkrManager.HasAuthority, HasAuthority, Local Role 3 (Authority), Remote Role 1 (Simulated Proxy), IsLocallyControlled

ServerToAll:
- Called from Character on a server: 
	AVGNetworkManager.HasAuthority, HasAuthority, Local Role 3 (Authority), Remote Role 1 (Simulated Proxy), IsLocallyControlled
- Both server and client got it
- If called from Characterd owned by client, it will just call the Client side:
	!ACGANetworkManager.HasAuthority, !HasAuthority, Local Role 2 (Autonomous), Remote Role 3 (Authority), IsLocallyControlled
*/

class AVGANetworkManager;

UCLASS(config=Game)
class AAddictHospitalCharacter : public ACharacter, public GKNetworkPlayer
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AAddictHospitalCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	//virtual void GetLifetimeReplicatedProps()

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);


protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

protected:

	UFUNCTION(Server, unreliable)
		virtual void RPCClientToServer(FGKNetworkSerializeToServer NetworkSerialize) override;

	UFUNCTION(Server, reliable)
		virtual void RPCClientToServerReliable(FGKNetworkSerializeToServer NetworkSerialize) override;

	//int CounterTest = 0;

	UFUNCTION(Client, unreliable)
		virtual void RPCServerToClient(FGKNetworkSerializeToClient NetworkSerialize) override;

	UFUNCTION(Client, reliable)
		virtual void RPCServerToClientReliable(FGKNetworkSerializeToClient NetworkSerialize) override;

	UFUNCTION(Client, reliable)
		virtual void RPCServerToClientCommandReliable(FGKNetworkSerializeToClientCommand NetworkSerialize) override;

private:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	// PlayerID assigned by server, replicated to all clients
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, ReplicatedUsing = OnRep_PlayerID, Category = GKNetwork, meta = (AllowPrivateAccess = "true"))
		uint8 PlayerID = 0;

	// Called when the PlayerID gets updated by server
	UFUNCTION()
		void OnRep_PlayerID();

public:
	virtual void SetPlayerID(uint8 Value) override { PlayerID = Value; }
	virtual uint8 GetPlayerID() const override { return PlayerID; }

// From Youtube voxels, covnerted from Blueprint
// https://www.youtube.com/watch?v=29vUp09JDAg&list=PL5hOItiyPlZPzFZUO4JUIrpoBQs-QuI64&index=1
private:
	//Settings
	int32 renderRange;
	int32 chunkLineElements;
	int32 voxelSize;

	int32 chunkSize;
	int32 chunkSizeHalf;
	FVector characterPosition;
	int32 chunkX;
	int32 chunkY;


};

