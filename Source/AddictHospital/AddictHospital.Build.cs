// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class AddictHospital : ModuleRules
{
	public AddictHospital(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { 
            "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", 
            "UMG", "OnlineSubsystem", "OnlineSubsystemSteam", "SlateCore", "XmlParser" });

        PrivateDependencyModuleNames.AddRange(new string[] { "ProceduralMeshComponent" });

        //IncludeSubdirectoriesInIncludePaths();

        PublicIncludePaths.AddRange
            (
            new string[] 
            {
                 "D:/Google Drive/Projects/AddictHospital/Source/AddictHospital",
            }
        );
    }

//    private void IncludeSubdirectoriesInIncludePaths()
//    {
//        AddDirectoriesRecursive(ModuleDirectory);
//    }

//    private void AddDirectoriesRecursive(string DirectoryPathToSearch)
//    {
//        foreach (string DirectoryPath in Directory.GetDirectories(DirectoryPathToSearch))
//        {
//            PrivateIncludePaths.Add(DirectoryPath);
//            AddDirectoriesRecursive(DirectoryPath);
//        }
//    }
}
