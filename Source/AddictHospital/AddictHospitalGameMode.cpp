// Copyright Epic Games, Inc. All Rights Reserved.

#include "AddictHospitalGameMode.h"
#include "AddictHospitalCharacter.h"
#include "UObject/ConstructorHelpers.h"

#include "Gimka/Tools/Macros/GKLogger.h"

AAddictHospitalGameMode::AAddictHospitalGameMode()
{
	// set default pawn class to our Blueprinted character
	GKLog("Constructor");
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(
		//	//TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
		TEXT("/Game/VGA/BPVGAPawnWithCamera"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
