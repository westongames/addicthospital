// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzlePlatforms/MovingPlatform.h"
#include "Gimka/Tools/Macros/GKLogger.h"

AMovingPlatform::AMovingPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//FVector Location = GetActorLocation();
	//Location += FVector(5 * DeltaTime, 0, 0);
	//SetActorLocation(Location);
}

void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	// Note: SetReplicates in BeginPlay, not constructor, too early.
	// Note: SetReplicates can only be called on server otherwise error
	if (HasAuthority())
	{
		GKLog("SetReplicate");
		//SetReplicates(true);
		//SetReplicateMovement(true);
	}
}
